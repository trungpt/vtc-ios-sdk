////
////  GoogleServiceHandler.h
////  VtcSDK
////
////  Created by Kent Vu on 7/27/16.
////  Copyright © 2016 VTCIntecom. All rights reserved.
////
//
//#import <Foundation/Foundation.h>
//#import <GoogleSignIn/GoogleSignIn.h>
//
//@interface GoogleServiceHandler : NSObject
//
//@property (nonatomic, assign) UIViewController *loginController;
//
//+ (GoogleServiceHandler *)sharedHandler;
//- (void)signIn:(void (^)(GIDGoogleUser *user, NSError *error))completion;
//- (void)signOut;
//- (NSString *)ggAccessToken;
//@end

//
//  VtcAuthenViewController.m
//  VtcSDK
//
//  Created by Kent Vu on 7/25/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//
//#import "VtcOtherLoginViewController.h"
#import "VtcLoginViewController.h"
#import "VtcAuthenViewController.h"
#import "UIImage+Additions.h"
#import "NSBundle+Additions.h"
#import "UIViewController+Additions.h"
#import "VtcSignUpViewController.h"
#import "VtcForgotPasswordViewController.h"
#import "FacebookHandler.h"
#import "GoogleServiceHandler.h"
#import "NetworkModal+Authen.h"
#import "VtcEnterUsernameViewController.h"
#import "VtcOTPViewController.h"
#import "VtcChangePasswordViewController.h"
#import "VtcAuthenNavController.h"
#import "VtcRoundedButton.h"
#import "VtcLoginTextField.h"
#import "SmartParser.h"
#import "UIView+UpdateAutoLayoutConstraints.h"
#import "VIDUser+Internal.h"
#import "SDKManager.h"
#import "SDKManager+Internal.h"
#import<AuthenticationServices/AuthenticationServices.h>
@interface VtcAuthenViewController () <UITextFieldDelegate, UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet VtcRoundedButton *btnQuickStart;

@property (weak, nonatomic) IBOutlet UIButton *signInWithApple;

@property (weak, nonatomic) IBOutlet UIView *viewForAppleSignIn;
@property (nonatomic, weak) IBOutlet VtcRoundedButton *btnSignIn;
@property (nonatomic, weak) IBOutlet VtcRoundedButton *btnFacebook;
@property (nonatomic, weak) IBOutlet VtcRoundedButton *btnGoogle;
@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfUsername;
@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnChangePass;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UILabel *lblSavePassword;

@property (weak, nonatomic) IBOutlet UIView *viewToAddButton;
@property (nonatomic, weak) IBOutlet UILabel *orLabel;
@property (nonatomic, weak) IBOutlet UILabel *lbGameVersion;
@property (nonatomic, weak) IBOutlet UILabel *lbSDKVersion;

@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@property (nonatomic, weak) IBOutlet UIButton *savePassword;

@property (nonatomic, weak) IBOutlet UIButton *forgotPasswordLandscape;
@property (nonatomic, weak) IBOutlet UIButton *forgotPasswordPortrait;
@property (nonatomic, weak) IBOutlet UIButton *btnFacebookLandscape;
@property (weak, nonatomic) IBOutlet UIButton *btnAppleLandscape;
@property (weak, nonatomic) IBOutlet UIView *viewForLandscape;

@property (nonatomic, weak) IBOutlet UIButton *btnGoogleLandscape;

@property (weak, nonatomic) IBOutlet UIView *viewHoverLanguage;
@property (weak, nonatomic) IBOutlet UIButton *btnLangEn;
@property (weak, nonatomic) IBOutlet UIButton *btnLangVi;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraintViewHover;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *topSpaceFromUsernameToLogo;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *topSpaceFromVersionLabelToCenter;

@end

@implementation VtcAuthenViewController

//API_AVAILABLE(ios(13.0))
//ASAuthorizationAppleIDButton *appleIDButton;

//NSString* const setCurrentIdentifier = @"setCurrentIdentifier";
- (void) viewDidAppear:(BOOL)animated{
    NSLog(@"testahihi viewDidAppear");
    if (_tfUsername.text && _tfUsername.text.length>3 && _tfPassword.text && _tfPassword.text.length > 3) {
NSLog(@"testahihi onSignIn");
        //Nếu thay đổi tài khoản thì không tự động đăng nhập
        //Luồng đăng nhập mới
//        NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultCachedUser];
//        NSString *oldUsername = @"";
//        if (userInfo) {
//            oldUsername = [SmartParser stringForKey:@"accountName" from:userInfo];
//        }ƒ
//        if([_registeringName isEqualToString: oldUsername])
//            [self onSignIn:_btnSignIn];
//        else{
//            _tfPassword.text=@"";
//            [VIDUser currentUser].password = @"";
//        }
        //
        if(self.needAutoLogin){
            //Tu dong dang nhap sau khi dang ky xong
            NSLog(@"testahihi needAutoLogin");
            self.needAutoLogin = NO;
            [self onSignIn:_btnSignIn];
        }
    }
}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    if ([SDKManager defaultManager].isShowCloseButtonInAuthenVC) {
        [self.closeButton setHidden:false];
    }
    
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
//    [self addCustomBackButton];
//    [self addCustomTitleView];
    self.portraitTopSpace = 8.0;
    self.landscapeTopSpace = 8.0; //30.0;
    
    _tfUsername.clearButtonMode = UITextFieldViewModeWhileEditing;
//    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultCachedUser];
    
    //
    [_tfPassword setHandleDidEndOnExit:^{
        [self onSignIn:_btnSignIn];
    }];
    
    _lbGameVersion.text = [NSString stringWithFormat:@"game: v%@", [[VtcConfig defaultConfig] gameVersion]];
    NSString *sdkVerString = [NSString stringWithFormat:@"sdk: v%@", VtcSDKVersionString];
    if ([NetworkModal sharedModal].isDevMode) {
        sdkVerString = [sdkVerString stringByAppendingString:@" (Dev)"];
    }
    _lbSDKVersion.text = sdkVerString;
    
    [self configUI];
}

//dang nhap Apple

- (void)observeAppleSignInState {
    if (@available(iOS 13.0, *)) {
        NSLog(@"observeAppleSignInState");
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center addObserver:self selector:@selector(handleSignInWithAppleStateChanged:) name:ASAuthorizationAppleIDProviderCredentialRevokedNotification object:nil];
    }
}

- (void)handleSignInWithAppleStateChanged:(id)noti {
    NSLog(@"%s", __FUNCTION__);
    NSLog(@"%@", noti);
}

//----------
- (void) settingLanguage
{
    NSLog(@"settingLanguage");
    
    self.tfUsername.placeholder = VTCLocalizeString(@"Username2");
    self.tfPassword.placeholder = VTCLocalizeString(@"Password");
    self.lblSavePassword.text = VTCLocalizeString(@"Remember");
    [self.btnFacebook setTitle:VTCLocalizeString(@"Login via Facebook") forState:UIControlStateNormal];
    [self.btnGoogle setTitle:VTCLocalizeString(@"Login via Google") forState:UIControlStateNormal];
//    NSLog(@"%@",VTCLocalizeString(@"Forgot password"));
    
//    [self.forgotPasswordPortrait setAttributedTitle:[[NSAttributedString alloc] initWithString:VTCLocalizeString(@"Forgot password") attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}] forState:UIControlStateNormal];
//
//    [self.forgotPasswordLandscape setAttributedTitle:[[NSAttributedString alloc] initWithString:VTCLocalizeString(@"Forgot password") attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}] forState:UIControlStateNormal];
    
    [self.btnChangePass setAttributedTitle:[[NSAttributedString alloc] initWithString:VTCLocalizeString(@"Change password") attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}] forState:UIControlStateNormal];
    [_btnSignIn setTitle:VTCLocalizeString(@"Login via VTC") forState:UIControlStateNormal];
    [_btnQuickStart setTitle:VTCLocalizeString(@"QuickStart") forState:UIControlStateNormal];
    
    self.orLabel.text = VTCLocalizeString(@"Or");
    NSString *registerString = VTCLocalizeString(@"You haven't got VTC account? Register now");
    if ([registerString containsString:@"Đăng ký tại đây"])
    {
        NSMutableAttributedString *mRegister = [[NSMutableAttributedString alloc] initWithString:registerString attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
        
        [mRegister addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:11.0/255.0 green:185.0/255.0 blue:250.0/255.0 alpha:1.0] range:[registerString rangeOfString:@"Đăng ký tại đây"]];
        [self.btnRegister setAttributedTitle:mRegister forState:UIControlStateNormal];
    }
    else
    {
        NSMutableAttributedString *mRegister = [[NSMutableAttributedString alloc] initWithString:registerString attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
        [mRegister addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:11.0/255.0 green:185.0/255.0 blue:250.0/255.0 alpha:1.0] range:[registerString rangeOfString:@"Register here"]];
        [self.btnRegister setAttributedTitle:mRegister forState:UIControlStateNormal];
    }
}

- (void)configUIWithLanguage:(NSString *)lang animated:(BOOL)animated
{
    NSLog(@"configUIWithLanguage: %@",lang);
    if ([lang isEqualToString:@"eng"])
    {
        self.btnLangEn.selected = YES;
        self.btnLangVi.selected = NO;
        if (animated)
        {
            self.leadingConstraintViewHover.constant = 0;
        }
        else
        {
            self.leadingConstraintViewHover.constant = 0;
        }
    }else if ([lang isEqualToString:@"khm"])
    {
        self.btnLangEn.selected = YES;
        self.btnLangVi.selected = NO;
        if (animated)
        {
            self.leadingConstraintViewHover.constant = 0;
        }
        else
        {
            self.leadingConstraintViewHover.constant = 0;
        }
    }else
    {
        self.btnLangEn.selected = NO;
        self.btnLangVi.selected = YES;
        if (animated)
        {
            self.leadingConstraintViewHover.constant = 35;
        }
        else
        {
            self.leadingConstraintViewHover.constant = 35;
        }
    }
    [[VtcConfig defaultConfig] setLanguage:lang];
}

- (void)configUI {

    if (![[UIDevice currentDevice].model containsString:@"iPad"]) { // not iPad
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        
        if (UIInterfaceOrientationIsPortrait(orientation)) {
            _forgotPasswordPortrait.hidden = NO;
            _forgotPasswordLandscape.hidden = YES;
            
            _topSpaceFromUsernameToLogo.constant = self.portraitTopSpace;
            _topSpaceFromVersionLabelToCenter.constant = 186.0;
            
            CGRect frame = _logo.frame;
            frame.size.height = 45;
            frame.size.width = 45;
            _logo.frame = frame;
        }
        else {
            
            _forgotPasswordPortrait.hidden = YES;
            _forgotPasswordLandscape.hidden = NO;
            [containerView setConstraintConstant:500 forAttribute:NSLayoutAttributeWidth];
            _topSpaceFromUsernameToLogo.constant = self.portraitTopSpace;
            _topSpaceFromVersionLabelToCenter.constant = 46.0;
            // not show label "Hoặc"
            _orLabel.hidden = YES;
            
            CGRect frame = _logo.frame;
            frame.size.height = 20;
            frame.size.width = 20;
            _logo.frame = frame;
        }
    }
    
    self.navigationController.navigationBar.translucent = NO;
//    NSLog(@"testahihi scroll to top");
    
//    CGPoint newPoint = CGPointMake(0, 50);
//    [self.mainscrollview setContentOffset:newPoint animated:NO];
    //Set Language
    
    [self reconfigOpenIDAuthen];
}

- (void)reconfigOpenIDAuthen {
    if (![[UIDevice currentDevice].model containsString:@"iPad"]) { // not iPad
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];

//        if (UIInterfaceOrientationIsPortrait(orientation)) {
            _btnFacebook.hidden = ![Global sharedInstance].enableFacebookLogin;
            _btnGoogle.hidden = ![Global sharedInstance].enableGoogleLogin;
//            _btnGoogleLandscape.hidden = YES;
//            _btnFacebookLandscape.hidden = YES;
//            _btnAppleLandscape.hidden = YES;
            _viewForLandscape.hidden = YES;
            // set hidden of label "Hoặc"
//            if (_btnFacebook.hidden && _btnGoogle.hidden) {
//                _orLabel.hidden = YES;
//            }
//            else {
//                _orLabel.hidden = NO;
//            }
//        }
//        else {
////            NSLog(@"Vao day roi nayyyyy");
//            _btnGoogle.hidden = YES;
//            _btnFacebook.hidden = YES;
//            _viewForAppleSignIn.hidden = YES;
//            _viewForLandscape.hidden = NO;
//
//            _btnGoogleLandscape.hidden = ![Global sharedInstance].enableGoogleLogin;
//            [_btnGoogleLandscape setConstraintConstant:_btnGoogleLandscape.hidden ? 0.0 : kWidthOfLandscapeSocialLoginButton forAttribute:NSLayoutAttributeWidth];
//            _btnFacebookLandscape.hidden = ![Global sharedInstance].enableFacebookLogin;
//            [_btnFacebookLandscape setConstraintConstant:_btnFacebookLandscape.hidden ? 0.0 : kWidthOfLandscapeSocialLoginButton forAttribute:NSLayoutAttributeWidth];
//        }
    }
    else { // iPad
        _viewForLandscape.hidden = YES;
        _btnFacebook.hidden = ![Global sharedInstance].enableFacebookLogin;
        _btnGoogle.hidden = ![Global sharedInstance].enableGoogleLogin;
        // set hidden of label "Hoặc"
//        if (_btnFacebook.hidden && _btnGoogle.hidden) {
//            _orLabel.hidden = YES;
//        }
//        else {
//            _orLabel.hidden = NO;
//        }
    }
    
    if (@available(iOS 13.0, *)) {
        
    }else{
        //an nut Sigin With Apple tren IOS cu:
        NSLog(@"testahihi an nut Sigin With Apple tren IOS cu");
        _btnAppleLandscape.hidden = YES;
        _viewForAppleSignIn.hidden = YES;
        
        [_btnAppleLandscape setConstraintConstant:0.0 forAttribute:NSLayoutAttributeWidth];
        [_viewForAppleSignIn setConstraintConstant:0.0 forAttribute:NSLayoutAttributeWidth];
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //Test dữ liệu
//    NSLog(@"testahihi Your username is %@", _registeringName);
//    NSLog(@"testahihi Your password is %@", _registeringPassword);
    //Luồng đăng nhập mới
//    _tfUsername.text = _registeringName;
    //Luồng đăng nhập mới
    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultCachedUser];
    if (_registeringName){
        _tfUsername.text = _registeringName;
    }else if (userInfo){
        _tfUsername.text = [SmartParser stringForKey:@"accountName" from:userInfo];
    }
    if (_registeringPassword) {
        _tfPassword.text = _registeringPassword;
        //        [Utilities showNotification:VTCLocalizeString(@"Register successfully")];
    } else {
        if (userInfo) {
            NSString *savedPassword = [SmartParser stringForKey:@"password" from:userInfo];
            if (savedPassword) {
                if (![savedPassword isEqualToString:@""]) {
                    _savePassword.selected = YES;
                }
                _tfPassword.text = savedPassword;
            }
        }
    }
    //
    
    [self configUIWithLanguage:[[VtcConfig defaultConfig] sdkLanguage] animated:NO];
    [self settingLanguage];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reconfigOpenIDAuthen) name:kNotificationGotFacebookGoogleAuthenConfig object:nil];
    /// Add Orientation notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationStatusDidChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
    // reset UI
    [self orientationStatusDidChange:nil];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.navigationController) {
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationGotFacebookGoogleAuthenConfig object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}

- (void)orientationStatusDidChange:(NSNotification *)notification {
    [self configUI];
}

- (BOOL)shouldAutorotate {
//    return NO;
    return [SDKManager defaultManager].allowRotationInLoginView;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    NSLog(@"testahihi loginViewOrientationMask:%lu",(unsigned long)[SDKManager defaultManager].loginViewOrientationMask);
    return [SDKManager defaultManager].loginViewOrientationMask;
}

- (IBAction)closeButtonTapped:(id)sender {
    NSLog(@"Close Button Tapped");
    [self.navigationController dismissViewControllerAnimated:NO completion:^{
        [SDKManager defaultManager].showingLoginView = NO;
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onSavePassword:(UIButton *)sender {
    sender.selected = !sender.selected;
}

- (IBAction)quickStart:(id)sender {
    NSLog(@"quickStart");
    
    UIAlertController * alert =  [UIAlertController
                                 alertControllerWithTitle:@"Chú ý"
                                 message:@"Dữ liệu gắn với tài khoản Chơi Ngay sẽ bị xóa khi bạn xóa game hoặc cài lại máy. Hãy lưu ý khi sử dụng hình thức đăng nhập này. Để an toàn khi đăng nhập hãy ưu tiên đăng nhập bằng VTC ID và Apple ID"
                                 preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction* ok = [UIAlertAction
                        actionWithTitle:@"Đồng ý"
                        style:UIAlertActionStyleDestructive
                        handler:^(UIAlertAction * action)
                        {
                            NSLog(@"OK clicked");
                            [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
                            
                                [[NetworkModal sharedModal] quickStartGame:@"" password:@"" completion:^(BOOL status, id responsedObject, NSError *error) {
                                    [(VtcRoundedButton *)sender setEnabled:YES];
                                    [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
                                    if (status) {
                                        NSLog(@"login success");
                                        [VIDUser currentUser].signedIn = YES;
                                        [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeQuickStart containerController:self.navigationController showWelcome:YES];
                                        
                                        [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeLoginVTC]];
                                    }
                                    else {
                                        if (error.code == -500) {
                                            VtcEnterUsernameViewController *controller = [VtcEnterUsernameViewController loadFromNibNamed:@"VtcEnterUsernameViewController"];
                                            controller.authenType = AuthenTypeQuickStart;
                                            
                                            [self.navigationController pushViewController:controller animated:NO];
                                        }
                                        else {
                                            [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
                                        }
                                    }
                                }];
//                            [alert dismissViewControllerAnimated:NO completion:nil];

                        }];

    UIAlertAction* cancel = [UIAlertAction
                            actionWithTitle:@"Hủy"
                           style:UIAlertActionStyleCancel
                           handler:^(UIAlertAction * action)
                           {
                                NSLog(@"Cancel clicked");
//                                [alert dismissViewControllerAnimated:NO completion:nil];

                           }];

    [alert addAction:ok];

    [alert addAction:cancel];

    [self presentViewController:alert animated:NO completion:nil];
    
    return;
    
}
- (IBAction)btnBackClicked:(id)sender {
    if (@available(iOS 13.0, *)) {
        //go to login screen
        VtcAuthenNavController *navController = (VtcAuthenNavController *)self.navigationController;
        VtcLoginViewController *controller = [VtcLoginViewController loadFromNibNamed:@"VtcLoginViewController"];
        [navController changeRootController:controller];
    }else{
        VtcAuthenNavController *navController = (VtcAuthenNavController *)self.navigationController;
        VtcLoginViewController *controller = [VtcLoginViewController loadFromNibNamed:@"VtcLoginViewController"];
        [navController changeRootController:controller];
    }
    
}

- (IBAction)onSignIn:(id)sender {
    if ([_tfUsername.text isEqualToString:@""]) {
        [Utilities showNotification:VTCLocalizeString(@"Missing username")];
        [_tfUsername becomeFirstResponder];
        return;
    }
    if ([_tfPassword.text isEqualToString:@""]) {
        [Utilities showNotification:VTCLocalizeString(@"Missing password")];
        [_tfPassword becomeFirstResponder];
        return;
    }
    if (_savePassword.selected) {
        [VIDUser currentUser].password = _tfPassword.text;
        [VIDUser currentUser].savePassword = YES;
    }
    else {
        [VIDUser currentUser].password = nil;
        [VIDUser currentUser].savePassword = NO;
    }
    [(VtcRoundedButton *)sender setEnabled:NO];
    NSString *userName = [_tfUsername.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
    [[NetworkModal sharedModal] loginWithVTCID:userName password:_tfPassword.text completion:^(BOOL status, id responsedObject, NSError *error) {
        [(VtcRoundedButton *)sender setEnabled:YES];
        [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
        if (status) {
            NSLog(@"login success");
            [VIDUser currentUser].signedIn = YES;
            [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeNormal containerController:self.navigationController showWelcome:YES];
            
            [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeLoginVTC]];
        }
        else {
            if (error.code == -1000) {
                NSLog(@"login with code -1000");
                VtcOTPViewController *controller = [VtcOTPViewController loadFromNibNamed:@"VtcOTPViewController"];
                controller.username = userName;
                controller.password = _tfPassword.text;
                [self.navigationController pushViewController:controller animated:NO];
            }
            else {
                NSLog(@"login failure");
                if ([error.userInfo objectForKey:@"message"] == nil)
                {
                    [Utilities showNotification:VTCLocalizeString(@"Login failed. Please try again")];
                }
                else
                {
                    [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
                }
//                [Utilities showNotification:VTCLocalizeString(@"Login failed. Please try again")];
//                if ([[[VtcConfig defaultConfig] sdkLanguage] isEqualToString:@"vie"])
//                {
                
//                }else
//                {
//                    [Utilities showNotification:@"Login failure"];
//                }
                
            }
        }
    }];
}

- (IBAction)onFacebook:(id)sender {
    [(VtcRoundedButton *)sender setEnabled:NO];
    [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
    [[FacebookHandler sharedHandler] signInfromViewController:self completion:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
        if (error) {
            // error
            [(VtcRoundedButton *)sender setEnabled:YES];
            NSLog(@"%@",error);
            [Utilities showNotification:error.localizedDescription];
        } else if (result.isCancelled) {
            // cancel
            NSLog(@"cancel");
            [(VtcRoundedButton *)sender setEnabled:YES];
        } else {
            [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
            VTCLog(@"Facebook token: %@", [[FBSDKAccessToken currentAccessToken] tokenString]);
            [[NetworkModal sharedModal] loginWithFacebook:[[FBSDKAccessToken currentAccessToken] tokenString] completion:^(BOOL status, id responsedObject, NSError *error) {
                [(VtcRoundedButton *)sender setEnabled:YES];
                [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
                if (status) {
                    [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeFacebook containerController:self.navigationController showWelcome:YES];
                    [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeLoginFacebook]];
                }
                else {
//                    NSLog(@"%@",error.code);
                    if (error.code == -500) {
                        VtcEnterUsernameViewController *controller = [VtcEnterUsernameViewController loadFromNibNamed:@"VtcEnterUsernameViewController"];
                        controller.authenType = AuthenTypeFacebook;
                        [self.navigationController pushViewController:controller animated:NO];
                    }
                    if (error.code == -1000) {
                        VtcOTPViewController *controller = [VtcOTPViewController loadFromNibNamed:@"VtcOTPViewController"];
                        controller.socialToken = [[FBSDKAccessToken currentAccessToken] tokenString];
                        controller.loginType = LoginTypeFacebook;
                        [self.navigationController pushViewController:controller animated:NO];
                    }
                    else {
                        [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
                    }
                }
            }];
        }
    }];
}

- (IBAction)onGoogle:(id)sender {
//    [GoogleServiceHandler sharedHandler].loginController = self;
//    [(VtcRoundedButton *)sender setEnabled:NO];
//    [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
//    [[GoogleServiceHandler sharedHandler] signIn:^(GIDGoogleUser *user, NSError *error) {
//        [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
//        if (error) {
//            [(VtcRoundedButton *)sender setEnabled:YES];
//            [Utilities showNotification:error.localizedDescription];
//            return;
//        }
//        else if (user) {
//            // Perform any operations on signed in user here.
//            NSString *accessToken = user.authentication.accessToken; // Safe to send to the server
//            [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
//            [[NetworkModal sharedModal] loginWithGoogle:accessToken completion:^(BOOL status, id responsedObject, NSError *error) {
//                [(VtcRoundedButton *)sender setEnabled:YES];
//                [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
//                if (status) {
//                    [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeGoogle containerController:self.navigationController showWelcome:YES];
//                    [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeLoginGoogle]];
//                }
//                else {
//                    if (error.code == -500) {
//                        VtcEnterUsernameViewController *controller = [VtcEnterUsernameViewController loadFromNibNamed:@"VtcEnterUsernameViewController"];
//                        controller.authenType = AuthenTypeGoogle;
//                        controller.openIDTokenString = accessToken;
//                        [self.navigationController pushViewController:controller animated:NO];
//                    }
//                    if (error.code == -1000) {
//                        VtcOTPViewController *controller = [VtcOTPViewController loadFromNibNamed:@"VtcOTPViewController"];
//                        controller.socialToken = accessToken;
//                        controller.loginType = LoginTypeGoogle;
//                        [self.navigationController pushViewController:controller animated:NO];
//                    }
//                    else {
//                        [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
//                    }
//                }
//            }];
//        }
//        else {
//            [(VtcRoundedButton *)sender setEnabled:YES];
//        }
//    }];
}

- (IBAction)onSignUp:(id)sender {
    VtcAuthenNavController *navController = (VtcAuthenNavController *)self.navigationController;
    VtcSignUpViewController *controller = [VtcSignUpViewController loadFromNibNamed:@"VtcSignUpViewController"];
    [navController changeRootController:controller];
//    [navController pushViewController:controller animated:true];
}

- (IBAction)onForgotPassword:(id)sender {
    VtcForgotPasswordViewController *controller = [VtcForgotPasswordViewController loadFromNibNamed:@"VtcForgotPasswordViewController"];
    [self.navigationController pushViewController:controller animated:NO];
}

- (IBAction)onChangePassword:(id)sender {
    VtcChangePasswordViewController *controller = [VtcChangePasswordViewController loadFromNibNamed:@"VtcChangePasswordViewController"];
    [self.navigationController pushViewController:controller animated:NO];
}
- (IBAction)btnLanguageTouchUpInside:(UIButton *)sender {
    if (sender.isSelected)
    {
        return;
    }
    if (sender == self.btnLangEn)
    {
        [self configUIWithLanguage:@"eng" animated:NO];
        
    }else
    {
        [self configUIWithLanguage:@"vie" animated:NO];
    }
    [self settingLanguage];
}

// MARK: UITextField Delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.activeTextField = textField;
    return YES;
}

@end

//
//  VtcAuthenViewController.h
//  VtcSDK
//
//  Created by Kent Vu on 7/25/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcAuthenBaseViewController.h"

@interface VtcAuthenViewController : VtcAuthenBaseViewController
@property (nonatomic) BOOL needAutoLogin;
@property (nonatomic, copy) NSString *registeringName;
@property (nonatomic, copy) NSString *registeringPassword;
@property (weak, nonatomic) IBOutlet UIImageView *vtclogo;
@property (weak, nonatomic) IBOutlet UIScrollView *mainscrollview;
@property (weak, nonatomic) IBOutlet UIImageView *logo;


@end

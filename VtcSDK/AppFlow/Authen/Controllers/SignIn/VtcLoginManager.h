//
//  VtcLoginManager.h
//  VtcSDK
//
//  Created by Admin on 10/23/18.
//  Copyright © 2018 VTCIntecom. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VtcLoginManager : NSObject
+ (instancetype) sharedManager;
- (void) autoSignInFrom:(UIViewController *)viewController;
- (void) autoSignInFacebookFrom:(UIViewController *)viewController;
- (void) autosignInGoogleFrom:(UIViewController *)viewController;
- (void) autoQuickStartFrom:(UIViewController *)viewController;
@end

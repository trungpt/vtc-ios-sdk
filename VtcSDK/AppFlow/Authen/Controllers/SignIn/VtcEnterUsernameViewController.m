//
//  VtcEnterUsernameViewController.m
//  VtcSDK
//
//  Created by Kent Vu on 8/2/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcEnterUsernameViewController.h"
#import "NetworkModal+Authen.h"
#import "FacebookHandler.h"
#import "VtcLoginTextField.h"
#import "VtcRoundedButton.h"

@interface VtcEnterUsernameViewController () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfUsername;
@property (nonatomic, weak) IBOutlet VtcRoundedButton *btnNext;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end

@implementation VtcEnterUsernameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.portraitTopSpace = [Global sharedInstance].gPortraitNavBarHeight+50.0;
    self.landscapeTopSpace = [Global sharedInstance].gLandscapeNavBarHeight+35.0;
    [self addCustomBackButton];
    [self addCustomTitleView];
    
    _tfUsername.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    [_tfUsername setHandleDidEndOnExit:^{
        [self onContinue:_btnNext];
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self settingLanguage];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void) settingLanguage
{
    self.lblTitle.text = [VTCLocalizeString(@"Enter username") uppercaseString];
    self.tfUsername.placeholder = VTCLocalizeString(@"Username");
    [self.btnNext setTitle:[VTCLocalizeString(@"Continue") uppercaseString] forState:UIControlStateNormal];
}

- (IBAction)onContinue:(id)sender {
    if ([_tfUsername.text isEqualToString:@""]) {
        [Utilities showNotification:VTCLocalizeString(@"Missing username")];
        [_tfUsername becomeFirstResponder];
        return;
    }
    if (_authenType == AuthenTypeApple) {
        [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
        [[NetworkModal sharedModal] registerWithApple:_openIDTokenString user:_tfUsername.text completion:^(BOOL status, id responsedObject, NSError *error) {
            
            [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
            if (status) {
                // appsflyer register With Facebook
                [[VtcEventLogging manager] setAppsFlyerInAppEvent:kAppsFlyerEventTypeAppleRegister values:[NSDictionary dictionaryWithObjectsAndKeys:@"1", kAppsFlyerEventTypeRegisterValueKey, nil]];
                
                [[Global sharedInstance] handleSignInResponse:responsedObject authenType: _authenType containerController:self.navigationController showWelcome:YES];
            }
            else {
                [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
            }
        }];
    }
    if (_authenType == AuthenTypeFacebook) {
        [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
        [[NetworkModal sharedModal] registerWithFacebook:[[FBSDKAccessToken currentAccessToken] tokenString] user:_tfUsername.text completion:^(BOOL status, id responsedObject, NSError *error) {
            
            [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
            if (status) {
                // appsflyer register With Facebook
                [[VtcEventLogging manager] setAppsFlyerInAppEvent:kAppsFlyerEventTypeFBRegister values:[NSDictionary dictionaryWithObjectsAndKeys:@"1", kAppsFlyerEventTypeRegisterValueKey, nil]];
                
                [[Global sharedInstance] handleSignInResponse:responsedObject authenType:_authenType containerController:self.navigationController showWelcome:YES];
            }
            else {
                [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
            }
        }];
    }
    if (_authenType == AuthenTypeGoogle) {
        NSAssert(_openIDTokenString, @"GoogleSignIn toke not set");
        [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
        [[NetworkModal sharedModal] registerWithGoogle:_openIDTokenString user:_tfUsername.text completion:^(BOOL status, id responsedObject, NSError *error) {
            [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
            if (status) {
                // appsflyer register With Google
                [[VtcEventLogging manager] setAppsFlyerInAppEvent:kAppsFlyerEventTypeGGRegister values:[NSDictionary dictionaryWithObjectsAndKeys:@"1", kAppsFlyerEventTypeRegisterValueKey, nil]];
                
                [[Global sharedInstance] handleSignInResponse:responsedObject authenType:_authenType containerController:self.navigationController showWelcome:YES];
            }
            else {
                [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
            }
        }];
    }
    if (_authenType == AuthenTypeQuickStart) {
        [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
        [[NetworkModal sharedModal] registerQuickStartGame:_tfUsername.text completion:^(BOOL status, id responsedObject, NSError *error) {
            [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
            if (status) {
                // appsflyer register With Google
                [[VtcEventLogging manager] setAppsFlyerInAppEvent:kAppsFlyerEventTypeQuickStartRegister values:[NSDictionary dictionaryWithObjectsAndKeys:@"1", kAppsFlyerEventTypeRegisterValueKey, nil]];
                
                [[Global sharedInstance] handleSignInResponse:responsedObject authenType:_authenType containerController:self.navigationController showWelcome:YES];
            }
            else {
                [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
            }
        }];
    }
}

// MARK: UITextField Delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.activeTextField = textField;
    return YES;
}

@end

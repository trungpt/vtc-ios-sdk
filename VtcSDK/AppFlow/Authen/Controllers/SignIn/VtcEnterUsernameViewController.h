//
//  VtcEnterUsernameViewController.h
//  VtcSDK
//
//  Created by Kent Vu on 8/2/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcAuthenBaseViewController.h"
#import "VIDUser.h"

@interface VtcEnterUsernameViewController : VtcAuthenBaseViewController

@property (nonatomic) AuthenType authenType;
@property (nonatomic, copy) NSString *openIDTokenString;

@end

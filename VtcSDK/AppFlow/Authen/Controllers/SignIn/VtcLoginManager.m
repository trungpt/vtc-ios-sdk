//
//  VtcLoginManager.m
//  VtcSDK
//
//  Created by Admin on 10/23/18.
//  Copyright © 2018 VTCIntecom. All rights reserved.
//

#import "SDKManager.h"
#import "SDKManager+Internal.h"
#import "VtcLoginManager.h"
#import "VtcLoginViewController.h"
#import "VtcSignUpViewController.h"
#import "VtcAuthenNavController.h"
#import "FacebookHandler.h"
#import "GoogleServiceHandler.h"
#import "VtcNavigationBar.h"
#import "VIDUser.h"
#import "VIDUser+Internal.h"
#import "NetworkModal+Authen.h"
#import "Global.h"
#import "SVProgressHUD.h"
#import "UIViewController+Additions.h"


@implementation VtcLoginManager
+ (instancetype)sharedManager
{
    static VtcLoginManager * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[VtcLoginManager alloc] init];    });
    return instance;
}
- (void)autoSignInFrom:(UIViewController *)viewController
{
//    NSLog(@"testahihi autoSignInFrom");
    VIDUser *user = [VIDUser currentUser];
    if (user.savePassword && (user.password.length > 0))
    {
        [self onSignInWithUsername:user.userName password:user.password parentViewController:viewController];
    }
    else
    {
       [[SDKManager defaultManager] openLoginFormFromViewController:viewController];
    }
}

- (void) autoQuickStartFrom:(UIViewController *)viewController{
    
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD show];
    [[NetworkModal sharedModal] quickStartGame:@"" password:@"" completion:^(BOOL status, id responsedObject, NSError *error) {
            [SVProgressHUD dismiss];
            if (status) {
                NSLog(@"login success");
                [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeQuickStart containerController:nil showWelcome:YES];
                
                [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeQuickStartLoginVTC]];
            }
            else {
                [[SDKManager defaultManager] openLoginFormFromViewController:viewController];
            }
        }];
}

- (void)autoSignInFacebookFrom:(UIViewController *)viewController
{
    NSLog(@"testahihi autoSignInFacebookFrom");
    if ([[FacebookHandler sharedHandler] fbTokenIsActive] && [[FacebookHandler sharedHandler] fbAccessToken] != nil)
    {
        [self onFacebookWithAccessToken:[[FacebookHandler sharedHandler] fbAccessToken] parentViewController:viewController];
    }
    else
    {
        [[SDKManager defaultManager] openLoginFormFromViewController:viewController];
    }
}
- (void)autosignInGoogleFrom:(UIViewController *)viewController
{
//    NSLog(@"testahihi autosignInGoogleFrom");
//    if ([[GoogleServiceHandler sharedHandler] ggAccessToken] != nil)
//    {
//        [self onGoogleWithAccessToken:[[GoogleServiceHandler sharedHandler] ggAccessToken] parentViewController:viewController];
//    }
//    else
//    {
//        [[SDKManager defaultManager] openLoginFormFromViewController:viewController];
//    }
}

//- (void)openLoginFormFromViewController:(UIViewController *)viewController {
////    NSLog(@"testahihi openLoginFormFromViewController VtcLoginManager");
//    NSLog(@"<VtcSDK> Show login view.");
//    //Luồng đăng nhập mới
//
//    VtcLoginViewController *controller = [VtcLoginViewController loadFromNibNamed:@"VtcLoginViewController"];
////    VtcSignUpViewController *controller = [VtcSignUpViewController loadFromNibNamed:@"VtcSignUpViewController"];
//    VtcAuthenNavController *nav = [[VtcAuthenNavController alloc] initWithNavigationBarClass:[VtcNavigationBar class] toolbarClass:nil];
//    [nav setViewControllers:@[controller]];
////    if (viewController) {
////        [viewController presentViewController:nav animated:NO completion:nil];
////    }
////    else {
////        [[UIViewController topViewController] presentViewController:nav animated:NO completion:nil];
////    }
//
//
//    NSLog(@"testahihi completion ??? ");
//
//    if (viewController) {
//        [viewController presentViewController:nav animated:NO completion:^{
//            NSLog(@"testahihi completion1");
//            nav.view.superview.frame = CGRectMake(0, 0, 200, 255);
//            nav.view.superview.center = viewController.view.center;
//            nav.view.layer.cornerRadius = 5;
//            nav.view.layer.masksToBounds = YES;
//        }];
//    }
//    else {
//        [[UIViewController topViewController] presentViewController:nav animated:NO completion:^{
//            NSLog(@"testahihi completion2");
//            nav.view.superview.frame = CGRectMake(0, 0, 200, 255);
//            nav.view.superview.center = [UIViewController topViewController].view.center;
//            nav.view.layer.cornerRadius = 5;
//            nav.view.layer.masksToBounds = YES;
//        }];
//    }
//
//
//}

- (IBAction)onSignInWithUsername:(NSString *) username password:(NSString *)password parentViewController:(UIViewController *)parentVC{
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD show];
    [[NetworkModal sharedModal] loginWithVTCID:username password:password completion:^(BOOL status, id responsedObject, NSError *error) {
        [SVProgressHUD dismiss];
        if (status) {
            NSLog(@"login success");
            [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeNormal containerController:nil showWelcome:YES];
            
            [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeLoginVTC]];
        }
        else {
            [[SDKManager defaultManager] openLoginFormFromViewController:parentVC];
        }
    }];
}

- (IBAction)onFacebookWithAccessToken:(NSString *)accessToken parentViewController:(UIViewController *)parentVC {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD show];
    [[NetworkModal sharedModal] loginWithFacebook:accessToken completion:^(BOOL status, id responsedObject, NSError *error) {
        [SVProgressHUD dismiss];
        if (status) {
            [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeFacebook containerController:nil showWelcome:YES];
            [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeLoginFacebook]];
        }
        else {
            [[SDKManager defaultManager] openLoginFormFromViewController:parentVC];
        }
    }];

}

- (IBAction)onGoogleWithAccessToken:(NSString *)accessToken parentViewController:(UIViewController *)parentVC {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD show];
    [[NetworkModal sharedModal] loginWithGoogle:accessToken completion:^(BOOL status, id responsedObject, NSError *error) {
        [SVProgressHUD dismiss];
        if (status) {
            [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeGoogle containerController:nil showWelcome:YES];
            [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeLoginGoogle]];
        }
        else {
            [[SDKManager defaultManager] openLoginFormFromViewController:parentVC];
        }
    }];
}
@end

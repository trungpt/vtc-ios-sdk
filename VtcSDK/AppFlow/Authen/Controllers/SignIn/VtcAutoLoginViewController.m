//
//  VtcAutoLoginViewController.m
//  VtcSDK
//
//  Created by Kent Vu on 8/5/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcAutoLoginViewController.h"
#import "VtcAuthenNavController.h"
#import "VtcLoginViewController.h"
#import "VIDUser.h"
#import "UIViewController+Additions.h"
#import "SDKManager+Internal.h"
#import "AppleIAPHelper.h"

@interface VtcAutoLoginViewController ()

@property (nonatomic, weak) IBOutlet UILabel *lbReport;
@property (nonatomic, weak) IBOutlet UILabel *lbGameVersion;
@property (nonatomic, weak) IBOutlet UILabel *lbSDKVersion;

@end

@implementation VtcAutoLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.portraitTopSpace = 80.0;
    self.landscapeTopSpace = 30.0;
    
    _lbGameVersion.text = [NSString stringWithFormat:@"game: v%@", [[VtcConfig defaultConfig] gameVersion]];
    NSString *sdkVerString = [NSString stringWithFormat:@"sdk: v%@", VtcSDKVersionString];
    if ([NetworkModal sharedModal].isDevMode) {
        sdkVerString = [sdkVerString stringByAppendingString:@" (Dev)"];
    }
    _lbSDKVersion.text = sdkVerString;
    
    NSDictionary *normalAttributes = @{
                                       NSFontAttributeName : [UIFont systemFontOfSize:14],
                                       NSForegroundColorAttributeName : ColorFromHEX(0x333333)
                                       };
    NSDictionary *coloredBoldAttributes = @{
                                            NSFontAttributeName : [UIFont boldSystemFontOfSize:14],
                                            NSForegroundColorAttributeName : [Global sharedInstance].gButtonColor
                                            };
    
    NSMutableAttributedString *line1 = [[NSMutableAttributedString alloc] init];
    [line1 appendAttributedString:[[NSAttributedString alloc] initWithString:@"Chào mừng bạn đã trở lại, " attributes:normalAttributes]];
    [line1 appendAttributedString:[[NSAttributedString alloc] initWithString:[Global sharedInstance].savedUserName ? [Global sharedInstance].savedUserName : @"" attributes:coloredBoldAttributes]];
    [line1 appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n" attributes:normalAttributes]];
    [line1 appendAttributedString:[[NSAttributedString alloc] initWithString:@"Bạn đã đăng nhập VTC Game thành công." attributes:normalAttributes]];
    [line1 appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n" attributes:normalAttributes]];
    [line1 appendAttributedString:[[NSAttributedString alloc] initWithString:@"Bạn có muốn tiếp tục sử dụng tài khoản này?" attributes:normalAttributes]];
    VTCLog(@"%@", line1);
    _lbReport.attributedText = line1;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onAgree:(id)sender {
    if ([SDKManager defaultManager].delegate && [[SDKManager defaultManager].delegate respondsToSelector:@selector(sdkManagerDidSignInWithUser:)]) {
        [[SDKManager defaultManager].delegate sdkManagerDidSignInWithUser:[VIDUser currentUser]];
    }
    [SDKManager defaultManager].showingLoginView = NO;
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)onAnotherAccount:(id)sender {
    // clear signed-in user info
    [[Global sharedInstance] handleSignOut];
    
    VtcAuthenNavController *navController = (VtcAuthenNavController *)self.navigationController;
    VtcLoginViewController *loginController = [VtcLoginViewController loadFromNibNamed:@"VtcLoginViewController"];
    [navController changeRootController:loginController];
}

@end

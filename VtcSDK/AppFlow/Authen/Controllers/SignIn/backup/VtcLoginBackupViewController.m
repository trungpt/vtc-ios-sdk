//
//  VtcLoginBackupViewController.m
//  VtcSDK
//
//  Created by Kent Vu on 7/25/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//
#import "SVProgressHUD.h"
//VtcLoginBackupViewController
#import "VtcAuthenViewController.h"
#import "VtcLoginViewController.h"
#import "UIImage+Additions.h"
#import "NSBundle+Additions.h"
#import "UIViewController+Additions.h"
#import "VtcSignUpViewController.h"
#import "VtcForgotPasswordViewController.h"
#import "FacebookHandler.h"
#import "GoogleServiceHandler.h"
#import "NetworkModal+Authen.h"
#import "VtcEnterUsernameViewController.h"
#import "VtcOTPViewController.h"
#import "VtcChangePasswordViewController.h"
#import "VtcAuthenNavController.h"
#import "VtcRoundedButton.h"
#import "VtcLoginTextField.h"
#import "SmartParser.h"
#import "UIView+UpdateAutoLayoutConstraints.h"
#import "VIDUser+Internal.h"
#import "SDKManager.h"
#import "SDKManager+Internal.h"
#import<AuthenticationServices/AuthenticationServices.h>
@interface VtcLoginBackupViewController () <UITextFieldDelegate, UIGestureRecognizerDelegate, ASAuthorizationControllerDelegate,ASAuthorizationControllerPresentationContextProviding>
@property (weak, nonatomic) IBOutlet VtcRoundedButton *btnQuickStart;

@property (weak, nonatomic) IBOutlet UIButton *signInWithApple;

@property (weak, nonatomic) IBOutlet UIView *viewForAppleSignIn;
@property (nonatomic, weak) IBOutlet VtcRoundedButton *btnSignIn;
@property (nonatomic, weak) IBOutlet VtcRoundedButton *btnFacebook;
@property (nonatomic, weak) IBOutlet VtcRoundedButton *btnGoogle;
@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfUsername;
@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnChangePass;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UILabel *lblSavePassword;

@property (nonatomic, weak) IBOutlet UILabel *orLabel;
@property (nonatomic, weak) IBOutlet UILabel *lbGameVersion;
@property (nonatomic, weak) IBOutlet UILabel *lbSDKVersion;

@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@property (nonatomic, weak) IBOutlet UIButton *savePassword;

@property (nonatomic, weak) IBOutlet UIButton *forgotPasswordLandscape;
@property (nonatomic, weak) IBOutlet UIButton *forgotPasswordPortrait;
@property (nonatomic, weak) IBOutlet UIButton *btnFacebookLandscape;
@property (weak, nonatomic) IBOutlet UIButton *btnAppleLandscape;
@property (weak, nonatomic) IBOutlet UIView *viewForLandscape;

@property (nonatomic, weak) IBOutlet UIButton *btnGoogleLandscape;

@property (weak, nonatomic) IBOutlet UIView *viewHoverLanguage;
@property (weak, nonatomic) IBOutlet UIButton *btnLangEn;
@property (weak, nonatomic) IBOutlet UIButton *btnLangVi;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraintViewHover;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *topSpaceFromUsernameToLogo;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *topSpaceFromVersionLabelToCenter;

@property (weak, nonatomic) IBOutlet UIView *viewForQuickStart;
@property (weak, nonatomic) IBOutlet UIView *viewForSignIn;
@property (weak, nonatomic) IBOutlet UIView *viewForRegister;

@end

@implementation VtcLoginBackupViewController

API_AVAILABLE(ios(13.0))
ASAuthorizationAppleIDButton *appleIDButton;

NSString* const setCurrentIdentifier = @"setCurrentIdentifier";

- (void) viewDidAppear:(BOOL)animated{
    //NSLog(@"testahihi viewDidAppear");
    
    if (_tfUsername.text && _tfUsername.text.length>3 && _tfPassword.text && _tfPassword.text.length > 3) {
        //        //NSLog(@"testahihi onSignIn");
        //Nếu thay đổi tài khoản thì không tự động đăng nhập
        //Luồng đăng nhập mới
        //        NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultCachedUser];
        //        NSString *oldUsername = @"";
        //        if (userInfo) {
        //            oldUsername = [SmartParser stringForKey:@"accountName" from:userInfo];
        //        }ƒ
        //        if([_registeringName isEqualToString: oldUsername])
        //            [self onSignIn:_btnSignIn];
        //        else{
        //            _tfPassword.text=@"";
        //            [VIDUser currentUser].password = @"";
        //        }
        //
        //        if(self.needAutoLogin){
        //            //Tu dong dang nhap sau khi dang ky xong
        //            //NSLog(@"testahihi needAutoLogin");
        //            self.needAutoLogin = NO;
        //            [self onSignIn:_btnSignIn];
        //        }
    }
}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [NetworkModal testEncryptionAndDecryption];
    
    if ([SDKManager defaultManager].isShowCloseButtonInAuthenVC) {
        [self.closeButton setHidden:false];
    }
    
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self addCustomBackButton];
    [self addCustomTitleView];
    self.portraitTopSpace = 8.0;
    self.landscapeTopSpace = 8.0; //30.0;
    
    _tfUsername.clearButtonMode = UITextFieldViewModeWhileEditing;
    //    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultCachedUser];
    
    //
    [_tfPassword setHandleDidEndOnExit:^{
        [self onSignIn:_btnSignIn];
    }];
    
    _lbGameVersion.text = [NSString stringWithFormat:@"game: v%@", [[VtcConfig defaultConfig] gameVersion]];
    NSString *sdkVerString = [NSString stringWithFormat:@"sdk: v%@", VtcSDKVersionString];
    if ([NetworkModal sharedModal].isDevMode) {
        sdkVerString = [sdkVerString stringByAppendingString:@" (Dev)"];
    }
    _lbSDKVersion.text = sdkVerString;
    
    [self configUI];
    
    //dang nhap Apple
    if (@available(iOS 13.0, *)) {
        [self observeAppleSignInState];
        [self setupUI];
    }else{
    }

}

//dang nhap Apple

- (void)observeAppleSignInState {
    if (@available(iOS 13.0, *)) {
        NSLog(@"observeAppleSignInState");
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center addObserver:self selector:@selector(handleSignInWithAppleStateChanged:) name:ASAuthorizationAppleIDProviderCredentialRevokedNotification object:nil];
    }
}

- (void)handleSignInWithAppleStateChanged:(id)noti {
    NSLog(@"%s", __FUNCTION__);
    NSLog(@"%@", noti);
}

- (void)setupUI {
    
    // Sign In With Apple
    //    _appleIDLoginInfoTextView = [[UITextView alloc] initWithFrame:CGRectMake(.0, 40.0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) * 0.4) textContainer:nil];
    //    _appleIDLoginInfoTextView.font = [UIFont systemFontOfSize:32.0];
    //    [self.view addSubview:_appleIDLoginInfoTextView];
    
    
    if (@available(iOS 13.0, *)) {
        NSLog(@"setupUI");
        NSLog(@"size for button: %f %f",self.viewForAppleSignIn.frame.size.width, self.viewForAppleSignIn.frame.size.height);
        // Sign In With Apple Button
        if ([SDKManager defaultManager].isSaveAccessToken)
        {
            VIDUser *user =  [VIDUser currentUser];
            if  (user.authenType == AuthenTypeApple && [VIDUser currentUser].appleSignInToken != nil && ![[VIDUser currentUser].appleSignInToken  isEqual: @""]){
                //NSLog(@"testahihi AppleSigin: Use Continue Button");
                appleIDButton = [[ASAuthorizationAppleIDButton alloc] initWithAuthorizationButtonType: ASAuthorizationAppleIDButtonTypeContinue authorizationButtonStyle: ASAuthorizationAppleIDButtonStyleBlack];
            }else{
                //NSLog(@"testahihi AppleSigin: Use SignIn Button");
                appleIDButton = [[ASAuthorizationAppleIDButton alloc] initWithAuthorizationButtonType: ASAuthorizationAppleIDButtonTypeSignIn authorizationButtonStyle: ASAuthorizationAppleIDButtonStyleBlack];
            }
        }else{
            //NSLog(@"testahihi AppleSigin isSaveAccessToken false: Use SignIn Button");
            appleIDButton = [[ASAuthorizationAppleIDButton alloc] initWithAuthorizationButtonType: ASAuthorizationAppleIDButtonTypeSignIn authorizationButtonStyle: ASAuthorizationAppleIDButtonStyleBlack];
        }
        
        //        appleIDButton.frame =  CGRectMake(.0, .0, self.viewForAppleSignIn.frame.size.width, self.viewForAppleSignIn.frame.size.height);
        //    CGPoint origin = CGPointMake(0.0, 0.0);
        //    CGRect frame = appleIDButton.frame;
        //    frame.origin = origin;
        //    appleIDButton.frame = frame;
        
        appleIDButton.userInteractionEnabled = YES; // Modify this if needed
        [self.viewForAppleSignIn addSubview:appleIDButton];
        //Add constraints to the Parent
        appleIDButton.translatesAutoresizingMaskIntoConstraints = NO;
        //Trailing
        NSLayoutConstraint *trailing =[NSLayoutConstraint
                                       constraintWithItem:appleIDButton
                                       attribute:NSLayoutAttributeTrailing
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:appleIDButton.superview
                                       attribute:NSLayoutAttributeTrailing
                                       multiplier:1.0f
                                       constant:0.f];
        
        //leading
        NSLayoutConstraint *leading =[NSLayoutConstraint
                                      constraintWithItem:appleIDButton
                                      attribute:NSLayoutAttributeLeading
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:appleIDButton.superview
                                      attribute:NSLayoutAttributeLeading
                                      multiplier:1.0f
                                      constant:0.f];
        //Trailing
        NSLayoutConstraint *top =[NSLayoutConstraint
                                  constraintWithItem:appleIDButton
                                  attribute:NSLayoutAttributeTop
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:appleIDButton.superview
                                  attribute:NSLayoutAttributeTop
                                  multiplier:1.0f
                                  constant:0.f];
        //Trailing
        NSLayoutConstraint *bottom =[NSLayoutConstraint
                                     constraintWithItem:appleIDButton
                                     attribute:NSLayoutAttributeBottom
                                     relatedBy:NSLayoutRelationEqual
                                     toItem:appleIDButton.superview
                                     attribute:NSLayoutAttributeBottom
                                     multiplier:1.0f
                                     constant:0.f];
        
        [self.viewForAppleSignIn addConstraint:leading];
        [self.viewForAppleSignIn addConstraint:trailing];
        [self.viewForAppleSignIn addConstraint:bottom];
        [self.viewForAppleSignIn addConstraint:top];
        
        [self.view layoutIfNeeded];
        
        NSLog(@"appleIDButton: %f %f", appleIDButton.frame.size.width, appleIDButton.frame.size.height);
        
        appleIDButton.cornerRadius = CGRectGetHeight(appleIDButton.frame) * 0.5;
        
        [appleIDButton addTarget:self action:@selector(handleAuthrization:) forControlEvents:UIControlEventTouchDown];
        
        [_btnAppleLandscape addTarget:self action:@selector(handleAppleSignInClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        //    [_signInWithApple addTarget:self action:@selector(handleAppleSignInClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        //    [_signInWithApple addTarget:self action:@selector(handleAppleSignInTouchedDown:) forControlEvents:UIControlEventTouchDown];
        //
        //    [_signInWithApple addTarget:self action:@selector(handleAppleSignInTouchedUp:) forControlEvents:UIControlEventTouchUpOutside];
        
    }
}

//- (void)handleAppleSignInTouchedUp:(UIButton *)sender {
//    if (@available(iOS 13.0, *)) {
//        [UIView animateWithDuration:0.5 animations:^{
//            [appleIDButton setAlpha:1.0];
//        }];
//    }
//}
//
//- (void)handleAppleSignInTouchedDown:(UIButton *)sender {
//    if (@available(iOS 13.0, *)) {
//        [UIView animateWithDuration:0.5 animations:^{
//            [appleIDButton setAlpha:0.5];
//        }];
//    }
//}

- (void)handleAppleSignInClicked:(UIButton *)sender {
    if (@available(iOS 13.0, *)) {
        //    [UIView animateWithDuration:0.5 animations:^{
        //        [appleIDButton setAlpha:1.0];
        //    }];
        [appleIDButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)handleAuthrization:(UIButton *)sender {
    if (@available(iOS 13.0, *)) {
        //NSLog(@"testahihi handleAuthrization");
        // A mechanism for generating requests to authenticate users based on their Apple ID.
        ASAuthorizationAppleIDProvider *appleIDProvider = [ASAuthorizationAppleIDProvider new];
        
        // Creates a new Apple ID authorization request.
        ASAuthorizationAppleIDRequest *request = appleIDProvider.createRequest;
        // The contact information to be requested from the user during authentication.
        request.requestedScopes = @[ASAuthorizationScopeFullName, ASAuthorizationScopeEmail];
        
        // A controller that manages authorization requests created by a provider.
        ASAuthorizationController *controller = [[ASAuthorizationController alloc] initWithAuthorizationRequests:@[request]];
        
        // A delegate that the authorization controller informs about the success or failure of an authorization attempt.
        controller.delegate = self;
        
        // A delegate that provides a display context in which the system can present an authorization interface to the user.
        controller.presentationContextProvider = self;
        
        if ([SDKManager defaultManager].isSaveAccessToken){
            VIDUser *user =  [VIDUser currentUser];
            if  (user.authenType == AuthenTypeApple && [VIDUser currentUser].appleSignInToken != nil && ![[VIDUser currentUser].appleSignInToken  isEqual: @""]){
                [self doAppleLogin:[VIDUser currentUser].appleSignInToken];
            }else{
                // starts the authorization flows named during controller initialization.
                [controller performRequests];
            }
        }else{
            // starts the authorization flows named during controller initialization.
            [controller performRequests];
        }
        
    }
}

- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithAuthorization:(ASAuthorization *)authorization  API_AVAILABLE(ios(13.0)){
    
    NSLog(@"%s", __FUNCTION__);
    NSLog(@"%@", controller);
    NSLog(@"%@", authorization);
    
    NSLog(@"authorization.credential：%@", authorization.credential);
    
    NSMutableString *mStr = [NSMutableString string];
    NSMutableString *appleUserInfo = [NSMutableString string];
    //    mStr = [_appleIDLoginInfoTextView.text mutableCopy];
    
    if ([authorization.credential isKindOfClass:[ASAuthorizationAppleIDCredential class]]) {
        // ASAuthorizationAppleIDCredential
        
        ASAuthorizationAppleIDCredential *appleIDCredential = authorization.credential;
        NSString *user = appleIDCredential.user;
        
        [[NSUserDefaults standardUserDefaults] setValue:user forKey:setCurrentIdentifier];
        [mStr appendString:user?:@""];
        
        [appleUserInfo appendString:@"\n\nuser:"];
        [appleUserInfo appendString:appleIDCredential.user?:@""];
        [appleUserInfo appendString:@"\n\nidentityToken:"];
        NSString *identityToken = [[NSString alloc] initWithData:appleIDCredential.identityToken encoding:NSASCIIStringEncoding]?:@"";
        [appleUserInfo appendString: identityToken];
        [appleUserInfo appendString:@"\n\nauthorizationCode:"];
        [appleUserInfo appendString:[[NSString alloc] initWithData:appleIDCredential.authorizationCode encoding:NSASCIIStringEncoding]?:@""];
        [appleUserInfo appendString:@"\n\nstate:"];
        [appleUserInfo appendString:appleIDCredential.state?:@""];
        
        NSLog(@"Apple User Info：%@", appleUserInfo);
        
        
        NSString *familyName = appleIDCredential.fullName.familyName;
        [mStr appendString:familyName?:@""];
        NSLog(@"familyName：%@", appleIDCredential.fullName.familyName);
        NSString *givenName = appleIDCredential.fullName.givenName;
        [mStr appendString:givenName?:@""];
        NSLog(@"givenName：%@", appleIDCredential.fullName.givenName);
        NSString *email = appleIDCredential.email;
        [mStr appendString:email?:@""];
        NSLog(@"email：%@", appleIDCredential.email);
        NSLog(@"mStr：%@", mStr);
        [mStr appendString:@"\n"];
        
        [self doAppleLogin: identityToken];
        
    } else if ([authorization.credential isKindOfClass:[ASPasswordCredential class]]) {
        ASPasswordCredential *passwordCredential = authorization.credential;
        NSString *user = passwordCredential.user;
        NSString *password = passwordCredential.password;
        [mStr appendString:user?:@""];
        [mStr appendString:password?:@""];
        [mStr appendString:@"\n"];
        NSLog(@"mStr：%@", mStr);
        //        _appleIDLoginInfoTextView.text = mStr;
    } else {
        mStr = [@"check" mutableCopy];
        //        _appleIDLoginInfoTextView.text = mStr;
    }
}

- (void)doAppleLogin: (NSString *) identityToken{
    
    //    [FWActivityIndicatorView showWhiteIndicatorInView: self.btnSignIn location:FWActivityIndicatorLocationRight];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD show];
    
    [VIDUser currentUser].appleSignInToken = identityToken;
    //NSLog(@"testahihi set appleSignInToken: %@", [VIDUser currentUser].appleSignInToken);
    
    [[NetworkModal sharedModal] loginWithApple: identityToken completion:^(BOOL status, id responsedObject, NSError *error) {
        
        //NSLog(@"testahihi loginWithApple");
        //                      [self.btnAppleLandscape setEnabled:YES];
        //                        [self.signInWithApple setEnabled:YES];
        //                            [FWActivityIndicatorView hideAllActivityIndicatorInView: self.btnSignIn];
        [SVProgressHUD dismiss];
        if (status) {
            [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeApple containerController:self.navigationController showWelcome:YES];
            [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeLoginApple]];
        }
        else {
            //                    NSLog(@"%@",error.code);
            if (error.code == -500) {
                VtcEnterUsernameViewController *controller = [VtcEnterUsernameViewController loadFromNibNamed:@"VtcEnterUsernameViewController"];
                controller.authenType = AuthenTypeApple;
                controller.openIDTokenString = identityToken;
                [self.navigationController pushViewController:controller animated:NO];
            }
            if (error.code == -1000) {
                VtcOTPViewController *controller = [VtcOTPViewController loadFromNibNamed:@"VtcOTPViewController"];
                controller.socialToken = identityToken;
                controller.loginType = AuthenTypeApple;
                [self.navigationController pushViewController:controller animated:NO];
            }
            else {
                [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
            }
        }
    }];
}
- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithError:(NSError *)error  API_AVAILABLE(ios(13.0)){
    
    NSLog(@"%s", __FUNCTION__);
    NSLog(@"error ：%@", error);
    NSString *errorMsg = nil;
    switch (error.code) {
        case ASAuthorizationErrorCanceled:
            errorMsg = @"ASAuthorizationErrorCanceled";
            break;
        case ASAuthorizationErrorFailed:
            errorMsg = @"ASAuthorizationErrorFailed";
            break;
        case ASAuthorizationErrorInvalidResponse:
            errorMsg = @"ASAuthorizationErrorInvalidResponse";
            break;
        case ASAuthorizationErrorNotHandled:
            errorMsg = @"ASAuthorizationErrorNotHandled";
            break;
        case ASAuthorizationErrorUnknown:
            errorMsg = @"ASAuthorizationErrorUnknown";
            break;
    }
    NSMutableString *mStr = [[NSMutableString alloc]init];
    //    NSMutableString *mStr = [[NSMutableString alloc]init];
    [mStr appendString:errorMsg];
    [mStr appendString:@"\n"];
    //    _appleIDLoginInfoTextView.text = [mStr copy];
    
    if (errorMsg) {
        return;
    }
    
    if (error.localizedDescription) {
        NSMutableString *mStr = [[NSMutableString alloc]init];
        [mStr appendString:error.localizedDescription];
        [mStr appendString:@"\n"];
        //        _appleIDLoginInfoTextView.text = [mStr copy];
    }
    NSLog(@"controller requests：%@", controller.authorizationRequests);
    /*
     ((ASAuthorizationAppleIDRequest *)(controller.authorizationRequests[0])).requestedScopes
     <__NSArrayI 0x2821e2520>(
     full_name,
     email
     )
     */
}

//! Tells the delegate from which window it should present content to the user.
- (ASPresentationAnchor)presentationAnchorForAuthorizationController:(ASAuthorizationController *)controller  API_AVAILABLE(ios(13.0)){
    
    NSLog(@"window：%s", __FUNCTION__);
    return self.view.window;
}

- (void)dealloc {
    
    if (@available(iOS 13.0, *)) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:ASAuthorizationAppleIDProviderCredentialRevokedNotification object:nil];
    }
}

//----------
- (void) settingLanguage
{
    NSLog(@"settingLanguage");
    
    self.tfUsername.placeholder = VTCLocalizeString(@"Username2");
    self.tfPassword.placeholder = VTCLocalizeString(@"Password");
    self.lblSavePassword.text = VTCLocalizeString(@"Remember");
    [self.btnFacebook setTitle:VTCLocalizeString(@"Login via Facebook") forState:UIControlStateNormal];
    [self.btnGoogle setTitle:VTCLocalizeString(@"Login via Google") forState:UIControlStateNormal];
    //    NSLog(@"%@",VTCLocalizeString(@"Forgot password"));
    
    //    [self.forgotPasswordPortrait setAttributedTitle:[[NSAttributedString alloc] initWithString:VTCLocalizeString(@"Forgot password") attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}] forState:UIControlStateNormal];
    //
    //    [self.forgotPasswordLandscape setAttributedTitle:[[NSAttributedString alloc] initWithString:VTCLocalizeString(@"Forgot password") attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}] forState:UIControlStateNormal];
    
    [self.btnChangePass setAttributedTitle:[[NSAttributedString alloc] initWithString:VTCLocalizeString(@"Change password") attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}] forState:UIControlStateNormal];
    //    [_btnSignIn setTitle:VTCLocalizeString(@"Login via VTC") forState:UIControlStateNormal];
    //    [_btnQuickStart setTitle:VTCLocalizeString(@"QuickStart") forState:UIControlStateNormal];
    
    self.orLabel.text = VTCLocalizeString(@"Or");
    NSString *registerString = VTCLocalizeString(@"You haven't got VTC account? Register now");
    if ([registerString containsString:@"Đăng ký tại đây"])
    {
        NSMutableAttributedString *mRegister = [[NSMutableAttributedString alloc] initWithString:registerString attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
        
        [mRegister addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:11.0/255.0 green:185.0/255.0 blue:250.0/255.0 alpha:1.0] range:[registerString rangeOfString:@"Đăng ký tại đây"]];
        [self.btnRegister setAttributedTitle:mRegister forState:UIControlStateNormal];
    }
    else
    {
        NSMutableAttributedString *mRegister = [[NSMutableAttributedString alloc] initWithString:registerString attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
        [mRegister addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:11.0/255.0 green:185.0/255.0 blue:250.0/255.0 alpha:1.0] range:[registerString rangeOfString:@"Register here"]];
        [self.btnRegister setAttributedTitle:mRegister forState:UIControlStateNormal];
    }
}

- (void)configUIWithLanguage:(NSString *)lang animated:(BOOL)animated
{
    NSLog(@"configUIWithLanguage: %@",lang);
    if ([lang isEqualToString:@"eng"])
    {
        self.btnLangEn.selected = YES;
        self.btnLangVi.selected = NO;
        if (animated)
        {
            self.leadingConstraintViewHover.constant = 0;
        }
        else
        {
            self.leadingConstraintViewHover.constant = 0;
        }
    }else if ([lang isEqualToString:@"khm"])
    {
        self.btnLangEn.selected = YES;
        self.btnLangVi.selected = NO;
        if (animated)
        {
            self.leadingConstraintViewHover.constant = 0;
        }
        else
        {
            self.leadingConstraintViewHover.constant = 0;
        }
    }else
    {
        self.btnLangEn.selected = NO;
        self.btnLangVi.selected = YES;
        if (animated)
        {
            self.leadingConstraintViewHover.constant = 35;
        }
        else
        {
            self.leadingConstraintViewHover.constant = 35;
        }
    }
    [[VtcConfig defaultConfig] setLanguage:lang];
}

- (void)configUI {
    
    if (![[UIDevice currentDevice].model containsString:@"iPad"]) { // not iPad
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        
        if (UIInterfaceOrientationIsPortrait(orientation)) {
            _forgotPasswordPortrait.hidden = NO;
            _forgotPasswordLandscape.hidden = YES;
            
            _topSpaceFromUsernameToLogo.constant = self.portraitTopSpace;
            _topSpaceFromVersionLabelToCenter.constant = 186.0;
            
            CGRect frame = _logo.frame;
            frame.size.height = 45;
            frame.size.width = 45;
            _logo.frame = frame;
        }
        else {
            
            _forgotPasswordPortrait.hidden = YES;
            _forgotPasswordLandscape.hidden = NO;
            [scrollContentView setConstraintConstant:500 forAttribute:NSLayoutAttributeWidth];
            _topSpaceFromUsernameToLogo.constant = self.portraitTopSpace;
            _topSpaceFromVersionLabelToCenter.constant = 46.0;
            // not show label "Hoặc"
            _orLabel.hidden = YES;
            
            CGRect frame = _logo.frame;
            frame.size.height = 20;
            frame.size.width = 20;
            _logo.frame = frame;
        }
    }else{
        //iPad:
        [scrollContentView setConstraintConstant:500 forAttribute:NSLayoutAttributeWidth];
    }
    
    self.navigationController.navigationBar.translucent = NO;
    //    //NSLog(@"testahihi scroll to top");
    
    //    CGPoint newPoint = CGPointMake(0, 50);
    //    [self.mainscrollview setContentOffset:newPoint animated:NO];
    //Set Language
    
    [self reconfigOpenIDAuthen];
}

- (void)reconfigOpenIDAuthen {
    if (![[UIDevice currentDevice].model containsString:@"iPad"]) { // not iPad
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        
        //        if (UIInterfaceOrientationIsPortrait(orientation)) {
        _btnFacebook.hidden = ![Global sharedInstance].enableFacebookLogin;
        _btnGoogle.hidden = ![Global sharedInstance].enableGoogleLogin;
        //            _btnGoogleLandscape.hidden = YES;
        //            _btnFacebookLandscape.hidden = YES;
        //            _btnAppleLandscape.hidden = YES;
        _viewForLandscape.hidden = YES;
        // set hidden of label "Hoặc"
        //            if (_btnFacebook.hidden && _btnGoogle.hidden) {
        //                _orLabel.hidden = YES;
        //            }
        //            else {
        //                _orLabel.hidden = NO;
        //            }
        //        }
        //        else {
        ////            NSLog(@"Vao day roi nayyyyy");
        //            _btnGoogle.hidden = YES;
        //            _btnFacebook.hidden = YES;
        //            _viewForAppleSignIn.hidden = YES;
        //            _viewForLandscape.hidden = NO;
        //
        //            _btnGoogleLandscape.hidden = ![Global sharedInstance].enableGoogleLogin;
        //            [_btnGoogleLandscape setConstraintConsttrant:_btnGoogleLandscape.hidden ? 0.0 : kWidthOfLandscapeSocialLoginButton forAttribute:NSLayoutAttributeWidth];
        //            _btnFacebookLandscape.hidden = ![Global sharedInstance].enableFacebookLogin;
        //            [_btnFacebookLandscape setConstraintConstant:_btnFacebookLandscape.hidden ? 0.0 : kWidthOfLandscapeSocialLoginButton forAttribute:NSLayoutAttributeWidth];
        //        }
    }
    else { // iPad
        _viewForLandscape.hidden = YES;
        _btnFacebook.hidden = ![Global sharedInstance].enableFacebookLogin;
        _btnGoogle.hidden = ![Global sharedInstance].enableGoogleLogin;
        // set hidden of label "Hoặc"
        //        if (_btnFacebook.hidden && _btnGoogle.hidden) {
        //            _orLabel.hidden = YES;
        //        }
        //        else {
        //            _orLabel.hidden = NO;
        //        }
    }
    
    if (@available(iOS 13.0, *)) {
        
    }else{
        //an nut Sigin With Apple tren IOS cu:
        //NSLog(@"testahihi an nut Sigin With Apple tren IOS cu");
        _btnAppleLandscape.hidden = YES;
        _viewForAppleSignIn.hidden = YES;
        
        [_btnAppleLandscape setConstraintConstant:0.0 forAttribute:NSLayoutAttributeWidth];
        [_viewForAppleSignIn setConstraintConstant:0.0 forAttribute:NSLayoutAttributeWidth];
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //Test dữ liệu
    //    //NSLog(@"testahihi Your username is %@", _registeringName);
    //    //NSLog(@"testahihi Your password is %@", _registeringPassword);
    //Luồng đăng nhập mới
    //    _tfUsername.text = _registeringName;
    //Luồng đăng nhập mới
    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultCachedUser];
    if (_registeringName){
        _tfUsername.text = _registeringName;
    }else if (userInfo){
        _tfUsername.text = [SmartParser stringForKey:@"accountName" from:userInfo];
    }
    if (_registeringPassword) {
        _tfPassword.text = _registeringPassword;
        //        [Utilities showNotification:VTCLocalizeString(@"Register successfully")];
    } else {
        if (userInfo) {
            NSString *savedPassword = [SmartParser stringForKey:@"password" from:userInfo];
            if (savedPassword) {
                if (![savedPassword isEqualToString:@""]) {
                    _savePassword.selected = YES;
                }
                _tfPassword.text = savedPassword;
            }
        }
    }
    //
    
    [self configUIWithLanguage:[[VtcConfig defaultConfig] sdkLanguage] animated:NO];
    [self settingLanguage];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reconfigOpenIDAuthen) name:kNotificationGotFacebookGoogleAuthenConfig object:nil];
    /// Add Orientation notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationStatusDidChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
    // reset UI
    [self orientationStatusDidChange:nil];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.navigationController) {
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationGotFacebookGoogleAuthenConfig object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}

- (void)orientationStatusDidChange:(NSNotification *)notification {
    [self configUI];
}

- (BOOL)shouldAutorotate {
    //    return NO;
    return [SDKManager defaultManager].allowRotationInLoginView;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    //NSLog(@"testahihi loginViewOrientationMask:%lu",(unsigned long)[SDKManager defaultManager].loginViewOrientationMask);
    return [SDKManager defaultManager].loginViewOrientationMask;
}

- (IBAction)skipBtnClicked:(id)sender {
    NSLog(@"OK clicked");
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD show];
    
    [[NetworkModal sharedModal] quickStartGame:@"" password:@"" completion:^(BOOL status, id responsedObject, NSError *error) {
        [(VtcRoundedButton *)sender setEnabled:YES];
        [SVProgressHUD dismiss];
        if (status) {
            NSLog(@"login success");
            [VIDUser currentUser].signedIn = YES;
            [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeQuickStart containerController:self.navigationController showWelcome:YES];
            
            [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeLoginVTC]];
        }
        else {
            if (error.code == -500) {
                VtcEnterUsernameViewController *controller = [VtcEnterUsernameViewController loadFromNibNamed:@"VtcEnterUsernameViewController"];
                controller.authenType = AuthenTypeQuickStart;
                
                [self.navigationController pushViewController:controller animated:NO];
            }
            else {
                [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
            }
        }
    }];
    //                            [alert dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)otherLoginMethodClicked:(id)sender {
    VtcAuthenNavController *navController = (VtcAuthenNavController *)self.navigationController;
    VtcLoginViewController *controller = [VtcLoginViewController loadFromNibNamed:@"VtcLoginViewController"];
    [navController changeRootController:controller];
}

- (IBAction)closeButtonTapped:(id)sender {
    NSLog(@"Close Button Tapped");
    [self.navigationController dismissViewControllerAnimated:NO completion:^{
        [SDKManager defaultManager].showingLoginView = NO;
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onSavePassword:(UIButton *)sender {
    sender.selected = !sender.selected;
}

- (IBAction)quickStart:(id)sender {
    NSLog(@"quickStart");
    
    UIAlertController * alert =  [UIAlertController
                                  alertControllerWithTitle:@"Chú ý"
                                  message:@"Dữ liệu gắn với tài khoản Chơi Ngay sẽ bị xóa khi bạn xóa game hoặc cài lại máy. Hãy lưu ý khi sử dụng hình thức đăng nhập này. Để an toàn khi đăng nhập hãy ưu tiên đăng nhập bằng VTC ID và Apple ID"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Đồng ý"
                         style:UIAlertActionStyleDestructive
                         handler:^(UIAlertAction * action)
                         {
        NSLog(@"OK clicked");
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
        [SVProgressHUD show];
        
        [[NetworkModal sharedModal] quickStartGame:@"" password:@"" completion:^(BOOL status, id responsedObject, NSError *error) {
            [(VtcRoundedButton *)sender setEnabled:YES];
            [SVProgressHUD dismiss];
            if (status) {
                NSLog(@"login success");
                [VIDUser currentUser].signedIn = YES;
                [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeQuickStart containerController:self.navigationController showWelcome:YES];
                
                [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeLoginVTC]];
            }
            else {
                if (error.code == -500) {
                    VtcEnterUsernameViewController *controller = [VtcEnterUsernameViewController loadFromNibNamed:@"VtcEnterUsernameViewController"];
                    controller.authenType = AuthenTypeQuickStart;
                    
                    [self.navigationController pushViewController:controller animated:NO];
                }
                else {
                    [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
                }
            }
        }];
        //                            [alert dismissViewControllerAnimated:NO completion:nil];
        
    }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Hủy"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
        NSLog(@"Cancel clicked");
        //                                [alert dismissViewControllerAnimated:NO completion:nil];
        
    }];
    
    [alert addAction:ok];
    
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:NO completion:nil];
    
    return;
    
}

- (IBAction)onSignIn:(id)sender {
    
    //luong moi
    VtcAuthenNavController *navController = (VtcAuthenNavController *)self.navigationController;
    VtcAuthenViewController *controller = [VtcAuthenViewController loadFromNibNamed:@"VtcAuthenViewController"];
    [navController changeRootController:controller];
    
    //luong cu
    //    if ([_tfUsername.text isEqualToString:@""]) {
    //        [Utilities showNotification:VTCLocalizeString(@"Missing username")];
    //        [_tfUsername becomeFirstResponder];
    //        return;
    //    }
    //    if ([_tfPassword.text isEqualToString:@""]) {
    //        [Utilities showNotification:VTCLocalizeString(@"Missing password")];
    //        [_tfPassword becomeFirstResponder];
    //        return;
    //    }
    //    if (_savePassword.selected) {
    //        [VIDUser currentUser].password = _tfPassword.text;
    //        [VIDUser currentUser].savePassword = YES;
    //    }
    //    else {
    //        [VIDUser currentUser].password = nil;
    //        [VIDUser currentUser].savePassword = NO;
    //    }
    //    [(VtcRoundedButton *)sender setEnabled:NO];
    //    NSString *userName = [_tfUsername.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    //    [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
    //    [[NetworkModal sharedModal] loginWithVTCID:userName password:_tfPassword.text completion:^(BOOL status, id responsedObject, NSError *error) {
    //        [(VtcRoundedButton *)sender setEnabled:YES];
    //        [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
    //        if (status) {
    //            NSLog(@"login success");
    //            [VIDUser currentUser].signedIn = YES;
    //            [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeNormal containerController:self.navigationController showWelcome:YES];
    //
    //            [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeLoginVTC]];
    //        }
    //        else {
    //            if (error.code == -1000) {
    //                NSLog(@"login with code -1000");
    //                VtcOTPViewController *controller = [VtcOTPViewController loadFromNibNamed:@"VtcOTPViewController"];
    //                controller.username = userName;
    //                controller.password = _tfPassword.text;
    //                [self.navigationController pushViewController:controller animated:NO];
    //            }
    //            else {
    //                NSLog(@"login failure");
    //                if ([error.userInfo objectForKey:@"message"] == nil)
    //                {
    //                    [Utilities showNotification:VTCLocalizeString(@"Login failed. Please try again")];
    //                }
    //                else
    //                {
    //                    [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
    //                }
    ////                [Utilities showNotification:VTCLocalizeString(@"Login failed. Please try again")];
    ////                if ([[[VtcConfig defaultConfig] sdkLanguage] isEqualToString:@"vie"])
    ////                {
    //
    ////                }else
    ////                {
    ////                    [Utilities showNotification:@"Login failure"];
    ////                }
    //
    //            }
    //        }
    //    }];
}

- (IBAction)onFacebook:(id)sender {
    [(VtcRoundedButton *)sender setEnabled:NO];
    [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
    [[FacebookHandler sharedHandler] signInfromViewController:self completion:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
        if (error) {
            // error
            [(VtcRoundedButton *)sender setEnabled:YES];
            NSLog(@"%@",error);
            [Utilities showNotification:error.localizedDescription];
        } else if (result.isCancelled) {
            // cancel
            NSLog(@"cancel");
            [(VtcRoundedButton *)sender setEnabled:YES];
        } else {
            [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
            VTCLog(@"Facebook token: %@", [[FBSDKAccessToken currentAccessToken] tokenString]);
            [[NetworkModal sharedModal] loginWithFacebook:[[FBSDKAccessToken currentAccessToken] tokenString] completion:^(BOOL status, id responsedObject, NSError *error) {
                [(VtcRoundedButton *)sender setEnabled:YES];
                [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
                if (status) {
                    [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeFacebook containerController:self.navigationController showWelcome:YES];
                    [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeLoginFacebook]];
                }
                else {
                    //                    NSLog(@"%@",error.code);
                    if (error.code == -500) {
                        VtcEnterUsernameViewController *controller = [VtcEnterUsernameViewController loadFromNibNamed:@"VtcEnterUsernameViewController"];
                        controller.authenType = AuthenTypeFacebook;
                        [self.navigationController pushViewController:controller animated:NO];
                    }
                    if (error.code == -1000) {
                        VtcOTPViewController *controller = [VtcOTPViewController loadFromNibNamed:@"VtcOTPViewController"];
                        controller.socialToken = [[FBSDKAccessToken currentAccessToken] tokenString];
                        controller.loginType = LoginTypeFacebook;
                        [self.navigationController pushViewController:controller animated:NO];
                    }
                    else {
                        [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
                    }
                }
            }];
        }
    }];
}

- (IBAction)onGoogle:(id)sender {
    [GoogleServiceHandler sharedHandler].loginController = self;
    [(VtcRoundedButton *)sender setEnabled:NO];
    [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
    [[GoogleServiceHandler sharedHandler] signIn:^(GIDGoogleUser *user, NSError *error) {
        [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
        if (error) {
            [(VtcRoundedButton *)sender setEnabled:YES];
            [Utilities showNotification:error.localizedDescription];
            return;
        }
        else if (user) {
            // Perform any operations on signed in user here.
            NSString *accessToken = user.authentication.accessToken; // Safe to send to the server
            [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
            [[NetworkModal sharedModal] loginWithGoogle:accessToken completion:^(BOOL status, id responsedObject, NSError *error) {
                [(VtcRoundedButton *)sender setEnabled:YES];
                [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
                if (status) {
                    [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeGoogle containerController:self.navigationController showWelcome:YES];
                    [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeLoginGoogle]];
                }
                else {
                    if (error.code == -500) {
                        VtcEnterUsernameViewController *controller = [VtcEnterUsernameViewController loadFromNibNamed:@"VtcEnterUsernameViewController"];
                        controller.authenType = AuthenTypeGoogle;
                        controller.openIDTokenString = accessToken;
                        [self.navigationController pushViewController:controller animated:NO];
                    }
                    if (error.code == -1000) {
                        VtcOTPViewController *controller = [VtcOTPViewController loadFromNibNamed:@"VtcOTPViewController"];
                        controller.socialToken = accessToken;
                        controller.loginType = LoginTypeGoogle;
                        [self.navigationController pushViewController:controller animated:NO];
                    }
                    else {
                        [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
                    }
                }
            }];
        }
        else {
            [(VtcRoundedButton *)sender setEnabled:YES];
        }
    }];
}

- (IBAction)onSignUp:(id)sender {
    
    //NSLog(@"testahihi onSignUp");
    VtcAuthenNavController *navController = (VtcAuthenNavController *)self.navigationController;
    VtcSignUpViewController *controller = [VtcSignUpViewController loadFromNibNamed:@"VtcSignUpViewController"];
    
    [navController setModalPresentationStyle:UIModalPresentationFormSheet];
    
    [navController changeRootController:controller];
    //    [navController pushViewController:controller animated:true];
}

- (IBAction)onForgotPassword:(id)sender {
    VtcForgotPasswordViewController *controller = [VtcForgotPasswordViewController loadFromNibNamed:@"VtcForgotPasswordViewController"];
    [self.navigationController pushViewController:controller animated:NO];
}

- (IBAction)onChangePassword:(id)sender {
    VtcChangePasswordViewController *controller = [VtcChangePasswordViewController loadFromNibNamed:@"VtcChangePasswordViewController"];
    [self.navigationController pushViewController:controller animated:NO];
}
- (IBAction)btnLanguageTouchUpInside:(UIButton *)sender {
    if (sender.isSelected)
    {
        return;
    }
    if (sender == self.btnLangEn)
    {
        [self configUIWithLanguage:@"eng" animated:NO];
        
    }else
    {
        [self configUIWithLanguage:@"vie" animated:NO];
    }
    [self settingLanguage];
}

// MARK: UITextField Delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.activeTextField = textField;
    return YES;
}

@end

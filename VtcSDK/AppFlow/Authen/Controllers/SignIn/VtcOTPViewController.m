//
//  VtcOTPViewController.m
//  VtcSDK
//
//  Created by Kent Vu on 8/3/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcOTPViewController.h"
#import "VtcLoginTextField.h"
#import "VtcRoundedButton.h"

@interface VtcOTPViewController () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfSelectMethod;
@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfOTP;
@property (nonatomic, weak) IBOutlet VtcRoundedButton *btnSignIn;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (nonatomic, weak) IBOutlet UILabel *lbText1;
@property (nonatomic, weak) IBOutlet UILabel *lbText2;

@end

@implementation VtcOTPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.portraitTopSpace = [Global sharedInstance].gPortraitNavBarHeight+50.0;
    self.landscapeTopSpace = [Global sharedInstance].gLandscapeNavBarHeight+35.0;
    [self addCustomBackButton];
    [self addCustomTitleView];
    [self updateTextForLabel];
    
    // default
    _tfSelectMethod.text = @"OTP SMS";
//    _tfOTP.text = @"123456";
    _tfOTP.delegate = self;
    [_tfOTP setHandleDidEndOnExit:^{
        [self onContinue:_btnSignIn];
    }];
}

- (void)updateTextForLabel {
    _lbText1.text = @"";
    _lbText2.text = @"";
    [[NetworkModal sharedModal] getOTPText:^(BOOL status, id responsedObject, NSError *error) {
        if (status) {
            @try {
                if ([[[VtcConfig defaultConfig] sdkLanguage] isEqualToString:@"vie"])
                {
                    NSString *htmlString = [[responsedObject firstObject] objectForKey:@"configValue"];
                    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUTF8StringEncoding]
                                                                                            options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                                                 documentAttributes:nil
                                                                                              error:nil];
                    _lbText1.attributedText = attributedString;
                }
                else
                {
                    NSString *htmlString = [[responsedObject lastObject] objectForKey:@"configValue"];
                    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUTF8StringEncoding]
                                                                                            options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                                                 documentAttributes:nil
                                                                                              error:nil];
                    _lbText1.attributedText = attributedString;
                }
                
            } @catch (NSException *exception) {
                VTCLog(@"Data không hợp lệ");
            } @finally {
                
            }
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [self settingLanguage];
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void) settingLanguage
{
    self.lblTitle.text = [VTCLocalizeString(@"Account security") uppercaseString];
    self.tfOTP.placeholder = VTCLocalizeString(@"OTP Code");
    [self.btnSignIn setTitle:[VTCLocalizeString(@"Login") uppercaseString] forState:UIControlStateNormal];
}

- (IBAction)onContinue:(id)sender {
    if ([_tfOTP.text isEqualToString:@""]) {
        [_tfOTP becomeFirstResponder];
        [Utilities showNotification:VTCLocalizeString(@"Missing OTP code")];
        return;
    }
    
    SecureType type;
    if ([_tfSelectMethod.text isEqualToString:@"OTP SMS"]) {
        type = SecureTypeSMS;
    }
    else {
        type = SecureTypeApp;
    }
    
    NSAssert(_username, @"did not inspire username");
    NSAssert(_password, @"did not inspire password");
    
    [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
    
    if (_socialToken) {
        [[NetworkModal sharedModal] verifySocialAccount:_socialToken type:_loginType secureCode:_tfOTP.text secureType:type completion:^(BOOL status, id responsedObject, NSError *error) {
            [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
            if (status) {
                [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeNormal containerController:self.navigationController showWelcome:YES];
            }
            else {
                [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
            }
        }];
    }
    else {
        [[NetworkModal sharedModal] verifyAccount:_username password:_password secureCode:_tfOTP.text secureType:type completion:^(BOOL status, id responsedObject, NSError *error) {
            [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
            if (status) {
                [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeNormal containerController:self.navigationController showWelcome:YES];
            }
            else {
                [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
            }
        }];
    }
    
}

- (IBAction)onOpenSMS:(id)sender {
    
}

- (IBAction)onOpenApp:(id)sender {
    
}

// MARK: UITextField Delegates
- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    // Prevent crashing undo bug – see note below.
    //    NSLog(@"testahihi shouldChangeCharactersInRange");
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 6;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == _tfSelectMethod) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:VTCLocalizeString(@"Choose OTP method") preferredStyle:UIAlertControllerStyleActionSheet];
        [alert addAction:[UIAlertAction actionWithTitle:@"SMS" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            _tfSelectMethod.text = @"OTP SMS";
            [alert dismissViewControllerAnimated:NO completion:nil];
            
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"APP" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            _tfSelectMethod.text = @"OTP APP";
            [alert dismissViewControllerAnimated:NO completion:nil];
            
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:VTCLocalizeString(@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [alert dismissViewControllerAnimated:NO completion:nil];
            
        }]];
        
        if ([[UIDevice currentDevice].model containsString:@"iPad"]) {
            [alert setModalPresentationStyle:UIModalPresentationPopover];
            [alert.view layoutIfNeeded];
            alert.popoverPresentationController.sourceView = containerView;
            alert.popoverPresentationController.sourceRect = textField.frame;
        }
        [self presentViewController:alert animated:NO completion:nil];
        return NO;
    }
    self.activeTextField = textField;
    return YES;
}

@end

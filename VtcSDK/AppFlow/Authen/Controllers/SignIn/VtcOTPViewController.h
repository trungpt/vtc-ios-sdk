//
//  VtcOTPViewController.h
//  VtcSDK
//
//  Created by Kent Vu on 8/3/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcAuthenBaseViewController.h"
#import "NetworkModal+Authen.h"

@interface VtcOTPViewController : VtcAuthenBaseViewController

@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *socialToken;
@property (nonatomic) LoginType loginType;

@end

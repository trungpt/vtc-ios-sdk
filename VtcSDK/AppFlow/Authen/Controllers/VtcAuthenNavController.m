//
//  VtcAuthenNavController.m
//  VtcSDK
//
//  Created by Kent Vu on 7/25/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcAuthenNavController.h"
#import "UIImage+Additions.h"
#import "SDKManager.h"

@interface VtcAuthenNavController ()

@end

@implementation VtcAuthenNavController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavigationBarHidden:YES animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    return [SDKManager defaultManager].allowRotationInLoginView;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
//    if ([SDKManager defaultManager].allowRotationInLoginView) {
//        return UIInterfaceOrientationMaskAll;
//    }
//    else {
        return [SDKManager defaultManager].loginViewOrientationMask;
//    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

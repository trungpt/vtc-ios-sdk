//
//  VtcChangePasswordViewController.m
//  VtcSDK
//
//  Created by Kent Vu on 8/4/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcChangePasswordViewController.h"
#import "VtcLoginTextField.h"
#import "UIImage+Additions.h"
#import "NetworkModal+Authen.h"
#import "VtcRoundedButton.h"

@interface VtcChangePasswordViewController () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfUsername;
@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfCurrentPassword;
@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfNewPassword;
@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfNewPasswordConfirm;
@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfOTPMethod;
@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfOTP;

@property (nonatomic, weak) IBOutlet VtcRoundedButton *btnDone;

@end

@implementation VtcChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.portraitTopSpace = [Global sharedInstance].gPortraitNavBarHeight+50.0;
    self.landscapeTopSpace = [Global sharedInstance].gLandscapeNavBarHeight+35.0;
    [self addCustomBackButton];
    [self addCustomTitleView];
    
    _tfOTPMethod.text = @"OTP SMS";
    
    [_tfOTP setHandleDidEndOnExit:^{
        [self onConfirm:_btnDone];
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// MARK: methods

- (IBAction)onConfirm:(id)sender {
    if ([_tfUsername.text isEqualToString:@""]) {
        [Utilities showNotification:@"Vui lòng nhập tên tài khoản"];
        [_tfUsername becomeFirstResponder];
        return;
    }
    if ([_tfCurrentPassword.text isEqualToString:@""]) {
        [Utilities showNotification:@"Vui lòng nhập mật khẩu hiện tại"];
        [_tfCurrentPassword becomeFirstResponder];
        return;
    }
    if ([_tfNewPassword.text isEqualToString:@""]) {
        [Utilities showNotification:@"Vui lòng nhập mật khẩu mới"];
        [_tfNewPassword becomeFirstResponder];
        return;
    }
    if ([_tfNewPasswordConfirm.text isEqualToString:@""]) {
        [Utilities showNotification:@"Vui lòng nhập lại mật khẩu mới"];
        [_tfNewPasswordConfirm becomeFirstResponder];
        return;
    }
    if (![_tfNewPassword.text isEqualToString:_tfNewPasswordConfirm.text]) {
        [Utilities showNotification:@"Nhập lại mật khẩu mới không đúng"];
        [_tfNewPasswordConfirm becomeFirstResponder];
        return;
    }
    if ([_tfOTP.text isEqualToString:@""]) {
        [Utilities showNotification:@"Vui lòng nhập mã xác thực"];
        [_tfOTP becomeFirstResponder];
        return;
    }
    SecureType type;
    if ([_tfOTPMethod.text isEqualToString:@"OTP SMS"]) {
        type = SecureTypeSMS;
    }
    else {
        type = SecureTypeApp;
    }
    
    [[NetworkModal sharedModal] changePassword:_tfUsername.text oldPassword:_tfCurrentPassword.text newPassword:_tfNewPassword.text secureCode:_tfOTP.text secureType:type completion:^(BOOL status, id responsedObject, NSError *error) {
        if (status) {
            [Utilities showNotification:@"Đổi mật khẩu thành công"];
            [self.navigationController popToRootViewControllerAnimated:NO];
        }
        else {
            [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
        }
    }];
}

// MARK: UITextField Delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == _tfOTPMethod) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Chọn phương thức nhận OTP" preferredStyle:UIAlertControllerStyleActionSheet];
        [alert addAction:[UIAlertAction actionWithTitle:@"SMS" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            _tfOTPMethod.text = @"OTP SMS";
            [alert dismissViewControllerAnimated:NO completion:nil];
            
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"APP" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            _tfOTPMethod.text = @"OTP APP";
            [alert dismissViewControllerAnimated:NO completion:nil];
            
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"Hủy" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [alert dismissViewControllerAnimated:NO completion:nil];
            
        }]];
        
        if ([[UIDevice currentDevice].model containsString:@"iPad"]) {
            [alert setModalPresentationStyle:UIModalPresentationPopover];
            [alert.view layoutIfNeeded];
            alert.popoverPresentationController.sourceView = containerView;
            alert.popoverPresentationController.sourceRect = textField.frame;
        }
        [self presentViewController:alert animated:NO completion:nil];
        return NO;
    }
    
    self.activeTextField = textField;
    return YES;
}

@end

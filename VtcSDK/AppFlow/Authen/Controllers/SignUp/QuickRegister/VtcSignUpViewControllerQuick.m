//
//  VtcSignUpViewController.m
//  VtcSDK
//
//  Created by Kent Vu on 7/26/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcSignUpViewControllerQuick.h"
#import "VtcLoginTextField.h"
#import "UIImage+Additions.h"
#import "NetworkModal+Authen.h"
#import "FacebookHandler.h"
#import "VtcEnterUsernameViewController.h"
#import "UIViewController+Additions.h"
#import "GoogleServiceHandler.h"
#import "VtcAuthenNavController.h"
#import "VtcLoginViewController.h"
#import "VtcRoundedButton.h"
#import "UIView+UpdateAutoLayoutConstraints.h"
#import "SDKManager.h"
#import "SDKManager+Internal.h"

@interface VtcSignUpViewControllerQuick () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfUsername;
@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfPassword;
@property (nonatomic, weak) IBOutlet VtcRoundedButton *btnSignUp;
@property (nonatomic, weak) IBOutlet VtcRoundedButton *btnFacebook;
@property (nonatomic, weak) IBOutlet VtcRoundedButton *btnGoogle;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;

@property (nonatomic, copy) NSString *registeringName;
@property (nonatomic, copy) NSString *registeringPassword;

@property (nonatomic, weak) IBOutlet UILabel *orLabel;
@property (nonatomic, weak) IBOutlet UILabel *lbGameVersion;
@property (nonatomic, weak) IBOutlet UILabel *lbSDKVersion;

@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@property (nonatomic, weak) IBOutlet UIButton *btnFacebookLandscape;
@property (nonatomic, weak) IBOutlet UIButton *btnGoogleLandscape;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *topSpaceFromUsernameToLogo;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *topSpaceFromVersionLabelToCenter;

@end

@implementation VtcSignUpViewControllerQuick

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([SDKManager defaultManager].isShowCloseButtonInAuthenVC) {
        [self.closeButton setHidden:false];
    }
    
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.portraitTopSpace = 80.0;
    self.landscapeTopSpace = 8.0; //30.0;
    
    _tfUsername.clearButtonMode = UITextFieldViewModeWhileEditing;
//    _tfUsername.text = @"testsdk";
//    _tfPassword.text = @"Test123";
    
    [_tfPassword setHandleDidEndOnExit:^{
        [self onSignUp:_btnSignUp];
    }];
    
    _lbGameVersion.text = [NSString stringWithFormat:@"game: v%@", [[VtcConfig defaultConfig] gameVersion]];
    NSString *sdkVerString = [NSString stringWithFormat:@"sdk: v%@", VtcSDKVersionString];
    if ([NetworkModal sharedModal].isDevMode) {
        sdkVerString = [sdkVerString stringByAppendingString:@" (Dev)"];
    }
    _lbSDKVersion.text = sdkVerString;
    
    [self configUI];
}

- (void)configUI {
    if (![[UIDevice currentDevice].model containsString:@"iPad"]) { // not iPad
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        
        if (UIInterfaceOrientationIsPortrait(orientation)) {
            _topSpaceFromUsernameToLogo.constant = 40.0;
            _topSpaceFromVersionLabelToCenter.constant = 237.0;
        }
        else {
            _topSpaceFromUsernameToLogo.constant = 10.0;
            _topSpaceFromVersionLabelToCenter.constant = 46.0;
            // not show label "Hoặc"
            _orLabel.hidden = YES;
        }
    }
    
    [self reconfigOpenIDAuthen];
}

- (void)reconfigOpenIDAuthen {
    if (![[UIDevice currentDevice].model containsString:@"iPad"]) { // not iPad
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        
        if (UIInterfaceOrientationIsPortrait(orientation)) {
            _btnFacebook.hidden = ![Global sharedInstance].enableFacebookLogin;
            _btnGoogle.hidden = ![Global sharedInstance].enableGoogleLogin;
            _btnGoogleLandscape.hidden = YES;
            _btnFacebookLandscape.hidden = YES;
            // set hidden of label "Hoặc"
            if (_btnFacebook.hidden && _btnGoogle.hidden) {
                _orLabel.hidden = YES;
            }
            else {
                _orLabel.hidden = NO;
            }
        }
        else {
            _btnGoogle.hidden = YES;
            _btnFacebook.hidden = YES;
            _btnGoogleLandscape.hidden = ![Global sharedInstance].enableGoogleLogin;
            [_btnGoogleLandscape setConstraintConstant:_btnGoogleLandscape.hidden ? 0.0 : kWidthOfLandscapeSocialLoginButton forAttribute:NSLayoutAttributeWidth];
            _btnFacebookLandscape.hidden = ![Global sharedInstance].enableFacebookLogin;
            [_btnFacebookLandscape setConstraintConstant:_btnFacebookLandscape.hidden ? 0.0 : kWidthOfLandscapeSocialLoginButton forAttribute:NSLayoutAttributeWidth];
        }
    }
    else { // iPad
        _btnFacebook.hidden = ![Global sharedInstance].enableFacebookLogin;
        _btnGoogle.hidden = ![Global sharedInstance].enableGoogleLogin;
        // set hidden of label "Hoặc"
        if (_btnFacebook.hidden && _btnGoogle.hidden) {
            _orLabel.hidden = YES;
        }
        else {
            _orLabel.hidden = NO;
        }
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self settingLanguage];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reconfigOpenIDAuthen) name:kNotificationGotFacebookGoogleAuthenConfig object:nil];
    /// Add Orientation notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationStatusDidChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
    // reset UI
    [self orientationStatusDidChange:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationGotFacebookGoogleAuthenConfig object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}
- (void) settingLanguage
{
    self.tfUsername.placeholder = VTCLocalizeString(@"Username2");
    self.tfPassword.placeholder = VTCLocalizeString(@"Password");
    [self.btnSignUp setTitle:[VTCLocalizeString(@"Register") uppercaseString] forState:UIControlStateNormal];
    self.orLabel.text = VTCLocalizeString(@"Or");
    [self.btnFacebook setTitle:VTCLocalizeString(@"Register via Facebook") forState:UIControlStateNormal];
    [self.btnGoogle setTitle:VTCLocalizeString(@"Register via Google") forState:UIControlStateNormal];
    NSString *loginString = VTCLocalizeString(@"You had got a VTC account? Login now");
    if ([loginString containsString:@"Đăng nhập tại đây"])
    {
        NSMutableAttributedString *mRegister = [[NSMutableAttributedString alloc] initWithString:loginString attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
        [mRegister addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:11.0/255.0 green:185.0/255.0 blue:250.0/255.0 alpha:1.0] range:[loginString rangeOfString:@"Đăng nhập tại đây"]];
        [self.btnLogin setAttributedTitle:mRegister forState:UIControlStateNormal];
    }
    else
    {
        NSMutableAttributedString *mRegister = [[NSMutableAttributedString alloc] initWithString:loginString attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
        [mRegister addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:11.0/255.0 green:185.0/255.0 blue:250.0/255.0 alpha:1.0] range:[loginString rangeOfString:@"Login here"]];
        [self.btnLogin setAttributedTitle:mRegister forState:UIControlStateNormal];
    }
}

- (void)orientationStatusDidChange:(NSNotification *)notification {
    [self configUI];
}

- (IBAction)closeButtonTapped:(id)sender {
    NSLog(@"Close Button Tapped");
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        [SDKManager defaultManager].showingLoginView = NO;
    }];
}

- (IBAction)onSignUp:(id)sender {
    
    if ([_tfUsername.text isEqualToString:@""]) {
        [Utilities showNotification:VTCLocalizeString(@"Missing username")];
        [_tfUsername becomeFirstResponder];
        return;
    }
    if ([_tfPassword.text isEqualToString:@""]) {
        [Utilities showNotification:VTCLocalizeString(@"Missing password")];
        [_tfPassword becomeFirstResponder];
        return;
    }
    [(VtcRoundedButton *)sender setEnabled:NO];
    [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
    [[NetworkModal sharedModal] registerVTCID:_tfUsername.text password:_tfPassword.text completion:^(BOOL status, id responsedObject, NSError *error) {
        [(VtcRoundedButton *)sender setEnabled:YES];
        [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
        if (status) {
            // save the new registered user
            [[NSUserDefaults standardUserDefaults] setObject:_tfUsername.text forKey:kUserDefaultCachedNewRegisteredUser];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            // appsflyer logging
            [[VtcEventLogging manager] setAppsFlyerInAppEvent:kAppsFlyerEventTypeRegister values:[NSDictionary dictionaryWithObjectsAndKeys:@"1", kAppsFlyerEventTypeRegisterValueKey, nil]];
//            [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeNormal containerController:self.navigationController showWelcome:YES];
            _registeringName = _tfUsername.text;
            _registeringPassword = _tfPassword.text;
            [self onSignIn:nil];
            
            [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeOther]];
        }
        else {
            [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
        }
    }];
}

- (IBAction)onFacebook:(id)sender {
    if ([_tfUsername.text isEqualToString:@""]) {
        [self loginFacebook:sender];
        return;
    }
    [(VtcRoundedButton *)sender setEnabled:NO];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:VTCLocalizeString(@"Do you want to map account named %@ with Facebook?"), _tfUsername.text] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:VTCLocalizeString(@"Accept") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
        [[FacebookHandler sharedHandler] signInfromViewController:self completion:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            
            if (error) {
                [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
                [(VtcRoundedButton *)sender setEnabled:YES];
            } else if (result.isCancelled) {
                [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
                [(VtcRoundedButton *)sender setEnabled:YES];
            } else {
                VTCLog(@"Facebook token: %@", [[FBSDKAccessToken currentAccessToken] tokenString]);
                [[NetworkModal sharedModal] registerWithFacebook:[[FBSDKAccessToken currentAccessToken] tokenString] user:_tfUsername.text completion:^(BOOL status, id responsedObject, NSError *error) {
                    [(VtcRoundedButton *)sender setEnabled:YES];
                    [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
                    if (status) {
                        // appsflyer register With Facebook
                        [[VtcEventLogging manager] setAppsFlyerInAppEvent:kAppsFlyerEventTypeFBRegister values:[NSDictionary dictionaryWithObjectsAndKeys:@"1", kAppsFlyerEventTypeRegisterValueKey, nil]];
                        
                        [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeFacebook containerController:self.navigationController showWelcome:YES];
                        
                        [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeOther]];
                    }
                    else {
                        [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
                    }
                }];
            }
        }];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:VTCLocalizeString(@"No, thanks") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        UIAlertController *alert2 = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:VTCLocalizeString(@"Continue with Facebook?")] preferredStyle:UIAlertControllerStyleAlert];
        [alert2 addAction:[UIAlertAction actionWithTitle:VTCLocalizeString(@"Accept") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [(VtcRoundedButton *)sender setEnabled:YES];
            [self loginFacebook:sender];
        }]];
        [alert2 addAction:[UIAlertAction actionWithTitle:VTCLocalizeString(@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [(VtcRoundedButton *)sender setEnabled:YES];
            [alert2 dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alert2 animated:YES completion:nil];
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)loginFacebook:(id)sender {
    // login via facebook normally
    [(VtcRoundedButton *)sender setEnabled:NO];
    [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
    [[FacebookHandler sharedHandler] signInfromViewController:self completion:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        if (error) {
            [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
            [(VtcRoundedButton *)sender setEnabled:YES];
        } else if (result.isCancelled) {
            [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
            [(VtcRoundedButton *)sender setEnabled:YES];
        } else {
            VTCLog(@"Facebook token: %@", [[FBSDKAccessToken currentAccessToken] tokenString]);
            [[NetworkModal sharedModal] loginWithFacebook:[[FBSDKAccessToken currentAccessToken] tokenString] completion:^(BOOL status, id responsedObject, NSError *error) {
                [(VtcRoundedButton *)sender setEnabled:YES];
                [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
                if (status) {
                    [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeFacebook containerController:self.navigationController showWelcome:YES];
                    [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeOther]];

                }
                else {
                    if (error.code == -500) {
                        VtcEnterUsernameViewController *controller = [VtcEnterUsernameViewController loadFromNibNamed:@"VtcEnterUsernameViewController"];
                        controller.authenType = AuthenTypeFacebook;
                        [self.navigationController pushViewController:controller animated:YES];
                    }
                    else {
                        [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
                    }
                }
            }];
        }
    }];
}

- (IBAction)onGoogle:(id)sender {
    if ([_tfUsername.text isEqualToString:@""]) {
        [self loginGoogle:sender];
        return;
    }
    [(VtcRoundedButton *)sender setEnabled:NO];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:VTCLocalizeString(@"Do you want to map account named %@ with Google?"), _tfUsername.text] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:VTCLocalizeString(@"Accept") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
        [[GoogleServiceHandler sharedHandler] signIn:^(GIDGoogleUser *user, NSError *error) {
            
            if (error) {
                [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
                [(VtcRoundedButton *)sender setEnabled:YES];
                return;
            }
            else if (user) {
                // Perform any operations on signed in user here.
                NSString *accessToken = user.authentication.accessToken; // Safe to send to the server
                [[NetworkModal sharedModal] registerWithGoogle:accessToken user:_tfUsername.text completion:^(BOOL status, id responsedObject, NSError *error) {
                    [(VtcRoundedButton *)sender setEnabled:YES];
                    [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
                    if (status) {
                        // appsflyer register With Google
                        [[VtcEventLogging manager] setAppsFlyerInAppEvent:kAppsFlyerEventTypeGGRegister values:[NSDictionary dictionaryWithObjectsAndKeys:@"1", kAppsFlyerEventTypeRegisterValueKey, nil]];
                        
                        [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeGoogle containerController:self.navigationController showWelcome:YES];
                        
                        [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeOther]];
                    }
                    else {
                        [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
                    }
                }];
            }
            else {
                [(VtcRoundedButton *)sender setEnabled:YES];
            }
        }];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:VTCLocalizeString(@"No, thanks") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        UIAlertController *alert2 = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:VTCLocalizeString(@"Continue with Google?")] preferredStyle:UIAlertControllerStyleAlert];
        [alert2 addAction:[UIAlertAction actionWithTitle:VTCLocalizeString(@"Accept") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [(VtcRoundedButton *)sender setEnabled:YES];
            [self loginGoogle:sender];
        }]];
        [alert2 addAction:[UIAlertAction actionWithTitle:VTCLocalizeString(@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [(VtcRoundedButton *)sender setEnabled:YES];
            [alert2 dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alert2 animated:YES completion:nil];
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)loginGoogle:(id)sender {
    [GoogleServiceHandler sharedHandler].loginController = self;
    [(VtcRoundedButton *)sender setEnabled:NO];
    [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
    [[GoogleServiceHandler sharedHandler] signIn:^(GIDGoogleUser *user, NSError *error) {
        
        if (error) {
            [(VtcRoundedButton *)sender setEnabled:YES];
            [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
            return;
        }
        if (user) {
            // Perform any operations on signed in user here.
            NSString *accessToken = user.authentication.accessToken; // Safe to send to the server
            [[NetworkModal sharedModal] loginWithGoogle:accessToken completion:^(BOOL status, id responsedObject, NSError *error) {
                [(VtcRoundedButton *)sender setEnabled:YES];
                [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
                if (status) {
                    [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeGoogle containerController:self.navigationController showWelcome:YES];
                    [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeOther]];
                }
                else {
                    if (error.code == -500) {
                        VtcEnterUsernameViewController *controller = [VtcEnterUsernameViewController loadFromNibNamed:@"VtcEnterUsernameViewController"];
                        controller.authenType = AuthenTypeGoogle;
                        controller.openIDTokenString = accessToken;
                        [self.navigationController pushViewController:controller animated:YES];
                    }
                    else {
                        [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
                    }
                }
            }];
        }
        
    }];
}

- (IBAction)onSignIn:(id)sender {
    VtcAuthenNavController *navController = (VtcAuthenNavController *)self.navigationController;
    VtcLoginViewController *loginController = [VtcLoginViewController loadFromNibNamed:@"VtcLoginViewController"];
    if (_registeringName && _registeringPassword) {
        loginController.registeringName = _registeringName;
        loginController.registeringPassword = _registeringPassword;
    }
    
    [navController changeRootController:loginController];
}

// MARK: UITextField Delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.activeTextField = textField;
    return YES;
}

@end

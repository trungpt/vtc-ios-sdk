//
//  EnterPassword
//  VtcSDK
//
//  Created by Kent Vu on 8/2/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "EnterPassword.h"
#import "NetworkModal+Authen.h"
#import "FacebookHandler.h"
#import "VtcLoginTextField.h"
#import "VtcRoundedButton.h"
#import "VtcAuthenNavController.h"
#import "UIViewController+Additions.h"
#import "UIImage+Additions.h"
#import "VtcLoginViewController.h"
@interface EnterPassword () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIButton *agreeTermLabel;
@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfPass;
@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfConfirmPass;
@property (nonatomic, weak) IBOutlet VtcRoundedButton *btnNext;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

//@property (weak, nonatomic) UIButton *closeTermButton;
@end

@implementation EnterPassword

UIButton *closeTermButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.portraitTopSpace = [Global sharedInstance].gPortraitNavBarHeight+50.0;
//    self.landscapeTopSpace = [Global sharedInstance].gLandscapeNavBarHeight+35.0;
    self.landscapeTopSpace = 20;
    [self addCustomBackButton];
    [self addCustomTitleView];
    
    _tfPass.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    [_tfPass setHandleDidEndOnExit:^{
        [self onContinue:_btnNext];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationStatusDidChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    [self configUI];
}

- (void)orientationStatusDidChange:(NSNotification *)notification {
    [self configUI];
}

- (void)configUI {
    
    _agreeTermLabel.hidden = ![Global sharedInstance].enableGoogleLogin;
    _agreeTermCheckbox.hidden = ![Global sharedInstance].enableGoogleLogin;
    
    if (![[UIDevice currentDevice].model containsString:@"iPad"]) { // not iPad
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        
        if (UIInterfaceOrientationIsPortrait(orientation)) {
            _passwordheight.constant = 45.0;
            _confirmpasswordheight.constant = 45.0;
            _btntopspace.constant = 380;
        }
        else {
            _passwordheight.constant = 30.0;
            _confirmpasswordheight.constant = 30.0;
            _btntopspace.constant = 250;
        }
    }
    
    
    if(_registeringPassword.length >= 4){
        _tfPass.text = _registeringPassword;
        _tfConfirmPass.text = _registeringPassword;
        [self onContinue: _btnNext];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self settingLanguage];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (IBAction)showTerm:(id)sender{
//    NSLog(@"testahihi show term");
    _webView.hidden = YES;
    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
    CGRect rect = CGRectMake(0,0,
                             self.view.frame.size.width,
                             self.view.frame.size.height
                             );
    _webView = [[WKWebView alloc] initWithFrame:rect configuration:configuration];
    NSURL *nsurl = [NSURL URLWithString:@"https://vtcgame.vn/tin-tuc-6/dieu-khoan-su-dung-21"];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:nsurl];
    [_webView loadRequest:requestObj];
    [self.view addSubview:_webView];
    
    //add close button
    if(!closeTermButton){
        closeTermButton.hidden = YES;
        closeTermButton = nil;
    }
    closeTermButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeTermButton addTarget:self
               action:@selector(closeWebView)
     forControlEvents:UIControlEventTouchUpInside];
//    [button setTitle:@"close" forState:UIControlStateNormal];
    
    [closeTermButton setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.2]];
    [closeTermButton setImageEdgeInsets: UIEdgeInsetsMake(5, 5, 5, 5)];
    UIImage *buttonImageNormal = [UIImage vtcImageNamed:@"vtc_authen_close@2x"];
    [closeTermButton setImage:buttonImageNormal forState:UIControlStateNormal];
    closeTermButton.frame = CGRectMake(30, 60, 30, 30);
    [self.view addSubview:closeTermButton];
}
-(void)closeWebView{
//    NSLog(@"closeWebView");
    _webView.hidden = YES;
    closeTermButton.hidden = YES;
}

- (IBAction)agreeTerm:(UIButton *)sender{
//    NSLog(@"testahihi agreeTerm");
    sender.selected = !sender.selected;
    if(!sender.selected)
        self.btnNext.hidden = YES;
    else
        self.btnNext.hidden = NO;
}
- (void) settingLanguage{
    self.lblTitle.text = VTCLocalizeString(@"Register Password");
    self.tfPass.placeholder = VTCLocalizeString(@"Password");
    self.tfConfirmPass.placeholder = VTCLocalizeString(@"ConfirmPassword");
    [self.btnNext setTitle:[VTCLocalizeString(@"Continue") uppercaseString] forState:UIControlStateNormal];
}
- (IBAction)onContinue:(id)sender {
    //check dinh dang mat khau
    if(_tfPass.text.length < 4 || _tfPass.text.length > 18 || [_tfPass.text rangeOfString:@" "].location != NSNotFound){
NSLog(@"testahihi dinh dang mat khau khong dung");
        [Utilities showNotification:@"Mật khẩu phải có 4 đến 18 ký tự, không chứa dấu cách"];
            return;
    }
    
    if (![_tfPass.text isEqualToString:_tfConfirmPass.text]) {
//        [Utilities showNotification:VTCLocalizeString(@"Missing password")];
        [_tfPass becomeFirstResponder];
        [Utilities showNotification:VTCLocalizeString(@"Confirm password not matching")];
        return;
    } else {
//        [Utilities showNotification:@"Next step"];
        [self sendRegisterRequest:@"3" sender:sender];
    }
}

- (void)sendRegisterRequest:(NSString *)step
                     sender:(id)sender{
    [(VtcRoundedButton *)sender setEnabled:NO];
    [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
    
    NSDictionary *userInfo = [[VIDUser currentUser] exportUserInfo];
    NSString *sign=[userInfo objectForKey:@"extend"];
    
    [[NetworkModal sharedModal] registerVTCIDNew:_registeringName
                                        password:_tfPass.text
                                     capchaToken:@""
                                             otp:@""
                                            step:step
                                            sign:sign
     
                                      completion:^(BOOL status, id responsedObject, NSError *error) {
                                          [(VtcRoundedButton *)sender setEnabled:YES];
                                          [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
                                          //tạm thời không gọi API
                                          if (status) {
                                              [[VIDUser currentUser] loadData:responsedObject];
                                              _registeringPassword = _tfPass.text;
//                                              [self goToSignin:_registeringName password:_registeringPassword];
                                              VtcAuthenNavController *navController = (VtcAuthenNavController *)self.navigationController;
                                              VtcLoginViewController *loginController = [VtcLoginViewController loadFromNibNamed:@"VtcLoginViewController"];
                                              loginController.registeringName = _registeringName;
                                              loginController.registeringPassword = _registeringPassword;
                                              loginController.needAutoLogin = YES;
                                              [navController changeRootController:loginController];
                                          }
                                          else {
                                              if((long)error.code == -46){
                                                  [self goToSignin:_registeringName password:_registeringPassword];
                                              }
                                              else{
                                                  [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
                                              }
                                              
                                          }
                                      }];
    
}

// MARK: UITextField Delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.activeTextField = textField;
    return YES;
}
@end

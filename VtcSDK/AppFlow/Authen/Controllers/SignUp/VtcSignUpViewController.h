//
//  VtcSignUpViewController.h
//  VtcSDK
//
//  Created by Kent Vu on 7/26/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "UIView+UpdateAutoLayoutConstraints.h"
#import <WebKit/WebKit.h>
//#import "ReCaptcha/ReCaptcha-Swift.h"
//#import "{YourProjectTargetName}-Swift.h"
@interface VtcSignUpViewController : VtcAuthenBaseViewController
@property (weak, nonatomic) WKWebView *webView;
@end

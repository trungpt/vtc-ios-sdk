//
//  EnterPassword
//  VtcSDK
//
//  Created by Kent Vu on 8/2/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcAuthenBaseViewController.h"
#import "VIDUser.h"
#import <WebKit/WebKit.h>
#import "../../../../Libs/Custom/Checkbox.h"
@interface EnterPassword : VtcAuthenBaseViewController
@property (weak, nonatomic) IBOutlet UIButton *term;
@property (weak, nonatomic) IBOutlet Checkbox *checkbox;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *confirmpasswordheight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btntopspace;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *passwordheight;
@property (nonatomic) AuthenType authenType;
@property (nonatomic, copy) NSString *openIDTokenString;
@property (nonatomic, copy) NSString *registeringName;
@property (nonatomic, copy) NSString *registeringPassword;

- (IBAction)showTerm:(id)sender;

- (IBAction)agreeTerm:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *agreeTermCheckbox;

@property (weak, nonatomic) WKWebView *webView;
@end

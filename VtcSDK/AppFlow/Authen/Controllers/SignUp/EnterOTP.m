//
//  EnterPassword
//  VtcSDK
//
//  Created by Kent Vu on 8/2/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "EnterOTP.h"
#import "NetworkModal+Authen.h"
#import "FacebookHandler.h"
#import "VtcLoginTextField.h"
#import "VtcRoundedButton.h"
#import "VtcAuthenNavController.h"
#import "UIViewController+Additions.h"

@interface EnterOTP () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *tfCuPhapOTP;
@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfPass;
@property (nonatomic, weak) IBOutlet VtcRoundedButton *btnNext;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *enterotp_hint;
@end

@implementation EnterOTP

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.portraitTopSpace = [Global sharedInstance].gPortraitNavBarHeight+50.0;
    self.landscapeTopSpace = 20;
    [self addCustomBackButton];
    [self addCustomTitleView];
    
    _tfPass.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    [_tfPass setHandleDidEndOnExit:^{
        [self onContinue:_btnNext];
    }];
    
    
    //lay cu phap OTP
    [[NetworkModal sharedModal] getDKOTPText:^(BOOL status, id responsedObject, NSError *error) {
        if (status) {
            @try {
                if ([[[VtcConfig defaultConfig] sdkLanguage] isEqualToString:@"vie"])
                {
                    NSString *htmlString = [[responsedObject firstObject] objectForKey:@"configValue"];
                    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUTF8StringEncoding]
                                                                                            options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                                                 documentAttributes:nil
                                                                                              error:nil];
                    _tfCuPhapOTP.attributedText = attributedString;
                }
                else
                {
                    NSString *htmlString = [[responsedObject lastObject] objectForKey:@"configValue"];
                    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUTF8StringEncoding]
                                                                                            options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                                                 documentAttributes:nil
                                                                                              error:nil];
                    _tfCuPhapOTP.attributedText = attributedString;
                }
            } @catch (NSException *exception) {
                VTCLog(@"Data không hợp lệ");
            } @finally {
                
            }
        }
    }];
}





- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    [self settingLanguage];
}

- (void) settingLanguage{
    NSDictionary *userInfo = [[VIDUser currentUser] exportUserInfo];
    NSString *accountUsingMobile=[userInfo objectForKey:@"accountUsingMobile"];
//    NSLog(@"testahihi accountUsingMobile:%@%@%d",accountUsingMobile,@"--",accountUsingMobile.length);
    
    //test
//    accountUsingMobile = @"taikhoantest123";
    
    if (accountUsingMobile.length > 0){
        //Kích hoạt tài khoản mới
        _lblTitle.text = @"KÍCH HOẠT TÀI KHOẢN ĐÃ TỒN TẠI SMSPLUS";
        NSString *str = [accountUsingMobile stringByReplacingCharactersInRange:NSMakeRange(accountUsingMobile.length - 3, 3) withString:@"***"];
        NSString *test = [NSString stringWithFormat:@"%@%@%@",@"Số điện thoại của bạn trùng với SMSPlus của tài khoản ",str,@". Bằng việc nhập mã OTP kích hoạt tài khoản này, SMSPlus của tài khoản trên sẽ bị hủy"];
NSLog(@"testahihi %@",test);
        _enterotp_hint.text = test;
        _enterotp_hint.textColor = [UIColor redColor];
        
    }else {
        //Kích hoạt tài khoản cũ
        _lblTitle.text = @"KÍCH HOẠT TÀI KHOẢN MỚI";
    }
    //    self.lblTitle.text = VTCLocalizeString(@"Register Password");
    //    self.tfPass.placeholder = VTCLocalizeString(@"Password");
    //    self.tfConfirmPass.placeholder = VTCLocalizeString(@"ConfirmPassword");
    //    [self.btnNext setTitle:[VTCLocalizeString(@"Continue") uppercaseString] forState:UIControlStateNormal];
}
- (void)sendRegisterRequest:(NSString *)step
                     sender:(id)sender{
    [(VtcRoundedButton *)sender setEnabled:NO];
    [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
    
    NSDictionary *userInfo = [[VIDUser currentUser] exportUserInfo];
    NSString *sign=[userInfo objectForKey:@"extend"];
    [[NetworkModal sharedModal] registerVTCIDNew:_registeringName
                                        password:@""
                                     capchaToken:@""
                                             otp:_tfPass.text
                                            step:step
                                            sign:sign
     
                                      completion:^(BOOL status, id responsedObject, NSError *error) {
                                          [(VtcRoundedButton *)sender setEnabled:YES];
                                          [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
                                          //tạm thời không gọi API
                                          if (status) {
                                              // save the new registered user
                                              [[VIDUser currentUser] loadData:responsedObject];
                                              [self goToEnterPassword:_registeringName password: _registeringPassword];
                                          }
                                          else {
                                              //            [Utilities showNotification:@"Xin chào, API gọi sai rồi"];
                                              if((long)error.code == -46){
                                                  [self goToSignin:_registeringName password:_registeringPassword];
                                              }
                                              else{
                                                  [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
                                              }
                                              
                                          }
                                      }];
    
}

- (IBAction)onContinue:(id)sender {
    if (!(_tfPass.text.length > 0)) {
        [Utilities showNotification:VTCLocalizeString(@"Missing OTP code")];
        //        [_tfPass becomeFirstResponder];
        //        [Utilities showNotification:VTCLocalizeString(@"Confirm password not matching")];
        return;
    } else {
        if(_tfPass.text.length <6){
            [Utilities showNotification:VTCLocalizeString(@"enter6characters")];
            return;
        }
        [self sendRegisterRequest:@"2" sender:sender];
        
    }
}

// MARK: UITextField Delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.activeTextField = textField;
    return YES;
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    // Prevent crashing undo bug – see note below.
//    NSLog(@"testahihi shouldChangeCharactersInRange");
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 6;
}

@end

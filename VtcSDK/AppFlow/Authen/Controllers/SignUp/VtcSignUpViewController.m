//
//  VtcSignUpViewController.m
//  VtcSDK
//
//  Created by Kent Vu on 7/26/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//#import "VtcSignUpViewControllerQuick.h"
//
//VtcLoginBackupViewController
#import "VIDUser+Internal.h"
#import "EnterOTP.h"
#import "EnterPassword.h"
#import "VtcLoginViewController.h"
#import "VtcAuthenViewController.h"
#import "VtcSignUpViewController.h"

#import "VtcLoginTextField.h"
#import "UIImage+Additions.h"
#import "NetworkModal+Authen.h"
#import "FacebookHandler.h"
#import "VtcEnterUsernameViewController.h"
#import "UIViewController+Additions.h"
#import "GoogleServiceHandler.h"
#import "VtcAuthenNavController.h"
#import "VtcLoginViewController.h"
#import "VtcRoundedButton.h"
#import "UIView+UpdateAutoLayoutConstraints.h"
#import "SDKManager.h"
#import "SDKManager+Internal.h"
#import "SmartParser.h"

@interface VtcSignUpViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *darkview;
@property (weak, nonatomic) IBOutlet UIScrollView *mainscrollview;
@property (weak, nonatomic) IBOutlet UIView *captchacontainer;
@property (weak, nonatomic) IBOutlet UIView *bottom_line_password;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerWidth;
@property (weak, nonatomic) IBOutlet VtcLoginTextField *tfConfirmPassword;
@property (weak, nonatomic) IBOutlet UIButton *agreeTermLabel;

@property (weak, nonatomic) IBOutlet UIButton *agreeTermCheckbox;
@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfUsername;
@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfPassword;
@property (nonatomic, weak) IBOutlet VtcRoundedButton *btnSignUp;
@property (nonatomic, weak) IBOutlet VtcRoundedButton *btnFacebook;
@property (nonatomic, weak) IBOutlet VtcRoundedButton *btnGoogle;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
//@property (weak, nonatomic) IBOutlet VtcRoundedButton *btnQuickSignUp;

@property (nonatomic, copy) NSString *registeringName;
@property (nonatomic, copy) NSString *registeringPassword;

@property (nonatomic, weak) IBOutlet UILabel *orLabel;
@property (nonatomic, weak) IBOutlet UILabel *lbGameVersion;
@property (nonatomic, weak) IBOutlet UILabel *lbSDKVersion;

@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@property (nonatomic, weak) IBOutlet UIButton *btnFacebookLandscape;
@property (nonatomic, weak) IBOutlet UIButton *btnGoogleLandscape;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *topSpaceFromUsernameToLogo;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *topSpaceFromVersionLabelToCenter;

@end



@implementation VtcSignUpViewController

//NSString *const reCAPTCHA_site_key = @"6LeeK5MUAAAAAGA0m3QduK5iwcv9ZVwJGbZRFgyj";
//NSString *const reCAPTCHA_site_key = @"6Lc7d5QUAAAAAKX1YBNxfmReFfuTMAcWwL8Ze31t";
//6Lc7d5QUAAAAAKX1YBNxfmReFfuTMAcWwL8Ze31t
//NSString *const reCAPTCHA_sercret_key = @"6LeeK5MUAAAAAOevo97cB--R_e1fR18m97HCHzLs";
UIButton *closeCaptchaButton;
NSString *const BASE_URL = @"http://sandbox.vtcgame.vn";
NSString *captchaToken;
id tempSender;
UIButton *closeTerm;
//NSString *const reCAPTCHA_site_key = @"6Lc_epUUAAAAAF3zktZjx9JF-yIzEnvIJIllwsYT";
//NSString *const BASE_URL = @"https://vtcgame.vn";

int const CAPTCHA_MIN_HEIGHT = 100;
int const CAPTCHA_BASE_Y = 120;
- (NSString*)getWebviewContent {
    
    //backup
    NSString *v2 = @"<!DOCTYPE html><html><head>"
    @"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">"
    @"<style> .text-xs-center { text-align: center; } .g-recaptcha { display: inline-block; } </style>"
    @"<script src=\"https://www.google.com/recaptcha/api.js?hl=vi\"></script>"
    @"<script type=\"text/javascript\">"
    @"function onloadCallback(data) {window.webkit.messageHandlers.interOp.postMessage(data);} "
    @"function postMessage(data) {window.webkit.messageHandlers.interOp.postMessage(data); } "
    @"function onDataCallback(response) { postMessage({'data': response}); } "
    @"function onDataExpiredCallback(error) {  postMessage(\"expired\"); } "
    @"function onDataErrorCallback(error) {  postMessage(\"error\"); } "
    //    @"grecaptcha.execute();"
    @"</script>"
    @"</head><body>"
    @"<div class=\"text-xs-center\">"
    @"<div class=\"g-recaptcha\" "
    @"data-sitekey=\"reCAPTCHA_site_key\" "
    @"data-size=\"invisible\" "
    @"data-callback=\"onDataCallback\" "
    @"data-expired-callback=\"onDataExpiredCallback\" "
    @"data-error-callback=\"onDataErrorCallback\">"
    @"</div></div></body>"
    
    @"</html>";
    //end backup
    
    //backup2
    //    NSString *v2 = @"<!DOCTYPE html><html><head>"
    //    @"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">"
    //    @"<style> .text-xs-center { text-align: center; } .g-recaptcha { display: inline-block; } </style>"
    //    @"<script src=\"https://www.google.com/recaptcha/api.js?hl=vi\"></script>"
    //    @"<script type=\"text/javascript\">"
    ////    @"function onloadCallback(data) {window.webkit.messageHandlers.interOp.postMessage(data);} "
    ////    @"function postMessage(data) {window.webkit.messageHandlers.interOp.postMessage(data); } "
    ////    @"function onDataCallback(response) { postMessage({'data': response}); } "
    ////    @"function onDataExpiredCallback(error) {  postMessage(\"expired\"); } "
    ////    @"function onDataErrorCallback(error) {  postMessage(\"error\"); } "
    ////    @"grecaptcha.execute();"
    //
    //
    //    @"var key = \"${apiKey}\";"
    //    @"var endpoint = \"${endpoint}\";"
    //    @"var shouldFail = ${shouldFail};"
    //
    //    @"var post = function(value) {"
    //    @"    window.webkit.messageHandlers.interOp.postMessage(data);"
    //    @"};"
    //
    //    @"var execute = function() {"
    //    @"    if (shouldFail) {"
    //    @"        post(\"error\");"
    //    @"    }"
    //    @"    else {"
    //    @"        post(${message});"
    //    @"    }"
    //    @"};"
    //
    //    @"var reset = function() {"
    //    @"    shouldFail = false;"
    //    @"    post({action: \"didLoad\"});"
    //    @"};"
    //
    //    @"</script>"
    //    @"</head><body>"
    //    @"<div class=\"text-xs-center\">"
    //    @"<div class=\"g-recaptcha\" "
    //    @"data-sitekey=\"reCAPTCHA_site_key\" "
    //    @"data-size=\"invisible\" "
    //    @"data-callback=\"onDataCallback\" "
    //    @"data-expired-callback=\"onDataExpiredCallback\" "
    //    @"data-error-callback=\"onDataErrorCallback\">"
    //    @"</div></div></body>"
    //
    //    @"</html>";
    //end backup2
    /*
     NSString *v3 = @"<!DOCTYPE html><html><head>"
     @"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">"
     @"<script src=\"https://www.google.com/recaptcha/api.js?hl=vi&render=reCAPTCHA_site_key\"></script>"
     @"<script>"
     @"  grecaptcha.ready(function() {"
     @"    grecaptcha.execute('reCAPTCHA_site_key', {action: 'homepage'}).then(function(token) {"
     @"      window.webkit.messageHandlers.interOp.postMessage({data: token});"
     @"    });"
     @"  });"
     @"</script>"
     @"</head><body>"
     @"</body></html>";
     */
    
    NSString *content = [v2 stringByReplacingOccurrencesOfString:@"reCAPTCHA_site_key"
                                                      withString:[SDKManager defaultManager].sitekeyRecapchaIos];
    
    //    NSLog(@"testday %@",content);
    return content;
}

- (void)setDefaultCaptcha {
    CGRect rect = CGRectMake(0,
                             _tfUsername.frame.origin.y + _tfUsername.frame.size.height,
                             self.mainscrollview.frame.size.width,
                             //                             CAPTCHA_MIN_HEIGHT
                             self.mainscrollview.frame.size.height
                             );
    [_webView setFrame:rect];
}

- (void)setFullCaptcha {
    CGRect rect = CGRectMake(0,0,
                             //                             _tfUsername.frame.origin.y + _tfUsername.frame.size.height,
                             self.mainscrollview.frame.size.width,
                             self.mainscrollview.frame.size.height
                             );
    [_webView setFrame:rect];
}

- (void)userContentController:(WKUserContentController *)userContentController
      didReceiveScriptMessage:(WKScriptMessage *)message {
    
    NSDictionary *sentData = (NSDictionary*)message.body;
    
    NSLog(@"testahihi userContentController");
    
    if ([[sentData allKeys] containsObject:@"data"]) {
        NSString *token = (NSString*)[sentData objectForKey:@"data"];
        //        NSLog(@"token = %@", token);
        captchaToken = token;
        //        [self setDefaultCaptcha];
        //        _webView.scrollView.contentSize.height = CAPTCHA_MIN_HEIGHT;
        //        _btnSignUp.enabled = YES;
        _darkview.hidden = YES;
        if(_webView)
            _webView.hidden = YES;
        [_webView removeFromSuperview];
        _webView = nil;
        [closeCaptchaButton removeFromSuperview];
        [self sendRegisterRequest:@"1" sender:tempSender];
        //        [self.mainscrollview willRemoveSubview:webView];
        
        //        [self sendRegisterRequest:@"1" sender:tempSender];
    } else {
        NSLog(@"Received: %@", (NSString*)message.body);
        _darkview.hidden = YES;
        [_webView removeFromSuperview];
        [self sendRegisterRequest:@"1" sender:tempSender];
        //        if(_webView)
        //            _webView.hidden = YES;
    }
}

static int kObservingContentSizeChangesContext;

- (void)startObservingContentSizeChangesInWebView:(WKWebView *)webView {
    [webView.scrollView addObserver:self forKeyPath:@"contentSize" options:0 context:&kObservingContentSizeChangesContext];
}

- (void)stopObservingContentSizeChangesInWebView:(WKWebView *)webView {
    [webView.scrollView removeObserver:self forKeyPath:@"contentSize" context:&kObservingContentSizeChangesContext];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    //    NSLog(@"testahihi captcha height: %.2f",_webView.scrollView.contentSize.height);
    //    NSLog(@"testahihi captcha width: %.2f",_webView.scrollView.contentSize.width);
    if (context == &kObservingContentSizeChangesContext) {
        //        UIScrollView *scrollView = object;
        //NSLog(@"%@ contentSize changed to %@", scrollView, NSStringFromCGSize(scrollView.contentSize));
        
        if (_webView.scrollView.contentSize.height > CAPTCHA_MIN_HEIGHT) {
            [self stopObservingContentSizeChangesInWebView: _webView];
            
            [self setFullCaptcha];
        }
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)webView:(WKWebView *)webView
didFinishNavigation:(WKNavigation *)aNavigation {
    [_webView evaluateJavaScript: @"grecaptcha.execute();" completionHandler:^(id Result, NSError * error) {
        NSLog(@"testahihi Result -> %@", Result);
        NSLog(@"testahihi Error -> %@", error);
    }];
}

- (void) showCaptcha{
    //add recaptchar
    NSLog(@"testahihi showCaptcha");
    NSLog(@"<VtcSDK> sitekeyRecapchaIos=%@", [SDKManager defaultManager].sitekeyRecapchaIos);
    
    if(_webView != nil){
        [self stopObservingContentSizeChangesInWebView: _webView];
    }
    if([SDKManager defaultManager].sitekeyRecapchaIos && [SDKManager defaultManager].sitekeyRecapchaIos.length>0){
        WKPreferences *preferences = [[WKPreferences alloc] init];
        preferences.javaScriptEnabled = YES;
        WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
        [configuration.userContentController addScriptMessageHandler:self name:@"interOp"];
        configuration.preferences = preferences;
        
        //    CGRect rect = CGRectMake(0,
        //                             _tfUsername.frame.origin.y + _tfUsername.frame.size.height,
        //                             self.mainscrollview.frame.size.width,
        //                             CAPTCHA_MIN_HEIGHT
        //                             );
        CGRect rect = CGRectMake(0,
                                 //                             CAPTCHA_BASE_Y,
                                 //                                 _tfUsername.frame.origin.y + _tfUsername.frame.size.height,
                                 0,
                                 self.mainscrollview.frame.size.width,
                                 self.mainscrollview.frame.size.height
                                 );
        //    WKWebView *webView = [[WKWebView alloc] initWithFrame:mainscrollview configuration:configuration];
        //    _webView = [[WKWebView alloc] initWithFrame:self.mainscrollview.frame configuration:configuration];
        _webView = [[WKWebView alloc] initWithFrame:rect configuration:configuration];
        //    _webView.navigationDelegate = self;
        
        [_webView.scrollView setDelegate:self];
        [_webView setNavigationDelegate: self];
        
        NSURL *nsurl = [NSURL URLWithString:BASE_URL];
        NSString *content = [self getWebviewContent];
        [_webView loadHTMLString:content baseURL:nsurl];
        _darkview.hidden = NO;
        [self.mainscrollview addSubview:_webView];
        [self startObservingContentSizeChangesInWebView: _webView];
        //nut dong captcha
        closeCaptchaButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [closeCaptchaButton addTarget:self
                               action:@selector(closeCaptchaView)
                     forControlEvents:UIControlEventTouchUpInside];
        //    [button setTitle:@"close" forState:UIControlStateNormal];
        [closeCaptchaButton setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.2]];
        [closeCaptchaButton setImageEdgeInsets: UIEdgeInsetsMake(5, 5, 5, 5)];
        UIImage *buttonImageNormal = [UIImage vtcImageNamed:@"vtc_authen_close@2x"];
        [closeCaptchaButton setImage:buttonImageNormal forState:UIControlStateNormal];
        closeCaptchaButton.frame = CGRectMake(30, 30, 30, 30);
        [self.view addSubview:closeCaptchaButton];
        
    }else{
        NSLog(@"testahihi ignoreCaptcha because wrong sitekey");
        captchaToken=@"";
        [self sendRegisterRequest:@"1" sender:tempSender];
    }
    
}

-(void)closeCaptchaView{
    _darkview.hidden = YES;
    if(_webView)
        _webView.hidden = YES;
    [_webView removeFromSuperview];
    _webView = nil;
    [closeCaptchaButton removeFromSuperview];
}

#pragma mark UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // do something in response to scroll
    //    if (_webView.scrollView.contentSize.height == CAPTCHA_MIN_HEIGHT)
    //        [self startObservingContentSizeChangesInWebView: _webView];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Luồng đăng nhập mới
    //    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultCachedUser];
    //    if (userInfo) {
    //        _tfUsername.text = [SmartParser stringForKey:@"accountName" from:userInfo];
    //    }
    //    [self showCaptcha];
    //-------------
    
    //    if([SDKManager defaultManager].allowRotationInLoginView){
    NSLog(@"testahihi allowRotationInLoginView YES");
    //    }else
    NSLog(@"testahihi allowRotationInLoginView No");
    
    if ([SDKManager defaultManager].isShowCloseButtonInAuthenVC) {
        [self.closeButton setHidden:false];
    }
    
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.portraitTopSpace = 80.0;
    self.landscapeTopSpace = 8.0; //30.0;
    
    _tfUsername.clearButtonMode = UITextFieldViewModeWhileEditing;
    //    _tfUsername.text = @"testsdk";
    //    _tfPassword.text = @"Test123";
    
    [_tfPassword setHandleDidEndOnExit:^{
        [self onSignUp:_btnSignUp];
    }];
    
    _lbGameVersion.text = [NSString stringWithFormat:@"game: v%@", [[VtcConfig defaultConfig] gameVersion]];
    NSString *sdkVerString = [NSString stringWithFormat:@"sdk: v%@", VtcSDKVersionString];
    if ([NetworkModal sharedModal].isDevMode) {
        sdkVerString = [sdkVerString stringByAppendingString:@" (Dev)"];
    }
    _lbSDKVersion.text = sdkVerString;
    
    [self configUI];
}

- (void)configUI {
    
    _agreeTermLabel.hidden = ![Global sharedInstance].enableGoogleLogin;
    _agreeTermCheckbox.hidden = ![Global sharedInstance].enableGoogleLogin;
    
    if (![[UIDevice currentDevice].model containsString:@"iPad"]) { // not iPad
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        
        if (UIInterfaceOrientationIsPortrait(orientation)) {
            _topSpaceFromUsernameToLogo.constant = 40.0;
            _topSpaceFromVersionLabelToCenter.constant = 237.0;
            _orLabel.hidden = YES;
        }
        else {
            [containerView setConstraintConstant:400 forAttribute:NSLayoutAttributeWidth];
            //            _containerWidth.constant = 800;
            _topSpaceFromUsernameToLogo.constant = 10.0;
            _topSpaceFromVersionLabelToCenter.constant = 46.0;
            // not show label "Hoặc"
            _orLabel.hidden = YES;
        }
    }else{
        //ipad
        [containerView setConstraintConstant:400 forAttribute:NSLayoutAttributeWidth];
    }
    
    [self reconfigOpenIDAuthen];
}

- (void)reconfigOpenIDAuthen {
    if (![[UIDevice currentDevice].model containsString:@"iPad"]) { // not iPad
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        
        if (UIInterfaceOrientationIsPortrait(orientation)) {
            //            _btnFacebook.hidden = ![Global sharedInstance].enableFacebookLogin;
            //            _btnGoogle.hidden = ![Global sharedInstance].enableGoogleLogin;
            _btnFacebook.hidden = YES;
            _btnGoogle.hidden = YES;
            _btnGoogleLandscape.hidden = YES;
            _btnFacebookLandscape.hidden = YES;
            // set hidden of label "Hoặc"
            if (_btnFacebook.hidden && _btnGoogle.hidden) {
                _orLabel.hidden = YES;
            }
            else {
                _orLabel.hidden = YES;
            }
        }
        else {
            
            _btnGoogle.hidden = YES;
            _btnFacebook.hidden = YES;
            _btnGoogleLandscape.hidden = YES;
            [_btnGoogleLandscape setConstraintConstant:_btnGoogleLandscape.hidden ? 0.0 : kWidthOfLandscapeSocialLoginButton forAttribute:NSLayoutAttributeWidth];
            _btnFacebookLandscape.hidden = YES;
            [_btnFacebookLandscape setConstraintConstant:_btnFacebookLandscape.hidden ? 0.0 : kWidthOfLandscapeSocialLoginButton forAttribute:NSLayoutAttributeWidth];
        }
    }
    else { // iPad
        
        _btnFacebook.hidden = YES;
        _btnGoogle.hidden = YES;
        // set hidden of label "Hoặc"
        if (_btnFacebook.hidden && _btnGoogle.hidden) {
            _orLabel.hidden = YES;
        }
        else {
            _orLabel.hidden = YES;
        }
    }
    
}
- (IBAction)btnBackClicked:(id)sender {
    if (@available(iOS 13.0, *)) {
        //go to login screen
        VtcAuthenNavController *navController = (VtcAuthenNavController *)self.navigationController;
        VtcLoginViewController *controller = [VtcLoginViewController loadFromNibNamed:@"VtcLoginViewController"];
        [navController changeRootController:controller];
    }else{
        VtcAuthenNavController *navController = (VtcAuthenNavController *)self.navigationController;
        VtcLoginViewController *controller = [VtcLoginViewController loadFromNibNamed:@"VtcLoginViewController"];
        [navController changeRootController:controller];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self settingLanguage];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reconfigOpenIDAuthen) name:kNotificationGotFacebookGoogleAuthenConfig object:nil];
    /// Add Orientation notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationStatusDidChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
    // reset UI
    [self orientationStatusDidChange:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationGotFacebookGoogleAuthenConfig object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}
- (void) settingLanguage
{
    self.tfUsername.placeholder = VTCLocalizeString(@"Username2");
    //    self.tfPassword.placeholder = VTCLocalizeString(@"Password");
    [self.btnSignUp setTitle:[VTCLocalizeString(@"Continue") uppercaseString] forState:UIControlStateNormal];
    //    [self.btnQuickSignUp setTitle:[VTCLocalizeString(@"QuickRegister") uppercaseString] forState:UIControlStateNormal];
    self.orLabel.text = VTCLocalizeString(@"Or");
    [self.btnFacebook setTitle:VTCLocalizeString(@"Register via Facebook") forState:UIControlStateNormal];
    [self.btnGoogle setTitle:VTCLocalizeString(@"Register via Google") forState:UIControlStateNormal];
    NSString *loginString = VTCLocalizeString(@"You had got a VTC account? Login now");
    if ([loginString containsString:@"Đăng nhập tại đây"])
    {
        NSMutableAttributedString *mRegister = [[NSMutableAttributedString alloc] initWithString:loginString attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
        [mRegister addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:11.0/255.0 green:185.0/255.0 blue:250.0/255.0 alpha:1.0] range:[loginString rangeOfString:@"Đăng nhập tại đây"]];
        [self.btnLogin setAttributedTitle:mRegister forState:UIControlStateNormal];
    }
    else
    {
        NSMutableAttributedString *mRegister = [[NSMutableAttributedString alloc] initWithString:loginString attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
        [mRegister addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:11.0/255.0 green:185.0/255.0 blue:250.0/255.0 alpha:1.0] range:[loginString rangeOfString:@"Login here"]];
        [self.btnLogin setAttributedTitle:mRegister forState:UIControlStateNormal];
    }
}

- (void)orientationStatusDidChange:(NSNotification *)notification {
    [self configUI];
}

- (IBAction)closeButtonTapped:(id)sender {
    NSLog(@"Close Button Tapped");
    [self.navigationController dismissViewControllerAnimated:NO completion:^{
        [SDKManager defaultManager].showingLoginView = NO;
    }];
}

- (void)sendRegisterRequest:(NSString *)step
                     sender:(id)sender{
    [(VtcRoundedButton *)sender setEnabled:NO];
    [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
    
    [[NetworkModal sharedModal] registerVTCIDNew:_tfUsername.text
                                        password:_tfPassword.text
                                     capchaToken:captchaToken
                                             otp:@""
                                            step:step
                                            sign:@""
     
                                      completion:^(BOOL status, id responsedObject, NSError *error) {
        [(VtcRoundedButton *)sender setEnabled:YES];
        [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
        if (status) {
            
            // save the new registered user
            //Utilities.registeringPassword = _tfPassword.text;
            [self goToEnterOTP:_tfUsername.text password:_tfPassword.text];
            _registeringName =_tfUsername.text;
            _registeringPassword =@"";
            [VIDUser currentUser].password = @"";
            [[VIDUser currentUser] loadData:responsedObject];
            NSDictionary *userInfo = [[VIDUser currentUser] exportUserInfo];
            NSString *sign=[userInfo objectForKey:@"extend"];
            
            //old code
            [[NSUserDefaults standardUserDefaults] setObject:_tfUsername.text forKey:kUserDefaultCachedNewRegisteredUser];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            // appsflyer logging
            [[VtcEventLogging manager] setAppsFlyerInAppEvent:kAppsFlyerEventTypeRegister values:[NSDictionary dictionaryWithObjectsAndKeys:@"1", kAppsFlyerEventTypeRegisterValueKey, nil]];
            //            [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeNormal containerController:self.navigationController showWelcome:YES];
            _registeringName = _tfUsername.text;
            _registeringPassword = _tfPassword.text;
            //                          [self onSignIn:nil];
            
            [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeOther]];
        }else {
            //              [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
            if((long)error.code == -46){
                _registeringName = _tfUsername.text;
                [Utilities showNotification:@"Tài khoản đã tồn tại. Vui lòng đăng nhập"];
                _registeringPassword =@"";
                [VIDUser currentUser].password = @"";
                [self onSignIn:nil];
            }
            else{
                [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
            }
        }
    }];
    
}

- (void) fastRegister: (id)sender{
    [(VtcRoundedButton *)sender setEnabled:NO];
    [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
    [[NetworkModal sharedModal] registerVTCID:_tfUsername.text password:_tfPassword.text completion:^(BOOL status, id responsedObject, NSError *error) {
        [(VtcRoundedButton *)sender setEnabled:YES];
        [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
        if (status) {
            // save the new registered user
            [[NSUserDefaults standardUserDefaults] setObject:_tfUsername.text forKey:kUserDefaultCachedNewRegisteredUser];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            // appsflyer logging
            [[VtcEventLogging manager] setAppsFlyerInAppEvent:kAppsFlyerEventTypeRegister values:[NSDictionary dictionaryWithObjectsAndKeys:@"1", kAppsFlyerEventTypeRegisterValueKey, nil]];
            //            [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeNormal containerController:self.navigationController showWelcome:YES];
            _registeringName = _tfUsername.text;
            _registeringPassword = _tfPassword.text;
            [self onSignIn:nil];
            
            [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeOther]];
        }
        else {
            [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
        }
    }];
}

- (IBAction)showTerm:(id)sender {
    //    NSLog(@"testahihi show term");
    _webView.hidden = YES;
    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
    CGRect rect = CGRectMake(0,0,
                             self.view.frame.size.width,
                             self.view.frame.size.height
                             );
    _webView = [[WKWebView alloc] initWithFrame:rect configuration:configuration];
    NSURL *nsurl = [NSURL URLWithString:@"https://vtcgame.vn/tin-tuc-6/dieu-khoan-su-dung-21"];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:nsurl];
    [_webView loadRequest:requestObj];
    [self.view addSubview:_webView];
    
    //add close button
    if(!closeTerm){
        closeTerm.hidden = YES;
        closeTerm = nil;
    }
    closeTerm = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeTerm addTarget:self
                  action:@selector(closeWebView)
        forControlEvents:UIControlEventTouchUpInside];
    //    [button setTitle:@"close" forState:UIControlStateNormal];
    
    [closeTerm setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.2]];
    [closeTerm setImageEdgeInsets: UIEdgeInsetsMake(5, 5, 5, 5)];
    UIImage *buttonImageNormal = [UIImage vtcImageNamed:@"vtc_authen_close@2x"];
    [closeTerm setImage:buttonImageNormal forState:UIControlStateNormal];
    closeTerm.frame = CGRectMake(30, 60, 30, 30);
    [self.view addSubview:closeTerm];
}

-(void)closeWebView{
    //    NSLog(@"closeWebView");
    _webView.hidden = YES;
    closeTerm.hidden = YES;
}

- (IBAction)agreeTerm:(UIButton *)sender {
    //    NSLog(@"testahihi agreeTerm");
    sender.selected = !sender.selected;
    if(!sender.selected)
        _btnSignUp.hidden = YES;
    else
        _btnSignUp.hidden = NO;
}


- (IBAction)onSignUp:(id)sender {
    //check user de trong
    if ([_tfUsername.text isEqualToString:@""]) {
        [Utilities showNotification:VTCLocalizeString(@"Missing username")];
        [_tfUsername becomeFirstResponder];
        return;
    }
    
    //check dinh dang mat khau
    if ([_tfPassword.text isEqualToString:@""]) {
        [Utilities showNotification:VTCLocalizeString(@"Missing password")];
        [_tfPassword becomeFirstResponder];
        return;
    }
    if(_tfPassword.text.length < 4 || _tfPassword.text.length > 18 || [_tfPassword.text rangeOfString:@" "].location != NSNotFound){
        NSLog(@"testahihi dinh dang mat khau khong dung");
        [Utilities showNotification:@"Mật khẩu phải có 4 đến 18 ký tự, không chứa dấu cách"];
        return;
    }
    //check password khong giong nhau
    if (![_tfPassword.text isEqualToString:_tfConfirmPassword.text]) {
        //        [Utilities showNotification:VTCLocalizeString(@"Missing password")];
        [_tfPassword becomeFirstResponder];
        [Utilities showNotification:VTCLocalizeString(@"Confirm password not matching")];
        return;
    } else {
        NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        if ([_tfUsername.text rangeOfCharacterFromSet:notDigits].location == NSNotFound)
        {
            // _tfUsername consists only of the digits 0 through 9
            //        [Utilities showNotification:@"Next step"];
            
            //        [Utilities showNotification:VTCLocalizeString(@"Missing password")];
            //        [_tfPassword becomeFirstResponder];
            //        return;
            //    }
            if (_tfUsername.text.length < 10){
                [Utilities showNotification:@"Vui lòng nhập SĐT hợp lệ"];
                [_tfUsername becomeFirstResponder];
                return;
            }
            [_tfUsername resignFirstResponder];
            tempSender = sender;
            if([SDKManager defaultManager].ignoreCaptcha){
                NSLog(@"testahihi ignoreCaptcha");
                captchaToken=@"";
                [self sendRegisterRequest:@"1" sender:sender];
            }else {
                [self showCaptcha];
            }
            
            //Thông báo tài khoản không hợp lệ
            //    [Utilities showNotification:@"Tài khoản không tồn tại. Vui lòng kiểm tra lại hoặc nhập số điện thoại của bạn để tạo tài khoản mới"];
            //Chuyển sang màn hình đăng nhập
            //    [Utilities showNotification:@"Mời bạn đăng nhập"];
            //    VtcAuthenNavController *navController = (VtcAuthenNavController *)self.navigationController;
            //    VtcSignUpViewController *controller = [VtcLoginViewController loadFromNibNamed:@"VtcLoginViewController"];
            //    _registeringName = _tfUsername.text;
            //    controller.registeringName = _registeringName;
            //    NSLog(@"%@", _registeringName);
            //    [navController changeRootController:controller];
            //--------------
            //Chuyển sang màn hình nhập pass
            //    VtcAuthenNavController *navController = (VtcAuthenNavController *)self.navigationController;
            //    EnterPassword *controller = [EnterPassword loadFromNibNamed:@"EnterPassword"];
            //    [navController changeRootController:controller];
            //--------------
            //
        }else{
            [self fastRegister: sender];
        }
    }
    
    
}

- (IBAction)onFacebook:(id)sender {
    if ([_tfUsername.text isEqualToString:@""]) {
        [self loginFacebook:sender];
        return;
    }
    [(VtcRoundedButton *)sender setEnabled:NO];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:VTCLocalizeString(@"Do you want to map account named %@ with Facebook?"), _tfUsername.text] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:VTCLocalizeString(@"Accept") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
        [[FacebookHandler sharedHandler] signInfromViewController:self completion:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            
            if (error) {
                [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
                [(VtcRoundedButton *)sender setEnabled:YES];
            } else if (result.isCancelled) {
                [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
                [(VtcRoundedButton *)sender setEnabled:YES];
            } else {
                VTCLog(@"Facebook token: %@", [[FBSDKAccessToken currentAccessToken] tokenString]);
                [[NetworkModal sharedModal] registerWithFacebook:[[FBSDKAccessToken currentAccessToken] tokenString] user:_tfUsername.text completion:^(BOOL status, id responsedObject, NSError *error) {
                    [(VtcRoundedButton *)sender setEnabled:YES];
                    [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
                    if (status) {
                        // appsflyer register With Facebook
                        [[VtcEventLogging manager] setAppsFlyerInAppEvent:kAppsFlyerEventTypeFBRegister values:[NSDictionary dictionaryWithObjectsAndKeys:@"1", kAppsFlyerEventTypeRegisterValueKey, nil]];
                        
                        [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeFacebook containerController:self.navigationController showWelcome:YES];
                        
                        [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeOther]];
                    }
                    else {
                        [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
                    }
                }];
            }
        }];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:VTCLocalizeString(@"No, thanks") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        UIAlertController *alert2 = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:VTCLocalizeString(@"Continue with Facebook?")] preferredStyle:UIAlertControllerStyleAlert];
        [alert2 addAction:[UIAlertAction actionWithTitle:VTCLocalizeString(@"Accept") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [(VtcRoundedButton *)sender setEnabled:YES];
            [self loginFacebook:sender];
        }]];
        [alert2 addAction:[UIAlertAction actionWithTitle:VTCLocalizeString(@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [(VtcRoundedButton *)sender setEnabled:YES];
            [alert2 dismissViewControllerAnimated:NO completion:nil];
        }]];
        [self presentViewController:alert2 animated:NO completion:nil];
    }]];
    [self presentViewController:alert animated:NO completion:nil];
}

- (void)loginFacebook:(id)sender {
    // login via facebook normally
    [(VtcRoundedButton *)sender setEnabled:NO];
    [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
    [[FacebookHandler sharedHandler] signInfromViewController:self completion:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        if (error) {
            [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
            [(VtcRoundedButton *)sender setEnabled:YES];
        } else if (result.isCancelled) {
            [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
            [(VtcRoundedButton *)sender setEnabled:YES];
        } else {
            VTCLog(@"Facebook token: %@", [[FBSDKAccessToken currentAccessToken] tokenString]);
            [[NetworkModal sharedModal] loginWithFacebook:[[FBSDKAccessToken currentAccessToken] tokenString] completion:^(BOOL status, id responsedObject, NSError *error) {
                [(VtcRoundedButton *)sender setEnabled:YES];
                [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
                if (status) {
                    [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeFacebook containerController:self.navigationController showWelcome:YES];
                    [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeOther]];
                    
                }
                else {
                    if (error.code == -500) {
                        VtcEnterUsernameViewController *controller = [VtcEnterUsernameViewController loadFromNibNamed:@"VtcEnterUsernameViewController"];
                        controller.authenType = AuthenTypeFacebook;
                        [self.navigationController pushViewController:controller animated:NO];
                    }
                    else {
                        [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
                    }
                }
            }];
        }
    }];
}

- (IBAction)onGoogle:(id)sender {
//    if ([_tfUsername.text isEqualToString:@""]) {
//        [self loginGoogle:sender];
//        return;
//    }
//    [(VtcRoundedButton *)sender setEnabled:NO];
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:VTCLocalizeString(@"Do you want to map account named %@ with Google?"), _tfUsername.text] preferredStyle:UIAlertControllerStyleAlert];
//    [alert addAction:[UIAlertAction actionWithTitle:VTCLocalizeString(@"Accept") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
//        [[GoogleServiceHandler sharedHandler] signIn:^(GIDGoogleUser *user, NSError *error) {
//            
//            if (error) {
//                [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
//                [(VtcRoundedButton *)sender setEnabled:YES];
//                return;
//            }
//            else if (user) {
//                // Perform any operations on signed in user here.
//                NSString *accessToken = user.authentication.accessToken; // Safe to send to the server
//                [[NetworkModal sharedModal] registerWithGoogle:accessToken user:_tfUsername.text completion:^(BOOL status, id responsedObject, NSError *error) {
//                    [(VtcRoundedButton *)sender setEnabled:YES];
//                    [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
//                    if (status) {
//                        // appsflyer register With Google
//                        [[VtcEventLogging manager] setAppsFlyerInAppEvent:kAppsFlyerEventTypeGGRegister values:[NSDictionary dictionaryWithObjectsAndKeys:@"1", kAppsFlyerEventTypeRegisterValueKey, nil]];
//                        
//                        [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeGoogle containerController:self.navigationController showWelcome:YES];
//                        
//                        [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeOther]];
//                    }
//                    else {
//                        [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
//                    }
//                }];
//            }
//            else {
//                [(VtcRoundedButton *)sender setEnabled:YES];
//            }
//        }];
//    }]];
//    [alert addAction:[UIAlertAction actionWithTitle:VTCLocalizeString(@"No, thanks") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//        UIAlertController *alert2 = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:VTCLocalizeString(@"Continue with Google?")] preferredStyle:UIAlertControllerStyleAlert];
//        [alert2 addAction:[UIAlertAction actionWithTitle:VTCLocalizeString(@"Accept") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            [(VtcRoundedButton *)sender setEnabled:YES];
//            [self loginGoogle:sender];
//        }]];
//        [alert2 addAction:[UIAlertAction actionWithTitle:VTCLocalizeString(@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//            [(VtcRoundedButton *)sender setEnabled:YES];
//            [alert2 dismissViewControllerAnimated:NO completion:nil];
//        }]];
//        [self presentViewController:alert2 animated:NO completion:nil];
//    }]];
//    [self presentViewController:alert animated:NO completion:nil];
}
//- (IBAction)quickSignupOnclick:(id)sender {
//    VtcAuthenNavController *navController = (VtcAuthenNavController *)self.navigationController;
//    VtcSignUpViewControllerQuick *controller = [VtcSignUpViewControllerQuick loadFromNibNamed:@"VtcSignUpViewControllerQuick"];
//    [navController popToRootViewControllerAnimated:true];
//    [navController changeRootController:controller];
//    //    [navController pushViewController:controller animated:true];
//}

- (void)loginGoogle:(id)sender {
//    [GoogleServiceHandler sharedHandler].loginController = self;
//    [(VtcRoundedButton *)sender setEnabled:NO];
//    [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
//    [[GoogleServiceHandler sharedHandler] signIn:^(GIDGoogleUser *user, NSError *error) {
//
//        if (error) {
//            [(VtcRoundedButton *)sender setEnabled:YES];
//            [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
//            return;
//        }
//        if (user) {
//            // Perform any operations on signed in user here.
//            NSString *accessToken = user.authentication.accessToken; // Safe to send to the server
//            [[NetworkModal sharedModal] loginWithGoogle:accessToken completion:^(BOOL status, id responsedObject, NSError *error) {
//                [(VtcRoundedButton *)sender setEnabled:YES];
//                [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
//                if (status) {
//                    [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeGoogle containerController:self.navigationController showWelcome:YES];
//                    [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeOther]];
//                }
//                else {
//                    if (error.code == -500) {
//                        VtcEnterUsernameViewController *controller = [VtcEnterUsernameViewController loadFromNibNamed:@"VtcEnterUsernameViewController"];
//                        controller.authenType = AuthenTypeGoogle;
//                        controller.openIDTokenString = accessToken;
//                        [self.navigationController pushViewController:controller animated:NO];
//                    }
//                    else {
//                        [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
//                    }
//                }
//            }];
//        }
//
//    }];
}

- (IBAction)onSignIn:(id)sender {
    NSLog(@"testahihi onSignIn");
    
    if (@available(iOS 13.0, *)) {
        VtcAuthenNavController *navController = (VtcAuthenNavController *)self.navigationController;
        VtcAuthenViewController *loginController = [VtcAuthenViewController loadFromNibNamed:@"VtcAuthenViewController"];
        //    if (_registeringName && _registeringPassword) {
        loginController.registeringName = _registeringName;
        loginController.registeringPassword = _registeringPassword;
        loginController.needAutoLogin = YES;
        //    }
        
        [navController popToRootViewControllerAnimated:true];
        //    [navController pushViewController: loginController animated:true];
        [navController changeRootController: loginController];
    }else{
        VtcAuthenNavController *navController = (VtcAuthenNavController *)self.navigationController;
        VtcAuthenViewController *controller = [VtcAuthenViewController loadFromNibNamed:@"VtcAuthenViewController"];
        controller.registeringName = _registeringName;
        controller.registeringPassword = _registeringPassword;
        controller.needAutoLogin = YES;
        
        [navController changeRootController:controller];
    }
    
    
}

// MARK: UITextField Delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.activeTextField = textField;
    return YES;
}

@end

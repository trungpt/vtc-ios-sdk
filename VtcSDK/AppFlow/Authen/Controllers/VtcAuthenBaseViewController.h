//
//  VtcAuthenBaseViewController.h
//  VtcSDK
//
//  Created by Kent Vu on 7/25/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScrollableViewController.h"

@interface VtcAuthenBaseViewController : ScrollableViewController

- (void) goToEnterPassword:(NSString *)username password:(NSString *)password;
- (void) goToEnterOTP:(NSString *)username password:(NSString *)password;
- (void) goToSignin:(NSString *)username password:(NSString *)password;
@end

////
////  EnterPassword
////  VtcSDK
////
////  Created by Kent Vu on 8/2/16.
////  Copyright © 2016 VTCIntecom. All rights reserved.
////
//
//#import "VtcAuthenBaseViewController.h"
//#import "VIDUser.h"
//
//@interface UpdatePlayerInfo : VtcAuthenBaseViewController
//
//@property (nonatomic) AuthenType authenType;
//@property (nonatomic, copy) NSString *openIDTokenString;
//@property (nonatomic, copy) NSString *registeringName;
//@property (nonatomic, copy) NSString *registeringPassword;
//- (void)sendRegisterRequest:(NSString *)step
//                     sender:(id)sender;
//@end

//
//  VtcFPEnterNewPasswordViewController.h
//  VtcSDK
//
//  Created by Kent Vu on 8/4/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcAuthenBaseViewController.h"

@interface VtcFPEnterNewPasswordViewController : VtcAuthenBaseViewController

@property (nonatomic, copy) NSString *username;
@property (nonatomic) BOOL resetByEmail;

@end

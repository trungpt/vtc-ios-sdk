//
//  VtcFPSelectMethodViewController.m
//  VtcSDK
//
//  Created by Kent Vu on 8/4/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcFPSelectMethodViewController.h"
#import "VtcFPEnterNewPasswordViewController.h"
#import "UIViewController+Additions.h"
#import "VtcAuthenNavController.h"
#import "VtcSignUpViewController.h"

@interface VtcFPSelectMethodViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UILabel *lblViaSMS;
@property (weak, nonatomic) IBOutlet UILabel *lblViaEmail;
@end

@implementation VtcFPSelectMethodViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.portraitTopSpace = [Global sharedInstance].gPortraitNavBarHeight+50.0;
    self.landscapeTopSpace = [Global sharedInstance].gLandscapeNavBarHeight+35.0;
    [self addCustomBackButton];
    [self addCustomTitleView];
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setupLanguage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) setupLanguage {
    self.lblTitle.text = [VTCLocalizeString(@"Forgot password") uppercaseString];
    [self.btnLogin setAttributedTitle:[[NSAttributedString alloc] initWithString:VTCLocalizeString(@"Login") attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}] forState:UIControlStateNormal];
    [self.btnRegister setAttributedTitle:[[NSAttributedString alloc] initWithString:VTCLocalizeString(@"Register") attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}] forState:UIControlStateNormal];
    self.lblViaSMS.text = VTCLocalizeString(@"Get back password via mobile");
    self.lblViaEmail.text = VTCLocalizeString(@"Get back password via email");
}

- (IBAction)onResetByOTP:(id)sender {
    VtcFPEnterNewPasswordViewController *controller = [VtcFPEnterNewPasswordViewController loadFromNibNamed:@"VtcFPEnterNewPasswordViewController"];
    controller.username = _username;
    controller.resetByEmail = NO;
    [self.navigationController pushViewController:controller animated:NO];
}

- (IBAction)onResetByEmail:(id)sender {
    VtcFPEnterNewPasswordViewController *controller = [VtcFPEnterNewPasswordViewController loadFromNibNamed:@"VtcFPEnterNewPasswordViewController"];
    controller.username = _username;
    controller.resetByEmail = YES;
    [self.navigationController pushViewController:controller animated:NO];
}

- (IBAction)onSignIn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (IBAction)onSignUp:(id)sender {
    VtcAuthenNavController *navController = (VtcAuthenNavController *)self.navigationController;
    VtcSignUpViewController *controller = [VtcSignUpViewController loadFromNibNamed:@"VtcSignUpViewController"];
    [navController changeRootController:controller];
}

@end

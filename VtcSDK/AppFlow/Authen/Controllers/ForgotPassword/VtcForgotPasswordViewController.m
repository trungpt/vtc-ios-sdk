//
//  VtcForgotPasswordViewController.m
//  VtcSDK
//
//  Created by Kent Vu on 7/26/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcForgotPasswordViewController.h"
#import "UIImage+Additions.h"
#import "UIViewController+Additions.h"
#import "VtcFPSelectMethodViewController.h"
#import "VtcLoginTextField.h"
#import "VtcSignUpViewController.h"
#import "VtcAuthenNavController.h"
#import "VtcRoundedButton.h"

@interface VtcForgotPasswordViewController () <UITextFieldDelegate>

    @property (weak, nonatomic) IBOutlet UILabel *lblTitleForgotPass;
    @property (nonatomic, weak) IBOutlet VtcLoginTextField *tfUsername;
@property (nonatomic, weak) IBOutlet VtcRoundedButton *btnContinue;
    @property (weak, nonatomic) IBOutlet UIButton *btnRegister;
    
@end

@implementation VtcForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.portraitTopSpace = [Global sharedInstance].gPortraitNavBarHeight+50.0;
    self.landscapeTopSpace = [Global sharedInstance].gLandscapeNavBarHeight+35.0;
    [self addCustomBackButton];
    [self addCustomTitleView];
    
    [_tfUsername setHandleDidEndOnExit:^{
        [self onContinue:_btnContinue];
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setupLanguage];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}
    
- (void)setupLanguage
{
    self.lblTitleForgotPass.text = [VTCLocalizeString(@"Forgot password") uppercaseString];
    self.tfUsername.placeholder = VTCLocalizeString(@"Username2");
    [self.btnRegister setAttributedTitle:[[NSAttributedString alloc] initWithString:VTCLocalizeString(@"Register") attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}] forState:UIControlStateNormal];
    [self.btnContinue setTitle:VTCLocalizeString(@"Continue") forState:UIControlStateNormal];
}

- (IBAction)onContinue:(id)sender {
    if ([_tfUsername.text isEqualToString:@""]) {
        [Utilities showNotification:VTCLocalizeString(@"Missing username")];
        [_tfUsername becomeFirstResponder];
        return;
    }
    
    VtcFPSelectMethodViewController *controller = [VtcFPSelectMethodViewController loadFromNibNamed:@"VtcFPSelectMethodViewController"];
    controller.username = _tfUsername.text;
    [self.navigationController pushViewController:controller animated:NO];
}

- (IBAction)onSignUp:(id)sender {
    VtcAuthenNavController *navController = (VtcAuthenNavController *)self.navigationController;
    VtcSignUpViewController *controller = [VtcSignUpViewController loadFromNibNamed:@"VtcSignUpViewController"];
    [navController changeRootController:controller];
}

// MARK: UITextField Delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.activeTextField = textField;
    return YES;
}


@end

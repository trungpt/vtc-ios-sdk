//
//  VtcFPSelectMethodViewController.h
//  VtcSDK
//
//  Created by Kent Vu on 8/4/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcAuthenBaseViewController.h"

@interface VtcFPSelectMethodViewController : VtcAuthenBaseViewController

@property (nonatomic, copy) NSString *username;

@end

//
//  VtcFPEnterNewPasswordViewController.m
//  VtcSDK
//
//  Created by Kent Vu on 8/4/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcFPEnterNewPasswordViewController.h"
#import "VtcLoginTextField.h"
#import "UIImage+Additions.h"
#import "NetworkModal+Authen.h"
#import "VtcAuthenNavController.h"
#import "VtcSignUpViewController.h"
#import "UIViewController+Additions.h"
#import "SmartParser.h"
#import "VtcRoundedButton.h"
#import "UIView+UpdateAutoLayoutConstraints.h"

@interface VtcFPEnterNewPasswordViewController () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfPassword;
@property (nonatomic, weak) IBOutlet UIView *bottomLinePassword;
@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfPasswordConfirm;
@property (nonatomic, weak) IBOutlet UIView *bottomLinePasswordConfirm;
@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfUsername;
@property (nonatomic, weak) IBOutlet UIView *bottomLineUsername;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *spacerFromUsernameToEmail;
@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfOTP;
@property (nonatomic, weak) IBOutlet VtcLoginTextField *tfEmail;
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UIButton *btnLogin;
@property (nonatomic, weak) IBOutlet UIButton *btnRegister;

@property (nonatomic, weak) IBOutlet UILabel *lbText1;
@property (nonatomic, weak) IBOutlet UILabel *lbText2;

@property (nonatomic, weak) IBOutlet VtcRoundedButton *btnConfirm;

@end

@implementation VtcFPEnterNewPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.portraitTopSpace = [Global sharedInstance].gPortraitNavBarHeight+50.0;
//    self.landscapeTopSpace = [Global sharedInstance].gLandscapeNavBarHeight+35.0;
    self.landscapeTopSpace = 20;
    [self addCustomBackButton];
    [self addCustomTitleView];
    
    [self updateTextForLabel];
    _tfUsername.text = _username;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    _tfEmail.hidden = !_resetByEmail;
    _tfOTP.hidden = _resetByEmail;
    
    [self settingLanguage];
    
    if (_resetByEmail) {
        [_tfPassword setConstraintConstant:0.0 forAttribute:NSLayoutAttributeHeight];
        _bottomLinePassword.hidden = YES;
        _bottomLinePasswordConfirm.hidden = YES;
        [_tfUsername setConstraintConstant:45.0 forAttribute:NSLayoutAttributeHeight];
        _tfUsername.hidden = NO;
        _bottomLineUsername.hidden = NO;
//        _tfPasswordConfirm.tfNext = _tfEmail;
        [_tfEmail setHandleDidEndOnExit:^{
            [self onChangePassword:_btnConfirm];
        }];
    }
    else {
        [_tfPassword setConstraintConstant:45.0 forAttribute:NSLayoutAttributeHeight];
        _bottomLinePassword.hidden = NO;
        _bottomLinePasswordConfirm.hidden = NO;
        [_tfUsername setConstraintConstant:0.0 forAttribute:NSLayoutAttributeHeight];
        _tfUsername.hidden = YES;
        _bottomLineUsername.hidden = YES;
        _spacerFromUsernameToEmail.constant = 0.0;
        _tfPasswordConfirm.tfNext = _tfOTP;
        [_tfOTP setHandleDidEndOnExit:^{
            [self onChangePassword:_btnConfirm];
        }];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
- (void) settingLanguage {
    self.lblTitle.text = [VTCLocalizeString(@"Forgot password") uppercaseString];
    [self.btnLogin setAttributedTitle:[[NSAttributedString alloc] initWithString:VTCLocalizeString(@"Login") attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}] forState:UIControlStateNormal];
    [self.btnRegister setAttributedTitle:[[NSAttributedString alloc] initWithString:VTCLocalizeString(@"Register") attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}] forState:UIControlStateNormal];
    [self.btnConfirm setTitle:[VTCLocalizeString(@"Change password") uppercaseString] forState:UIControlStateNormal];
    self.tfPassword.placeholder = VTCLocalizeString(@"Enter new password");
    self.tfOTP.placeholder = VTCLocalizeString(@"Enter OTP code");
    self.tfEmail.placeholder = VTCLocalizeString(@"Enter your email");
    self.tfUsername.placeholder = VTCLocalizeString(@"Username");
    self.tfPasswordConfirm.placeholder = VTCLocalizeString(@"Confirm new password");
}

// MARK: methods
- (IBAction)onSignIn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (IBAction)onSignUp:(id)sender {
    VtcAuthenNavController *navController = (VtcAuthenNavController *)self.navigationController;
    VtcSignUpViewController *controller = [VtcSignUpViewController loadFromNibNamed:@"VtcSignUpViewController"];
    [navController changeRootController:controller];
}

- (void)updateTextForLabel {
    /*
    NSDictionary *normalAttributes = @{
                                       NSFontAttributeName : [UIFont fontWithName:@"Roboto-Regular" size:14],
                                       NSForegroundColorAttributeName : ColorFromHEX(0x333333)
                                     };
    NSDictionary *boldAttributes = @{
                                     NSFontAttributeName : [UIFont fontWithName:@"Roboto-Bold" size:14],
                                     NSForegroundColorAttributeName : ColorFromHEX(0x333333)
                                   };
    NSDictionary *coloredBoldAttributes = @{
                                            NSFontAttributeName : [UIFont fontWithName:@"Roboto-Bold" size:14],
                                            NSForegroundColorAttributeName : ColorFromHEX(0xfd5207)
                                          };
    */ 
    _lbText1.text = @"";
    _lbText2.text = @"";
    if (_resetByEmail) {
        /*
        NSMutableAttributedString *line1 = [[NSMutableAttributedString alloc] init];
        [line1 appendAttributedString:[[NSAttributedString alloc] initWithString:@"OTP " attributes:coloredBoldAttributes]];
        [line1 appendAttributedString:[[NSAttributedString alloc] initWithString:@"đã được gửi vào địa chỉ " attributes:normalAttributes]];
        [line1 appendAttributedString:[[NSAttributedString alloc] initWithString:@"email gắn kết " attributes:coloredBoldAttributes]];
        [line1 appendAttributedString:[[NSAttributedString alloc] initWithString:@"với tài khoản. Hãy " attributes:normalAttributes]];
        [line1 appendAttributedString:[[NSAttributedString alloc] initWithString:@"check mail " attributes:coloredBoldAttributes]];
        [line1 appendAttributedString:[[NSAttributedString alloc] initWithString:@"để nhập mã" attributes:normalAttributes]];
        _lbText2.attributedText = line1;
        */
    }
    else {
        /*
        NSMutableAttributedString *line1 = [[NSMutableAttributedString alloc] init];
        [line1 appendAttributedString:[[NSAttributedString alloc] initWithString:@"Để lấy mã " attributes:normalAttributes]];
        [line1 appendAttributedString:[[NSAttributedString alloc] initWithString:@"OTP " attributes:coloredBoldAttributes]];
        [line1 appendAttributedString:[[NSAttributedString alloc] initWithString:@"bạn dùng " attributes:normalAttributes]];
        [line1 appendAttributedString:[[NSAttributedString alloc] initWithString:@"số điện thoại gắn kết" attributes:coloredBoldAttributes]];
        _lbText1.attributedText = line1;
        
        NSMutableAttributedString *line2 = [[NSMutableAttributedString alloc] init];
        [line2 appendAttributedString:[[NSAttributedString alloc] initWithString:@"Soạn tin: " attributes:boldAttributes]];
        [line2 appendAttributedString:[[NSAttributedString alloc] initWithString:@"EB OTP " attributes:coloredBoldAttributes]];
        [line2 appendAttributedString:[[NSAttributedString alloc] initWithString:@"gửi đến " attributes:normalAttributes]];
        [line2 appendAttributedString:[[NSAttributedString alloc] initWithString:@"1900 1530" attributes:coloredBoldAttributes]];
        _lbText2.attributedText = line2;
        */
        [[NetworkModal sharedModal] getOTPText:^(BOOL status, id responsedObject, NSError *error) {
            if (status) {
                @try {
                    NSString *htmlString = [[responsedObject firstObject] objectForKey:@"configValue"];
                    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUTF8StringEncoding]
                                                                                            options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                                                 documentAttributes:nil
                                                                                              error:nil];
                    _lbText1.attributedText = attributedString;
                } @catch (NSException *exception) {
                    VTCLog(@"Data không hợp lệ");
                } @finally {
                    
                }
            }
        }];
    }
}

- (IBAction)onChangePassword:(id)sender {
    if (_resetByEmail) {
        if ([_tfEmail.text isEqualToString:@""]) {
            [Utilities showNotification:VTCLocalizeString(@"Missing email")];
            [_tfEmail becomeFirstResponder];
            return;
        }
    }
    else {
        if ([_tfPassword.text isEqualToString:@""]) {
            [Utilities showNotification:VTCLocalizeString(@"Missing new password")];
            [_tfPassword becomeFirstResponder];
            return;
        }
        if ([_tfPasswordConfirm.text isEqualToString:@""]) {
            [Utilities showNotification:VTCLocalizeString(@"Please confirm the new password")];
            [_tfPasswordConfirm becomeFirstResponder];
            return;
        }
        
        if (![_tfPassword.text isEqualToString:_tfPasswordConfirm.text]) {
            [Utilities showNotification:VTCLocalizeString(@"Confirm password not matching")];
            [_tfPasswordConfirm becomeFirstResponder];
            return;
        }
        
        if ([_tfOTP.text isEqualToString:@""]) {
            [Utilities showNotification:VTCLocalizeString(@"Missing OTP code")];
            [_tfOTP becomeFirstResponder];
            return;
        }
    }
    
    [FWActivityIndicatorView showWhiteIndicatorInView:sender location:FWActivityIndicatorLocationRight];
    if (_resetByEmail) {
        [[NetworkModal sharedModal] resetPassword:@"" forUser:_username emailAddress:_tfEmail.text sign:nil completion:^(BOOL status, id responsedObject, NSError *error) {
            [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
            if (status) {
                [Utilities showNotification:VTCLocalizeString(@"Please check your mailbox to continue")];
            }
            else {
                [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
            }
            
            /*
            if (status) {
                NSString *extendCode = [SmartParser stringForKey:@"extend" from:responsedObject];
                if (extendCode) {
                    [[NetworkModal sharedModal] resetPassword:_tfPassword.text forUser:_username emailAddress:_tfEmail.text sign:extendCode completion:^(BOOL status, id responsedObject, NSError *error) {
                        
                        [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
                        if (status) {
                            [Utilities showNotification:@"Đặt lại mật khẩu thành công"];
                            [self.navigationController popToRootViewControllerAnimated:NO];
                        }
                        else {
                            [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
                        }
                    }];
                }
                else {
                    [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
                    [Utilities showNotification:@"Có lỗi xảy ra. Đặt lại mật khẩu không thành công"];
                }
            }
            else {
                [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
                [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
            }
            */ 
        }];
    }
    else {
        NSLog(@"\ntestahihi _resetByEmail = false\n");
        [[NetworkModal sharedModal] resetPassword:_tfPassword.text forUser:_username secureCode:_tfOTP.text completion:^(BOOL status, id responsedObject, NSError *error) {
            [FWActivityIndicatorView hideAllActivityIndicatorInView:sender];
            if (status) {
                [Utilities showNotification:VTCLocalizeString(@"Reset password successfully")];
                [self.navigationController popToRootViewControllerAnimated:NO];
            }
            else {
                [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
            }
        }];
    }
}

// MARK: UITextField Delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.activeTextField = textField;
    return YES;
}

@end

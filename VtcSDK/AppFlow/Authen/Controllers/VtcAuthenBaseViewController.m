//
//  VtcAuthenBaseViewController.m
//  VtcSDK
//
//  Created by Kent Vu on 7/25/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//
//VtcLoginBackupViewController
#import "VtcAuthenBaseViewController.h"
#import "SignUp/EnterPassword.h"
#import "SignUp/EnterOTP.h"
#import "VtcAuthenNavController.h"
#import "UIViewController+Additions.h"
#import "SignIn/VtcLoginViewController.h"
@interface VtcAuthenBaseViewController ()

@end

@implementation VtcAuthenBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (@available(iOS 13.0, *)) {
        self.modalInPresentation = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) goToSignin:(NSString *)username password:(NSString *)password{
    if (@available(iOS 13.0, *)) {
        VtcAuthenNavController *navController = (VtcAuthenNavController *)self.navigationController;
            VtcLoginViewController *loginController = [VtcLoginViewController loadFromNibNamed:@"VtcLoginViewController"];
            //    if (_registeringName && _registeringPassword) {
            loginController.registeringName = username;
            loginController.registeringPassword = password;
            
        //    NSLog(@"testahihi %@",navController.viewControllers.firstObject.nibName);
        //    navController.viewcontrolers
        //    [navController popToRootViewControllerAnimated:true];
        //    [navController pushViewController:loginController animated:true];
            [navController changeRootController:loginController];
            //    }
    }else{
        VtcAuthenNavController *navController = (VtcAuthenNavController *)self.navigationController;
        VtcLoginViewController *controller = [VtcLoginViewController loadFromNibNamed:@"VtcLoginViewController"];
        [navController changeRootController:controller];
    }
}
- (void) goToEnterPassword:(NSString *)username password:(NSString *)password{
    //Chuyển sang màn hình nhập pass
//    NSLog(@"testahihi goToEnterPassword");
    VtcAuthenNavController *navController = (VtcAuthenNavController *)self.navigationController;
    EnterPassword *controller = [EnterPassword loadFromNibNamed:@"EnterPassword"];
    controller.registeringName = username;
    controller.registeringPassword = password;
    [navController popViewControllerAnimated:false];
    [navController pushViewController:controller animated:true];
}
- (void) goToEnterOTP:(NSString *)username password:(NSString *)password{
    //Chuyển sang màn hình nhập pass
//    NSLog(@"testahihi goToEnterOTP");
    VtcAuthenNavController *navController = (VtcAuthenNavController *)self.navigationController;
    EnterOTP *controller = [EnterOTP loadFromNibNamed:@"EnterOTP"];
    controller.registeringName = username;
    controller.registeringPassword = password;
    [navController pushViewController:controller animated:true];
}
@end

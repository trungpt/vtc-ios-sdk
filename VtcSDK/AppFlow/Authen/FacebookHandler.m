//
//  FacebookHandler.m
//  VtcSDK
//
//  Created by Kent Vu on 7/27/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "FacebookHandler.h"

@interface FacebookHandler ()

@property (nonatomic, strong) FBSDKLoginManager *loginManager;

@end

@implementation FacebookHandler

+ (FacebookHandler *)sharedHandler {
    static FacebookHandler *sharedHandler_ = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedHandler_ = [[FacebookHandler alloc] init];
    });
    return sharedHandler_;
}

- (instancetype)init
{
    if (self = [super init])
    {
        self.loginManager = [[FBSDKLoginManager alloc] init];
    }
    return self;
}

- (void)signInfromViewController:(UIViewController *)viewController completion:(void (^)(FBSDKLoginManagerLoginResult *result, NSError *error))completion {
    if (_loginManager == nil) {
        self.loginManager = [[FBSDKLoginManager alloc] init];
    }
    [_loginManager logOut];
//    _loginManager.loginBehavior = FBSDKLoginBehaviorWeb;
    [_loginManager logInWithPermissions: @[@"public_profile", @"email"]
                         fromViewController:viewController
                                    handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                        completion(result, error);
                                    }];
}

- (void)signOut {
    if (_loginManager) {
        [_loginManager logOut];
    }
}

- (NSString *)fbAccessToken
{
    return [FBSDKAccessToken currentAccessToken].tokenString;
}
- (BOOL)fbTokenIsActive
{
    return [FBSDKAccessToken currentAccessToken];
}
@end

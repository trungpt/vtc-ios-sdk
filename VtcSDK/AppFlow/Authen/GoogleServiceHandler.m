////
////  GoogleServiceHandler.m
////  VtcSDK
////
////  Created by Kent Vu on 7/27/16.
////  Copyright © 2016 VTCIntecom. All rights reserved.
////
//
//#import "GoogleServiceHandler.h"
//
//typedef void (^VtcGoogleSignInBlock)(GIDGoogleUser *user, NSError *error);
//
//@interface GoogleServiceHandler () <GIDSignInUIDelegate, GIDSignInDelegate>
//
//@property (nonatomic, copy) VtcGoogleSignInBlock completionBlock;
//
//@end
//
//@implementation GoogleServiceHandler
//
//+ (GoogleServiceHandler *)sharedHandler {
//    static GoogleServiceHandler *sharedHandler_ = nil;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        sharedHandler_ = [[GoogleServiceHandler alloc] init];
//        [sharedHandler_ customInit];
//    });
//    return sharedHandler_;
//}
//
//- (void)customInit {
//    [GIDSignIn sharedInstance].uiDelegate = self;
//    [GIDSignIn sharedInstance].delegate = self;
//}
//
//// MARK: methods
//
//- (void)signIn:(void (^)(GIDGoogleUser *user, NSError *error))completion {
//    self.completionBlock = completion;
//    [[GIDSignIn sharedInstance] signIn];
//}
//
//- (void)signOut {
//    [[GIDSignIn sharedInstance] signOut];
//}
//
//- (NSString *)ggAccessToken
//{
//    return [[GIDSignIn sharedInstance] currentUser].authentication.accessToken;
//}
//// MARK: GoogleSignIn delegates
//
//- (void)signIn:(GIDSignIn *)signIn
//didSignInForUser:(GIDGoogleUser *)user
//     withError:(NSError *)error {
//    if (_completionBlock) {
//        _completionBlock(user, nil);
//    }
//}
//
//- (void)signIn:(GIDSignIn *)signIn
//didDisconnectWithUser:(GIDGoogleUser *)user
//     withError:(NSError *)error {
//    // Perform any operations when the user disconnects from app here.
//    [[GIDSignIn sharedInstance] signOut];
//    if (_completionBlock) {
//        _completionBlock(nil, error);
//    }
//}
//
//// Stop the UIActivityIndicatorView animation that was started when the user
//// pressed the Sign In button
//- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
//    //    [myActivityIndicator stopAnimating];
//    VTCLog(@"%@", error.description);
//}
//
//// Present a view that prompts the user to sign in with Google
//- (void)signIn:(GIDSignIn *)signIn
//presentViewController:(UIViewController *)viewController {
//    if (_loginController) {
//        [_loginController presentViewController:viewController animated:NO completion:^{
//            
//        }];
//    }
//}
//
//// Dismiss the "Sign in with Google" view
//- (void)signIn:(GIDSignIn *)signIn
//dismissViewController:(UIViewController *)viewController {
//    if (_loginController) {
//        [_loginController.navigationController dismissViewControllerAnimated:NO completion:^{
//            _completionBlock(nil, nil);
//        }];
//    }
//}
//
//@end

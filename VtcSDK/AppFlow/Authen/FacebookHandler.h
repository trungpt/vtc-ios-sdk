//
//  FacebookHandler.h
//  VtcSDK
//
//  Created by Kent Vu on 7/27/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface FacebookHandler : NSObject

+ (FacebookHandler *)sharedHandler;

- (void)signInfromViewController:(UIViewController *)viewController completion:(void (^)(FBSDKLoginManagerLoginResult *result, NSError *error))completion;
- (void)signOut;
- (BOOL) fbTokenIsActive;
- (NSString *) fbAccessToken;
@end

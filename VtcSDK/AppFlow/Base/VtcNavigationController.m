//
//  VtcNavigationController.m
//  VtcSDK
//
//  Created by Kent Vu on 8/2/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcNavigationController.h"

@interface VtcNavigationController ()

@end

@implementation VtcNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [[self navigationBar] setBarTintColor:[Global sharedInstance].globalMainColor];
    [[self navigationBar] setTintColor:ColorFromHEX(0x333333)];
    CGFloat currentBarHeight = self.navigationBar.bounds.size.height;
    [[self navigationBar] setTitleVerticalPositionAdjustment:(44-currentBarHeight)/2 forBarMetrics:UIBarMetricsDefault];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationDidChange:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
}

- (void)orientationDidChange:(NSNotification *)notification {
    CGFloat currentBarHeight = self.navigationBar.bounds.size.height;
    if (![[UIDevice currentDevice].model containsString:@"iPad"]) {
        if (currentBarHeight == [Global sharedInstance].gPortraitNavBarHeight) {
            currentBarHeight = [Global sharedInstance].gLandscapeNavBarHeight;
        }
        else {
            currentBarHeight = [Global sharedInstance].gPortraitNavBarHeight;
        }
    }
    [[self navigationBar] setTitleVerticalPositionAdjustment:(44-currentBarHeight)/2 forBarMetrics:UIBarMetricsDefault];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)changeRootController:(UIViewController *)newRootVC {
    UIViewController *currentRootVC = self.viewControllers.firstObject;
    newRootVC.view.frame = currentRootVC.view.frame;
    [self setViewControllers:@[newRootVC] animated:NO];
}

- (void)changeRootController:(UIViewController *)newRootVC animation:(UIViewAnimationOptions)animationType  {
    UIViewController *currentRootVC = self.viewControllers.firstObject;
    newRootVC.view.frame = currentRootVC.view.frame;
    
    [UIView transitionWithView:self.view
                      duration:0.7
                       options:animationType
                    animations:^{
                        // do something
                        self.viewControllers = @[newRootVC];
                    }
                    completion:^(BOOL finished) {
                        
                    }];
    
}

@end

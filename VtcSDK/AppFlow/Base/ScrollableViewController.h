//
//  ScrollableViewController.h
//  VtcSDK
//
//  Created by Kent Vu on 7/25/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ScrollableViewController : UIViewController {
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIScrollView *mainScrollView;
    __weak IBOutlet UIView *scrollContentView;
    __weak IBOutlet UIView *hiddenTopObject;
}

@property (nonatomic, weak) UITextField *activeTextField;
@property (nonatomic) IBInspectable CGFloat portraitTopSpace;
@property (nonatomic) IBInspectable CGFloat landscapeTopSpace;

@property (nonatomic, strong) UIBarButtonItem *backBtn;
@property (nonatomic, strong) UIBarButtonItem *closeBtn;

- (void)addCustomBackButton;
- (void)addCustomTitleView;
- (void)addCustomCloseButton;
- (void)onBackBtn;
- (void)onCloseBtn;
- (void)orientationDidChange:(NSNotification *)notification;

@end

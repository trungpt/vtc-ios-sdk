//
//  VtcNavigationController.h
//  VtcSDK
//
//  Created by Kent Vu on 8/2/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VtcNavigationController : UINavigationController

- (void)changeRootController:(UIViewController *)newRootVC;
- (void)changeRootController:(UIViewController *)newRootVC animation:(UIViewAnimationOptions)animationType;

@end

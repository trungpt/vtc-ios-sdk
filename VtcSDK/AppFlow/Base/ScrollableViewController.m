//
//  ScrollableViewController.m
//  VtcSDK
//
//  Created by Kent Vu on 7/25/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "ScrollableViewController.h"
#import "UIImage+Additions.h"
#import "UIView+UpdateAutoLayoutConstraints.h"

@interface ScrollableViewController () <UINavigationControllerDelegate, UIGestureRecognizerDelegate>

@end

@implementation ScrollableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (containerView) {
        CGSize screenSize = [UIScreen mainScreen].bounds.size;
        CGFloat width = (screenSize.width > screenSize.height) ? screenSize.height : screenSize.width;
        if ([[UIDevice currentDevice].model containsString:@"iPad"]) {
            width = 320.0; //414
        }
        [containerView setConstraintConstant:width forAttribute:NSLayoutAttributeWidth];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleKeyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleKeyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationDidChange:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    
    [self updateTopSpace];
    [self updateBarItemsVerticalPosition:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.navigationController) {
        if (self.navigationController.viewControllers.count==1) {
            self.navigationController.interactivePopGestureRecognizer.delegate = nil;
            self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        }
        else {
            self.navigationController.interactivePopGestureRecognizer.delegate = self;
            self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        }
        
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
}

- (void)orientationDidChange:(NSNotification *)notification {
//    self.navigationItem.titleView = nil;
//    [self addCustomTitleView];
    [self updateTopSpace];
    if ([[UIDevice currentDevice].model containsString:@"iPad"]) {
        [self updateBarItemsVerticalPosition:NO];
    }
    else
        [self updateBarItemsVerticalPosition:YES];
    
}

- (void)updateBarItemsVerticalPosition:(BOOL)reverse {
    CGFloat currentBarHeight = self.navigationController.navigationBar.bounds.size.height;
    if (reverse) {
        if (currentBarHeight == [Global sharedInstance].gPortraitNavBarHeight) {
            currentBarHeight = [Global sharedInstance].gLandscapeNavBarHeight;
        }
        else {
            currentBarHeight = [Global sharedInstance].gPortraitNavBarHeight;
        }
    }
    
    VTCLog(@"currentBarHeight = %f", currentBarHeight);
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if (UIInterfaceOrientationIsPortrait(orientation)) {
        if (_backBtn) {
            [_backBtn setBackgroundVerticalPositionAdjustment:(44-[Global sharedInstance].gPortraitNavBarHeight)/2 forBarMetrics:UIBarMetricsDefault];
        }
        if (_closeBtn) {
            [_closeBtn setBackgroundVerticalPositionAdjustment:(44-[Global sharedInstance].gPortraitNavBarHeight)/2 forBarMetrics:UIBarMetricsDefault];
        }
    }
    else {
        if (_backBtn) {
            [_backBtn setBackgroundVerticalPositionAdjustment:([Global sharedInstance].gLandscapeNavBarHeight-32)/2 forBarMetrics:UIBarMetricsDefault];
        }
        if (_closeBtn) {
            [_closeBtn setBackgroundVerticalPositionAdjustment:([Global sharedInstance].gLandscapeNavBarHeight-32)/2 forBarMetrics:UIBarMetricsDefault];
        }
    }
}

// MARK: custom methods
- (void)addCustomBackButton {
    self.backBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage vtcImageNamed:@"back"] style:UIBarButtonItemStyleDone target:self action:@selector(onBackBtn)];
    CGFloat currentBarHeight = self.navigationController.navigationBar.bounds.size.height;
    self.navigationItem.leftBarButtonItem = _backBtn;
    [_backBtn setBackgroundVerticalPositionAdjustment:(44-currentBarHeight)/2 forBarMetrics:UIBarMetricsDefault];
}

- (void)addCustomCloseButton {
    self.closeBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage vtcImageNamed:@"btn_close_white"] style:UIBarButtonItemStylePlain target:self action:@selector(onCloseBtn)];
    CGFloat currentBarHeight = self.navigationController.navigationBar.bounds.size.height;
    self.navigationItem.rightBarButtonItem = _closeBtn;
    [_closeBtn setBackgroundVerticalPositionAdjustment:(44-currentBarHeight)/2 forBarMetrics:UIBarMetricsDefault];
}

- (void)onBackBtn {
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:NO];
    }
    else {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

- (void)onCloseBtn {
    if (self.navigationController) {
        [self.navigationController dismissViewControllerAnimated:NO completion:nil];
    }
    else {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

- (void)addCustomTitleView {
    UIImageView *titleView_ = [[UIImageView alloc] initWithImage:[UIImage vtcImageNamed:@"vtc-logo-light"]];
    [titleView_ sizeToFit];
    self.navigationItem.titleView = titleView_;
}

- (void)updateTopSpace {
    if (hiddenTopObject) {
        CGFloat topSpace = 0.0;
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        VTCLog(@"orientation = %ld", (long)orientation);
        if(orientation == UIInterfaceOrientationLandscapeRight ||
           orientation == UIInterfaceOrientationLandscapeLeft) {
            // handle something for landscape
            if ([[UIDevice currentDevice].model containsString:@"iPad"]) {
                topSpace = _portraitTopSpace;
            }
            else
                topSpace = _landscapeTopSpace;
        }
        else {
            // handle something for portrait
            topSpace = _portraitTopSpace;
        }
        
        [hiddenTopObject setConstraintConstant:topSpace forAttribute:NSLayoutAttributeHeight];
        [self.view layoutSubviews];
    }
}

- (void)setPortraitTopSpace:(CGFloat)portraitTopSpace {
    _portraitTopSpace = portraitTopSpace;
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if(orientation == UIInterfaceOrientationPortraitUpsideDown ||
       orientation == UIInterfaceOrientationPortrait) {
        // handle something for landscape
        if (hiddenTopObject) {
            [hiddenTopObject setConstraintConstant:_portraitTopSpace forAttribute:NSLayoutAttributeHeight];
        }
    }
}

- (void)setLandscapeTopSpace:(CGFloat)landscapeTopSpace {
    _landscapeTopSpace = landscapeTopSpace;
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if(orientation == UIInterfaceOrientationLandscapeRight ||
       orientation == UIInterfaceOrientationLandscapeLeft) {
        // handle something for landscape
        if (hiddenTopObject) {
            if (![[UIDevice currentDevice].model containsString:@"iPad"]) {
                [hiddenTopObject setConstraintConstant:_landscapeTopSpace forAttribute:NSLayoutAttributeHeight];
            }
        }
    }
}

// MARK: observer methods

- (void)handleKeyboardDidShow:(NSNotification *)notification {
//    NSAssert(_activeTextField, @"Active textfield did not set yet");
    if (!_activeTextField) return;
    CGSize kbSize = [[notification.userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
    mainScrollView.contentInset = edgeInsets;
    mainScrollView.scrollIndicatorInsets = edgeInsets;
    CGRect contentRect = scrollContentView.frame;
    contentRect.size.height = contentRect.size.height - kbSize.height;
    if (CGRectContainsRect(contentRect, _activeTextField.frame)) {
        [mainScrollView scrollRectToVisible:_activeTextField.frame animated:NO];
    }
    
}

- (void)handleKeyboardDidHide:(NSNotification *)notification {
    mainScrollView.contentInset = UIEdgeInsetsZero;
    mainScrollView.scrollIndicatorInsets = UIEdgeInsetsZero;
}

@end

//
//  VtcEventLogging.m
//  VtcSDK
//
//  Created by Kent Vu on 7/29/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcEventLogging.h"
#import "NetworkModal+Tracking.h"
#import "NetworkModal+Info.h"
#import <AdSupport/ASIdentifierManager.h>
#import <AppsFlyerLib/AppsFlyerTracker.h>
#import "SmartParser.h"
#import "SDKManager.h"
#import "Firebase.h"

@interface VtcEventLogging () <AppsFlyerTrackerDelegate>

@property (nonatomic, strong) NSMutableArray *eventQueue;
@property (nonatomic, copy) NSURL *cachedOpenURL;
@property (nonatomic) BOOL requestedGAKey;

@end

@implementation VtcEventLogging

+ (VtcEventLogging *)manager {
    static VtcEventLogging *manager_ = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager_ = [[VtcEventLogging alloc] init];
        [manager_ customInit];
    });
    return manager_;
}

- (void)customInit {
    self.hitType = TrackingHitTypeEvent;
    self.eventQueue = [[NSMutableArray alloc] init];
}

- (void)setCurrentGAId:(NSString *)currentGAId {
    _currentGAId = currentGAId;
//    self.tracker = [[GAI sharedInstance] trackerWithName:@"MobileSDK"
//                                              trackingId:_currentGAId];
//    [_tracker setAllowIDFACollection:YES];
//    _currentGAClientId = [_tracker get:kGAIClientId];
    
    if (_cachedOpenURL) {
        [self hitCampaignParametersFromUrl:_cachedOpenURL];
        _cachedOpenURL = nil;
    }
}

- (NSString *)getCurrentGAId {
    if (_currentGAId && ![_currentGAId isKindOfClass:[NSNull class]]) {
        return _currentGAId;
    }
    else {
        return @"";
    }
}

- (NSString *)getCurrentGAClientId {
    if (_currentGAClientId && ![_currentGAClientId isKindOfClass:[NSNull class]]) {
        return _currentGAClientId;
    }
    else {
        return @"";
    }
}

- (void)startGoogleConversionWithID:(NSString *)idString label:(NSString *)label value:(NSString *)value {
    [VtcEventLogging manager].gcId = idString;
    [VtcEventLogging manager].gcLabel = label;
    [VtcEventLogging manager].gcValue = value;
}

- (void)handleApplication:(UIApplication *)application openURL:(NSURL *)url
        sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
//    if (_tracker) {
//        [self hitCampaignParametersFromUrl:url];
//    }
//    else {
        self.cachedOpenURL = url;
//    }
}

- (void)hitCampaignParametersFromUrl:(NSURL *)url {
//    NSString *urlString = [url absoluteString];
//    GAIDictionaryBuilder *hitParams = [[GAIDictionaryBuilder alloc] init];
//    [hitParams setCampaignParametersFromUrl:urlString];
//    NSDictionary *hitParamsDict = [hitParams build];
//    [_tracker set:kGAIScreenName value:@"screen init"];
//    [_tracker send:[[[GAIDictionaryBuilder createScreenView] setAll:hitParamsDict] build]];
}

- (void)pushEventToGAQueue:(NSMutableDictionary *)event {
    VTCLog(@"current GA id: %@", [[VtcEventLogging manager] getCurrentGAId]);
    NSLog(@"current GA id: %@", [[VtcEventLogging manager] getCurrentGAId]);
    if ([[[VtcEventLogging manager] getCurrentGAId] isEqualToString:@""]) {
        [_eventQueue addObject:event];
        [self requestGAGCInfo:0];
    }
    else {
//        [_tracker send:event];
//        [[GAI sharedInstance] dispatch];
    }
}

- (void)requestGAGCInfo:(NSInteger)counter {
    if (counter >= 3) return;
    if (_requestedGAKey) return;
    _requestedGAKey = YES;
    NSLog(@"GET GA Info");
    [[NetworkModal sharedModal] getGAInfo:^(BOOL status, id responsedObject, NSError *error) {
        VTCLog(@"GA ID = %@", responsedObject);
        NSLog(@"GA response: %@", responsedObject);
        _requestedGAKey = NO;
        
        if (status) {
            //get recaptcha sitekey
            @try {
                NSString *gaId = [SmartParser stringForKey:@"sitekeyRecapchaIos" from:responsedObject];
                if (gaId && ![gaId isEqualToString:@""]) {
                    [SDKManager defaultManager].sitekeyRecapchaIos = gaId;
//                    NSLog(@"<VtcSDK> sitekeyRecapchaIos=%@", [SDKManager defaultManager].sitekeyRecapchaIos);
                }
                
            } @catch (NSException *exception) {
                VTCLog(@"can not get sitekey key with error: %@", exception.description);
            } @finally {
                
            }
            // Google Analytics
            @try {
                NSString *gaId = [SmartParser stringForKey:@"gaTrackingId" from:responsedObject];
                if (gaId && ![gaId isEqualToString:@""]) {
                    NSLog(@"<VtcSDK> sending GoogleAnalytics: id=%@", gaId);
                    [VtcEventLogging manager].currentGAId = gaId;
                    for (NSMutableDictionary *eve in _eventQueue) {
//                        [_tracker send:eve];
//                        [[GAI sharedInstance] dispatch];
                    }
                }
                
            } @catch (NSException *exception) {
                VTCLog(@"can not get GA key with error: %@", exception.description);
            } @finally {
                
            }
            
            // Google Conversion
            @try {
                NSDictionary *conversionData = [SmartParser objectForKey:@"conversion" from:responsedObject];
                if (conversionData) {
                    NSString *conversionId = [SmartParser stringForKey:@"conversionId" from:conversionData];
                    NSString *conversionLabel = [SmartParser stringForKey:@"label" from:conversionData];
                    NSString *conversionValue = [SmartParser stringForKey:@"value" from:conversionData];
                    if (conversionId && conversionLabel && conversionValue) {
                        NSLog(@"<VtcSDK> sending GoogleConversion: id=%@ - label=%@ - value=%@", conversionId, conversionLabel, conversionValue);
                        [self startGoogleConversionWithID:conversionId label:conversionLabel value:conversionValue];
                    }
                }
                
            } @catch (NSException *exception) {
                VTCLog(@"can not get GC id with error: %@", exception.description);
            } @finally {
                
            }
            
            // Config login Facebook, Google
            @try {
                NSNumber *fbEnable = [SmartParser objectForKey:@"showFacebookButton" from:responsedObject];
                NSNumber *ggEnable = [SmartParser objectForKey:@"showGoogleButton" from:responsedObject];
                if (fbEnable && ggEnable) {
                    [Global sharedInstance].enableFacebookLogin = [fbEnable boolValue];
                    [Global sharedInstance].enableGoogleLogin = [ggEnable boolValue];
                }
            } @catch (NSException *exception) {
                VTCLog(@"no config for FB, GG authen. Use the default value.");
            } @finally {
                // push a notification
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationGotFacebookGoogleAuthenConfig object:nil];
            }
        }
        else {
            [self requestGAGCInfo:counter+1];
        }
    }];
    
}

- (void)hitActivity:(APICategoryType)activity
            hitType:(TrackingHitType)hitType
           category:(GACategoryType)category
             action:(GAActionType)action
              label:(GALabelType)label
              value:(NSNumber *)value
         completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    [self hitCustomActivity:activity category:[VtcEventLogging getGACategoryTypeString:category] action:[VtcEventLogging getGAActionTypeString:action] label:[VtcEventLogging getGALabelTypeString:label] value:value completion:completionBlock];
}

- (void)hitCustomActivity:(NSInteger)activity
                 category:(NSString *)category
                   action:(NSString *)action
                    label:(NSString *)label
                    value:(NSNumber *)value
               completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    // hit to GA
//    NSMutableDictionary *event = [[GAIDictionaryBuilder createEventWithCategory:category
//                                                                         action:action
//                                                                          label:label
//                                                                          value:value] build];
    [self pushEventToGAQueue:@{}];
    
    // log to server
    [[NetworkModal sharedModal] logActivity:activity action:action category:category completion:^(BOOL status, id responsedObject, NSError *error) {
        if (status) {
            VTCLog(@"hit activity %@ successed", [VtcEventLogging getAPICategoryTypeString:activity]);
        }
        else {
            VTCLog(@"hit activity %@ failed", [VtcEventLogging getAPICategoryTypeString:activity]);
        }
        if (completionBlock) {
            completionBlock(status, responsedObject, error);
        }
    }];
}

- (void)hitActivity:(APICategoryType)activity extendData:(NSString *)extend amount:(NSInteger)amountNumber forUser:(NSString *)userName userId:(NSString *)userId completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    [[NetworkModal sharedModal] logActivity:activity extendData:extend amount:amountNumber forUser:userName userId:userId completion:^(BOOL status, id responsedObject, NSError *error) {
        if (status) {
            VTCLog(@"hit activity %@ successed", [VtcEventLogging getAPICategoryTypeString:activity]);
        }
        else {
            VTCLog(@"hit activity %@ failed", [VtcEventLogging getAPICategoryTypeString:activity]);
        }
        if (completionBlock) {
            completionBlock(status, responsedObject, error);
        }
    }];
}

- (void)logStartInAppWithOrderNo:(NSString *)orderNo forUser:(NSString *)userName userId:(NSString *)userId completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    [[NetworkModal sharedModal] logStartInAppWithOrderNo:orderNo forUser:userName userId:userId completion:^(BOOL status, id responsedObject, NSError *error) {
        if (status) {
            VTCLog(@"log finish inapp successed");
        }
        else {
            VTCLog(@"log finish inapp failed");
        }
        if (completionBlock) {
            completionBlock(status, responsedObject, error);
        }
    }];
}

- (void)logFinishInAppWithOrderNo:(NSString *)orderNo forUser:(NSString *)userName userId:(NSString *)userId completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    [[NetworkModal sharedModal] logFinishInAppWithOrderNo:orderNo forUser:userName userId:userId completion:^(BOOL status, id responsedObject, NSError *error) {
        if (status) {
            VTCLog(@"log finish inapp successed");
        }
        else {
            VTCLog(@"log finish inapp failed");
        }
        if (completionBlock) {
            completionBlock(status, responsedObject, error);
        }
    }];
}

// MARK: GACategoryType

+ (NSString *)getGACategoryTypeString:(GACategoryType)type {
    switch (type) {
        case GACategoryTypeAuthen:
            return @"authen";
            break;
        case GACategoryTypePayment:
            return @"payment";
            break;
        case GACategoryTypeTracking:
            return @"tracking";
            break;
            
        default: // GACategoryTypeInAppEvent
            return @"inappEvent";
            break;
    }
}

+ (NSString *)getGAActionTypeString:(GAActionType)type {
    switch (type) {
        case GAActionTypeFirstOpen:
            return @"firstOpen";
            break;
        case GAActionTypeOpenApp:
            return @"openApp";
            break;
        case GAActionTypeCloseApp:
            return @"closeApp";
            break;
        case GAActionTypeCrashApp:
            return @"crash";
            break;
        case GAActionTypeInstalledApp:
            return @"installed";
            break;
        case GAActionTypeLogin:
            return @"login";
            break;
        case GAActionTypeRegister:
            return @"register";
            break;
        case GAActionTypeUTMCampaign:
            return @"utm_campaign";
            break;
        case GAActionTypeUTMMedium:
            return @"utm_medium";
            break;
        case GAActionTypeUTMSource:
            return @"utm_source";
            break;
        default: // GAActionTypeInGame
            return @"ingame";
            break;
    }
}

+ (NSString *)getGALabelTypeString:(GALabelType)type {
    switch (type) {
        case GALabelTypeVTCID:
            return @"vtcid";
            break;
        case GALabelTypeVTC:
            return @"vtc";
            break;
        case GALabelTypeFacebook:
            return @"fb";
            break;
        case GALabelTypeApple:
        return @"apple";
        break;
        case GALabelTypeGoogle:
            return @"google";
            break;
        case GALabelTypeUTM:
            return @"value";
            break;
        case GALabelTypeVTCPay:
            return @"vtcpay";
            break;
        case GALabelTypeInAppPurchase:
            return @"Inapp Purchase";
            break;
        default:
            return @"";
            break;
    }
}

+ (NSString *)getAPICategoryTypeString:(APICategoryType)type {
    switch (type) {
        case APICategoryTypeFirstOpen:
            return @"FIRST_OPEN";
            break;
        case APICategoryTypeOpen:
            return @"OPEN";
            break;
        case APICategoryTypeCloseApp:
            return @"CLOSE_APP";
            break;
        case APICategoryTypeCrashApp:
            return @"CRASH_APP";
            break;
        case APICategoryTypeClick:
            return @"CLICK";
            break;
        case APICategoryTypePayment:
            return @"PAYMENT";
            break;
        case APICategoryTypeInstall:
            return @"INSTALL";
            break;
        case APICategoryTypeLoginVTC:
            return @"LOGIN_VTC";
            break;
        case APICategoryTypeLoginGoogle:
            return @"LOGIN_GOOGLE";
            break;
        case APICategoryTypeLoginFacebook:
            return @"LOGIN_FB";
            break;
            
        default:
            return @"OTHER";
            break;
    }
}

// - MARK: AppsFlyer
- (void)configAppsFlyer {
    [self configAppsFlyerDevKey:[[VtcConfig defaultConfig] appsFlyerDevKey] appleAppID:[[VtcConfig defaultConfig] appStoreID]];
}

- (void)configAppsFlyerDevKey:(NSString *)devKey appleAppID:(NSString *)storeAppId {
    if (!devKey || !storeAppId) return;
    if ([devKey isEqualToString:@""] || [storeAppId isEqualToString:@""]) return;
    [AppsFlyerTracker sharedTracker].appsFlyerDevKey = devKey;
    [AppsFlyerTracker sharedTracker].appleAppID = storeAppId;
    [AppsFlyerTracker sharedTracker].delegate = self;
}

- (void)setAppsFlyerInAppEvent:(NSString *)eventName values:(NSDictionary *)valueOfEvent {
    [[AppsFlyerTracker sharedTracker] trackEvent:eventName withValues:valueOfEvent];
}

// convert UTM info
-(void)onConversionDataReceived:(NSDictionary*)installData {
    
    id status = [installData objectForKey:@"af_status"];
    if([status isEqualToString:@"Non-organic"]) {
        id sourceID = [installData objectForKey:@"media_source"];
        id campaign = [installData objectForKey:@"campaign"];
        NSLog(@"This is a none organic install. Media source: %@  Campaign: %@",sourceID,campaign);
        [[Global sharedInstance] setUtmString:[NSString stringWithFormat:@"utm_source=%@&utm_campaign=%@", sourceID, campaign]];
    } else if([status isEqualToString:@"Organic"]) {
        NSLog(@"This is an organic install.");
    }
}
-(void)onConversionDataRequestFailure:(NSError *) error {
    NSLog(@"%@",error);
}

// -MARK: Checking With FireBase
+ (void)checkingByFireBaseWithEventName:(NSString *)eventName {
    
    NSDictionary *data = @{@"name": eventName};
    NSNotificationCenter *notification = [NSNotificationCenter defaultCenter];
    [notification postNotificationName:kNotificationCheckingWithFireBase object:nil userInfo:data];

    [FIRAnalytics logEventWithName:eventName parameters:@{@"name": eventName}];

    
}


@end

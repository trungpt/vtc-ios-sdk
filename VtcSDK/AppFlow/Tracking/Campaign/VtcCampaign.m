//
//  VtcCampaign.m
//  VtcSDK
//
//  Created by Kent Vu on 7/29/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcCampaign.h"

@implementation VtcCampaign

+ (VtcCampaign *)currentCampaign {
    static VtcCampaign *currentCampaign_ = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        currentCampaign_ = [[VtcCampaign alloc] init];
    });
    return currentCampaign_;
}

- (NSString *)getCampaignName {
    if (_campaignName) {
        return _campaignName;
    }
    else {
        return @"";
    }
}

- (NSString *)getSourceName {
    if (_sourceName) {
        return _sourceName;
    }
    else {
        return @"";
    }
}

- (NSString *)getMediumName {
    if (_mediumName) {
        return _mediumName;
    }
    else {
        return @"";
    }
}

- (NSString *)getAgencyName {
    if (_agencyName) {
        return _agencyName;
    }
    else {
        return @"";
    }
}

@end

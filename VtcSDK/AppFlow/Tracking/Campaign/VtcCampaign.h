//
//  VtcCampaign.h
//  VtcSDK
//
//  Created by Kent Vu on 7/29/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VtcCampaign : NSObject

+ (VtcCampaign *)currentCampaign;

@property (nonatomic, copy, getter=getCampaignName) NSString *campaignName;
@property (nonatomic, copy, getter=getSourceName) NSString *sourceName;
@property (nonatomic, copy, getter=getMediumName) NSString *mediumName;
@property (nonatomic, copy, getter=getAgencyName) NSString *agencyName;

@end

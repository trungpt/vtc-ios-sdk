//
//  VtcWebViewController.h
//  VtcSDK
//
//  Created by Kent Vu on 9/6/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcPaymentBaseViewController.h"

@interface VtcWebViewController : VtcPaymentBaseViewController

@property (nonatomic, strong) NSString *URLString;
@property (nonatomic, strong) NSString *htmlString;

@end

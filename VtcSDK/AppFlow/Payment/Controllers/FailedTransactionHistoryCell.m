//
//  FailedTransactionHistoryCell.m
//  VtcSDK
//
//  Created by Tuan Anh Vu on 5/12/17.
//  Copyright © 2017 VTCIntecom. All rights reserved.
//

#import "FailedTransactionHistoryCell.h"

@implementation FailedTransactionHistoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

@implementation RetryButton

- (void)addActivity {
    [self setTitle:@"" forState:UIControlStateNormal];
    [FWActivityIndicatorView showInView:self];
}

- (void)removeActivity {
    [self setTitle:VTCLocalizeString(@"Retry") forState:UIControlStateNormal];
    [FWActivityIndicatorView hideAllActivityIndicatorInView:self];
}

@end

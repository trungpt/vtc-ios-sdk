//
//  VtcShopItemCell.m
//  VtcSDK
//
//  Created by Kent Vu on 8/7/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcShopItemCell.h"
#import "UIView+UpdateAutoLayoutConstraints.h"

@implementation VtcShopItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _lbSaleOff.textAlignment = NSTextAlignmentCenter;
    _lbSaleOff.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onBuy:(id)sender {
    
}

- (void)setPromotedPercent:(NSInteger)promoted {
//    promoted = random()%100;
    if (promoted <= 0) {
        _lbSaleOff.hidden = YES;
    }
    else {
        _lbSaleOff.hidden = NO;
    }
    _lbSaleOff.text = [NSString stringWithFormat:@"+%ld%%", (long) promoted];
    [_lbSaleOff sizeToFit];
    CGRect rect = _lbSaleOff.frame;
    [_lbSaleOff setConstraintConstant:rect.size.width+8 forAttribute:NSLayoutAttributeWidth];
    _lbSaleOff.layer.cornerRadius = (rect.size.width+8)/2;
}

@end

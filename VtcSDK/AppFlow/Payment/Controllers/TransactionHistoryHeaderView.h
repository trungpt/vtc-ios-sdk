//
//  TransactionHistoryHeaderView.h
//  VtcSDK
//
//  Created by Tuan Anh Vu on 5/12/17.
//  Copyright © 2017 VTCIntecom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionHistoryHeaderView : UITableViewHeaderFooterView

@property(strong, nonatomic) IBOutlet UILabel *lbStatus;
@property (weak, nonatomic) IBOutlet UILabel *lbDetail;
@property (weak, nonatomic) IBOutlet UILabel *lbTime;
@property (weak, nonatomic) IBOutlet UILabel *lbStt;

@end

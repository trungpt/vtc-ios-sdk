//
//  VtcShopViewController.m
//  VtcSDK
//
//  Created by Kent Vu on 8/5/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcShopViewController.h"
#import "ScrollableViewController.h"
#import "VtcShopItemCell.h"
#import "NSBundle+Additions.h"
#import "UIViewController+Additions.h"
#import "UIView+UpdateAutoLayoutConstraints.h"
#import "NetworkModal+Payment.h"
#import "VPurchasedItem.h"
#import "UIImageView+AFNetworking.h"
#import "UIImage+Additions.h"
#import "UIViewController+Additions.h"
#import "NetworkModal+Info.h"
#import "SmartParser.h"
#import "VtcPaymentReportView.h"
#import "KLCPopup.h"
#import "SDKManager+Internal.h"
#import "SwitchStatusButton.h"
#import "CircleImageView.h"
#import "VtcWebViewController.h"
#import "VtcBorderedRoundedView.h"
#import "VtcBorderedRoundedButton.h"
#import "AppleIAPHelper.h"
#import "TransactionHistoryController.h"
#import "VtcPaymentNavController.h"
#import "VtcNavigationBar.h"
#import "NetworkModal+Authen.h"


@interface VtcShopViewController () <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tblShop;
@property (nonatomic, weak) IBOutlet UIView *leftView;
@property (nonatomic, weak) IBOutlet UIView *contentLeftView;
@property (nonatomic, weak) IBOutlet UIView *portraitBottomView;
@property (nonatomic, weak) IBOutlet UIView *portraitTopView;
@property (nonatomic, weak) IBOutlet VtcBorderedRoundedView *landscapeBottomView;

@property (nonatomic, weak) IBOutlet UISegmentedControl *methodsSegmentedControl;

@property (nonatomic, weak) IBOutlet UIImageView *imvPAvatar;
@property (nonatomic, weak) IBOutlet UILabel *lbPUsername;
@property (nonatomic, weak) IBOutlet UIButton *btnPTerm;
@property (nonatomic, weak) IBOutlet UIButton *btnPHelp;
@property (nonatomic, weak) IBOutlet UIButton *btnPHistory;

@property (nonatomic, weak) IBOutlet UIImageView *imvLAvatar;
@property (nonatomic, weak) IBOutlet UILabel *lbLUsername;

@property (nonatomic, strong) NSMutableArray *inappPurchasedItems;
@property (nonatomic, strong) NSMutableArray *normalPurchasedItems;
@property (nonatomic, strong) NSMutableArray *monthlyVIPPurchasedItems;
@property (nonatomic, assign) NSArray *purchasedItems;

@property (nonatomic, copy) NSString *wapURLString;
@property (nonatomic, copy) NSString *guideURLString;
@property (nonatomic, copy) NSString *policyURLString;
@property (nonatomic, strong) VPurchasedItem *currentItem;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *topSpaceToLandscapeBottomViewConstraint;

@property (nonatomic, weak) UIButton *previousAddedButton;
@property (nonatomic, weak) SwitchStatusButton *currentLandscapePackageButton;

@property (nonatomic, strong) NSMutableArray *addedLandscapePackageButtons;

@end

@implementation VtcShopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.portraitTopSpace = [Global sharedInstance].gPortraitNavBarHeight;
    self.landscapeTopSpace = [Global sharedInstance].gLandscapeNavBarHeight;
    [self addCustomTitleView];
    
    [Global sharedInstance].shopVC = self;
    
    mainScrollView.delegate = self;
    self.addedLandscapePackageButtons = [[NSMutableArray alloc] init];
    
    // config UI for the segment control
    _methodsSegmentedControl.hidden = YES;
    [_methodsSegmentedControl setConstraintConstant:40.0 forAttribute:NSLayoutAttributeHeight];
    [_methodsSegmentedControl setTitleTextAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:13], NSForegroundColorAttributeName : ColorFromHEX(0x333333)} forState:UIControlStateNormal];
    [_methodsSegmentedControl setTitleTextAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:13], NSForegroundColorAttributeName : ColorFromHEX(0xffffff)} forState:UIControlStateSelected];
    _methodsSegmentedControl.tintColor = [Global sharedInstance].gButtonColor;
    _methodsSegmentedControl.selectedSegmentIndex = 0;
    
    // config for tableview
    [_tblShop registerNib:[UINib nibWithNibName:@"VtcShopItemCell" bundle:[NSBundle vtcBundle]] forCellReuseIdentifier:@"shop_item_cell"];
    _tblShop.rowHeight = UITableViewAutomaticDimension;
    _tblShop.estimatedRowHeight = 106;
    _tblShop.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    // set the text for logged-in username
    _lbLUsername.text = [VIDUser currentUser].userName;
    _lbPUsername.text = [VIDUser currentUser].userName;
    // set the avatar
    if ([VIDUser currentUser].avatarURL && ![[VIDUser currentUser].avatarURL isEqualToString:@""]) {
        [_imvLAvatar setImageWithURL:[NSURL URLWithString:[VIDUser currentUser].avatarURL] placeholderImage:[UIImage vtcImageNamed:@"user_icon_landscape"]];
        [_imvPAvatar setImageWithURL:[NSURL URLWithString:[VIDUser currentUser].avatarURL] placeholderImage:[UIImage vtcImageNamed:@"user_icon_portrait"]];
    }
    else {
        [_imvLAvatar setImage:[UIImage vtcImageNamed:@"user_icon_landscape"]];
        [_imvPAvatar setImage:[UIImage vtcImageNamed:@"user_icon_portrait"]];
    }
    
    // set background color for portrait bottom view
    _portraitBottomView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    
    // draw the border for avatar view
    _imvPAvatar.layer.borderWidth = 2.0;
    _imvPAvatar.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    _imvLAvatar.layer.borderWidth = 2.0;
    _imvLAvatar.layer.borderColor = [ColorFromHEX(0x333333) CGColor];
    
    // draw the border for landscape bottom view
    _landscapeBottomView.layer.borderColor = [ColorFromHEX(0x0BB9FA) CGColor];
    
    // appsflyer logging
    [[VtcEventLogging manager] setAppsFlyerInAppEvent:kAppsFlyerEventTypeInitialCheckout values:[NSDictionary dictionaryWithObjectsAndKeys:@"1", kAppsFlyerEventTypeInitialCheckoutValueKey, nil]];
    
    // call api to update package data
    [self updatePaymentData];
    // call api to get wap link and vco balance
    
    // empty the finish inapp log
    [[AppleIAPHelper sharedHelper] emptyInAppLogStack];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    // reset the delegate of helper because it can be lost
    if (_currentLandscapePackageButton) {
        [self onLandscapePackageButton:_currentLandscapePackageButton];
    }
    [self refreshUI];
    
}

- (void)updatePaymentData {
    NSLog(@"testahihi updatePaymentData");
    [[NetworkModal sharedModal] getPackageInfo:^(BOOL status, id responsedObject, NSError *error) {
        if (status) {
            // add the iap if enable
            NSLog(@"%@",responsedObject);
            NSDictionary *iapData = [SmartParser objectForKey:@"inAppPurchase" from:responsedObject];
            if (iapData) {
                NSArray *packageData = [SmartParser objectForKey:@"packages" from:iapData];
                if (packageData) {
                    if (packageData.count > 0) {
                        // add the iap list package
                        self.inappPurchasedItems = [[NSMutableArray alloc] init];
                        for (NSDictionary *package in packageData) {
                            [_inappPurchasedItems addObject:[VPurchasedItem itemFromData:package]];
                        }
                        NSString *packageName = [SmartParser objectForKey:@"title" from:iapData];
                        // create the package button and paste to landscape-leftview
                        [self addLandscapePackageButtonWithTitle:packageName item:_inappPurchasedItems];
                    }
                }
            }
            
            // add normal payment if enable
            
            // setup segment control
            _methodsSegmentedControl.hidden = NO;
            if (_addedLandscapePackageButtons.count < 2) { // remove the segment control view
                [_methodsSegmentedControl removeFromSuperview];
                [_portraitTopView setConstraintConstant:0.0 forAttribute:NSLayoutAttributeHeight];
            }
            else if (_addedLandscapePackageButtons.count == 2) {
                [_methodsSegmentedControl removeSegmentAtIndex:2 animated:NO];
            }
            
            /*
            [6/27/17, 1:37:34 PM]  Kent: khóa luôn cả cái đoạn  //setup segment control vào
            [6/27/17, 1:37:39 PM]  Kent: rồi thay = 2 dòng này
            [6/27/17, 1:37:40 PM]  Kent: [_methodsSegmentedControl removeFromSuperview];
            [_portraitTopView setConstraintConstant:0.0 forAttribute:NSLayoutAttributeHeight];
            [_methodsSegmentedControl removeFromSuperview];
            [_portraitTopView setConstraintConstant:0.0 forAttribute:NSLayoutAttributeHeight];
             */
            // add the landscape term btn if enable
            NSString *termData = [SmartParser objectForKey:@"termContent" from:responsedObject];
            if (termData) {
                _btnPTerm.hidden = NO;
                _policyURLString = termData;
                NSString *termTitle = [SmartParser objectForKey:@"termTitle" from:responsedObject];
                [self addLandscapeSupportButtonWithTitle:termTitle icon:[UIImage vtcImageNamed:@"info-icon-black"] data:termData selector:nil];
            }
            else {
                _btnPTerm.hidden = YES;
            }
            
            // add the landscape guide btn if enable
            
            NSString *helpData = [SmartParser objectForKey:@"guideContent" from:responsedObject];
            if (helpData) {
                _btnPHelp.hidden = NO;
                _guideURLString = helpData;
                NSString *helpTitle = [SmartParser objectForKey:@"guideTitle" from:responsedObject];
                [self addLandscapeSupportButtonWithTitle:helpTitle icon:[UIImage vtcImageNamed:@"help-icon-black"] data:helpData selector:nil];
            }
            else {
                _btnPHelp.hidden = YES;
            }
            
            // add the landscape history
            NSNumber *showHistory = [SmartParser objectForKey:@"showTransactionHistory" from:responsedObject];
            if (showHistory.integerValue == 1) {
                _btnPHistory.hidden = NO;
                [self addLandscapeSupportButtonWithTitle:VTCLocalizeString(@"Transaction history") icon:[UIImage vtcImageNamed:@"landscape_history_icon"] data:helpData selector:@selector(onTransactionHistory:)];
            }
            else {
                _btnPHistory.hidden = YES;
            }
            
            // re-space the bottom view
            if (_previousAddedButton) {
                [_contentLeftView removeConstraint:_topSpaceToLandscapeBottomViewConstraint];
                NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:_previousAddedButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_landscapeBottomView attribute:NSLayoutAttributeTop multiplier:1.0 constant:-16];
                constraint.priority = UILayoutPriorityRequired;
                [_contentLeftView addConstraint:constraint];
            }
            
            // show/hide the vco label
            [self refreshUI];
        }
        else {
            [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
        }
    }];
}

- (void)addLandscapePackageButtonWithTitle:(NSString *)btnTitle item:(NSArray *)listItem {
    SwitchStatusButton *packageButton = [[SwitchStatusButton alloc] initWithFrame:CGRectZero];
    packageButton.translatesAutoresizingMaskIntoConstraints = NO;
    if (btnTitle) {
        [packageButton setTitle:btnTitle forState:UIControlStateNormal];
        // setup the title for related segment of segment control
        [_methodsSegmentedControl setTitle:btnTitle forSegmentAtIndex:_addedLandscapePackageButtons.count];
    }
    packageButton.items = listItem;
    packageButton.tag = _addedLandscapePackageButtons.count;
    [packageButton addTarget:self action:@selector(onLandscapePackageButton:) forControlEvents:UIControlEventTouchUpInside];
    [_contentLeftView addSubview:packageButton];
    // add constraints
    if (_previousAddedButton) {
        [_contentLeftView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[prev]-8-[btn(50)]" options:0 metrics:nil views:@{@"prev":_previousAddedButton, @"btn":packageButton}]];
    }
    else {
        [_contentLeftView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[btn(50)]" options:0 metrics:nil views:@{@"btn":packageButton}]];
    }
    [_contentLeftView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[btn]" options:0 metrics:nil views:@{@"btn":packageButton}]];
    [_contentLeftView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[btn]-10-|" options:0 metrics:nil views:@{@"btn":packageButton}]];
    _previousAddedButton = packageButton;
    if (!_currentLandscapePackageButton) { // set the default option
        _currentLandscapePackageButton = packageButton;
    }
    [_addedLandscapePackageButtons addObject:packageButton];
}

- (void)onLandscapePackageButton:(SwitchStatusButton *)button {
    if (_currentLandscapePackageButton) {
        _currentLandscapePackageButton.selected = NO;
    }
    button.selected = YES;
    _currentLandscapePackageButton = button;
    _purchasedItems = button.items;
    [_tblShop reloadData];
    if (_methodsSegmentedControl) {
        if (_methodsSegmentedControl.selectedSegmentIndex != button.tag) {
            [_methodsSegmentedControl setSelectedSegmentIndex:button.tag];
        }
    }
}

- (void)addLandscapeSupportButtonWithTitle:(NSString *)btnTitle icon:(UIImage *)iconImage data:(NSString *)buttonData selector:(SEL)buttonSelector {
    VtcBorderedRoundedButton *supportButton = [VtcBorderedRoundedButton buttonWithType:UIButtonTypeCustom];
    supportButton.translatesAutoresizingMaskIntoConstraints = NO;
    [supportButton setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    supportButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    if (btnTitle) {
        [supportButton setTitle:[NSString stringWithFormat:@"   %@", btnTitle] forState:UIControlStateNormal];
    }
    [supportButton setImage:iconImage forState:UIControlStateNormal];
    [supportButton setImageEdgeInsets:UIEdgeInsetsMake(0.0, 10.0, 0.0, 0.0)];
    supportButton.extendData = buttonData;
    if (buttonSelector) {
        [supportButton addTarget:self action:buttonSelector forControlEvents:UIControlEventTouchUpInside];
    }
    else {
        [supportButton addTarget:self action:@selector(onLandscapeSupportButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    [_contentLeftView addSubview:supportButton];
    
    if (_previousAddedButton) {
        [_contentLeftView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[prev]-16-[btn(44)]" options:0 metrics:nil views:@{@"prev":_previousAddedButton, @"btn":supportButton}]];
    }
    else {
        [_contentLeftView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[btn(44)]" options:0 metrics:nil views:@{@"btn":supportButton}]];
    }
    [_contentLeftView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[btn]" options:0 metrics:nil views:@{@"btn":supportButton}]];
    [_contentLeftView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[btn]-10-|" options:0 metrics:nil views:@{@"btn":supportButton}]];
    _previousAddedButton = supportButton;
}

- (void)onLandscapeSupportButton:(VtcBorderedRoundedButton *)button {
    NSString *urlString = (NSString *)button.extendData;
    [self openWapLinkControllerWithLink:urlString];
}

- (void)orientationDidChange:(NSNotification *)notification {
    [super orientationDidChange:notification];
    
    [self refreshUI];
}

- (void)refreshUI {
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if(orientation == UIInterfaceOrientationLandscapeRight ||
       orientation == UIInterfaceOrientationLandscapeLeft) {
        // handle something for landscape
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        if (width < 500) { // if device width is too short, dont split the screen (iPhone 3.7")
            [self splitPaymentScreen:NO];
        }
        else {
            [self splitPaymentScreen:YES];
        }
    }
    else {
        if ([[UIDevice currentDevice].model containsString:@"iPad"]) { // if device is iPad, split the screen with all orientation
            [self splitPaymentScreen:YES];
        }
        else {
            [self splitPaymentScreen:NO];
        }
    }
}

- (void)splitPaymentScreen:(BOOL)splited {
    if (splited) {
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        [_leftView setConstraintConstant:(width < 600.0) ? 220.0 : 250.0 forAttribute:NSLayoutAttributeWidth];
        _leftView.hidden = NO;
        [_portraitBottomView setConstraintConstant:0.0 forAttribute:NSLayoutAttributeHeight];
        _portraitBottomView.hidden = YES;
        [_portraitTopView setConstraintConstant:0.0 forAttribute:NSLayoutAttributeHeight];
        _portraitTopView.hidden = YES;
    }
    else {
        [_leftView setConstraintConstant:0 forAttribute:NSLayoutAttributeWidth];
        _leftView.hidden = YES;
        [_portraitBottomView setConstraintConstant:40.0 forAttribute:NSLayoutAttributeHeight];
        _portraitBottomView.hidden = NO;
        if (_addedLandscapePackageButtons.count > 1) {
            [_portraitTopView setConstraintConstant:56.0 forAttribute:NSLayoutAttributeHeight];
            _portraitTopView.hidden = NO;
        }
    }
    
    if (_currentLandscapePackageButton) {
        [self onLandscapePackageButton:_currentLandscapePackageButton];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBuy:(UIButton *)sender {
    NSInteger row = sender.tag;
    [self loadItemAtIndex:row];
}

- (void)processInAppPurchase {
    NSLog(@"testahihi processInAppPurchase");
    NSInteger monthly = (_currentItem.packageType == PackageItemTypeMonthlyCard) ? 2 : 1;
    [HDNotificationView showNotificationViewWithImage:nil title:nil message:VTCLocalizeString(@"Purchasing")  attributedText:nil isAutoHide:NO];
    
    [[NetworkModal sharedModal] createOrderIdForPackage:_currentItem.itemKey packageType:monthly completion:^(BOOL status, id responsedObject, NSError *error) {
        VTCLog(@"Order-No = %@", responsedObject);
        
        if (status) {
            @try {
                [AppleIAPHelper sharedHelper].currentItem = _currentItem;
                [Global sharedInstance].currentOrderNo = [NSString stringWithFormat:@"%@", responsedObject];
                [AppleIAPHelper sharedHelper].currentItem.orderNo = [Global sharedInstance].currentOrderNo;
            } @catch (NSException *exception) {
                NSLog(@"<VtcSDK> exception: %@", exception.description);
            } @finally {
                [[AppleIAPHelper sharedHelper] buyProductWithIdentifier:_currentItem.productIdentifier];
            }
        }
        else {
            NSString *errString = VTCLocalizeString(@"Purchase failed. Please try again");
            @try {
                errString = [error.userInfo objectForKey:@"message"];
            } @catch (NSException *exception) {
                NSLog(@"<VtcSDK> exception: %@", exception.description);
            } @finally {
                [Utilities showNotification:errString];
                if ((error.code == -401) || (error.code == -303)) {
                    __block UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:VTCLocalizeString(@"Session's expired") message:VTCLocalizeString(@"Do you want to relogin now?") preferredStyle:UIAlertControllerStyleAlert];
                    [alertVC addAction:[UIAlertAction actionWithTitle:VTCLocalizeString(@"Later") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                        [alertVC dismissViewControllerAnimated:YES completion:nil];
                    }]];
                    [alertVC addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [alertVC dismissViewControllerAnimated:NO completion:^{
                            NSLog(@"dismiss alert");
                        }];
                        [[SDKManager defaultManager] signInFromViewController:self];
                    }]];
                    [self presentViewController:alertVC animated:NO completion:nil];
                }
            }
        }
    }];
}

- (void)loadItemAtIndex:(NSInteger)index {
    @try {
        self.currentItem = [_purchasedItems objectAtIndex:index];
    } @catch (NSException *exception) {
        VTCLog(@"exception: %@", exception.description);
    } @finally {
        if (_currentItem) {
            VTCLog(@"select package: %@", _currentItem.itemName);
            VTCLog(@"%@", _currentItem.itemKey);
            VTCLog(@"%@", _currentItem.itemImageURL);
            VTCLog(@"%@", _currentItem.itemDescription);
            VTCLog(@"%ld", (long)_currentItem.itemPriceUSD);
            VTCLog(@"%ld", (long)_currentItem.itemPriceVND);
            VTCLog(@"%ld", (long)_currentItem.itemPriceGold);
            [self processInAppPurchase];
        }
        else {
            [Utilities showNotification:@"Chọn gói hàng không thành công"];
        }
    }
}


- (void)openWapLinkControllerWithLink:(NSString *)URLString {
    // open webview
    VtcWebViewController *controller = [VtcWebViewController loadFromNibNamed:@"VtcWebViewController"];
    controller.URLString = URLString;
    [self.navigationController pushViewController:controller animated:NO];
}


- (IBAction)segmentControlSelected:(UISegmentedControl *)control {
    [self onLandscapePackageButton:[_addedLandscapePackageButtons objectAtIndex:control.selectedSegmentIndex]];
}

- (IBAction)onGuideBtn:(id)sender {
    if (!_policyURLString) {
        [Utilities showNotification:VTCLocalizeString(@"Content's not available. Please try again later")];
        return;
    }
    [self openWapLinkControllerWithLink:_guideURLString];
}

- (IBAction)onPolicyBtn:(id)sender {
    if (!_policyURLString) {
        [Utilities showNotification:VTCLocalizeString(@"Content's not available. Please try again later")];
        return;
    }
    [self openWapLinkControllerWithLink:_policyURLString];
}

- (IBAction)onTransactionHistory:(id)sender {
    TransactionHistoryController *historyVC = [TransactionHistoryController loadFromNibNamed:@"TransactionHistoryController"];
    VtcPaymentNavController *historyNavController = [[VtcPaymentNavController alloc] initWithNavigationBarClass:[VtcNavigationBar class] toolbarClass:nil];
    [historyNavController setViewControllers:@[historyVC]];
    historyNavController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    historyNavController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self.navigationController presentViewController:historyNavController animated:NO completion:^{
        // do something
    }];
}


#pragma mark - table view delegates & datasources

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _purchasedItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VtcShopItemCell *cell = (VtcShopItemCell *) [tableView dequeueReusableCellWithIdentifier:@"shop_item_cell" forIndexPath:indexPath];
    NSInteger row = indexPath.row;
    // cell initialization
    VPurchasedItem *item = [_purchasedItems objectAtIndex:row];

        if (item.itemImageURL) {
            [cell.imvItem setImageWithURL:[NSURL URLWithString:item.itemImageURL] placeholderImage:[UIImage vtcImageNamed:@"temp_gold"]];
        }
        else {
            [cell.imvItem setImage:[UIImage vtcImageNamed:@"temp_gold"]];
        }
        if (item.promotionPercent > 0) {
            cell.lbItemDescription.text = item.promotionText;
            cell.lbItemDescription.font = [UIFont systemFontOfSize:12];
        }
        else {
            cell.lbItemDescription.text = @"";
        }

    cell.lbItemName.text = item.itemName;
    cell.lbItemValue.text = item.itemDescription;
    [cell setPromotedPercent:item.promotionPercent];
    [cell.btnBuy setConstraintConstant:75.0 forAttribute:NSLayoutAttributeWidth];
    cell.btnBuy.tag = row;
    [cell.btnBuy addTarget:self action:@selector(onBuy:) forControlEvents:UIControlEventTouchUpInside];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    [self loadItemAtIndex:row];
}

@end

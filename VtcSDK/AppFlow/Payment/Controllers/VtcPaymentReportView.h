//
//  VtcPaymentReportView.h
//  VtcSDK
//
//  Created by Kent Vu on 8/11/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import <UIKit/UIKit.h>
@class KLCPopup;

@interface VtcPaymentReportView : UIView

@property (nonatomic, weak) KLCPopup *currentPopup;
@property (nonatomic, copy) NSString *currency;

+ (VtcPaymentReportView *)reportViewWithFrame:(CGRect)rect receivedGold:(NSInteger)goldNumber currency:(NSString *)currency;
- (void)setMessage:(NSString *)msgString;

@end

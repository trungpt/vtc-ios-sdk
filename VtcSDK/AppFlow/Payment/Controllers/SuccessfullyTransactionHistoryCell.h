//
//  SuccessfullyTransactionHistoryCell.h
//  VtcSDK
//
//  Created by Tuan Anh Vu on 5/12/17.
//  Copyright © 2017 VTCIntecom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuccessfullyTransactionHistoryCell : UITableViewCell

@property(strong, nonatomic) IBOutlet UILabel *lbSTT;
@property(strong, nonatomic) IBOutlet UILabel *lbDateTime;
@property(strong, nonatomic) IBOutlet UILabel *lbPackageName;
@property(strong, nonatomic) IBOutlet UILabel *lbStatus;

@end


//
//  VtcPaymentBaseViewController.h
//  VtcSDK
//
//  Created by Kent Vu on 8/5/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "ScrollableViewController.h"

@interface VtcPaymentBaseViewController : ScrollableViewController

@end

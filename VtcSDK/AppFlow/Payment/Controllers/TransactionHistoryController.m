//
//  TransactionHistoryController.m
//  VtcSDK
//
//  Created by Tuan Anh Vu on 5/10/17.
//  Copyright © 2017 VTCIntecom. All rights reserved.
//

#import "TransactionHistoryController.h"
#import "UIImage+Additions.h"
#import "NSBundle+Additions.h"
#import "UIView+UpdateAutoLayoutConstraints.h"
#import "SuccessfullyTransactionHistoryCell.h"
#import "FailedTransactionHistoryCell.h"
#import "TransactionHistoryHeaderView.h"
#import "NetworkModal+Payment.h"
#import "SmartParser.h"
#import "AppleIAPHelper.h"
@class RetryButton;

typedef enum : NSUInteger {
    TransactionHistoryTypeFailed = 0,
    TransactionHistoryTypeSuccess = 1,
    TransactionHistoryTypeRefund = 2
} TransactionHistoryType;

@interface TransactionHistoryController () <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tblFailedHistory;
@property (nonatomic, weak) IBOutlet UITableView *tblSuccessHistory;
@property (nonatomic, weak) IBOutlet UITableView *tblRefundHistory;
@property (nonatomic, weak) IBOutlet UISegmentedControl *tabview;

@property (nonatomic, strong) NSMutableArray *tblFailedData;
@property (nonatomic, strong) NSMutableArray *tblSuccessData;
@property (nonatomic, strong) NSMutableArray *tblRefundData;
@property (nonatomic) TransactionHistoryType tabType;

@property (nonatomic) BOOL loadedFailedTransaction;
@property (nonatomic) BOOL loadedSuccessTransaction;
@property (nonatomic) BOOL loadedRefundTransaction;

@property (nonatomic, strong) UIRefreshControl *successFreshControl;
@property (nonatomic) NSInteger successCurrentPage;
@property (nonatomic) NSInteger successPageSize;

@property (nonatomic) NSInteger refundCurrentPage;
@property (nonatomic) NSInteger refundPageSize;

@end

@implementation TransactionHistoryController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.portraitTopSpace = [Global sharedInstance].gPortraitNavBarHeight;
    self.landscapeTopSpace = [Global sharedInstance].gLandscapeNavBarHeight;
    [self addCustomTitleView];
    UIBarButtonItem *closeBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage vtcImageNamed:@"btn_close_white"] style:UIBarButtonItemStyleDone target:self action:@selector(onCloseButton:)];
    self.navigationItem.rightBarButtonItem = closeBtn;
    
    [_tabview setConstraintConstant:40.0 forAttribute:NSLayoutAttributeHeight];
    [_tabview setTitleTextAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:13], NSForegroundColorAttributeName : ColorFromHEX(0x333333)} forState:UIControlStateNormal];
    [_tabview setTitleTextAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:13], NSForegroundColorAttributeName : ColorFromHEX(0xffffff)} forState:UIControlStateSelected];
    _tabview.tintColor = [Global sharedInstance].gButtonColor;
    
    [_tblSuccessHistory registerNib:[UINib nibWithNibName:@"SuccessfullyTransactionHistoryCell" bundle:[NSBundle vtcBundle]] forCellReuseIdentifier:@"successfully_trans_cell"];
    [_tblSuccessHistory registerNib:[UINib nibWithNibName:@"TransactionHistoryHeaderView" bundle:[NSBundle vtcBundle]] forHeaderFooterViewReuseIdentifier:@"header"];
    _successFreshControl = [[UIRefreshControl alloc] init];
    [_successFreshControl addTarget:self action:@selector(triggerSuccessRefreshControl:) forControlEvents:UIControlEventValueChanged];
    [_tblSuccessHistory addSubview:_successFreshControl];
    [_tblSuccessHistory sendSubviewToBack:_successFreshControl];
    
    [_tblRefundHistory registerNib:[UINib nibWithNibName:@"SuccessfullyTransactionHistoryCell" bundle:[NSBundle vtcBundle]] forCellReuseIdentifier:@"successfully_trans_cell"];
    [_tblRefundHistory registerNib:[UINib nibWithNibName:@"TransactionHistoryHeaderView" bundle:[NSBundle vtcBundle]] forHeaderFooterViewReuseIdentifier:@"header"];
    
    [_tblFailedHistory registerNib:[UINib nibWithNibName:@"FailedTransactionHistoryCell" bundle:[NSBundle vtcBundle]] forCellReuseIdentifier:@"failed_trans_cell"];
    [_tblFailedHistory registerNib:[UINib nibWithNibName:@"TransactionHistoryHeaderView" bundle:[NSBundle vtcBundle]] forHeaderFooterViewReuseIdentifier:@"header"];
    
    _tblSuccessData = [[NSMutableArray alloc] init];
    _tblFailedData = [[NSMutableArray alloc] init];
    _tblRefundData = [[NSMutableArray alloc] init];
    
    _tabview.selectedSegmentIndex = 0;
    [_tabview sendActionsForControlEvents:UIControlEventValueChanged];
    
    _successCurrentPage = 1;
    _successPageSize = 10;
    
    _refundCurrentPage = 1;
    _refundPageSize = 10;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self settingLanguage];
}
- (void) settingLanguage
{
    [self.tabview setTitle:[VTCLocalizeString(@"Unsuccessful") uppercaseString] forSegmentAtIndex:0];
    [self.tabview setTitle:[VTCLocalizeString(@"Successful") uppercaseString] forSegmentAtIndex:1];
    [self.tabview setTitle:[VTCLocalizeString(@"Refund") uppercaseString] forSegmentAtIndex:2];
}

- (IBAction)onRetry:(RetryButton *)sender {
    NSDictionary *rowData = [_tblFailedData objectAtIndex:sender.tag];
    [sender addActivity];
    [[AppleIAPHelper sharedHelper] verifyReceiptWithVTCServerForTransaction:rowData retryTransaction:YES completion:^(BOOL result, BOOL remove) {
        [sender removeActivity];
        if (remove) {
            [_tblFailedData removeObjectAtIndex:sender.tag];
            [_tblFailedHistory reloadData];
            [[NSUserDefaults standardUserDefaults] setObject:_tblFailedData forKey:kUserDefaultStackOfIAPPayment];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }];
}

- (void)triggerSuccessRefreshControl:(UIRefreshControl *)refreshControl {
    _successCurrentPage = 1;
    [self reloadSuccessTransaction];
}

- (void)reloadSuccessTransaction {
    [FWActivityIndicatorView showInView:_tblSuccessHistory];
    [[NetworkModal sharedModal] listSuccessfulTransactions:_successCurrentPage size:_successPageSize completion:^(BOOL status, id responsedObject, NSError *error) {
        [FWActivityIndicatorView hideAllActivityIndicatorInView:_tblSuccessHistory];
        if ([_successFreshControl isRefreshing]) {
            [_successFreshControl endRefreshing];
        }
        if (responsedObject) {
            if (_successCurrentPage == 1) [_tblSuccessData removeAllObjects];
            NSArray *transactions = [SmartParser objectForKey:@"responses" from:responsedObject];
            [_tblSuccessData addObjectsFromArray:transactions];
            [_tblSuccessHistory reloadData];
        }
    }];
}

- (IBAction)segmentControlSelected:(id)sender {
    // first, remove the table data array
    switch (_tabview.selectedSegmentIndex) {
        case 0:
        {
            _tabType = TransactionHistoryTypeFailed;
            _tblFailedHistory.hidden = NO;
            _tblSuccessHistory.hidden = YES;
            _tblRefundHistory.hidden = YES;
            if (!_loadedFailedTransaction) {
                [_tblFailedData removeAllObjects];
                NSArray *cachedTransactions = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultStackOfIAPPayment];
                [_tblFailedData addObjectsFromArray:cachedTransactions];
                [_tblFailedHistory reloadData];
                _loadedFailedTransaction = YES;
            }
        }
            break;
        case 1:
        {
            _tabType = TransactionHistoryTypeSuccess;
            _tblFailedHistory.hidden = YES;
            _tblSuccessHistory.hidden = NO;
            _tblRefundHistory.hidden = YES;
            [_tblSuccessHistory reloadData]; // call here to reload the header
            
            if (!_loadedSuccessTransaction) {
                [self reloadSuccessTransaction];
                _loadedSuccessTransaction = YES;
            }
            
        }
            break;
        default: // refund
        {
            _tabType = TransactionHistoryTypeRefund;
            _tblFailedHistory.hidden = YES;
            _tblSuccessHistory.hidden = YES;
            _tblRefundHistory.hidden = NO;
            [_tblRefundHistory reloadData];
            
            if (!_loadedRefundTransaction) {
                [FWActivityIndicatorView showInView:_tblRefundHistory];
                [[NetworkModal sharedModal] listRefundTransactions:1 size:10 completion:^(BOOL status, id responsedObject, NSError *error) {
                    [FWActivityIndicatorView hideAllActivityIndicatorInView:_tblRefundHistory];
                    if (responsedObject) {
                        [_tblRefundData removeAllObjects];
                        NSArray *transactions = [SmartParser objectForKey:@"responses" from:responsedObject];
                        [_tblRefundData addObjectsFromArray:transactions];
                        [_tblRefundHistory reloadData];
                    }
                }];
                _loadedRefundTransaction = YES;
            }
        }
            break;
    }
}

- (IBAction)onCloseButton:(id)sender {
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
}


// MARK: - tableview
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == _tblFailedHistory) {
        return _tblFailedData.count;
    }
    else if (tableView == _tblSuccessHistory) {
        return _tblSuccessData.count;
    }
    else {
        return _tblRefundData.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == _tblFailedHistory) {
        FailedTransactionHistoryCell *cell = (FailedTransactionHistoryCell *)[tableView dequeueReusableCellWithIdentifier:@"failed_trans_cell" forIndexPath:indexPath];
        NSDictionary *rowData = [_tblFailedData objectAtIndex:indexPath.row];
        cell.lbSTT.text = [NSString stringWithFormat:@"%ld", (long)indexPath.row+1];
        NSNumber *payDateTimeStamp = [SmartParser objectForKey:@"purchaseDate" from:rowData];
        NSDate *payDate = [NSDate dateWithTimeIntervalSince1970:payDateTimeStamp.doubleValue];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd-MM-yyyy"];
        cell.lbDateTime.text = [formatter stringFromDate:payDate];
        cell.lbPackageName.text = [SmartParser stringForKey:@"packageName" from:rowData];
        cell.btnRetry.tag = indexPath.row;
        [cell.btnRetry addTarget:self action:@selector(onRetry:) forControlEvents:UIControlEventTouchUpInside];
        return  cell;
    }
    else if (tableView == _tblSuccessHistory) {
        SuccessfullyTransactionHistoryCell *cell = (SuccessfullyTransactionHistoryCell *)[tableView dequeueReusableCellWithIdentifier:@"successfully_trans_cell" forIndexPath:indexPath];
        NSDictionary *rowData = [_tblSuccessData objectAtIndex:indexPath.row];
        cell.lbSTT.text = [NSString stringWithFormat:@"%ld", (long)indexPath.row+1];
        cell.lbDateTime.text = [SmartParser stringForKey:@"date" from:rowData];
        cell.lbPackageName.text = [SmartParser stringForKey:@"detailMsg" from:rowData];
        cell.lbStatus.text = [SmartParser stringForKey:@"status" from:rowData];
        return  cell;
    }
    else {
        SuccessfullyTransactionHistoryCell *cell = (SuccessfullyTransactionHistoryCell *)[tableView dequeueReusableCellWithIdentifier:@"successfully_trans_cell" forIndexPath:indexPath];
        NSDictionary *rowData = [_tblRefundData objectAtIndex:indexPath.row];
        cell.lbSTT.text = [NSString stringWithFormat:@"%ld", (long)indexPath.row+1];
        cell.lbDateTime.text = [SmartParser stringForKey:@"date" from:rowData];
        cell.lbPackageName.text = [SmartParser stringForKey:@"detailMsg" from:rowData];
        cell.lbStatus.text = [SmartParser stringForKey:@"status" from:rowData];
        return  cell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    TransactionHistoryHeaderView *header = (TransactionHistoryHeaderView *)[tableView dequeueReusableHeaderFooterViewWithIdentifier:@"header"];
    header.lbStt.text = [VTCLocalizeString(@"Order") uppercaseString];
    header.lbTime.text = [VTCLocalizeString(@"Time") uppercaseString];
    header.lbDetail.text = [VTCLocalizeString(@"Detail") uppercaseString];
    header.lbStatus.text = [VTCLocalizeString(@"Status") uppercaseString];
    header.lbStatus.hidden = (tableView == _tblFailedHistory) ? YES : NO;
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40.0;
}

// MARK: Scrollview delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    if ([scrollView.panGestureRecognizer translationInView:scrollView.superview].y > 0) {
        // handle dragging to top
        return;
    }
    
    CGFloat endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height + 50.0;
    if (endScrolling >= scrollView.contentSize.height) {
        if (scrollView == _tblSuccessHistory) {
            _successCurrentPage += 1;
            [self reloadSuccessTransaction];
        }
    }
    
}

@end

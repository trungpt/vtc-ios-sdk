//
//  VtcShopItemCell.h
//  VtcSDK
//
//  Created by Kent Vu on 8/7/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VtcShopItemCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *imvItem;
@property (nonatomic, weak) IBOutlet UILabel *lbItemName;
@property (nonatomic, weak) IBOutlet UILabel *lbItemValue;
@property (nonatomic, weak) IBOutlet UILabel *lbItemDescription;
@property (nonatomic, weak) IBOutlet UIButton *btnBuy;
@property (nonatomic, weak) IBOutlet UILabel *lbSaleOff;

- (void)setPromotedPercent:(NSInteger)promoted;

@end

//
//  VtcPaymentReportView.m
//  VtcSDK
//
//  Created by Kent Vu on 8/11/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcPaymentReportView.h"
#import "NSBundle+Additions.h"
#import "KLCPopup.h"

@interface VtcPaymentReportView ()

@property (nonatomic,weak) IBOutlet UILabel *lbResult;

@end

@implementation VtcPaymentReportView

+ (VtcPaymentReportView *)reportViewWithFrame:(CGRect)rect receivedGold:(NSInteger)goldNumber currency:(NSString *)currency {
    VtcPaymentReportView *view = (VtcPaymentReportView *)[[NSBundle vtcBundle] loadNibNamed:@"VtcPaymentReportView" owner:nil options:nil].firstObject;
    view.frame = rect;
    view.lbResult.text = [NSString stringWithFormat:@"%ld %@", (long)goldNumber, currency];
    return view;
}

- (void)setMessage:(NSString *)msgString {
    self.lbResult.text = msgString;
    [Global sharedInstance].paymentMessage = nil;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.translatesAutoresizingMaskIntoConstraints = NO;
}

- (IBAction)onClose:(id)sender {
    if (_currentPopup) {
        [_currentPopup dismiss:YES];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.frame = CGRectMake(0, 0, 320, 250);
}

@end

//
//  VtcPaymentNavController.m
//  VtcSDK
//
//  Created by Kent Vu on 8/5/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcPaymentNavController.h"
#import "SDKManager.h"

@interface VtcPaymentNavController ()

@end

@implementation VtcPaymentNavController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    return [SDKManager defaultManager].allowRotationInPaymentView;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
//    if ([SDKManager defaultManager].allowRotationInPaymentView) {
//        return UIInterfaceOrientationMaskAll;
//    }
//    else {
        return [SDKManager defaultManager].paymentViewOrientationMask;
//    }
}

@end

//
//  VtcWebViewController.m
//  VtcSDK
//
//  Created by Kent Vu on 9/6/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcWebViewController.h"
#import <WebKit/WebKit.h>
#import "UIViewController+Additions.h"
#import "UIImage+Additions.h"
@interface VtcWebViewController ()

@property (weak, nonatomic) WKWebView *webView;

@end

@implementation VtcWebViewController
UIButton *closeWebButton;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self addCustomTitleView];
    [self addCustomBackButton];
    self.portraitTopSpace = [Global sharedInstance].gPortraitNavBarHeight;
    self.landscapeTopSpace = [Global sharedInstance].gLandscapeNavBarHeight;
    
    //switch to webkitview
    
    WKPreferences *preferences = [[WKPreferences alloc] init];
    preferences.javaScriptEnabled = YES;
    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
    [configuration.userContentController addScriptMessageHandler:self name:@"interOp"];
    configuration.preferences = preferences;
    
    //    CGRect rect = CGRectMake(0,
    //                             _tfUsername.frame.origin.y + _tfUsername.frame.size.height,
    //                             self.mainscrollview.frame.size.width,
    //                             CAPTCHA_MIN_HEIGHT
    //                             );
    CGRect rect = CGRectMake(0,0,
                             self.view.frame.size.width,
                             self.view.frame.size.height
                             );
    //    WKWebView *webView = [[WKWebView alloc] initWithFrame:mainscrollview configuration:configuration];
    //    _webView = [[WKWebView alloc] initWithFrame:self.mainscrollview.frame configuration:configuration];
    _webView = [[WKWebView alloc] initWithFrame:rect configuration:configuration];
    //    _webView.navigationDelegate = self;
    
    [_webView.scrollView setDelegate:self];
    [_webView setNavigationDelegate: self];
    
    //
//    _webView.delegate = self;
    if (_URLString) {
        NSURL *URL = [NSURL URLWithString:_URLString];
        NSURLRequest *req = [NSURLRequest requestWithURL:URL];
        [_webView loadRequest:req];
    }
    if (_htmlString) {
        [_webView loadHTMLString:_htmlString baseURL:nil];
    }
    [self.view addSubview:_webView];
    
    //add close button
//    if(!closeWebButton){
//        closeWebButton.hidden = YES;
//        closeWebButton = nil;
//    }
//    closeWebButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [closeWebButton addTarget:self
//                        action:@selector(closeWebView)
//              forControlEvents:UIControlEventTouchUpInside];
//    //    [button setTitle:@"close" forState:UIControlStateNormal];
//
//    [closeWebButton setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.2]];
//    [closeWebButton setImageEdgeInsets: UIEdgeInsetsMake(5, 5, 5, 5)];
//    UIImage *buttonImageNormal = [UIImage vtcImageNamed:@"vtc_authen_close@2x"];
//    [closeWebButton setImage:buttonImageNormal forState:UIControlStateNormal];
//    closeWebButton.frame = CGRectMake(30, 60, 30, 30);
//    [self.view addSubview:closeWebButton];
    
    
//    [_webView setScalesPageToFit:YES];
}

-(void)closeWebView{
    //    NSLog(@"closeWebView");
    _webView.hidden = YES;
    closeWebButton.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}
//
//- (void)webView:(WKWebView *)webView
//didFinishNavigation:(WKNavigation *)aNavigation {
//    [FWActivityIndicatorView hideAllActivityIndicatorInView:webView];
//}

@end

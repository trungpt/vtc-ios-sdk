//
//  VtcPaymentBaseViewController.m
//  VtcSDK
//
//  Created by Kent Vu on 8/5/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcPaymentBaseViewController.h"
#import "UIImage+Additions.h"
#import "SDKManager+Internal.h"

@interface VtcPaymentBaseViewController ()

@end

@implementation VtcPaymentBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addCustomCloseButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onCloseBtn {
    [SDKManager defaultManager].showingPaymentView = NO;
    [super onCloseBtn];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

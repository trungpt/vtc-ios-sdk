//
//  FailedTransactionHistoryCell.h
//  VtcSDK
//
//  Created by Tuan Anh Vu on 5/12/17.
//  Copyright © 2017 VTCIntecom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RetryButton : UIButton

- (void)addActivity;
- (void)removeActivity;

@end

@interface FailedTransactionHistoryCell : UITableViewCell

@property(strong, nonatomic) IBOutlet UILabel *lbSTT;
@property(strong, nonatomic) IBOutlet UILabel *lbDateTime;
@property(strong, nonatomic) IBOutlet UILabel *lbPackageName;
@property(strong, nonatomic) IBOutlet RetryButton *btnRetry;

@end

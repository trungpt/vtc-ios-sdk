//
//  VtcPaymentNavController.h
//  VtcSDK
//
//  Created by Kent Vu on 8/5/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcNavigationController.h"

@interface VtcPaymentNavController : VtcNavigationController

@end

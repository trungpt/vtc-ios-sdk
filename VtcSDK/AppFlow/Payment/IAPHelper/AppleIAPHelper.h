//
//  SHAppleIAPHelper.h
//  SohaSDK
//
//  Created by Kent Vu on 4/9/15.
//  Copyright (c) 2015 Sohagame Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Foundation/Foundation.h>
#import "StoreKit/StoreKit.h"
@class VPurchasedItem;

@protocol AppleIAPHelperDelegate <NSObject>

- (void)paymentInAppPurchaseDidFailWithTransaction:(SKPaymentTransaction *)transaction;
- (void)paymentInAppPurchaseDidSuccessWithTransaction:(SKPaymentTransaction *)transaction;

@end

@interface AppleIAPHelper : NSObject <SKProductsRequestDelegate, SKPaymentTransactionObserver> {
    SKProductsRequest * request_;
    NSString *currentOrderID_;
}

@property (weak, nonatomic) id <AppleIAPHelperDelegate> delegate;
@property (nonatomic) NSInteger packageType;
@property (nonatomic, strong) VPurchasedItem *currentItem;

/**
 * @brief get the sharing helper.
 */
+ (AppleIAPHelper *)sharedHelper;

/**
 * @brief send the request to Apple payment server.
 * @param productIdentifier the handling product identifier.
 * @note server will return the result of verification of this product after this step.
 */
- (void)buyProductWithIdentifier:(NSString *)productIdentifier;
/**
 * @brief send the request to Apple payment server to restore all purchased products.
 * @note almost games just have comsumable products.
 */
- (void)restoreAllCompletedTransactions;

/**
 * @brief send the receipt to App Store to verify again
 * @transaction the current transaction want to verify.
 * @note require for secure reason.
 */
//- (void)verifyReceiptWithAppStoreForTransaction:(SKPaymentTransaction *)transaction;

/**
 * @brief check the saved receipt stack to ensure no transaction was not done yet
 * @note require.
 */

- (void)emptyInAppLogStack;
- (void)verifyReceiptWithVTCServerForTransaction:(NSDictionary *)transaction retryTransaction:(BOOL)retry completion:(void (^)(BOOL result, BOOL remove))completionBlock;

@end

//
//  SHAppleIAPHelper.m
//  SohaSDK
//
//  Created by Kent Vu on 4/9/15.
//  Copyright (c) 2015 Sohagame Corporation. All rights reserved.
//

#import "AppleIAPHelper.h"
#import "AFNetworkReachabilityManager.h"
#import "Base64.h"
#import "NetworkModal+Payment.h"
#import <AppsFlyerLib/AppsFlyerTracker.h>
#import "VtcEventLogging.h"
#import "SmartParser.h"
#import "VPurchasedItem.h"
#import "SDKManager.h"
#import "SVProgressHUD.h"
#import "UIViewController+Additions.h"

@interface AppleIAPHelper () {
    
}

@property (nonatomic, assign) BOOL trackingHelper;

@property (nonatomic, assign) BOOL verifyingATransaction;

@end

@implementation AppleIAPHelper

- (void)dealloc {
    if (_trackingHelper) {
        [[SKPaymentQueue defaultQueue] removeTransactionObserver:[AppleIAPHelper sharedHelper]];
    }
}

+ (AppleIAPHelper *)sharedHelper {
    static AppleIAPHelper * _sharedHelper = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedHelper = [[AppleIAPHelper alloc] init];
        // load store
        [[SKPaymentQueue defaultQueue] addTransactionObserver:_sharedHelper];
        _sharedHelper.trackingHelper = YES;
    });
    return _sharedHelper;
}

- (instancetype)init {
    
    if ((self = [super init])) {
        // Check for previously purchased products
    }
    return self;
}

#pragma mark - Step by step payment process
// step 1: create request
- (void)buyProductWithIdentifier:(NSString *)productIdentifier {
    VTCLog(@"SDK/In-App/Step 1: productIdentifier = %@", productIdentifier);
    if (![AFNetworkReachabilityManager sharedManager].reachable) {
        [Utilities showNotification:VTCLocalizeString(@"No Internet connection")];
    } else {
        // log start inapp
        [[VtcEventLogging manager] logStartInAppWithOrderNo:[Global sharedInstance].currentOrderNo forUser:[VIDUser currentUser].userName userId:[VIDUser currentUser].userId completion:nil];
        // request to verify product
        NSSet *productSet = [NSSet setWithObject:productIdentifier];
        [self requestProduct:productSet];
    }
}
// step 1.1: restore purchased product - not gonna happen in almost game
- (void)restoreAllCompletedTransactions {
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

// step 2: send request
- (void)requestProduct:(NSSet *)productSet {
    VTCLog(@"SDK/In-App/Step 2: start request product = %@", [productSet allObjects]);
    request_ = [[SKProductsRequest alloc] initWithProductIdentifiers:productSet];
    request_.delegate = self;
    [request_ start];
}

// step 2.1: waiting for the result of step 2

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
    VTCLog(@"SDK/In-App/Step 2.1: FAILED with error = %@", error.description);
    if (_delegate && [_delegate respondsToSelector:@selector(paymentInAppPurchaseDidFailWithTransaction:)]) {
        [_delegate paymentInAppPurchaseDidFailWithTransaction:nil];
    }
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    VTCLog(@"SDK/In-App/Step 2.2: PASSED step 2");
    request_ = nil;
    // FAIL
    if (response.products) {
        if (response.products.count == 0) {
            [Utilities showNotification:VTCLocalizeString(@"No product found. Please try again")];
            if (_delegate && [_delegate respondsToSelector:@selector(paymentInAppPurchaseDidFailWithTransaction:)]) {
                [_delegate paymentInAppPurchaseDidFailWithTransaction:nil];
            }
            return;
        }
    }
    // SUCCESS => go to step 3
    [[AppleIAPHelper sharedHelper] purchaseProduct:[response.products firstObject]];
}

// step 3: buy the available selected product
- (void)purchaseProduct:(SKProduct *)product{
    //SKProductsRequest
    VTCLog(@"SDK/In-App/Step 3: start purchase package with id = %@", product.productIdentifier);
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

// step 3.1: result of step 3
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    
    for (SKPaymentTransaction *transaction in transactions)
    {
        VTCLog(@"SDK/In-App/Step 3.1: product = %@ - trans id = %@", transaction.payment.productIdentifier, transaction.transactionIdentifier);
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                VTCLog(@"SDK/In-App/Step 3.1: successed");
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                VTCLog(@"SDK/In-App/Step 3.1: failed");
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                VTCLog(@"SDK/In-App/Step 3.1: restored");
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStatePurchasing:
                VTCLog(@"SDK/In-App/Step 3.1: purchasing");
                // ignore it
                break;
            case SKPaymentTransactionStateDeferred:
                VTCLog(@"SDK/In-App/Step 3.1: deferred");
                [self failedTransaction:transaction];
                break;
            default:
                VTCLog(@"SDK/In-App/Step 3.1: unknow");
                [self failedTransaction:transaction];
                break;
        }
    }
}

// step 4: the result of payment process
- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    [Utilities hideNotificationView];
    @try {
        [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    }
    @catch (NSException *exception) {
        VTCLog(@"exception: %@", exception.description);
    }
    @finally {
        // try to verify client-ly
//        [self verifyReceiptWithAppStoreForTransaction:transaction];
    }
    
    NSLog(@"<VtcSDK> originalTransaction id = %@", transaction.originalTransaction.transactionIdentifier);
    NSLog(@"<VtcSDK> transaction id = %@", transaction.transactionIdentifier);
    [self logFinishInAppOf:[VIDUser currentUser].userName userId:[VIDUser currentUser].userId orderNumber:[Global sharedInstance].currentOrderNo];
    
    // verify this transaction
    [self verifyReceiptWithVTCServerForTransaction:[self dictInfoFromTransaction:transaction] retryTransaction:NO completion:nil];
    // show the progress only at the first pay
    [SVProgressHUD showWithStatus:VTCLocalizeString(@"We're verifying your transaction. Please wait a second")];
}

- (void)logFinishInAppOf:(NSString *)userName userId:(NSString *)userId orderNumber:(NSString *)orderNo {
    [[VtcEventLogging manager] logFinishInAppWithOrderNo:orderNo forUser:userName userId:userId completion:^(BOOL status, id responsedObject, NSError *error) {
        if (!status) {
            // cached
            @try {
                [self cacheLogInApp:@{
                                      @"orderNo" : orderNo ? orderNo : @"",
                                      @"username" : userName ? userName : @"",
                                      @"userId" : userId ? userId : @""
                                      }];
            } @catch (NSException *exception) {
                NSLog(@"<VtcSDK> can not cache the log finish inapp");
            } @finally {
                // do nothing
                // can not cache
            }
        }
    }];
}

- (void)cacheLogInApp:(NSDictionary *)logInfo {
    if (!logInfo) return;
    NSArray *finishLogStack = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultStackOfIAPFinishLog];
    NSMutableArray *newStack = nil;
    if (finishLogStack) {
        newStack = [[NSMutableArray alloc] initWithArray:finishLogStack];
    }
    else {
        newStack = [[NSMutableArray alloc] init];
    }
    // push new cache to bottom of stack
    [newStack addObject:logInfo];
    [[NSUserDefaults standardUserDefaults] setObject:newStack forKey:kUserDefaultStackOfIAPFinishLog];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)emptyInAppLogStack {
    NSArray *finishLogStack = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultStackOfIAPFinishLog];
    NSLog(@"<VtcSDK> cached log: %@", finishLogStack);
    if (!finishLogStack) return;
    if ([finishLogStack count] == 0) return;
    NSMutableArray *editableStack = [[NSMutableArray alloc] initWithArray:finishLogStack];
    // get the first from stack
    NSDictionary *log = editableStack.firstObject;
    [editableStack removeObjectAtIndex:0];
    [[NSUserDefaults standardUserDefaults] setObject:editableStack forKey:kUserDefaultStackOfIAPFinishLog];
    [[NSUserDefaults standardUserDefaults] synchronize];
    // try to log to server
    [self logFinishInAppOf:[log objectForKey:@"username"] userId:[log objectForKey:@"userId"] orderNumber:[log objectForKey:@"orderNo"]];
    // do it again
    [self emptyInAppLogStack];
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    
    @try {
        [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    }
    @catch (NSException *exception) {
        VTCLog(@"exception: %@", exception.description);
    }
    @finally {
        // do something
    }
    VTCLog(@"SDK/In-App/Step 4.3: purchase FAILED with error = %@", transaction.error.description);
    
    // is been called when some transaction was failed
    // do something SDK want at here if error happens
    if (transaction.error.code != SKErrorPaymentCancelled) {
//        [Utilities showNotification:transaction.error.localizedDescription];
    }
    [Utilities showNotification:VTCLocalizeString(@"Purchase failed. Please try again")];
    if (_delegate && [_delegate respondsToSelector:@selector(paymentInAppPurchaseDidFailWithTransaction:)]) {
        [_delegate paymentInAppPurchaseDidFailWithTransaction:transaction];
    }
}

/*
- (void)verifyReceiptWithAppStoreForTransaction:(SKPaymentTransaction *)transaction {
    // Load the receipt from the app bundle.
    NSData *receiptData = [NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]];
    if (!receiptData) {
        return;
    }
    
    // ... Send the receipt data to your server ...
    
    // Create the JSON object that describes the request
    NSError *error;
    NSDictionary *requestContents = @{
                                      @"receipt-data": [receiptData base64EncodedStringWithOptions:0]
                                      };
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:requestContents
                                                          options:0
                                                            error:&error];
    
    if (!requestData) { 
        // ... Handle error ...
    }
    
    // Create a POST request with the receipt data.
//    NSURL *storeURL = [NSURL URLWithString:@"https://buy.itunes.apple.com/verifyReceipt"];
    NSURL *storeURL = [NSURL URLWithString:@"https://sandbox.itunes.apple.com/verifyReceipt"];
    NSMutableURLRequest *storeRequest = [NSMutableURLRequest requestWithURL:storeURL];
    [storeRequest setHTTPMethod:@"POST"];
    [storeRequest setHTTPBody:requestData];
    
    // Make a connection to the iTunes Store on a background queue.
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:storeRequest queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               if (connectionError) {
                                   //... Handle error ...
                               } else {
                                   NSError *error;
                                   NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                   if (!jsonResponse) {
                                       // ... Handle error ...
                                   }
                                   // ... Send a response back to the device ...
                                   else {
                                       VTCLog(@"response: %@", jsonResponse);
                                   }
                               }
                           }];

}
*/

- (void)removeCachedTransactions {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kUserDefaultStackOfIAPPayment];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)cacheListTransactions:(NSArray *)arrayOfTransactionInfo {
    if (arrayOfTransactionInfo) return;
    [[NSUserDefaults standardUserDefaults] setObject:arrayOfTransactionInfo forKey:kUserDefaultStackOfIAPPayment];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSDictionary *)dictInfoFromTransaction:(SKPaymentTransaction *)transaction {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    @try {
        // param info
        [dict setObject:[VIDUser currentUser].userName ? [VIDUser currentUser].userName : @"" forKey:@"username"];
        [dict setObject:[VIDUser currentUser].accessToken ? [VIDUser currentUser].accessToken : @"" forKey:@"access_token"];
        // body info
        [dict setObject:[Utilities getIPAddress] forKey:@"fromIp"];
        [dict setObject:[NSNumber numberWithInteger:[VIDUser currentUser].authenType] forKey:@"authenType"];
        [dict setObject:@"1" forKey:@"typeIAP"];
        NSData *receiptData = [NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]];
        [dict setObject:[receiptData base64EncodedStringWithOptions:0] forKey:@"receiptData"];
        [dict setObject:transaction.transactionIdentifier ? transaction.transactionIdentifier : @"" forKey:@"transactionIdentifier"];
        [dict setObject:[NSNumber numberWithLong:(long)[[NSDate date] timeIntervalSince1970]] forKey:@"purchaseDate"];
        [dict setObject:transaction.payment.productIdentifier forKey:@"productIdentifier"];
        [dict setObject:[VtcConfig defaultConfig].areaId ? [VtcConfig defaultConfig].areaId : @"" forKey:@"serverGame"];
        [dict setObject:[AppleIAPHelper sharedHelper].currentItem.orderNo ? [AppleIAPHelper sharedHelper].currentItem.orderNo : @"" forKey:@"uuid"];
        [dict setObject:[NSNumber numberWithInteger:_packageType] forKey:@"packageType"];
        [dict setObject:[VtcConfig defaultConfig].extendData ? [VtcConfig defaultConfig].extendData : @"" forKey:@"extendData"];
        // header info
        [dict setObject:[[VtcConfig defaultConfig] appId] forKey:@"client_id"];
        //others
        [dict setObject:[NSNumber numberWithInteger:_currentItem.itemPriceGold] forKey:@"priceGold"];
        [dict setObject:_currentItem.currency ? _currentItem.currency : @"" forKey:@"currency"];
        [dict setObject:_currentItem.itemName ? _currentItem.itemName : @"" forKey:@"packageName"];
    
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK> Cache IAP transaction failed. Can not retrieve info dictionary with error: %@", exception.description);
    } @finally {
        return dict;
    }
}

- (void)verifyReceiptWithVTCServerForTransaction:(NSDictionary *)transaction retryTransaction:(BOOL)retry completion:(void (^)(BOOL result, BOOL remove))completionBlock {
    if (_verifyingATransaction) {
        if (completionBlock) completionBlock(NO, NO);
        return;
    }
    
    if ([Global sharedInstance].isReachable == NO) {
        NSLog(@"<VtcSDK> Stop verify in-app purchase because no internet connection found.");
        if (completionBlock) completionBlock(NO, NO);
        return;
    }
    
    NSString *transactionId = [transaction objectForKey:@"transactionIdentifier"];
    if (transactionId == nil) {
        NSLog(@"<VtcSDK> Verifying transaction failed. Found no transaction id");
        if (completionBlock) completionBlock(NO, NO);
        return;
    }
    // verify with server
    // send receipt to server
    _verifyingATransaction = YES;
//    [SVProgressHUD show];
    [[NetworkModal sharedModal] verifyTransaction:transaction completion:^(BOOL status, id responsedObject, NSError *error) {
        // turn off the flag
        [SVProgressHUD dismiss];
        _verifyingATransaction = NO;
        if (status) {
            if (completionBlock) completionBlock(YES, YES);
            @try {
                [Global sharedInstance].paymentMessage = [SmartParser stringForKey:@"Description" from:responsedObject];
                [Global sharedInstance].paymentGoldNumber = [[transaction objectForKey:@"priceGold"] integerValue];
                [Global sharedInstance].paymentCurrency = [transaction objectForKey:@"currency"];
            } @catch (NSException *exception) {
                VTCLog(@"%@", exception.description);
            } @finally {
                if (_delegate && [_delegate respondsToSelector:@selector(paymentInAppPurchaseDidSuccessWithTransaction:)]) {
                    [_delegate paymentInAppPurchaseDidSuccessWithTransaction:nil];
                }
                else {
                    [Utilities showPaymentResultPopup];
                }
            }
            
            // Checking With FireBase
            [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypePayment]];
            
            // appsflyer logging
            @try {
                NSNumber *revenue = [SmartParser objectForKey:@"Amount" from:responsedObject];
                if (revenue) {
                    VTCLog(@"%@", revenue.stringValue);
                    [[VtcEventLogging manager] setAppsFlyerInAppEvent:kAppsFlyerEventTypePurchase values:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                          revenue.stringValue, AFEventParamRevenue,
                                                                                                          @"VND", AFEventParamCurrency,
                                                                                                          nil]];
                }
            } @catch (NSException *exception) {
                VTCLog(@"%@", exception.description);
            } @finally {
                [[VtcEventLogging manager] setAppsFlyerInAppEvent:kAppsFlyerEventTypePurchase values:[NSDictionary dictionaryWithObjectsAndKeys:@"1", kAppsFlyerEventTypePurchaseValueKey, nil]];
            }
        }
        else {
            if (_delegate && [_delegate respondsToSelector:@selector(paymentInAppPurchaseDidFailWithTransaction:)]) {
                [_delegate paymentInAppPurchaseDidFailWithTransaction:nil];
            }
            
            // if verify fail because network problem, keep the cache for later, else, remove it
            if (error) {
                if (error.code == 404) {
                    if (completionBlock) completionBlock(NO, NO);
                    [Utilities showNotification:VTCLocalizeString(@"Verify failed. Please contact us to solve the problem")];
                    NSLog(@"error code = %ld", (long)error.code);
                    NSLog(@"transaction: %@", transaction);
                    //[retainedSelft cacheSingleTransactionInfo:transaction];
                    NSArray *cachedTransactions = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultStackOfIAPPayment];
                    NSMutableArray *editableTransactions = [[NSMutableArray alloc] init];
                    if (!cachedTransactions) {
                        [editableTransactions addObjectsFromArray:cachedTransactions];
                    }
                    [editableTransactions addObject:transaction];
                    NSLog(@"pre-log: %@", editableTransactions);
                    [[NSUserDefaults standardUserDefaults] setObject:editableTransactions forKey:kUserDefaultStackOfIAPPayment];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                else {
                    if (completionBlock) completionBlock(NO, YES);
                    NSString *message = [error.userInfo objectForKey:@"message"];
                    if (!message) {
                        message = VTCLocalizeString(@"Verify failed. Please contact us to solve the problem");
                    }
                    [Utilities showNotification:message];
                    if ((error.code == -401) || (error.code == -303)) {
                        if (!retry) {
                            __block UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:VTCLocalizeString(@"Session's expired") message:VTCLocalizeString(@"Do you want to relogin now?") preferredStyle:UIAlertControllerStyleAlert];
                            [alertVC addAction:[UIAlertAction actionWithTitle:VTCLocalizeString(@"Later") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                                [alertVC dismissViewControllerAnimated:NO completion:nil];
                            }]];
                            [alertVC addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                [alertVC dismissViewControllerAnimated:NO completion:^{
                                    NSLog(@"dismiss alert");
                                }];
                                [[SDKManager defaultManager] signInFromViewController:[UIViewController topViewController]];
                            }]];
                            [[UIViewController topViewController] presentViewController:alertVC animated:NO completion:nil];
                        }
                    }
                }
            }
            else {
                if (completionBlock) completionBlock(NO, YES);
            }
        }
    }];
}

@end

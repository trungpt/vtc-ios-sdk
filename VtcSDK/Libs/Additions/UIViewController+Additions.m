//
//  UIViewController+Additions.m
//  VtcSDK
//
//  Created by Kent Vu on 7/25/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "UIViewController+Additions.h"
#import "NSBundle+Additions.h"

@implementation UIViewController (Additions)

+ (id)loadFromNibNamed:(NSString *)nibName {
    return [[NSClassFromString(nibName) alloc] initWithNibName:nibName bundle:[NSBundle vtcBundle]];
}

+ (UIViewController *)topViewController {
    UIViewController *topVC = [[UIApplication sharedApplication] keyWindow].rootViewController;
    while (topVC.presentedViewController) {
        topVC = topVC.presentedViewController;
    }
    NSAssert(topVC, @"Error: UIWindow's rootViewController is nil");
    return topVC;
}

@end

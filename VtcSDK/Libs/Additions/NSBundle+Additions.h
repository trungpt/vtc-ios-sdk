//
//  NSBundle+Additions.h
//  Soha
//
//  Created by Anh Tran on 10/5/12.
//  Copyright (c) 2012 SohaGame. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSBundle (Additions)

+ (NSBundle*)vtcBundle;

@end

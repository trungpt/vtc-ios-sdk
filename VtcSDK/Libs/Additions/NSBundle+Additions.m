//
//  NSBundle+Additions.m
//  Soha
//
//  Created by Anh Tran on 10/5/12.
//  Copyright (c) 2012 SohaGame. All rights reserved.
//

#import "NSBundle+Additions.h"

@implementation NSBundle (Additions)

+ (NSBundle*)vtcBundle {
    static NSBundle *gMainBundle = nil;
    if(gMainBundle==nil)
    {
        NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"VtcSDKResource" ofType:@"bundle"];
        if (bundlePath) {
            gMainBundle = [[NSBundle alloc] initWithPath:bundlePath];
        }
    
        if (!gMainBundle) {
            VTCLog(@"ERROR! Cannot load resources");
        }
        else {
            VTCLog(@"Load resource ok.");
        }
    }
    
    return gMainBundle;
}

@end

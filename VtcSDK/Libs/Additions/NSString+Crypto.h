//
//  NSString+Crypto.h
//  VtcSDK
//
//  Created by Kent Vu on 8/11/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Crypto)

- (NSString *)MD5;

@end

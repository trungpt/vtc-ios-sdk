//
//  UIImage+Additions.m
//  VtcSDK
//
//  Created by Kent Vu on 7/26/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "UIImage+Additions.h"
#import "NSBundle+Additions.h"

@implementation UIImage (Additions)

+ (UIImage *)vtcImageNamed:(NSString *)imageName {
    return [UIImage imageNamed:imageName inBundle:[NSBundle vtcBundle] compatibleWithTraitCollection:nil];
}

@end

//
//  UIViewController+Additions.h
//  VtcSDK
//
//  Created by Kent Vu on 7/25/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Additions)

+ (id)loadFromNibNamed:(NSString *)nibName;
+ (UIViewController *)topViewController;

@end

//
//  UIImage+Additions.h
//  VtcSDK
//
//  Created by Kent Vu on 7/26/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Additions)

+ (UIImage *)vtcImageNamed:(NSString *)imageName;

@end

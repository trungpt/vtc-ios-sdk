//
//  SwitchStatusButton.h
//  VtcSDK
//
//  Created by Kent Vu on 8/21/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SwitchStatusButton : UIButton

@property (nonatomic, strong) NSArray *items;

@end

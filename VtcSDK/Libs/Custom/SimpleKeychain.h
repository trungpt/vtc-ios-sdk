//
//  SimpleKeychain.h
//  HealthTrackingForSon
//
//  Created by Kent Vu on 7/4/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SimpleKeychain : NSObject

+ (void)save:(NSString *)service data:(id)data;
+ (id)load:(NSString *)service;
+ (void)delete:(NSString *)service;

@end

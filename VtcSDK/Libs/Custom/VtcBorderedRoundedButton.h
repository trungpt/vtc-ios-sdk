//
//  VtcBorderedRoundedButton.h
//  VtcSDK
//
//  Created by Kent Vu on 9/5/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcRoundedButton.h"

@interface VtcBorderedRoundedButton : VtcRoundedButton

@property (nonatomic, strong) id extendData;

@end

//
//  VtcBorderedRoundedButton.m
//  VtcSDK
//
//  Created by Kent Vu on 9/5/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcBorderedRoundedButton.h"

@implementation VtcBorderedRoundedButton

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void)customInit {
    [super customInit];
    [self setRadius:5.0];
    self.layer.borderWidth = 1.0;
    self.layer.borderColor = [ColorFromHEX(0xd3d3d3) CGColor];
    self.layer.cornerRadius = 5.0;
}

@end

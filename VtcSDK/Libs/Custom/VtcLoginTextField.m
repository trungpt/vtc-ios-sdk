//
//  VtcLoginTextField.m
//  VtcSDK
//
//  Created by Kent Vu on 7/25/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcLoginTextField.h"
#import "UIImage+Additions.h"

@interface VtcLoginTextField ()
@property (strong, nonatomic) UIButton * eyeButton;
@property (assign, nonatomic) BOOL isSelectedEye;
@end

@implementation VtcLoginTextField

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void)customInit {
    self.borderStyle = UITextBorderStyleNone;
    self.isSelectedEye = NO;
    if (_tfNext) {
        self.returnKeyType = UIReturnKeyNext;
    }
    else {
        self.returnKeyType = UIReturnKeyGo;
    }
    [self addTarget:self action:@selector(textFieldDidEndOnExit) forControlEvents:UIControlEventEditingDidEndOnExit];
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    CGFloat height = bounds.size.height;
    UIEdgeInsets insets = UIEdgeInsetsMake(0, height, 0, height);
    CGRect paddedRect = UIEdgeInsetsInsetRect(bounds, insets);

    if (self.rightViewMode == UITextFieldViewModeAlways || self.rightViewMode == UITextFieldViewModeUnlessEditing) {
        return [self adjustRectWithWidthRightView:paddedRect];
    }
    return paddedRect;
}

- (CGRect)placeholderRectForBounds:(CGRect)bounds {
    CGFloat height = bounds.size.height;
    UIEdgeInsets insets = UIEdgeInsetsMake(0, height, 0, height);
    CGRect paddedRect = UIEdgeInsetsInsetRect(bounds, insets);

    if (self.rightViewMode == UITextFieldViewModeAlways || self.rightViewMode == UITextFieldViewModeUnlessEditing) {
        return [self adjustRectWithWidthRightView:paddedRect];
    }
    return paddedRect;
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    CGFloat height = bounds.size.height;
    UIEdgeInsets insets = UIEdgeInsetsMake(0, height, 0, height);
    CGRect paddedRect = UIEdgeInsetsInsetRect(bounds, insets);

    if (self.rightViewMode == UITextFieldViewModeAlways || self.rightViewMode == UITextFieldViewModeWhileEditing) {
        return [self adjustRectWithWidthRightView:paddedRect];
    }
    return paddedRect;
}

- (CGRect)adjustRectWithWidthRightView:(CGRect)bounds {
    CGRect paddedRect = bounds;
    paddedRect.size.width -= CGRectGetWidth(self.rightView.frame);

    return paddedRect;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CGFloat height = rect.size.height;
    if (_leftViewImage) {
        UIImageView *leftViewImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, height, height)];
        leftViewImageView.contentMode = UIViewContentModeCenter;
        leftViewImageView.image = _leftViewImage;
        self.leftView = leftViewImageView;
        self.leftViewMode = UITextFieldViewModeAlways;
    }
    else {
        self.leftViewMode = UITextFieldViewModeNever;
    }
    
    if (_rightViewImage) {
        UIImageView *rightViewImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, height, height)];
        rightViewImageView.contentMode = UIViewContentModeCenter;
        rightViewImageView.image = _rightViewImage;
        self.rightView = rightViewImageView;
        self.rightViewMode = UITextFieldViewModeAlways;
    }
    else {
        self.rightViewMode = UITextFieldViewModeNever;
    }
    
    if (_passwordEyeEnable) {
        self.eyeButton = [[UIButton alloc] init];
        [self.eyeButton setImage:[UIImage vtcImageNamed:@"eye-icon-inactive"] forState:UIControlStateNormal];
        [self.eyeButton setImage:[UIImage vtcImageNamed:@"eye-icon-active"] forState:UIControlStateSelected];
        [self.eyeButton addTarget:self action:@selector(onEyeButton:) forControlEvents:UIControlEventTouchUpInside];
        CGFloat tfHeight = rect.size.height;
        self.eyeButton.frame = CGRectMake(0, 0, tfHeight, tfHeight);
        self.rightViewMode = UITextFieldViewModeAlways;
        self.rightView = self.eyeButton;
        self.eyeButton.selected = self.isSelectedEye;
        self.secureTextEntry = !self.isSelectedEye;
    }
    
}

- (IBAction)onEyeButton:(UIButton *)eyeBtn {
    self.isSelectedEye = !self.isSelectedEye;
    self.eyeButton.selected = self.isSelectedEye;
    self.secureTextEntry = !self.isSelectedEye;
//    self.secureTextEntry = !self.isSecureTextEntry;
//    [self.eyeButton setSelected:!self.isSecureTextEntry];
    self.font = nil;
    self.font = [UIFont systemFontOfSize:15];
    [self resignFirstResponder];
}

- (void)textFieldDidEndOnExit {
    if (_tfNext) {
        [_tfNext becomeFirstResponder];
    }
    if (self.handleDidEndOnExit != nil) {
        self.handleDidEndOnExit();
    }
}

- (void)setLeftViewImage:(UIImage *)leftViewImage {
    _leftViewImage = leftViewImage;
    if (_leftViewImage) {
        CGFloat height = self.bounds.size.height;
        UIImageView *leftViewImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 0.8*height, 0.8*height)];
        leftViewImageView.contentMode = UIViewContentModeLeft;
        leftViewImageView.image = _leftViewImage;
        self.leftView = leftViewImageView;
        self.leftViewMode = UITextFieldViewModeAlways;
    }
    else {
        self.leftViewMode = UITextFieldViewModeNever;
    }
}

- (void)setRightViewImage:(UIImage *)rightViewImage {
    _rightViewImage = rightViewImage;
    if (_rightViewImage) {
        CGFloat height = self.bounds.size.height;
        UIImageView *rightViewImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, height, height)];
        rightViewImageView.contentMode = UIViewContentModeLeft;
        rightViewImageView.image = _rightViewImage;
        self.rightView = rightViewImageView;
        self.rightViewMode = UITextFieldViewModeAlways;
    }
    else {
        self.rightViewMode = UITextFieldViewModeNever;
    }
}


@end

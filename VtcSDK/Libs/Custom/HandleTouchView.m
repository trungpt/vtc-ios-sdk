//
//  HandleTouchView.m
//  VtcSDK
//
//  Created by Kent Vu on 8/1/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "HandleTouchView.h"

@implementation HandleTouchView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void)customInit {
    [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onBackground)]];
}

- (void)onBackground {
    [self endEditing:YES];
}

@end

//
//  VtcRoundedButton.m
//  VtcSDK
//
//  Created by Kent Vu on 7/25/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcRoundedButton.h"

@implementation VtcRoundedButton

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void)customInit {
    self.layer.masksToBounds = YES;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    if (_radius > 0) {
        self.layer.cornerRadius = _radius;
    }
    if (_radiusInFloatPercent > 0) {
        self.layer.cornerRadius = _radiusInFloatPercent*rect.size.height;
    }
}

@end

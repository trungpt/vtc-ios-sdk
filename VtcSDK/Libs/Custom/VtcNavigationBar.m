//
//  VtcNavigationBar.m
//  VtcSDK
//
//  Created by Kent Vu on 8/16/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcNavigationBar.h"

@implementation VtcNavigationBar

- (CGSize)sizeThatFits:(CGSize)size {
    CGSize newSize;
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if(orientation == UIInterfaceOrientationLandscapeRight ||
       orientation == UIInterfaceOrientationLandscapeLeft) {
        if ([[UIDevice currentDevice].model containsString:@"iPad"]) {
            newSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, [Global sharedInstance].gPortraitNavBarHeight);
        }
        else {
            newSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, [Global sharedInstance].gLandscapeNavBarHeight);
        }
    }
    else {
        newSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, [Global sharedInstance].gPortraitNavBarHeight);
    }
    
    return newSize;
}

@end

//
//  VtcLoginTextField.h
//  VtcSDK
//
//  Created by Kent Vu on 7/25/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VtcLoginTextField : UITextField

@property (nonatomic, weak) IBOutlet UITextField *tfPrevious;
@property (nonatomic, weak) IBOutlet UITextField *tfNext;

@property (nonatomic) IBInspectable UIImage *leftViewImage;
@property (nonatomic) IBInspectable UIImage *rightViewImage;
@property (nonatomic) IBInspectable BOOL passwordEyeEnable;

@property (nonatomic, copy) void (^handleDidEndOnExit)();

@end

//
//  VtcNavigationBar.h
//  VtcSDK
//
//  Created by Kent Vu on 8/16/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VtcNavigationBar : UINavigationBar

@end

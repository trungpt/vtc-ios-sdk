//
//  CircleImageView.m
//  VtcSDK
//
//  Created by Kent Vu on 8/9/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "CircleImageView.h"

@implementation CircleImageView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void)customInit {
    self.layer.masksToBounds = YES;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    if (rect.size.width != rect.size.height) {
        rect.size.height = rect.size.width;
        self.frame = rect;
    }
    self.layer.cornerRadius = rect.size.width/2;
}

@end

//
//  VtcBorderedRoundedView.m
//  VtcSDK
//
//  Created by Kent Vu on 9/5/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcBorderedRoundedView.h"

@implementation VtcBorderedRoundedView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void)customInit {
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 5.0;
    self.layer.borderWidth = 1.0;
    self.layer.borderColor = [ColorFromHEX(0xd3d3d3) CGColor];
}

@end

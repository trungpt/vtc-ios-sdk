//
//  SwitchStatusButton.m
//  VtcSDK
//
//  Created by Kent Vu on 8/21/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "SwitchStatusButton.h"
#import "UIImage+Additions.h"

@interface SwitchStatusButton ()

@property (nonatomic, strong) UIImageView *rightImageView;

@end

@implementation SwitchStatusButton

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void)customInit {
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [self setTitleColor:ColorFromHEX(0x333333) forState:UIControlStateNormal];
    [self setTitleColor:ColorFromHEX(0xffffff) forState:UIControlStateSelected];
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 5.0;
    
    // add right arrow
    self.rightImageView = [[UIImageView alloc] init];
    [self addSubview:_rightImageView];
    _rightImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self updateStatus];
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    [self updateStatus];
}

- (void)updateStatus {
    if (self.selected) {
        self.backgroundColor = ColorFromHEX(0x0bb9fa);
        self.titleLabel.font = [UIFont boldSystemFontOfSize:15];
        _rightImageView.image = [UIImage vtcImageNamed:@"arrow-right-white"];
        [_rightImageView sizeToFit];
    }
    else {
        self.backgroundColor = ColorFromHEX(0xe1e1e1);
        self.titleLabel.font = [UIFont systemFontOfSize:15];
        _rightImageView.image = [UIImage vtcImageNamed:@"arrow-right-black"];
        [_rightImageView sizeToFit];
    }
    [self layoutSubviews];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect sRect = self.frame;
    CGSize iSize = _rightImageView.image.size;
    _rightImageView.frame = CGRectMake(sRect.size.width-iSize.width-8, (sRect.size.height-iSize.height)/2, iSize.width, iSize.height);
}

@end

//
//  CircleImageView.h
//  VtcSDK
//
//  Created by Kent Vu on 8/9/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CircleImageView : UIImageView

@end

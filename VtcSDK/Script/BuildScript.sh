
echo "Go to Step 1 - Initialize"
# Step 1: Initialize
VTC_FRAMEWORK_LOCATION="${BUILT_PRODUCTS_DIR}/${TARGET_NAME}.framework"
UNIVERSAL_FRAMEWORK_BUILD_DIR="${PROJECT_DIR}/../build/"
rm -rf "${UNIVERSAL_FRAMEWORK_BUILD_DIR}"
mkdir -p "${UNIVERSAL_FRAMEWORK_BUILD_DIR}"

# Step 3: Finish & export artifact
# Copy the framework to the destination folders
ditto "${VTC_FRAMEWORK_LOCATION}" "${UNIVERSAL_FRAMEWORK_BUILD_DIR}/${TARGET_NAME}.framework"
# Copy the resources bundle to the user's desktop
ditto "${BUILT_PRODUCTS_DIR}/VtcSDKResource.bundle" "${UNIVERSAL_FRAMEWORK_BUILD_DIR}/VtcSDKResource.bundle"


#########

exec > /tmp/my_log_file.txt 2>&1
set -e

echo "Go to Step 1 - Initialize"
# Step 1: Initialize
# If we're already inside this script then die
if [ -n "$RW_MULTIPLATFORM_BUILD_IN_PROGRESS" ]; then
exit 0
fi
export VTC_MULTIPLATFORM_BUILD_IN_PROGRESS=1

VTC_FRAMEWORK_LOCATION="${BUILT_PRODUCTS_DIR}/${TARGET_NAME}.framework"
rm -rf "${VTC_FRAMEWORK_LOCATION}"
ALL_BUILD_LOCATION="${BUILT_PRODUCTS_DIR}/../"
UNIVERSAL_FRAMEWORK_BUILD_DIR="${PROJECT_DIR}/../build/"
rm -rf "${UNIVERSAL_FRAMEWORK_BUILD_DIR}"
mkdir -p "${UNIVERSAL_FRAMEWORK_BUILD_DIR}"

function build_library {
# Will rebuild the static library as specified
#     build_static_library sdk
xcrun xcodebuild -project "${PROJECT_FILE_PATH}" \
-target "${TARGET_NAME}" \
-configuration "${CONFIGURATION}" \
-sdk "${1}" \
ONLY_ACTIVE_ARCH=NO \
BUILD_DIR="${BUILD_DIR}" \
OBJROOT="${OBJROOT}" \
BUILD_ROOT="${BUILD_ROOT}" \
SYMROOT="${SYMROOT}" $ACTION
}

echo "Go to Step 2 - Deploy & build"
# Step 2: Deploy & build
# Extract the platform (iphoneos/iphonesimulator) from the SDK name
if [[ "$SDK_NAME" =~ ([A-Za-z]+) ]]; then
VTC_SDK_PLATFORM=${BASH_REMATCH[1]}
else
echo "Could not find platform name from SDK_NAME: $SDK_NAME"
exit 1
fi

# Determine the other platform
if [ "$VTC_SDK_PLATFORM" == "iphoneos" ]; then
VTC_OTHER_SDK_PLATFORM=iphonesimulator
else
VTC_OTHER_SDK_PLATFORM=iphoneos
fi

echo "${VTC_SDK_PLATFORM}"
echo "${VTC_OTHER_SDK_PLATFORM}"

# Find the build directory
if [[ "$BUILT_PRODUCTS_DIR" =~ (.*)$VTC_SDK_PLATFORM$ ]]; then
# Build the curent platform.
build_library "${VTC_SDK_PLATFORM}"
echo "--come here--"
VTC_OTHER_BUILT_PRODUCTS_DIR="${ALL_BUILD_LOCATION}/${CONFIGURATION}-${VTC_OTHER_SDK_PLATFORM}"
VTC_OTHER_FRAMEWORK_LOCATION="${VTC_OTHER_BUILT_PRODUCTS_DIR}/${TARGET_NAME}.framework"

# Build the other platform.
build_library "${VTC_OTHER_SDK_PLATFORM}"

# If we're currently building for iphonesimulator, then need to rebuild
#   to ensure that we get both i386 and x86_64
if [ "$VTC_SDK_PLATFORM" == "iphonesimulator" ]; then
build_library "${VTC_SDK_PLATFORM}"
fi

if [ -d "${VTC_OTHER_FRAMEWORK_LOCATION}" ]; then
# Join the 2 framework into 1 and push into the .framework
lipo -create "${VTC_FRAMEWORK_LOCATION}/${TARGET_NAME}" "${VTC_OTHER_FRAMEWORK_LOCATION}/${TARGET_NAME}" -output "${VTC_FRAMEWORK_LOCATION}/${TARGET_NAME}"
else
echo "Could not find other framework file."
fi
else
echo "Could not find other platform build directory."
fi

echo "Go to Step 3 - Copy the target to build folder"
# Step 3: Finish & export artifact
# Copy the framework to the destination folders
ditto "${VTC_FRAMEWORK_LOCATION}" "${UNIVERSAL_FRAMEWORK_BUILD_DIR}/${TARGET_NAME}.framework"
# Copy the resources bundle to the user's desktop
ditto "${BUILT_PRODUCTS_DIR}/VtcSDKResource.bundle" "${UNIVERSAL_FRAMEWORK_BUILD_DIR}/VtcSDKResource.bundle"



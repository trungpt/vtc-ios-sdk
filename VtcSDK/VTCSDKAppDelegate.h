//
//  VTCSDKAppDelegate.h
//  VtcSDK
//
//  Created by Kent Vu on 3/30/17.
//  Copyright © 2017 VTCIntecom. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VTCSDKAppDelegate : NSObject

+ (VTCSDKAppDelegate *)loadSDKAppDelegate;

- (void)application:(UIApplication *)application sdk_didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;
- (void)application:(UIApplication *)application sdk_didFailToRegisterForRemoteNotificationsWithError:(NSError *)error;
- (void)application:(UIApplication *)application sdk_didReceiveRemoteNotification:(NSDictionary *)userInfo;

@end

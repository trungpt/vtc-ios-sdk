//
//  Global.m
//  VtcSDK
//
//  Created by Kent Vu on 7/25/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "Global.h"
#import "VIDUser.h"
#import "SDKManager.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
//#import <GoogleSignIn/GoogleSignIn.h>
#import "VtcEventLogging.h"
#import "NetworkModal+Tracking.h"
#import "SDKManager+Internal.h"
#import "AppleIAPHelper.h"
#import "SimpleKeychain.h"
#import "AppleIAPHelper.h"
#import "VtcShopViewController.h"
#import "SVProgressHUD.h"

@interface Global () <AppleIAPHelperDelegate>

@property (nonatomic, copy) NSString *utmString;

@end

@implementation Global

+ (Global *)sharedInstance {
    static Global *sharedInstance_ = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance_ = [[Global alloc] init];
        [sharedInstance_ setup];
    });
    return sharedInstance_;
}
- (NSString *)language
{
    return [[VtcConfig defaultConfig] sdkLanguage];
}

- (void)setup {
    
    self.globalMainColor = ColorFromHEX(0xffffff);
    self.gButtonColor = ColorFromHEX(0x0bb9fa);
    self.gPortraitNavBarHeight = 60.0; // 70.0
    self.gLandscapeNavBarHeight = 40.0;
    [AppleIAPHelper sharedHelper].delegate = self;
    
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
}

- (NSString *)getPushDeviceToken {
    if (_pushDeviceToken)
        return _pushDeviceToken;
    else
        return @"";
}

- (NSString *)getUtmString {
    if (_utmString)
        return _utmString;
    else
        return @"";
}

- (NSString *)getAdvertisingIdentifier {
    if (_advertisingIdentifier) return _advertisingIdentifier;
    else return @"";
}


- (void)setUtmString:(NSString *)utmString {
    if (utmString == nil)
    {
        return;
    }
    NSString *savedUrlString = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultCachedUTM];
    if (savedUrlString) {
        _utmString = utmString;
        return;
    }
    _utmString = utmString;

    [[NSUserDefaults standardUserDefaults] setObject:_utmString forKey:kUserDefaultCachedUTM];
    [[NSUserDefaults standardUserDefaults] synchronize];
    // re-hit to server to update utm string
//    [[NetworkModal sharedModal] hitInstallEventToServer:0];
}

- (void)handleSignInResponse:(id)loginData authenType:(AuthenType)type containerController:(UIViewController *)controller showWelcome:(BOOL)welcome {
    
    // save user info
    VTCLog(@"%@", loginData);
    NSLog(@"testahihi handleSignInResponse: %ld", (long)type);
    
    NSString *tempAppleSignInToken = [VIDUser currentUser].appleSignInToken;
    
    NSLog(@"testahihi appleSignInToken: %@", tempAppleSignInToken);
    if (type == AuthenTypeApple){
        NSLog(@"testahihi AuthenTypeApple");
    }else if (type == AuthenTypeFacebook){
        NSLog(@"testahihi AuthenTypeFacebook");
    }else if (type == AuthenTypeNormal){
        NSLog(@"testahihi AuthenTypeNormal");
    }else if (type == AuthenTypeGoogle){
        NSLog(@"testahihi AuthenTypeGoogle");
    }else if (type == AuthenTypeQuickStart){
        NSLog(@"testahihi AuthenTypeQuickStart");
    }else{
        NSLog(@"testahihi AuthenType Unknow");
    }
    
    [[VIDUser currentUser] loadData:loginData];
    [VIDUser currentUser].authenType = type;
    [VIDUser currentUser].appleSignInToken = tempAppleSignInToken;
    NSDictionary *userInfo = [[VIDUser currentUser] exportUserInfo];
    [[NSUserDefaults standardUserDefaults] setObject:userInfo forKey:kUserDefaultCachedUser];
    [[NSUserDefaults standardUserDefaults] synchronize];
    // appsflyer logging
    [[VtcEventLogging manager] setAppsFlyerInAppEvent:kAppsFlyerEventTypeLogin values:[NSDictionary dictionaryWithObjectsAndKeys:[VIDUser currentUser].userId, kAppsFlyerEventTypeLoginValueKey, nil]];
    [[VtcEventLogging manager] setAppsFlyerInAppEvent:kAppsFlyerEventTypeGMOLogin values:@{kAppsFlyerEventTypeGMOLoginValueKey: [VIDUser currentUser].userId}];
    if (welcome) {
        if ([SDKManager defaultManager].delegate && [[SDKManager defaultManager].delegate respondsToSelector:@selector(sdkManagerDidSignInWithUser:)]) {
            [[SDKManager defaultManager].delegate sdkManagerDidSignInWithUser:[VIDUser currentUser]];
        }
    }
    
    if (controller) {
        [controller dismissViewControllerAnimated:NO completion:^{
            [SDKManager defaultManager].showingLoginView = NO;
            if (welcome) [self showWelcomeNotification];
        }];
    }
    else {
        if (welcome) [self showWelcomeNotification];
    }
}

- (void)showWelcomeNotification {
    NSDictionary *normalAttributes = @{
                                       NSFontAttributeName : [UIFont systemFontOfSize:14],
                                       NSForegroundColorAttributeName : ColorFromHEX(0xffffff)
                                       };
    NSDictionary *coloredBoldAttributes = @{
                                            NSFontAttributeName : [UIFont boldSystemFontOfSize:14],
                                            NSForegroundColorAttributeName : ColorFromHEX(0x0BB9FA)
                                            };
    
    NSMutableAttributedString *line1 = [[NSMutableAttributedString alloc] init];
    [line1 appendAttributedString:[[NSAttributedString alloc] initWithString:[VTCLocalizeString(@"Hello") stringByAppendingString:@" "]  attributes:normalAttributes]];
    @try {
        VTCLog(@"%@", [VIDUser currentUser].userName);
        [line1 appendAttributedString:[[NSAttributedString alloc] initWithString:[VIDUser currentUser].userName attributes:coloredBoldAttributes]];
    } @catch (NSException *exception) {
        VTCLog(@"username not exist");
    } @finally {
        
    }
    
    [Utilities showAttributedTextNotification:line1];
    
}

- (void)handleSignOut {
    // fb
    [[FBSDKLoginManager new] logOut];
    // gg
//    [[GIDSignIn sharedInstance] signOut];
    // remove current user data
    [Global sharedInstance].savedUserName = nil;
    [[VIDUser currentUser] resetData];
    NSDictionary *userInfo = [[VIDUser currentUser] exportUserInfo];
    [[NSUserDefaults standardUserDefaults] setObject:userInfo forKey:kUserDefaultCachedUser];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

// MARK: Apple In-App delegate
- (void)paymentInAppPurchaseDidSuccessWithTransaction:(SKPaymentTransaction *)transaction {
    if (_shopVC) {
        [_shopVC.navigationController dismissViewControllerAnimated:NO completion:nil];
    }
    [SDKManager defaultManager].showingPaymentView = NO;
    [Utilities showPaymentResultPopup];
    if ([SDKManager defaultManager].delegate && [[SDKManager defaultManager].delegate respondsToSelector:@selector(sdkManagerDidPurchaseSuccessfully)]) {
        [[SDKManager defaultManager].delegate sdkManagerDidPurchaseSuccessfully];
    }
    [Global sharedInstance].currentOrderNo = nil;
}

- (void)paymentInAppPurchaseDidFailWithTransaction:(SKPaymentTransaction *)transaction {
    [Global sharedInstance].currentOrderNo = nil;
    if (_shopVC) {
        if (_shopVC.navigationController.viewControllers.count > 1) {
            [_shopVC.navigationController popViewControllerAnimated:NO];
        }
    }
}

@end

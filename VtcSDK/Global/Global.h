//
//  Global.h
//  VtcSDK
//
//  Created by Kent Vu on 7/25/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "VIDUser.h"
@class VtcShopViewController;

@interface Global : NSObject

+ (Global *)sharedInstance;

/*
 Device token's used to push notification
 */

@property (nonatomic) BOOL isReachable;

@property (nonatomic, copy, getter=getPushDeviceToken) NSString *pushDeviceToken;
@property (nonatomic, copy, getter=getAdvertisingIdentifier) NSString *advertisingIdentifier;

@property (nonatomic, strong) UIColor *globalMainColor;
@property (nonatomic, strong) UIColor *gButtonColor;

@property (nonatomic, copy) NSString *savedUserName;

@property (nonatomic) CGFloat gPortraitNavBarHeight;
@property (nonatomic) CGFloat gLandscapeNavBarHeight;

@property (nonatomic) BOOL flagOpenApp;
@property (nonatomic) BOOL bufferLinkChecked;
@property (nonatomic) BOOL isWaitingForShowLogin;
@property (nonatomic) BOOL isWaitingForShowRegister;

@property (nonatomic) BOOL enableFacebookLogin;
@property (nonatomic) BOOL enableGoogleLogin;

@property (nonatomic, assign) VtcShopViewController *shopVC;

@property (nonatomic, readonly) NSString * language;
/*
 UTM distribution URL
 Ex: "democlpt://?utm_source=facebook&utm_campaign=demo&utm_medium=1706"
 
 @"clpt://?utm_source=home&utm_campaign=direct&utm_medium="
 */

@property (nonatomic) NSInteger paymentGoldNumber;
@property (nonatomic, copy) NSString *paymentMessage;
@property (nonatomic, copy) NSString *paymentCurrency;
@property (nonatomic, copy) NSString *currentOrderNo;

- (NSString *)getUtmString;
- (void)setUtmString:(NSString *)utmString;

- (void)handleSignInResponse:(id)loginData authenType:(AuthenType)type containerController:(UIViewController *)controller showWelcome:(BOOL)welcome;
- (void)handleSignOut;

@end

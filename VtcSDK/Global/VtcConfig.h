//
//  VtcConfig.h
//  VtcSDK
//
//  Created by Kent Vu on 7/29/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VtcConfig : NSObject

@property (nonatomic, copy) NSString *areaId;
@property (nonatomic, copy) NSString *extendData;

+ (VtcConfig *)defaultConfig;
- (NSString *)sdkLanguage;
- (NSString *)appId;
- (NSString *)appSecret;
- (NSString *)appsFlyerDevKey;
- (NSString *)appStoreID;
- (NSString *)gameVersion;
- (BOOL)isDirectVersion;
- (NSString *)defaultUtmString;
- (long)gameVersionNumber;
- (void) setLanguage:(NSString *)language;

@end

//
//  VtcConfig.m
//  VtcSDK
//
//  Created by Kent Vu on 7/29/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VtcConfig.h"
@interface VtcConfig ()

@property (nonatomic, strong) NSDictionary *configDictionary;
@property (nonatomic, strong) NSMutableDictionary *mConfigDict;

@end

@implementation VtcConfig

+ (VtcConfig *)defaultConfig {
    static VtcConfig *defaultConfig_ = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        defaultConfig_ = [[VtcConfig alloc] init];

    });
    return defaultConfig_;
}
- (instancetype)init
{
    if (self = [super init])
    {
        self.configDictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"VtcSDK-Info" ofType:@"plist"]];
        self.mConfigDict = [[NSMutableDictionary alloc] initWithDictionary:self.configDictionary];
    }
    return self;
}

- (NSString *)sdkLanguage {
    
    NSAssert(_mConfigDict != nil, @"Please import VTC Config file");
    if ([_mConfigDict objectForKey:@"LANGUAGE"] == nil)
    {
        return @"vie";
    }
    return [_mConfigDict objectForKey:@"LANGUAGE"];
}
- (void)setLanguage:(NSString *)language
{
    [_mConfigDict setValue:language forKey:@"LANGUAGE"];
}

- (NSString *)appId {
    NSAssert(_configDictionary != nil, @"Please import VTC Config file");
//    if ([[self sdkLanguage] isEqualToString:@"vie"])
//    {
        return [_configDictionary objectForKey:@"VTC_APP_ID"];
//    }
//    else
//    {
//        return [_configDictionary objectForKey:@"VTC_APP_ID_EN"];
//    }
    
}

- (NSString *)appSecret {
    NSAssert(_configDictionary != nil, @"Please import VTC Config file");
    return [_configDictionary objectForKey:@"VTC_APP_SECRET"];
}

- (NSString *)appsFlyerDevKey {
    NSAssert(_configDictionary != nil, @"Please import VTC Config file");
    return [_configDictionary objectForKey:@"APPSFLYER_DEV_KEY"];
}

- (NSString *)appStoreID {
    NSAssert(_configDictionary != nil, @"Please import VTC Config file");
    return [_configDictionary objectForKey:@"APPSTORE_ID"];
}

- (BOOL)isDirectVersion {
    NSAssert(_configDictionary != nil, @"Please import VTC Config file");
    return [[_configDictionary objectForKey:@"DIRECT_VERSION"] boolValue];
}

- (NSString *)defaultUtmString {
    NSAssert(_configDictionary != nil, @"Please import VTC Config file");
    return [_configDictionary objectForKey:@"UTM_STRING"];
}

- (NSString *)gameVersion {
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
}

- (long)gameVersionNumber {
    NSString *gver = [self gameVersion];
    gver = [gver stringByReplacingOccurrencesOfString:@"." withString:@""];
    return (long)[gver integerValue];
}

@end

//
//  Utilities.m
//  VtcSDK
//
//  Created by Kent Vu on 7/25/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "Utilities.h"
#import "NSBundle+Additions.h"
#import <CoreText/CoreText.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#import "HDNotificationView.h"
#import "UIViewController+Additions.h"
#import "VtcPaymentReportView.h"
#import "KLCPopup.h"

@interface NSNull (JSON)
@end

@implementation NSNull (JSON)

- (NSUInteger)length { return 0; }

- (NSInteger)integerValue { return 0; };

- (float)floatValue { return 0; };

- (NSString *)description { return @"0(NSNull)"; }

- (NSArray *)componentsSeparatedByString:(NSString *)separator { return @[]; }

- (id)objectForKey:(id)key { return nil; }

- (BOOL)boolValue { return NO; }

@end

@implementation Utilities

/* Some note:
 Followed the order, sdk used Roboto family in  the sdk (UI + code).
 This is not the default font in system. And, I must used a way to load this font to sdk.
 But the func load custom has some problem, it works not perfectly in some case,
 and maybe lead to some crash (almost on iOS 10+).
 So, I decide to remove temporary this font in code (but keep in UIs file) to avoid crash issue.
 If you (the next developer) find a way to fix this problem, please replace all "systemFont" in sdk with Roboto family font.
 Thank you!
 Kent
 */


+ (void)loadAllCustomFont {
//    [self loadCustomFontWithName:@"Roboto-Regular"];
//    [self loadCustomFontWithName:@"Roboto-Bold"];
//    [self loadCustomFontWithName:@"Roboto-Italic"];
//    [self loadCustomFontWithName:@"Roboto-Light"];
}

/*
+ (void)loadCustomFontWithName:(NSString *)fontName {
    
    NSData *inData = [NSData dataWithContentsOfFile:[[NSBundle vtcBundle] pathForResource:fontName ofType:@"ttf"]];
    CFErrorRef error;
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((CFDataRef)inData);
    CGFontRef font = CGFontCreateWithDataProvider(provider);
    if (!CTFontManagerRegisterGraphicsFont(font, &error)) {
        CFStringRef errorDescription = CFErrorCopyDescription(error);
        VTCLog(@"Failed to load font: %@", errorDescription);
        CFRelease(errorDescription);
    }
    CFRelease(font);
    CFRelease(provider);
    
}
*/

+ (NSString *)getIPAddress {
    
    NSString *address = @"192.168.1.1";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
    
}

+ (void)showNotification:(NSString *)message {
    [HDNotificationView showNotificationViewWithImage:nil
                                                title:nil
                                              message:message
                                       attributedText:nil
                                           isAutoHide:YES
                                              onTouch:^{
                                                  [HDNotificationView hideNotificationViewOnComplete:nil];
                                              }];
}
+ (void)showPersistantNotification:(NSString *)message {
    [HDNotificationView showNotificationViewWithImage:nil
                                                title:nil
                                              message:message
                                       attributedText:nil
                                           isAutoHide:NO
                                              onTouch:^{
                                                  [HDNotificationView hideNotificationViewOnComplete:nil];
                                              }];
}

+ (void)hideNotificationView {
    [HDNotificationView hideNotificationView];
}

+ (void)showAttributedTextNotification:(NSAttributedString *)message {
    [HDNotificationView showNotificationViewWithImage:nil
                                                title:nil
                                              message:nil
                                       attributedText:message
                                           isAutoHide:YES
                                              onTouch:^{
                                                  [HDNotificationView hideNotificationViewOnComplete:nil];
                                              }];
}


+ (void)showAlertWithMessage:(NSString *)message from:(UIViewController*)rootViewController {
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertVC addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [alertVC dismissViewControllerAnimated:NO completion:nil];
    }]];
    [rootViewController presentViewController:alertVC animated:NO completion:nil];
}

+ (void)showAlertWithMessage:(NSString *)message {
    [Utilities showAlertWithMessage:message from:[UIViewController topViewController]];
}

+ (NSString *)getScreenResolution {
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    return [NSString stringWithFormat:@"%0.0fx%0.0f", screenSize.width, screenSize.height];
}

+ (void)showPaymentResultPopup {
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    CGFloat width = (screenSize.width > screenSize.height) ? screenSize.height : screenSize.width;
    VtcPaymentReportView *reportView = [VtcPaymentReportView reportViewWithFrame:CGRectMake(0, 0, width, 250) receivedGold:[Global sharedInstance].paymentGoldNumber currency:[Global sharedInstance].paymentCurrency];
    NSLog(@"paymentMessage: %@ , gold number: %ld, currency: %@", [Global sharedInstance].paymentMessage, (long)[Global sharedInstance].paymentGoldNumber, [Global sharedInstance].paymentCurrency);
    if ([Global sharedInstance].paymentMessage != nil) {
        [reportView setMessage:[Global sharedInstance].paymentMessage];
    }
    KLCPopup *popup = [KLCPopup popupWithContentView:reportView
                                            showType:KLCPopupShowTypeShrinkIn
                                         dismissType:KLCPopupDismissTypeShrinkOut
                                            maskType:KLCPopupMaskTypeDimmed
                            dismissOnBackgroundTouch:YES
                               dismissOnContentTouch:NO];
    
    NSLog(@"payment popup: %@ %@", reportView, popup);
    
    reportView.currentPopup = popup;
    [popup show];
    popup.didFinishDismissingCompletion = ^{
        [Global sharedInstance].paymentGoldNumber = 0;
        [Global sharedInstance].paymentCurrency = @"";
        [Global sharedInstance].paymentMessage = nil;
    };
}

+ (NSString *)localizeStringForString:(NSString *)inputString {
    NSDictionary *languageDict = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle vtcBundle] pathForResource:@"LocalizableString" ofType:@"plist"]];
    NSString *localizedString = [[languageDict objectForKey:inputString] objectForKey:[[VtcConfig defaultConfig] sdkLanguage]];
    if (localizedString)
        return localizedString;
    else
        return [[languageDict objectForKey:inputString] objectForKey:[self localeStringOf:SDKLanguageViet]];
}

+ (NSString *)localeStringOf:(SDKLanguage)language {
    NSString *languageKey = @"vie";
    switch (language) {
        case SDKLanguageLaos:
            languageKey = @"lao";
            break;
        case SDKLanguageCambodia:
            languageKey = @"khm";
            break;
        case SDKLanguageMyanmar:
            languageKey = @"mys";
            break;
        case SDKLanguagePhilipine:
            languageKey = @"phl";
            break;
        case SDKLanguageIndonesia:
            languageKey = @"idn";
            break;
        case SDKLanguageThailand:
            languageKey = @"tha";
            break;
        case SDKLanguageEnglish:
            languageKey = @"eng";
            break;
            
        default:
            languageKey = @"vie";
            break;
    }
    return languageKey;
}

@end

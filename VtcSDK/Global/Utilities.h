//
//  Utilities.h
//  VtcSDK
//
//  Created by Kent Vu on 7/25/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utilities : NSObject
+ (void)loadAllCustomFont;
+ (NSString *)getIPAddress;
+ (void)showNotification:(NSString *)message;
+ (void)showPersistantNotification:(NSString *)message;
+ (void)hideNotificationView;
+ (void)showAttributedTextNotification:(NSAttributedString *)message;
+ (void)showAlertWithMessage:(NSString *)message;
+ (void)showAlertWithMessage:(NSString *)message from:(UIViewController*)rootViewController;
+ (NSString *)getScreenResolution;
+ (void)showPaymentResultPopup;
+ (NSString *)localizeStringForString:(NSString *)inputString;
+ (NSString *)localeStringOf:(SDKLanguage)language;

@end

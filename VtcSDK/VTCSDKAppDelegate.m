//
//  VTCSDKAppDelegate.m
//  VtcSDK
//
//  Created by Kent Vu on 3/30/17.
//  Copyright © 2017 VTCIntecom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "VTCSDKAppDelegate.h"
#import <objc/runtime.h>
#import <UserNotifications/UserNotifications.h>
#import "NetworkModal+APNS.h"
#import "SmartParser.h"
#import "UIViewController+Additions.h"
#import "SDKManager.h"

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

NSArray* ClassGetSubclasses(Class parentClass) {
    
    int numClasses = objc_getClassList(NULL, 0);
    Class *classes = NULL;
    
    classes = (Class *)malloc(sizeof(Class) * numClasses);
    numClasses = objc_getClassList(classes, numClasses);
    
    NSMutableArray *result = [NSMutableArray array];
    for (NSInteger i = 0; i < numClasses; i++) {
        Class superClass = classes[i];
        do {
            superClass = class_getSuperclass(superClass);
        } while(superClass && superClass != parentClass);
        
        if (superClass == nil) continue;
        [result addObject:classes[i]];
    }
    
    free(classes);
    
    return result;
}

Class getClassWithProtocolInHierarchy(Class searchClass, Protocol* protocolToFind) {
    if (!class_conformsToProtocol(searchClass, protocolToFind)) {
        if ([searchClass superclass] == nil)
            return nil;
        Class foundClass = getClassWithProtocolInHierarchy([searchClass superclass], protocolToFind);
        if (foundClass)
            return foundClass;
        return searchClass;
    }
    return searchClass;
}

BOOL checkIfInstanceOverridesSelector(Class instance, SEL selector) {
    Class instSuperClass = [instance superclass];
    return [instance instanceMethodForSelector: selector] != [instSuperClass instanceMethodForSelector: selector];
}

BOOL injectSelector(Class newClass, SEL newSel, Class addToClass, SEL makeLikeSel) {
    Method newMeth = class_getInstanceMethod(newClass, newSel);
    IMP imp = method_getImplementation(newMeth);
    
    const char* methodTypeEncoding = method_getTypeEncoding(newMeth);
    // Keep - class_getInstanceMethod for existing detection.
    //    class_addMethod will successfuly add if the addToClass was loaded twice into the runtime.
    BOOL existing = class_getInstanceMethod(addToClass, makeLikeSel) != NULL;
    
    if (existing) {
        class_addMethod(addToClass, newSel, imp, methodTypeEncoding);
        newMeth = class_getInstanceMethod(addToClass, newSel);
        Method orgMeth = class_getInstanceMethod(addToClass, makeLikeSel);
        if (makeLikeSel == @selector(application:didRegisterForRemoteNotificationsWithDeviceToken:)) {
            // in case project has integrated OneSignal, we need to do something hacked
            SEL oneSignalSel = @selector(oneSignalDidRegisterForRemoteNotifications:deviceToken:);
            BOOL onesignalExisting = class_getInstanceMethod(addToClass, oneSignalSel) != NULL;
            if (onesignalExisting) {
                orgMeth = class_getInstanceMethod(addToClass, oneSignalSel);
            }
        }
        if (makeLikeSel == @selector(application:didReceiveRemoteNotification:)) {
            // in case project has integrated OneSignal, we need to do something hacked
            SEL oneSignalSel = @selector(oneSignalReceivedRemoteNotification:userInfo:);
            BOOL onesignalExisting = class_getInstanceMethod(addToClass, oneSignalSel) != NULL;
            if (onesignalExisting) {
                orgMeth = class_getInstanceMethod(addToClass, oneSignalSel);
            }
        }
        method_exchangeImplementations(orgMeth, newMeth);
    }
    else
        class_addMethod(addToClass, makeLikeSel, imp, methodTypeEncoding);
    
    return existing;
}

// Try to find out which class to inject to
void injectToProperClass(SEL newSel, SEL makeLikeSel, NSArray* delegateSubclasses, Class myClass, Class delegateClass) {
    
    // Find out if we should inject in delegateClass or one of its subclasses.
    // CANNOT use the respondsToSelector method as it returns TRUE to both implementing and inheriting a method
    // We need to make sure the class actually implements the method (overrides) and not inherits it to properly perform the call
    // Start with subclasses then the delegateClass
    
    for(Class subclass in delegateSubclasses) {
        if (checkIfInstanceOverridesSelector(subclass, makeLikeSel)) {
            injectSelector(myClass, newSel, subclass, makeLikeSel);
            return;
        }
    }
    
    // No subclass overrides the method, try to inject in delegate class
    injectSelector(myClass, newSel, delegateClass, makeLikeSel);
    
}

@interface VTCSDKAppDelegate ()

- (void)sdkLoadedTagSelector;
- (void)setVTCSDKAppDelegate:(id<UIApplicationDelegate>)delegate;

@end

@implementation VTCSDKAppDelegate

static Class delegateClass = nil;
// Store an array of all UIAppDelegate subclasses to iterate over in cases where UIAppDelegate swizzled methods are not overriden in main AppDelegate
// But rather in one of the subclasses
static NSArray* delegateSubclasses = nil;

- (void)sdkLoadedTagSelector {}

+ (VTCSDKAppDelegate *)loadSDKAppDelegate {
    static VTCSDKAppDelegate *sdkAppDelegate = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sdkAppDelegate = [[VTCSDKAppDelegate alloc] init];
    });
    return sdkAppDelegate;
}

- (void)setVTCSDKAppDelegate:(id<UIApplicationDelegate>)delegate {

    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if(!error){
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
        }];
    }
    else {
        // Code for old versions (< 10.0)
        // register push notification
        UIUserNotificationType allNotificationTypes = (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    
    Class newClass = [VTCSDKAppDelegate class];
    delegateClass = getClassWithProtocolInHierarchy([delegate class], @protocol(UIApplicationDelegate));
    delegateSubclasses = ClassGetSubclasses(delegateClass);
    
    injectToProperClass(@selector(application:sdk_didRegisterForRemoteNotificationsWithDeviceToken:), @selector(application:didRegisterForRemoteNotificationsWithDeviceToken:), delegateSubclasses, newClass, delegateClass);
    injectToProperClass(@selector(application:sdk_didReceiveRemoteNotification:), @selector(application:didReceiveRemoteNotification:), delegateSubclasses, newClass, delegateClass);
    injectToProperClass(@selector(application:sdk_didFailToRegisterForRemoteNotificationsWithError:), @selector(application:didFailToRegisterForRemoteNotificationsWithError:), delegateSubclasses, newClass, delegateClass);
    
    
    if ([self respondsToSelector:@selector(setVTCSDKAppDelegate:)]) {
        [self setVTCSDKAppDelegate:delegate];
    }
}

- (void)application:(UIApplication *)application sdk_didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    if ([self respondsToSelector:@selector(application:sdk_didRegisterForRemoteNotificationsWithDeviceToken:)]) {
        [self application:application sdk_didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
    }
//    NSString *_deviceToken = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
//    _deviceToken = [_deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
//    NSLog(@"<VtcSDK> Device token get from sdk: %@", _deviceToken);
//    [Global sharedInstance].pushDeviceToken = _deviceToken;
//
//    [SDKManager handleCheckingUninstallWithAF:deviceToken];
}

- (void)application:(UIApplication *)application sdk_didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    if ([self respondsToSelector:@selector(application:sdk_didFailToRegisterForRemoteNotificationsWithError:)]) {
        [self application:application sdk_didFailToRegisterForRemoteNotificationsWithError:error];
    }
    VTCLog(@"error: %@", error.description);
}

- (void)application:(UIApplication *)application sdk_didReceiveRemoteNotification:(NSDictionary *)userInfo {
    if ([self respondsToSelector:@selector(application:sdk_didReceiveRemoteNotification:)]) {
        [self application:application sdk_didReceiveRemoteNotification:userInfo];
    }
    VTCLog(@"user info: %@", userInfo);
    
    @try {
        NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
        __block __weak NSString *type = @"";
        NSString *time = [SmartParser stringForKey:@"time" from:apsInfo];
        NSString *notifId = [SmartParser stringForKey:@"noticeId" from:apsInfo];;
        NSString *uId = [SmartParser stringForKey:@"accountId" from:apsInfo];
        NSString *uName = [SmartParser stringForKey:@"accountName" from:apsInfo];
        
        if ( application.applicationState == UIApplicationStateInactive) {
            NSLog(@"<VtcSDK> clicked from background");
            type = kAPNSClickedType;
            [[NetworkModal sharedModal] postbackForAPNSType:type time:time notificationId:notifId fromUser:uId userName:uName];
        }
        else if (application.applicationState == UIApplicationStateBackground) {
            NSLog(@"<VtcSDK> received APNS in background");
            type = kAPNSReceivedType;
            [[NetworkModal sharedModal] postbackForAPNSType:type time:time notificationId:notifId fromUser:uId userName:uName];
        }
        else if (application.applicationState == UIApplicationStateActive) {
            NSLog(@"<VtcSDK> received APNS in foreground");
            type = kAPNSReceivedType;
            [[NetworkModal sharedModal] postbackForAPNSType:type time:time notificationId:notifId fromUser:uId userName:uName];
            
            NSDictionary *alertInfo = [apsInfo objectForKey:@"alert"];
            NSString *title = [alertInfo objectForKey:@"title"];
            NSString *body = [alertInfo objectForKey:@"body"];
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:body preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                type = kAPNSClickedType;
//                [[NetworkModal sharedModal] postbackForAPNSType:type time:time notificationId:notifId fromUser:uId userName:uName];
            }]];
            [[UIViewController topViewController] presentViewController:alertController animated:NO completion:nil];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
    } @finally {
        
    }
}

@end


@implementation UIApplication (SDK)

+ (void)load {
    // Prevent Xcode storyboard rendering process from crashing with custom IBDesignable Views
    // https://github.com/OneSignal/OneSignal-iOS-SDK/issues/160
    NSProcessInfo *processInfo = [NSProcessInfo processInfo];
    if ([[processInfo processName] isEqualToString:@"IBDesignablesAgentCocoaTouch"])
        return;
    
    if (SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(@"8.0"))
        return;
    
    // Double loading of class detection.
    BOOL existing = injectSelector([VTCSDKAppDelegate class], @selector(sdkLoadedTagSelector), self, @selector(sdkLoadedTagSelector));
    if (existing) {
        NSLog(@"Already swizzled UIApplication.setDelegate. Make sure the OneSignal library wasn't loaded into the runtime twice!");
        return;
    }
    
    // Swizzle - UIApplication delegate
    injectSelector([VTCSDKAppDelegate class], @selector(setVTCSDKAppDelegate:), [UIApplication class], @selector(setDelegate:));
}

@end

//
//  VPurchasedItem.h
//  VtcSDK
//
//  Created by Kent Vu on 8/8/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, PaymentItemType) {
    PaymentItemTypeAppleInApp,
    PaymentItemTypeAll
};

typedef NS_ENUM(NSInteger, PackageItemType) {
    PackageItemTypeNormal = 1,
    PackageItemTypeMonthlyCard = 2
};

@interface VPurchasedItem : NSObject

@property (nonatomic, copy) NSString *itemKey;
@property (nonatomic, copy) NSString *itemName;
@property (nonatomic, copy) NSString *productIdentifier;
@property (nonatomic, copy) NSString *itemImageURL;
@property (nonatomic, copy) NSString *itemDescription;
@property (nonatomic, copy) NSString *itemContent;

@property (nonatomic) NSInteger itemPriceGold;
@property (nonatomic) NSInteger itemPriceUSD;
@property (nonatomic) NSInteger itemPriceVND;

@property (nonatomic) NSInteger moneyScale;
@property (nonatomic) NSInteger promotionPercent;
@property (nonatomic, copy) NSString *promotionText;
@property (nonatomic) NSInteger totalGold;

@property (nonatomic) PaymentItemType itemType;
@property (nonatomic) PackageItemType packageType;

@property (nonatomic, copy) NSString *currency;

@property (nonatomic, copy) NSString *orderNo;

+ (VPurchasedItem *)itemFromData:(NSDictionary *)itemInfo;

@end

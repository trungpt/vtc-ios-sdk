//
//  VIDUser+Internal.h
//  VtcSDK
//
//  Created by Kent Vu on 2/27/17.
//  Copyright © 2017 VTCIntecom. All rights reserved.
//

#import "VIDUser.h"

@interface VIDUser ()

@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *billingToken;
@property (nonatomic, assign) BOOL savePassword;
@end

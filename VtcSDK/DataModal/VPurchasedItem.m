//
//  VPurchasedItem.m
//  VtcSDK
//
//  Created by Kent Vu on 8/8/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VPurchasedItem.h"
#import "SmartParser.h"

@implementation VPurchasedItem

+ (VPurchasedItem *)itemFromData:(NSDictionary *)itemInfo {
    VPurchasedItem *item = [[VPurchasedItem alloc] init];
    [item loadData:itemInfo];
    return item;
}

- (void)loadData:(NSDictionary *)itemInfo {
    _itemKey = [SmartParser stringForKey:@"key" from:itemInfo];
    _itemName = [SmartParser stringForKey:@"name" from:itemInfo];
    _productIdentifier = [SmartParser stringForKey:@"mappingKeyApple" from:itemInfo];
    _itemImageURL = [SmartParser stringForKey:@"imageUrl" from:itemInfo];
    _itemDescription = [SmartParser stringForKey:@"description" from:itemInfo];
    _itemContent = [SmartParser stringForKey:@"content" from:itemInfo];
    
    _itemPriceGold = [SmartParser intForKey:@"priceGold" from:itemInfo];
    _itemPriceUSD = [SmartParser intForKey:@"priceUsd" from:itemInfo];
    _itemPriceVND = [SmartParser intForKey:@"priceVnd" from:itemInfo];
    
    _moneyScale = [SmartParser intForKey:@"moneyScale" from:itemInfo];
    _promotionPercent = [SmartParser intForKey:@"promotionalPercent" from:itemInfo];
    _promotionText = [SmartParser stringForKey:@"promotionalText" from:itemInfo];
    _totalGold = [SmartParser intForKey:@"totalGold" from:itemInfo];
    
    _currency = [SmartParser stringForKey:@"currency" from:itemInfo];
    
    NSInteger itemType = [SmartParser intForKey:@"packagePaymentType" from:itemInfo];
    if (itemType == 3) {
        _itemType = PaymentItemTypeAppleInApp;
    }
    else {
        _itemType = PaymentItemTypeAll;
    }
    NSInteger packageType = [SmartParser intForKey:@"packageType" from:itemInfo];
    if (packageType == 1) {
        _packageType = PackageItemTypeNormal;
    }
    else {
        _packageType = PackageItemTypeMonthlyCard;
    }
    _orderNo = @"";
}

@end

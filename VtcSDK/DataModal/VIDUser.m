//
//  VIDUser.m
//  VtcSDK
//
//  Created by Kent Vu on 7/25/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "VIDUser.h"
#import "SmartParser.h"

@interface VIDUser ()

@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *billingToken;
@property (nonatomic, assign) BOOL savePassword;
@end

@implementation VIDUser

+ (VIDUser *)currentUser {
    static VIDUser *currentUser_ = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        currentUser_ = [[VIDUser alloc] init];
    });
    return currentUser_;
}

- (void)loadData:(NSDictionary *)userInfo {
    if (!userInfo) {
        return;
    }
    _appleSignInToken = [SmartParser stringForKey:@"appleSignInToken" from:userInfo];
    _accountUsingMobile = [SmartParser stringForKey:@"accountUsingMobile" from:userInfo];
    _extend = [SmartParser stringForKey:@"extend" from:userInfo];
    _userId =  [SmartParser stringForKey:@"accountId" from:userInfo];
    _accessToken = [SmartParser stringForKey:@"accessToken" from:userInfo];
    _userName = [SmartParser stringForKey:@"accountName" from:userInfo];
    if ([userInfo objectForKey:@"savePassword"])
    {
        _savePassword = [SmartParser boolForKey:@"savePassword" from:userInfo];
    }
    if ([userInfo objectForKey: @"password"]) {
        _password = [SmartParser stringForKey:@"password" from:userInfo];
        _savePassword = _password.length > 0;
    }
    _billingToken = [SmartParser stringForKey:@"billingAccessToken" from:userInfo];
    _expiration = [[SmartParser numberForKey:@"expiration" from:userInfo] doubleValue];
    _currentGameVer = [SmartParser stringForKey:@"gameVersion" from:userInfo];
    NSString *gver = [_currentGameVer stringByReplacingOccurrencesOfString:@"." withString:@""];
    _currentGameVerNumber = (long)[gver integerValue];
    _email = [SmartParser stringForKey:@"email" from:userInfo];
    _mobile = [SmartParser stringForKey:@"mobile" from:userInfo];
    _userStatus = [SmartParser intForKey:@"userStatus" from:userInfo];
//    _vcoinBalance = [SmartParser intForKey:@"vcoinBalance" from:userInfo];
    _avatarURL = [SmartParser stringForKey:@"avatarUrl" from:userInfo];
    _authenType = [SmartParser intForKey:@"authenType" from:userInfo];
    
}

- (void)resetData {
    NSLog(@"testahihi resetData");
//    _signedIn = NO;
//    _appleSignInToken = nil;
    _accountUsingMobile = nil;
    _extend = nil;
    _userId = nil;
    _accessToken = nil;
    _billingToken = nil;
    _expiration = 0.0;
    _authenType = AuthenTypeNormal;
    _mobile = nil;
    _email = nil;
    _currentGameVer = nil;
    _currentGameVerNumber = 0;
    _userStatus = 0;
    _avatarURL = nil;
    _savePassword = NO;
    _password = @"";
//    _vcoinBalance = 0;
}

- (NSDictionary *)exportUserInfo {
    VTCLog(@"%@", _userName);
    VTCLog(@"%@", _accessToken);
    VTCLog(@"%@", _billingToken);
    VTCLog(@"%@", _avatarURL);
    
    return [NSDictionary dictionaryWithObjectsAndKeys:
            _appleSignInToken ? _appleSignInToken: @"", @"appleSignInToken",
            _accountUsingMobile ? _accountUsingMobile: @"", @"accountUsingMobile",
            _extend ? _extend : @"", @"extend",
            _userId ? _userId : @"", @"accountId",
            _accessToken ? _accessToken : @"", @"accessToken",
            _userName ? _userName : @"", @"accountName",
            _password ? _password : @"", @"password",
            _billingToken ? _billingToken : @"", @"billingAccessToken",
            [NSNumber numberWithInteger:_expiration], @"expiration",
            [NSNumber numberWithInteger:_authenType], @"authenType",
//            [NSNumber numberWithInteger:_vcoinBalance], @"vcoinBalance",
            _avatarURL ? _avatarURL : @"", @"avatarUrl",
            _currentGameVer ? _currentGameVer : @"", @"gameVersion",
            [NSNumber numberWithInteger:_currentGameVerNumber], @"gameVersionNumber",
            _email ? _email : @"", @"email",
            _mobile ? _mobile : @"", @"mobile",
            [NSNumber numberWithInteger:_userStatus], @"userStatus",
            @(_savePassword), @"savePassword",
            nil];
}

@end

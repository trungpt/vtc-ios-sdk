//
//  SDKManager.m
//  VtcSDK
//
//  Created by Kent Vu on 7/25/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "SDKManager.h"
#import "SmartParser.h"
#import "VPurchasedItem.h"
#import "SVProgressHUD.h"
#import "VtcLoginBackupViewController.h"
#import "VtcLoginViewController.h"
#import "VtcEnterUsernameViewController.h"
#import "VtcOTPViewController.h"
#import "VIDUser.h"
#import <Bolts/Bolts.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
//#import <GoogleSignIn/GoogleSignIn.h>
#import "VtcLoginViewController.h"
#import "VtcSignUpViewController.h"
#import "VtcAuthenNavController.h"
#import "UIViewController+Additions.h"
#import "NSBundle+Additions.h"
#import "NetworkModal+Payment.h"
#import "NetworkModal+Tracking.h"
#import "NetworkModal+Authen.h"
#import "VtcAutoLoginViewController.h"
#import "VtcShopViewController.h"
#import "VtcPaymentNavController.h"
#import "NetworkModal+Info.h"
#import "VtcNavigationBar.h"
#import "SimpleKeychain.h"
#import <AdSupport/ASIdentifierManager.h>
#import <SafariServices/SafariServices.h>
#import <AppsFlyerLib/AppsFlyerTracker.h>
#import "AppleIAPHelper.h"
#import "VTCSDKAppDelegate.h"
#import "NSString+Crypto.h"
#import "VtcLoginManager.h"
#import <FBSDKShareKit/FBSDKSharing.h>
#import <FBSDKShareKit/FBSDKShareDialog.h>
#import <FBSDKShareKit/FBSDKShareLinkContent.h>
#import <FBSDKShareKit/FBSDKSharePhotoContent.h>
#import <FBSDKShareKit/FBSDKSharePhoto.h>
#import "UIImage+Additions.h"
#import "Firebase.h"
#import "VIDUser+Internal.h"
@interface CustomPresentationController: UIPresentationController

@end

@implementation CustomPresentationController

- (CGRect)frameOfPresentedViewInContainerView {
    return CGRectMake(0.0, 0.0, 0.05, 0.05);
}

@end

@interface SDKManager () <SFSafariViewControllerDelegate, UIViewControllerTransitioningDelegate >

@property (nonatomic, strong) VtcPaymentNavController *paymentNavController;
@property (nonatomic) BOOL checkedBufferLink;
@property (nonatomic) BOOL checkingInstall;
@property (nonatomic) BOOL showingLoginView;
@property (nonatomic) BOOL showingPaymentView;

@property (nonatomic, strong) UIViewController *safariVC;

@end

@implementation SDKManager

+ (SDKManager *)defaultManager {
    static SDKManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[SDKManager alloc] init];
        manager.allowRotationInLoginView = NO;
        manager.isShowCloseButtonInAuthenVC = NO;
        manager.loginViewOrientationMask = UIInterfaceOrientationMaskLandscape;
        manager.allowRotationInPaymentView = NO;
        manager.paymentViewOrientationMask = UIInterfaceOrientationMaskLandscape;
        manager.isSandbox = NO;
        manager.ignoreCaptcha = NO;
        manager.isSaveAccessToken = NO;
        manager.sitekeyRecapchaIos = @"";
    });
    return manager;
}

// MARK: methods
- (void)initSDK {
    
    [Utilities loadAllCustomFont];
    // handle Global Exceptions
    NSSetUncaughtExceptionHandler(&sdkGlobalExceptionHandler);
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusNotReachable:
            {
                NSLog(@"<VtcSDK> đứt kết nối");
                [Global sharedInstance].isReachable = NO;
                [Utilities showPersistantNotification:@"Vui lòng kiểm tra lại kết nối..."];
            }
                break;
                
            default: {
                NSLog(@"<VtcSDK> kết nối tốt");
                [Global sharedInstance].isReachable = YES;
                [HDNotificationView hideNotificationView];
            }
                break;
        }
    }];
    
    // save Ad-id
    NSString *cachedADId = [SimpleKeychain load:kKeychainAdvertisingIdentify];
    if (cachedADId) {
        NSLog(@"testahihi cached AD Id = %@", cachedADId);
        NSLog(@"testahihi Md5 Ads Id = %@", [cachedADId MD5]);
        [Global sharedInstance].advertisingIdentifier = cachedADId;
    }
    else {
        [Global sharedInstance].advertisingIdentifier = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
        NSLog(@"testahihi current AD Id = %@", [Global sharedInstance].advertisingIdentifier);
        NSLog(@"testahihi Md5 Ads Id = %@", [[Global sharedInstance].advertisingIdentifier MD5]);
        [SimpleKeychain save:kKeychainAdvertisingIdentify data:[Global sharedInstance].advertisingIdentifier];
    }
    
    // load default UTM if exist
    NSString *defaultUtm = [[VtcConfig defaultConfig] defaultUtmString];
    if (![defaultUtm isEqualToString:@""]) {
        [[Global sharedInstance] setUtmString:defaultUtm];
    }
    
    [[UILabel appearanceWhenContainedIn:[UISegmentedControl class], nil] setNumberOfLines:0];
    
    [FIRApp configure];
}


- (void)checkBufferLink {
    // Luong moi cho Pokemon, thu nghiem bo qua check bufferLink
    NSLog(@"testahihi Luong cho Pokemon, thu nghiem bo qua check bufferLink");
    _checkedBufferLink = YES;
    [Global sharedInstance].bufferLinkChecked = YES;
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kUserDefaultLoadedBufferLink];
    [[NSUserDefaults standardUserDefaults] synchronize];
    return;
    // luong cu
    
    if (_checkedBufferLink) return;
    
    BOOL loaded = [[NSUserDefaults standardUserDefaults] boolForKey:kUserDefaultLoadedBufferLink];
    if (loaded) {
        NSLog(@"testahihi checkBufferLink loaded");
        [Global sharedInstance].bufferLinkChecked = YES;
        [self showControllerInQueue]; // If they exist
        return;
    }
    
    
    _checkedBufferLink = YES;
    __block NSString *baseURL = nil;
    [[NetworkModal sharedModal] getBufferLink:^(BOOL status, id responsedObject, NSError *error) {
        if (status) {
            @try {
                baseURL = (NSString *)responsedObject;
                
            } @catch (NSException *exception) {
                VTCLog(@"%@", exception.description);
            } @finally {
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?client_id=%@", baseURL, [VtcConfig defaultConfig].appId]];
                //                NSLog(@"testahihi <VtcSDK> check BufferLink: %@", url);
                if ([SFSafariViewController class]) {
                    _safariVC = [[SFSafariViewController alloc] initWithURL:url];
                    [(SFSafariViewController *)_safariVC setDelegate:self];
                    _safariVC.view.frame = CGRectMake(0, 0, 0.05, 0.05);
                    _safariVC.view.alpha = 0.05;
                    _safariVC.view.userInteractionEnabled = NO;
                    VTCLog(@"%@", [UIViewController topViewController]);
                    _safariVC.transitioningDelegate = self;
                    _safariVC.modalPresentationStyle = UIModalPresentationCustom;
                    [[UIViewController topViewController] presentViewController:_safariVC animated:false completion:nil];
                    [self performSelector:@selector(timeoutOfSafariController) withObject:nil afterDelay:3.0];
                }
                else {
                    NSLog(@"testahihi checkBufferLink !SFSafariViewController");
                    [Global sharedInstance].bufferLinkChecked = YES;
                    [self showControllerInQueue]; // If they exist
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kUserDefaultLoadedBufferLink];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    // check install log
                    [[NetworkModal sharedModal] performSelector:@selector(checkInstall) withObject:nil afterDelay:3.0];
                }
            }
            
        }
    }];
}

- (void)timeoutOfSafariController {
    NSLog(@"testahihi <VtcSDK> load buffer link too slow, cancelled.");
    if (_safariVC != nil) {
        [_safariVC dismissViewControllerAnimated:NO completion:^{
            NSLog(@"testahihi checkBufferLink cancelled");
            [Global sharedInstance].bufferLinkChecked = YES;
            [self showControllerInQueue]; // If they exist
            _safariVC = nil;
            // check install log
            [[NetworkModal sharedModal] performSelector:@selector(checkInstall) withObject:nil afterDelay:3.0];
        }];
    }
}


- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source {
    if (presented == _safariVC) {
        return [[CustomPresentationController alloc] initWithPresentedViewController:presented presentingViewController:presenting];
    }
    return [[UIPresentationController alloc] initWithPresentedViewController:presented presentingViewController:presenting];
}

- (void)safariViewController:(SFSafariViewController *)controller didCompleteInitialLoad:(BOOL)didLoadSuccessfully {
    if (didLoadSuccessfully) {
        NSLog(@"testahihi <VtcSDK> load buffer link successfully");
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kUserDefaultLoadedBufferLink];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else {
        NSLog(@"testahihi <VtcSDK> load buffer link failed");
    }
    [_safariVC dismissViewControllerAnimated:NO completion:^{
        NSLog(@"testahihi checkBufferLink safariViewController");
        [Global sharedInstance].bufferLinkChecked = YES;
        [self showControllerInQueue]; // If they exist
        _safariVC = nil;
        // check install log
        [[NetworkModal sharedModal] performSelector:@selector(checkInstall) withObject:nil afterDelay:3.0];
    }];
}

- (void)showControllerInQueue {
    if ([Global sharedInstance].isWaitingForShowLogin) {
        // [Utilities hideNotificationView];
        NSLog(@"testahihi testahihi isWaitingForShowLogin");
        [Global sharedInstance].isWaitingForShowLogin = NO;
        //Luồng đăng nhập mới
        [self signIn];
        //TODO: New prototype: auto check login/register
        //        [self registerNow];
    }
    else if ([Global sharedInstance].isWaitingForShowRegister) {
        // [Utilities hideNotificationView];
        NSLog(@"testahihi testahihi isWaitingForShowRegister");
        [Global sharedInstance].isWaitingForShowRegister = NO;
        [self registerNow];
    }
}

void sdkGlobalExceptionHandler(NSException *exception)
{
    //    NSArray *stack = [exception callStackReturnAddresses];
    //    VTCLog(@"Stack trace: %@", stack);
    [[VtcEventLogging manager] hitActivity:APICategoryTypeCrashApp
                                   hitType:TrackingHitTypeEvent
                                  category:GACategoryTypeInAppEvent
                                    action:GAActionTypeCrashApp
                                     label:GALabelTypeNone
                                     value:nil completion:nil];
}

- (void)signIn {
    NSLog(@"testahihi signin");
    [self signInFromViewController:nil];
}

- (void)signInFromViewController:(UIViewController *)parentController {
    NSLog(@"testahihi signInFromViewController");
    NSLog(@"testahihi Luong cho Pokemon, thu nghiem bo qua check bufferLink");
    //    if ([Global sharedInstance].bufferLinkChecked == NO) { // if buffer link checking controller did not dismiss yet, dont show any controller
    //        NSLog(@"<VtcSDK> Bufferlink checking controller did not dismiss yet, dont show any controller");
    //        [Global sharedInstance].isWaitingForShowLogin = YES;
    //        return;
    //    }
    
    //    if (_showingLoginView) {
    NSLog(@"testahihi <VtcSDK> ... is showing login view");
    //        return;
    //    }
    
    VIDUser *user =  [VIDUser currentUser];
    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultCachedUser];
    
    [user loadData:userInfo];
    /*
     // auto login
     if (user.accessToken && user.userName && user.userId) {
     if (![user.accessToken isEqualToString:@""] && ![user.userName isEqualToString:@""] && ![user.userId isEqualToString:@""]) {
     [[NetworkModal sharedModal] autoSignInToUser:user.userName  userId:user.userId accessToken:user.accessToken completion:^(BOOL status, id responsedObject, NSError *error) {
     if (status) {
     [Global sharedInstance].savedUserName = user.userName;
     [self openAutoLoginFromViewController:parentController completion:^(VtcAuthenNavController *autoLoginNavigationController) {
     [[Global sharedInstance] handleSignInResponse:responsedObject authenType:user.authenType containerController:nil showWelcome:NO];
     }];
     }
     else {
     // clear cached data
     [[Global sharedInstance] handleSignOut];
     [self openLoginFormFromViewController:parentController];
     }
     }];
     return;
     }
     }
     // */
    if (self.isSaveAccessToken)
    {
        NSLog(@"testahihi isSaveAccessToken");
        if (user.authenType == AuthenTypeNormal)
        {
            NSLog(@"testahihi isSaveAccessToken AuthenTypeNormal");
            [[VtcLoginManager sharedManager] autoSignInFrom:parentController];
        }else if (user.authenType == AuthenTypeQuickStart)
        {
            NSLog(@"testahihi isSaveAccessToken AuthenTypeQuickStart");
            [[VtcLoginManager sharedManager] autoQuickStartFrom:parentController];
        }
        else if (user.authenType == AuthenTypeFacebook)
        {
            NSLog(@"testahihi isSaveAccessToken AuthenTypeFacebook");
            [[VtcLoginManager sharedManager] autoSignInFacebookFrom:parentController];
        }
        else if  (user.authenType == AuthenTypeGoogle)
        {
            NSLog(@"testahihi isSaveAccessToken AuthenTypeGoogle");
            [[VtcLoginManager sharedManager] autosignInGoogleFrom:parentController];
        }
        else if  (user.authenType == AuthenTypeApple)
        {
            NSLog(@"testahihi isSaveAccessToken AuthenTypeApple");
            //luong Apple khong tu dong dang nhap
            //            [self signOut];
            //            _showingLoginView = YES;
            
            //            if ([VIDUser currentUser].appleSignInToken != nil && ![[VIDUser currentUser].appleSignInToken  isEqual: @""]){
            //                NSLog(@"testahihi Autosignin appleSignInToken:%@", [VIDUser currentUser].appleSignInToken);
            //                [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
            //                [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
            //                [SVProgressHUD show];
            //                [[NetworkModal sharedModal] loginWithApple: [VIDUser currentUser].appleSignInToken completion:^(BOOL status, id responsedObject, NSError *error) {
            //                    NSLog(@"testahihi loginWithApple");
            //                    [SVProgressHUD dismiss];
            //                    if (status) {
            //                        [[Global sharedInstance] handleSignInResponse:responsedObject authenType:AuthenTypeApple containerController: parentController showWelcome:YES];
            //                        [VtcEventLogging checkingByFireBaseWithEventName:[VtcEventLogging getAPICategoryTypeString:APICategoryTypeLoginApple]];
            //                    }
            //                    else {
            //                        [self openLoginFormFromViewController:parentController];
            //                    }
            //                }];
            //            }else{
            NSLog(@"testahihi appleSignInToken: nil");
            [self openLoginFormFromViewController:parentController];
            //            }
        }
    }
    else
    {
        //Luồng đăng nhập mới
        NSLog(@"testahihi is not SaveAccessToken");
        _showingLoginView = YES;
        [self openLoginFormFromViewController:parentController];
        //        [self registerFromViewController:parentController];
    }
    
}

- (void)registerNow {
    //    NSLog(@"testahihi testahihi registerNow");
    [self registerFromViewController:nil];
}

- (void)registerFromViewController:(UIViewController *)parentController {
    //    NSLog(@"testahihi testahihi registerFromViewController");
    if ([Global sharedInstance].bufferLinkChecked == NO) { // if buffer link checking controller did not dismiss yet, dont show any controller
        [Global sharedInstance].isWaitingForShowRegister = YES;
        // [Utilities showPersistantNotification:@"Hệ thống đăng nhập đang khởi tạo..."];
        return;
    }
    //    if (_showingLoginView) return;
    //    _showingLoginView = YES;
    VtcSignUpViewController *controller = [VtcSignUpViewController loadFromNibNamed:@"VtcSignUpViewController"];
    VtcAuthenNavController *nav = [[VtcAuthenNavController alloc] initWithNavigationBarClass:[VtcNavigationBar class] toolbarClass:nil];
    [nav setViewControllers:@[controller]];
    if (parentController) {
        [parentController presentViewController:nav animated:NO completion:nil];
    }
    else {
        [[UIViewController topViewController] presentViewController:nav animated:NO completion:nil];
    }
}

- (void)openLoginFormFromViewController:(UIViewController *)viewController {
    NSLog(@"testahihi <VtcSDK> Show login view.");
    if (@available(iOS 13.0, *)) {
        VtcLoginViewController *controller = [VtcLoginViewController loadFromNibNamed:@"VtcLoginViewController"];
        VtcAuthenNavController *nav = [[VtcAuthenNavController alloc] initWithNavigationBarClass:[VtcNavigationBar class] toolbarClass:nil];
        [nav setViewControllers:@[controller]];
        nav.modalPresentationStyle = UIModalPresentationOverFullScreen;
        if (viewController) {
            [viewController presentViewController:nav animated:NO completion: nil];
        }
        else {
            [[UIViewController topViewController] presentViewController:nav animated:NO  completion: nil];
        }
    }else{
        VtcAuthenNavController *nav = [[VtcAuthenNavController alloc] initWithNavigationBarClass:[VtcNavigationBar class] toolbarClass:nil];
        VtcLoginViewController *controller = [VtcLoginViewController loadFromNibNamed:@"VtcLoginViewController"];
        [nav setViewControllers:@[controller]];
        if (viewController) {
            [viewController presentViewController:nav animated:NO  completion: nil];
        }
        else {
            [[UIViewController topViewController] presentViewController:nav animated:NO  completion: nil];
        }
    }
    
    
}

- (void)openAutoLoginFromViewController:(UIViewController *)parentController completion:(void (^)(VtcAuthenNavController *autoLoginNavigationController))completionBlock {
    
    NSLog(@"testahihi <VtcSDK> Open auto login view.");
    
    VtcAutoLoginViewController *controller = [VtcAutoLoginViewController loadFromNibNamed:@"VtcAutoLoginViewController"];
    VtcAuthenNavController *nav = [[VtcAuthenNavController alloc] initWithNavigationBarClass:[VtcNavigationBar class] toolbarClass:nil];
    [nav setViewControllers:@[controller]];
    if (parentController) {
        [parentController presentViewController:nav animated:NO completion:^{
            completionBlock(nav);
        }];
    }
    else {
        [[UIViewController topViewController] presentViewController:nav animated:NO completion:^{
            completionBlock(nav);
        }];
    }
    
}

- (void)signOut {
    //    NSLog(@"testahihi signOut");
    _showingLoginView = NO;
    [[Global sharedInstance] handleSignOut];
    self.isSaveAccessToken = NO;
    //    if(!self.isSaveAccessToken) NSLog(@"testahihi _isSaveAccessToken = NO");
    // reopen sign-in interface
    if ([SDKManager defaultManager].delegate && [[SDKManager defaultManager].delegate respondsToSelector:@selector(sdkManagerDidSignOut)]) {
        NSLog(@"testahihi <VtcSDK> Sign Out");
        [[SDKManager defaultManager].delegate sdkManagerDidSignOut];
    }
}

- (void)openShop {
    [self openShopFromViewController:nil];
}

- (void)openShopFromViewController:(UIViewController *)parentController {
    if (_showingPaymentView) return;
    _showingPaymentView = YES;
    
    if (![VIDUser currentUser].accessToken) {
        [Utilities showNotification:@"Vui lòng đăng nhập trước khi tiến hành thanh toán"];
        return;
    }
    
    VtcShopViewController *controller = [VtcShopViewController loadFromNibNamed:@"VtcShopViewController"];
    self.paymentNavController = [[VtcPaymentNavController alloc] initWithNavigationBarClass:[VtcNavigationBar class] toolbarClass:nil];
    [self.paymentNavController setViewControllers:@[controller]];
    if (parentController) {
        [parentController presentViewController:_paymentNavController animated:NO completion:nil];
    }
    else {
        [[UIViewController topViewController] presentViewController:_paymentNavController animated:NO completion:nil];
    }
    
}

- (void)closeShop {
    if (_paymentNavController) {
        [_paymentNavController dismissViewControllerAnimated:YES completion:^{
            _paymentNavController = nil;
            _showingPaymentView = NO;
        }];
    }
}


- (void)buyPackage:(NSString *)packageID parentViewController:(UIViewController *)parentController {
    //    if (!areaId) {
    //        [Utilities showNotification:@"areaId must be not null"];
    //    }
    //    [VtcConfig defaultConfig].areaId = areaId;
    //    if (dataString) {
    //        [VtcConfig defaultConfig].extendData = dataString;
    //    }
    //    else {
    //        [VtcConfig defaultConfig].extendData = @"";
    //    }
    NSLog(@"testahihi buyPackage areaId = %d, packageID = %@", [VtcConfig defaultConfig].areaId, packageID);
    
    //    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    //    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD show];
    
    [[NetworkModal sharedModal] checkPackage:packageID serverID:[VtcConfig defaultConfig].areaId completion:^(BOOL status, id responsedObject, NSError *error) {
        [SVProgressHUD dismiss];
        //        NSLog(@"testahihi responsedObject:%@", responsedObject);
        
        if (status) {
            // add the iap if enable
            //            NSLog(@"%@",responsedObject);
            
            NSString *message = [SmartParser objectForKey:@"message" from:responsedObject];
            NSLog(@"testahihi message:%@", message);
            if (![message  isEqual: @"Success"]){
                [Utilities showNotification:message];
            }else{
                NSDictionary *iapData = [SmartParser objectForKey:@"inAppPurchase" from:[SmartParser objectForKey:@"info" from:responsedObject]];
                if (iapData) {
                    NSLog(@"testahihi iapData:%@", iapData);
                    NSArray *packageData = [SmartParser objectForKey:@"packages" from:iapData];
                    if (packageData) {
                        if (packageData.count > 0) {
                            // add the iap list package
                            NSMutableArray *inappPurchasedItems = [[NSMutableArray alloc] init];
                            for (NSDictionary *package in packageData) {
                                [inappPurchasedItems addObject:[VPurchasedItem itemFromData:package]];
                            }
//                            NSString *packageName = [SmartParser objectForKey:@"title" from:iapData];
                            VPurchasedItem *currentItem = [inappPurchasedItems objectAtIndex:0];
                            
                            NSLog(@"testahihi processInAppPurchase");
                            NSInteger monthly = (currentItem.packageType == PackageItemTypeMonthlyCard) ? 2 : 1;
                            [HDNotificationView showNotificationViewWithImage:nil title:nil message:VTCLocalizeString(@"Purchasing")  attributedText:nil isAutoHide:NO];
                            
                            [[NetworkModal sharedModal] createOrderIdForPackage:currentItem.itemKey packageType:monthly completion:^(BOOL status, id responsedObject, NSError *error) {
                                VTCLog(@"Order-No = %@", responsedObject);
                                
                                if (status) {
                                    @try {
                                        [AppleIAPHelper sharedHelper].currentItem = currentItem;
                                        [Global sharedInstance].currentOrderNo = [NSString stringWithFormat:@"%@", responsedObject];
                                        [AppleIAPHelper sharedHelper].currentItem.orderNo = [Global sharedInstance].currentOrderNo;
                                    } @catch (NSException *exception) {
                                        NSLog(@"<VtcSDK> exception: %@", exception.description);
                                    } @finally {
                                        [[AppleIAPHelper sharedHelper] buyProductWithIdentifier:currentItem.productIdentifier];
                                    }
                                }
                                else {
                                    NSString *errString = VTCLocalizeString(@"Purchase failed. Please try again");
                                    @try {
                                        errString = [error.userInfo objectForKey:@"message"];
                                    } @catch (NSException *exception) {
                                        NSLog(@"<VtcSDK> exception: %@", exception.description);
                                    } @finally {
                                        [Utilities showNotification:errString];
                                        if ((error.code == -401) || (error.code == -303)) {
                                            __block UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:VTCLocalizeString(@"Session's expired") message:VTCLocalizeString(@"Do you want to relogin now?") preferredStyle:UIAlertControllerStyleAlert];
                                            [alertVC addAction:[UIAlertAction actionWithTitle:VTCLocalizeString(@"Later") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                                                [alertVC dismissViewControllerAnimated:YES completion:nil];
                                            }]];
                                            [alertVC addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                [alertVC dismissViewControllerAnimated:NO completion:^{
                                                    NSLog(@"dismiss alert");
                                                }];
                                                [[SDKManager defaultManager] signInFromViewController:self];
                                            }]];
                                            [parentController presentViewController:alertVC animated:NO completion:nil];
                                        }
                                    }
                                }
                            }];
                            
                        }
                    }
                }
            }
            
        }
        else {
            [Utilities showNotification:[error.userInfo objectForKey:@"message"]];
        }
        
    }];
}

- (void)updateGameInfo:(NSString *)areaId extendData:(NSString *)dataString {
    if (!areaId) {
        [Utilities showNotification:@"areaId must be not null"];
    }
    [VtcConfig defaultConfig].areaId = areaId;
    if (dataString) {
        [VtcConfig defaultConfig].extendData = dataString;
    }
    else {
        [VtcConfig defaultConfig].extendData = @"";
    }
}

- (void)hitActivity:(APICategoryType)activity extendData:(NSString *)extend forUser:(NSString *)userName userId:(NSString *)userId completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    [[VtcEventLogging manager] hitActivity:activity extendData:extend amount:0 forUser:userName userId:userId completion:completionBlock];
}

- (void)hitActivity:(APICategoryType)activity extendData:(NSString *)extend amount:(NSInteger)amountNumber forUser:(NSString *)userName userId:(NSString *)userId completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    [[VtcEventLogging manager] hitActivity:activity extendData:extend amount:amountNumber forUser:userName userId:userId completion:completionBlock];
}

- (void)hitCustomActivity:(NSInteger)activity
                 category:(NSString *)category
                   action:(NSString *)action
                    label:(NSString *)label
                    value:(NSNumber *)value
               completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    [[VtcEventLogging manager] hitCustomActivity:activity
                                        category:category
                                          action:action
                                           label:label
                                           value:value
                                      completion:completionBlock];
}

- (void)logAppsFlyerInGameEventCompleteTutorial {
    [[VtcEventLogging manager] setAppsFlyerInAppEvent:kAppsFlyerEventTypeCompleteTutorial values:[NSDictionary dictionaryWithObjectsAndKeys:@"1", kAppsFlyerEventTypeCompleteTutorialValueKey, nil]];
}
- (void)logAppsFlyerInGameEventLevelAchieved {
    [[VtcEventLogging manager] setAppsFlyerInAppEvent:kAppsFlyerEventTypeLevelAchieved values:[NSDictionary dictionaryWithObjectsAndKeys:@"1", kAppsFlyerEventTypeLevelAchievedValueKey, nil]];
}

- (void)logAppsFlyerCustomInGameEvent:(NSString *)eventName values:(NSDictionary *)valueOfEvent {
    [[VtcEventLogging manager] setAppsFlyerInAppEvent:eventName values:valueOfEvent];
}

- (void)changeSDKMainColor:(UIColor *)mainColor {
    if (mainColor == nil) {
        return;
    }
    [Global sharedInstance].globalMainColor = mainColor;
}

// MARK: handle application delegate methods

+ (void)handleApplication:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[SDKManager defaultManager] handleApplication:application didFinishLaunchingWithOptions:launchOptions];
    //    [VTCSDKAppDelegate loadSDKAppDelegate];
    [[SDKManager defaultManager] initSDK];
    
}

- (void)handleApplication:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // hide the status bar
    //    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleApplicationWillEnterBackground:)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleApplicationWillEnterForeground:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleApplicationWillTerminate:) name:UIApplicationWillTerminateNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleApplicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    // Facebook
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    
    // load utmLink
    NSString *urlString = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultCachedUTM];
    
    if (urlString) {
        VTCLog(@"<VtcSDK> cached utm = %@", urlString);
        [[Global sharedInstance] setUtmString:urlString];
    }
    else {
        if (launchOptions[UIApplicationLaunchOptionsURLKey] == nil) {
            [FBSDKAppLinkUtility fetchDeferredAppLink:^(NSURL *url, NSError *error) {
                if (error) {
                    VTCLog(@"Received error while fetching deferred app link %@", error);
                }
                if (url) {
                    NSString *urlString = url.absoluteString;
                    VTCLog(@"deferred app link %@", urlString);
                    [[Global sharedInstance] setUtmString:urlString];
                }
            }];
        }
    }
    
    // Google
    NSString *clientID = [[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"GoogleService-Info" ofType:@"plist"]] objectForKey:@"CLIENT_ID"];
    //    NSLog(@"testahihi testahihi pathForResource:%@",[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"GoogleService-Info" ofType:@"plist"]]);
    //    NSLog(@"testahihi testahihi clientID:%@",clientID);
    //    VTCLog(@"GoogleClientID = %@", clientID);
    //    [GIDSignIn sharedInstance].clientID = clientID;
    
    // AppsFlyer
    [[VtcEventLogging manager] configAppsFlyer];
    
    @try {
        NSDictionary *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (notification) {
            NSLog(@"testahihi <VtcSDK> clicked on a push notification");
            //            [[VTCSDKAppDelegate loadSDKAppDelegate] application:application sdk_didReceiveRemoteNotification:notification];
        } else {
            NSLog(@"testahihi <VtcSDK> app did not recieve notification");
        }
    } @catch (NSException *exception) {
        NSLog(@"testahihi %@", exception.description);
    } @finally {
        
    }
    
}

+ (BOOL)handleApplication:(UIApplication *)application openURL:(NSURL *)url options:(nonnull NSDictionary<NSString *,id> *)options {
    return [[SDKManager defaultManager] handleApplication:application openURL:url options:options];
}

- (BOOL)handleApplication:(UIApplication *)application openURL:(NSURL *)url options:(nonnull NSDictionary<NSString *,id> *)options {
    VTCLog(@"url = %@", url);
    
    NSString *urlString = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultCachedUTM];
    NSString *referString = [NSString stringWithFormat:@"itc%@://cookie=", [VtcConfig defaultConfig].appId];
    NSLog(@"testahihi <VtcSDK> open url = %@", url.absoluteString);
    if (!urlString) {
        NSString *urlString = url.absoluteString;
        // check utm from buffer link
        if ([urlString containsString:referString]) {
            NSString *utmString = [[urlString componentsSeparatedByString:@"cookie="] lastObject];
            if (![utmString isEqualToString:@""]) {
                VTCLog(@"utm: %@", utmString);
                [[Global sharedInstance] setUtmString:utmString];
            }
        }
        
        // check utm from Facebook campaigns
        BFURL *parsedUrl = [BFURL URLWithInboundURL:url sourceApplication:nil];
        if ([parsedUrl appLinkData]) {
            VTCLog(@"app link data: %@", [parsedUrl appLinkData]);
            [[Global sharedInstance] setUtmString:urlString];
        }
    }
    
    // check Google & other campaigns
    [[VtcEventLogging manager] handleApplication:application
                                         openURL:url
                               sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                      annotation:nil];
    
    // handle URL for Facebook login
    [[FBSDKApplicationDelegate sharedInstance] application:application
                                                   openURL:url
                                         sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                annotation:nil];
    // handle URL for G-SignIn login
    //    [[GIDSignIn sharedInstance] handleURL:url
    //                        sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
    //                               annotation:nil];
    
    return YES;
}

+ (BOOL)handleApplication:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[SDKManager defaultManager] handleApplication:application openURL:url sourceApplication:sourceApplication annotation:annotation];
}

- (BOOL)handleApplication:(UIApplication *)application openURL:(NSURL *)url
        sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    NSString *urlString = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultCachedUTM];
    NSString *referString = [NSString stringWithFormat:@"itc%@://cookie=", [VtcConfig defaultConfig].appId];
    NSLog(@"testahihi <VtcSDK> open url = %@", url.absoluteString);
    if (!urlString) {
        NSString *urlString = url.absoluteString;
        // check utm from buffer link
        if ([urlString containsString:referString]) {
            NSString *utmString = [[urlString componentsSeparatedByString:@"cookie="] lastObject];
            if (![utmString isEqualToString:@""]) {
                VTCLog(@"utm: %@", utmString);
                [[Global sharedInstance] setUtmString:utmString];
            }
        }
        
        // check utm from Facebook campaigns
        BFURL *parsedUrl = [BFURL URLWithInboundURL:url sourceApplication:sourceApplication];
        if ([parsedUrl appLinkData]) {
            VTCLog(@"app link data: %@", [parsedUrl appLinkData]);
            [[Global sharedInstance] setUtmString:urlString];
        }
    }
    
    // check Google & other campaigns
    [[VtcEventLogging manager] handleApplication:application
                                         openURL:url
                               sourceApplication:sourceApplication
                                      annotation:annotation];
    
    // handle URL for Facebook login
    [[FBSDKApplicationDelegate sharedInstance] application:application
                                                   openURL:url
                                         sourceApplication:sourceApplication
                                                annotation:annotation];
    // handle URL for G-SignIn login
    //    [[GIDSignIn sharedInstance] handleURL:url
    //                        sourceApplication:sourceApplication
    //                               annotation:annotation];
    
    return YES;
}


/*
 - THIS METHOD WILL BE FIRED WHEN THE APP IS ACTIVED FIRST TIME AT OPENNING MOMENT OR WHEN APP COME BACK FROM BACKGROUND MODE
 - HERE THE PLACE WE NEED TO CALL SOMETHING TO LOG APP EVENT RELATED TO TRACKING ACTIVITY
 */
+ (void)handleApplicationDidBecomeActive:(UIApplication *)application {
    [[SDKManager defaultManager] handleApplicationDidBecomeActive:application];
}

- (void)handleApplicationDidBecomeActive:(UIApplication *)application {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    NSLog(@"testahihi <VtcSDK> Bắt đầu theo dõi kết nối");
    
    // check cookie
    [self checkBufferLink];
    
    // AppsFlyer
    [[AppsFlyerTracker sharedTracker] trackAppLaunch];
    
    // log Facebook active event
    [FBSDKAppEvents activateApp];
    
    // log app first open event
    BOOL firstOpen = [[NSUserDefaults standardUserDefaults] boolForKey:kUserDefaultFirstOpen];
    if (!firstOpen) {
        VTCLog(@"EVENT FIRST OPEN FIRED");
        [[VtcEventLogging manager] hitActivity:APICategoryTypeFirstOpen
                                       hitType:TrackingHitTypeEvent
                                      category:GACategoryTypeInAppEvent
                                        action:GAActionTypeFirstOpen
                                         label:GALabelTypeNone
                                         value:nil completion:^(BOOL status, id responsedObject, NSError *error) {
            if (status) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kUserDefaultFirstOpen];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }];
    }
    
    // log app open event
    BOOL open = [Global sharedInstance].flagOpenApp;
    if (!open) {
        VTCLog(@"EVENT OPEN FIRED");
        [[VtcEventLogging manager] hitActivity:APICategoryTypeOpen
                                       hitType:TrackingHitTypeEvent
                                      category:GACategoryTypeInAppEvent
                                        action:GAActionTypeOpenApp
                                         label:GALabelTypeNone
                                         value:nil completion:^(BOOL status, id responsedObject, NSError *error) {
            [Global sharedInstance].flagOpenApp = YES;
        }];
    }
}

- (void)handleApplicationWillEnterBackground:(NSNotification *)notification {
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
    NSLog(@"testahihi <VtcSDK> Dừng theo dõi kết nối");
}

- (void)handleApplicationWillEnterForeground:(NSNotification *)notification {
    
}

+ (void)facebookShareWithLink:(NSString *)link {
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:link];
    
    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    dialog.fromViewController = [UIViewController topViewController];
    dialog.shareContent = content;
    dialog.mode = FBSDKShareDialogModeShareSheet;
    [dialog show];
}

+ (void)faceBookShareWithImage:(NSArray *)images {
    if (images.count <= 0) {
        return;
    }
    FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
    
    NSMutableArray *arrayPhoto = [[NSMutableArray alloc] init];
    
    for(UIImage *image in images) {
        FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
        photo.image = image;
        photo.userGenerated = YES;
        [arrayPhoto addObject:photo];
    }
    
    content.photos = [NSArray arrayWithArray:arrayPhoto];
    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    dialog.fromViewController = [UIViewController topViewController];
    dialog.shareContent = content;
    dialog.mode = FBSDKShareDialogModeShareSheet;
    [dialog show];
}


/**
 checking uninstall With AF
 
 @param deviceToken <#deviceToken description#>
 */
+ (void)handleCheckingUninstallWithAF:(NSData *)deviceToken {
    [[AppsFlyerTracker sharedTracker] registerUninstall:deviceToken];
    NSLog(@"testahihi Checking uninstall with appsflyer");
}

/*
 - THIS METHOD WILL BE FIRED WHEN THE APP IS GOING TO ENDED
 */

- (void)handleApplicationWillTerminate:(NSNotification *)notification {
    [[NSUserDefaults standardUserDefaults] synchronize];
    // log close app event
    [[VtcEventLogging manager] hitActivity:APICategoryTypeCloseApp
                                   hitType:TrackingHitTypeEvent
                                  category:GACategoryTypeInAppEvent
                                    action:GAActionTypeCloseApp
                                     label:GALabelTypeNone
                                     value:nil completion:nil];
}

/* Utilities */

- (void)showNotificationMessage:(NSString *)message {
    [Utilities showNotification:message];
}

- (void)hardcodeUTM:(NSString *)utmString {
    if (!utmString) return;
    [[Global sharedInstance] setUtmString:utmString];
}

- (NSString *)getDeviceToken {
    return [[Global sharedInstance] getPushDeviceToken];
}


@end

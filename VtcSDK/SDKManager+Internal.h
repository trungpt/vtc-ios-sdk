//
//  SDKManager+Internal.h
//  VtcSDK
//
//  Created by Kent Vu on 8/10/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#ifndef SDKManager_Internal_h
#define SDKManager_Internal_h

#import "SDKManager.h"

@interface SDKManager (Internal)

@property (nonatomic) BOOL checkingInstall;
@property (nonatomic) BOOL showingLoginView;
@property (nonatomic) BOOL showingPaymentView;

@end

#endif /* SDKManager_Internal_h */

//
//  SmartParser.h
//  VtcSDK
//
//  Created by Kent Vu on 8/8/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SmartParser : NSObject

+(NSNumber *)numberForKey:(NSString *)key from:(NSDictionary *)data;
+ (NSInteger)intForKey:(NSString *)key from:(NSDictionary *)data;
+ (CGFloat)floatForKey:(NSString *)key from:(NSDictionary *)data;
+(id)objectForKey:(NSString *)key from:(NSDictionary *)data;
+ (NSString *)stringForKey:(NSString *)key from:(NSDictionary *)data;
+ (BOOL) boolForKey:(NSString *)key from:(NSDictionary *)data;
@end

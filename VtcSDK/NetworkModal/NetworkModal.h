//
//  NetworkModal.h
//  VtcSDK
//
//  Created by Kent Vu on 7/28/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "AFHTTPSessionManager.h"

static NSString *productBaseURL = @"https://apisdk.vtcgame.vn/sdk/"; // Product
static NSString *productApnsBaseURL = @"https://apipush.vtcgame.vn/"; // Product

//static NSString *baseURL = @"http://117.103.207.90"; // Developer
static NSString *sanboxBaseURL = @"http://apisdk1.vtcgame.vn/sdk/"; // Developer
static NSString *sabboxApnsBaseURL = @"http://117.103.207.109:8182/"; // Developer

static NSString *baseURL = @"https://apisdk.vtcgame.vn/sdk/";
static NSString *apnsBaseURL = @"https://apipush.vtcgame.vn/";

@interface NetworkModal : AFHTTPSessionManager

+ (NetworkModal *)sharedModal;
- (NSString *)baseURLString;
- (BOOL)isDevMode;
- (void)getRequest:(NSString *)requestPath parameters:(NSDictionary *)param completion:(void (^)(BOOL status, id responsedObject, NSError *error))completion;
- (void)getRequest:(NSString *)requestPath parameters:(NSDictionary *)param headerFields:(NSDictionary *)headers completion:(void (^)(BOOL status, id responsedObject, NSError *error))completion;
- (void)postRequest:(NSString *)requestPath parameters:(NSDictionary *)param completion:(void (^)(BOOL status, id responsedObject, NSError *error))completion;
- (void)postRequest:(NSString *)requestPath parameters:(NSDictionary *)param rawData:(NSDictionary *)rawBody headerFields:(NSDictionary *)headers completion:(void (^)(BOOL status, id responsedObject, NSError *error))completion;

@end

@interface ErrorDetail : NSObject

+ (ErrorDetail *)sharedInstace;
- (NSString *)descriptionForErrorCode:(NSInteger)code;

@end

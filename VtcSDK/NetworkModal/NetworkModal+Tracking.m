//
//  NetworkModal+Tracking.m
//  VtcSDK
//
//  Created by Kent Vu on 7/29/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "NetworkModal+Tracking.h"
#import "UIDevice-Hardware.h"
#import "SDKManager+Internal.h"
#import "SimpleKeychain.h"

@implementation NetworkModal (Tracking)

- (void)checkInstall {
    if ([SDKManager defaultManager].checkingInstall) {
        return;
    }
    NSNumber *cachedInstall = [SimpleKeychain load:kKeychainLoggedInstall];
    NSLog(@"<VtcSDK> cachedInstall = %@", cachedInstall);
    if (cachedInstall != nil) {
        return;
    }
    [SDKManager defaultManager].checkingInstall = YES;
    
    [self hitInstallEventToServer:0];
}

- (void)hitInstallEventToServer:(NSInteger)retryCounter {
    if (retryCounter > 2) {
        return;
    }

    [[VtcEventLogging manager] hitActivity:APICategoryTypeInstall
                                   hitType:TrackingHitTypeEvent
                                  category:GACategoryTypeInAppEvent
                                    action:GAActionTypeInstalledApp
                                     label:GALabelTypeNone
                                     value:nil completion:^(BOOL status, id responsedObject, NSError *error) {
                                         
                                         if (error) { // if error, retry this
                                             [self hitInstallEventToServer:retryCounter+1];
                                         }
                                         else {
                                             [SimpleKeychain save:kKeychainLoggedInstall data:[NSNumber numberWithBool:YES]];
                                         }
                                     }];
}

- (void)logActivity:(APICategoryType)type action:(NSString *)action category:(NSString *)cat completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    NSDictionary *trackingModel;
    NSDictionary *param;
    NSDictionary *headers;
    @try {
        trackingModel = @{
                          @"hitType" : @"event",
                          @"gaTrackingId" : [[VtcEventLogging manager] getCurrentGAId],
                          @"gaClientId" : [[VtcEventLogging manager] getCurrentGAClientId],
                          @"category" : cat,
                          @"action" : action
                          };
        param = @{
                  @"activityType" : [NSNumber numberWithInteger:type],
                  @"trackingModel" : trackingModel
                  };
        headers = @{
                    @"client_id" : [[VtcConfig defaultConfig] appId]
                    };
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><LogActivity> some parameter has corrupted. Error: %@", exception.description);
    } @finally {
        VTCLog(@"%@", param);
        [self postRequest:@"app/activity" parameters:nil rawData:param headerFields:headers completion:^(BOOL status, id responsedObject, NSError *error) {
            VTCLog(@"%@", responsedObject);
            completionBlock(status, responsedObject, error);
        }];
    }
}

- (void)logActivity:(APICategoryType)type extendData:(NSString *)extend amount:(NSInteger)amountNumber forUser:(NSString *)userName userId:(NSString *)userId completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    NSDictionary *headers;
    @try {
        [param setObject:[NSNumber numberWithInteger:type] forKey:@"activityType"];
        if (extend != nil) {
            if (![extend isEqualToString:@""]) {
                [param setObject:[extend uppercaseString] forKey:@"activityTypeExtend"];
            }
        }
        if (amountNumber > 0) {
            [param setObject:[NSNumber numberWithInteger:amountNumber] forKey:@"amount"];
        }
        if (userName != nil) {
            if (![userName isEqualToString:@""]) {
                [param setObject:userName forKey:@"accountName"];
            }
        }
        if (userId != nil) {
            if (![userId isEqualToString:@""]) {
                [param setObject:userId forKey:@"accountId"];
            }
        }
        
        if ([extend isEqualToString:kStartLogInApp] || [extend isEqualToString:kFinishLogInApp]) {
            [param setObject:[Global sharedInstance].currentOrderNo ? [Global sharedInstance].currentOrderNo : @"" forKey:@"orderNo"];
        }
        
        VTCLog(@"%@", param);
        
        headers = @{
                    @"client_id" : [[VtcConfig defaultConfig] appId]
                    };
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><LogActivity> some parameter has corrupted. Error: %@", exception.description);
    } @finally {
        [self postRequest:@"app/activity" parameters:nil rawData:param headerFields:headers completion:^(BOOL status, id responsedObject, NSError *error) {
            VTCLog(@"%@", responsedObject);
            completionBlock(status, responsedObject, error);
        }];
    }
}

- (void)logStartInAppWithOrderNo:(NSString *)orderNo forUser:(NSString *)userName userId:(NSString *)userId completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    NSDictionary *headers;
    @try {
        [param setObject:[NSNumber numberWithInteger:APICategoryTypeOther] forKey:@"activityType"];
        [param setObject:kStartLogInApp forKey:@"activityTypeExtend"];
        if (userName != nil) {
            if (![userName isEqualToString:@""]) {
                [param setObject:userName forKey:@"accountName"];
            }
        }
        if (userId != nil) {
            if (![userId isEqualToString:@""]) {
                [param setObject:userId forKey:@"accountId"];
            }
        }
        [param setObject:orderNo forKey:@"orderNo"];
        VTCLog(@"%@", param);
        
        headers = @{
                    @"client_id" : [[VtcConfig defaultConfig] appId]
                    };
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><LogActivity> some parameter has corrupted. Error: %@", exception.description);
    } @finally {
        [self postRequest:@"app/activity" parameters:nil rawData:param headerFields:headers completion:^(BOOL status, id responsedObject, NSError *error) {
            VTCLog(@"%@", responsedObject);
            completionBlock(status, responsedObject, error);
        }];
    }
}

- (void)logFinishInAppWithOrderNo:(NSString *)orderNo forUser:(NSString *)userName userId:(NSString *)userId completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    NSDictionary *headers;
    @try {
        [param setObject:[NSNumber numberWithInteger:APICategoryTypeOther] forKey:@"activityType"];
        [param setObject:kFinishLogInApp forKey:@"activityTypeExtend"];
        if (userName != nil) {
            if (![userName isEqualToString:@""]) {
                [param setObject:userName forKey:@"accountName"];
            }
        }
        if (userId != nil) {
            if (![userId isEqualToString:@""]) {
                [param setObject:userId forKey:@"accountId"];
            }
        }
        [param setObject:orderNo forKey:@"orderNo"];
        VTCLog(@"%@", param);
        
        headers = @{
                    @"client_id" : [[VtcConfig defaultConfig] appId]
                    };
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><LogActivity> some parameter has corrupted. Error: %@", exception.description);
    } @finally {
        [self postRequest:@"app/activity" parameters:nil rawData:param headerFields:headers completion:^(BOOL status, id responsedObject, NSError *error) {
            VTCLog(@"%@", responsedObject);
            completionBlock(status, responsedObject, error);
        }];
    }
}

@end

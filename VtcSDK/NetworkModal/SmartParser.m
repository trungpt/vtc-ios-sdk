//
//  SmartParser.m
//  VtcSDK
//
//  Created by Kent Vu on 8/8/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "SmartParser.h"

@implementation SmartParser

/************************* SmartParser **************************/

+(NSNumber *)numberForKey:(NSString *)key from:(NSDictionary *)data {
    if (![data objectForKey:key]) {
        return [NSNumber numberWithInteger:0];
    }
    if ([[data objectForKey:key] isKindOfClass:[NSNull class]]) {
        return [NSNumber numberWithInteger:0];
    }
    return [data objectForKey:key];
}

+ (NSInteger)intForKey:(NSString *)key from:(NSDictionary *)data {
    if (![data objectForKey:key]) {
        return 0;
    }
    if ([[data objectForKey:key] isKindOfClass:[NSNull class]]) {
        return 0;
    }
    if ([[data objectForKey:key] isKindOfClass:[NSNumber class]]) {
        return [[data objectForKey:key] integerValue];
    }
    if ([[data objectForKey:key] isKindOfClass:[NSString class]]) {
        return [[data objectForKey:key] integerValue];
    }
    return 0;
}

+ (BOOL) boolForKey:(NSString *)key from:(NSDictionary *)data {
    if (![data objectForKey:key]) {
        return NO;
    }
    if ([[data objectForKey:key] isKindOfClass:[NSNull class]]) {
        return NO;
    }
    if ([[data objectForKey:key] isKindOfClass:[NSNumber class]]) {
        return [[data objectForKey:key] boolValue];
    }
    if ([[data objectForKey:key] isKindOfClass:[NSString class]]) {
        return [[data objectForKey:key] boolValue];
    }
    return NO;
}

+ (CGFloat)floatForKey:(NSString *)key from:(NSDictionary *)data {
    if (![data objectForKey:key]) {
        return 0.0;
    }
    if ([[data objectForKey:key] isKindOfClass:[NSNull class]]) {
        return 0.0;
    }
    if ([[data objectForKey:key] isKindOfClass:[NSNumber class]]) {
        return [[data objectForKey:key] floatValue];
    }
    if ([[data objectForKey:key] isKindOfClass:[NSString class]]) {
        return [[data objectForKey:key] floatValue];
    }
    return 0.0;
}

+ (id)objectForKey:(NSString *)key from:(NSDictionary *)data {
    if (![data objectForKey:key]) {
        return nil;
    }
    if ([[data objectForKey:key] isKindOfClass:[NSNull class]]) {
        return nil;
    }
    return [data objectForKey:key];
}

+ (NSString *)stringForKey:(NSString *)key from:(NSDictionary *)data {
    if (![data objectForKey:key]) {
        return nil;
    }
    if ([[data objectForKey:key] isKindOfClass:[NSNull class]]) {
        return nil;
    }
    if ([[data objectForKey:key] isKindOfClass:[NSString class]]) {
        return (NSString *)[data objectForKey:key];
    }
    return nil;
}

@end

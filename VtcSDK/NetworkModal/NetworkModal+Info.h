//
//  NetworkModal+Info.h
//  VtcSDK
//
//  Created by Kent Vu on 8/9/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "NetworkModal.h"

@interface NetworkModal (Info)

- (void)getBufferLink:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)getGAInfo:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)getPaymentViewText:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)getPackageInfo:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;

@end

//
//  NetworkModal+Tracking.h
//  VtcSDK
//
//  Created by Kent Vu on 7/29/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "NetworkModal.h"

@interface NetworkModal (Tracking)

- (void)checkInstall;
- (void)hitInstallEventToServer:(NSInteger)retryCounter;
- (void)logActivity:(APICategoryType)type action:(NSString *)action category:(NSString *)cat completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)logActivity:(APICategoryType)type extendData:(NSString *)extend amount:(NSInteger)amountNumber forUser:(NSString *)userName userId:(NSString *)userId completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)logStartInAppWithOrderNo:(NSString *)orderNo forUser:(NSString *)userName userId:(NSString *)userId completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)logFinishInAppWithOrderNo:(NSString *)orderNo forUser:(NSString *)userName userId:(NSString *)userId completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;

@end

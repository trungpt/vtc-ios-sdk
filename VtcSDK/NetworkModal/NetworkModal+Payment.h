//
//  NetworkModal+Payment.h
//  VtcSDK
//
//  Created by Kent Vu on 8/8/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "NetworkModal.h"

@interface NetworkModal (Payment)

- (void)retrieveAllPaymentPackages:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)retrieveAllInAppPackages:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)retrieveAllNormalPackages:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;

// for In-App
- (void)createOrderIdForPackage:(NSString *)packId packageType:(NSInteger)type completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)verifyTransaction:(NSDictionary *)transactionInfo completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)listSuccessfulTransactions:(NSInteger)page size:(NSInteger)pageSize completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)listRefundTransactions:(NSInteger)page size:(NSInteger)pageSize completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)checkPackage:(NSString *)packId serverID:(NSInteger)serverID completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
@end

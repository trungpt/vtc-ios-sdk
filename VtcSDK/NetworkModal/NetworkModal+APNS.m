//
//  NetworkModal+APNS.m
//  VtcSDK
//
//  Created by Kent Vu on 3/9/17.
//  Copyright © 2017 VTCIntecom. All rights reserved.
//

#import "NetworkModal+APNS.h"
#import "UIDevice-Hardware.h"

@implementation NetworkModal (APNS)

- (void)postbackForAPNSType:(NSString *)type time:(NSString *)postTime notificationId:(NSString *)notifId fromUser:(NSString *)userId userName:(NSString *)userName {
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@/pushnoti/push-notice/comfirm-notice-reveive", apnsBaseURL] parameters:nil error:nil];
    
    req.timeoutInterval = 30;
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSMutableDictionary *trueParams = [[NSMutableDictionary alloc] init];
    
    @try {
        [trueParams setObject:[[VtcConfig defaultConfig] appId] forKey:@"clientId"];
        [trueParams setObject:generalDeviceType forKey:@"deviceType"];
        [trueParams setObject:[[Global sharedInstance] getPushDeviceToken] forKey:@"deviceToken"];
        [trueParams setObject:[[UIDevice currentDevice] model] forKey:@"manufacture"];
        [trueParams setObject:[[UIDevice currentDevice] modelName] forKey:@"modelName"];
        [trueParams setObject:[NSString stringWithFormat:@"%@ %@", [[UIDevice currentDevice] systemName], [[UIDevice currentDevice] systemVersion]] forKey:@"operatingSystem"];
        [trueParams setObject:[[UIDevice currentDevice] systemVersion] forKey:@"operatingSystemVersion"];
        [trueParams setObject:[Global sharedInstance].advertisingIdentifier forKey:@"adsId"];
        [trueParams setObject:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] forKey:@"gameVersion"];
        [trueParams setObject:VtcSDKVersionString forKey:@"sdkVersion"];
        if (userId != nil) {
            [trueParams setObject:userId forKey:@"accountId"];
        }
        if (userName != nil) {
            [trueParams setObject:userName forKey:@"accountName"];
        }
        if (postTime != nil) {
            [trueParams setObject:postTime forKey:@"time"];
        }
        if (notifId != nil) {
            [trueParams setObject:notifId forKey:@"noticeId"];
        }
        if (type != nil) {
            [trueParams setObject:type forKey:@"type"];
        }
        VTCLog(@"%@", trueParams);
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><APNS> Can not generating param to log apns. Error: %@", exception.description);
    } @finally {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:trueParams options:0 error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
        
        [[manager dataTaskWithRequest:req uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
            if (!error) {
                // postback success
                NSLog(@"<VtcSDK> postback APNS for %@ success", type);
            } else {
                NSError *err = [[NSError alloc] initWithDomain:@"pushnoti/push-notice/comfirm-notice-reveive" code:404 userInfo:@{@"message" : @"Không có kết nối"}];
                NSLog(@"<VtcSDK> postback APNS failed: %@", err.description);
            }
        }] resume];
    }
}

@end

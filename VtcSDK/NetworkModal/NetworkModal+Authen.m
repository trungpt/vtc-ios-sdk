//
//  NetworkModal+Authen.m
//  VtcSDK
//
//  Created by Kent Vu on 7/28/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//
#import "CommonCrypto/CommonCryptor.h"
#import "NetworkModal+Authen.h"
#import "UIDevice-Hardware.h"
#import "VtcCampaign.h"
#import "VtcSDK.h"
#import "NSString+Crypto.h"
#import "UIKit/UIKit.h"
static NSString *serviceID  = @"100000";

@implementation NetworkModal (Authen)

// MARK: authen & register


//--code swift--
//let OS_TYPE = 3 // ios
//
//private let OS_CODE = "ios"
//
//private let VTC_REQUEST_SIGN = "ee8d858aba8412fc916622de58178376"
//
//private let deviceSerial = ((UIDevice.current.identifierForVendor?.uuidString)?.replacingOccurrences(of: "-", with: "")) ?? ""
//
//private let systemVersion = UIDevice.current.systemVersion
//private let deviceName = UIDevice.current.name.encodeUrl()

//NSString *deviceDetail = "fingerprint:\(deviceSerial)-\(deviceSerial);devicebrowser:VTCAPP-ios;OS:ios-\(systemVersion);device:\(deviceName);devicetype:Mobile;resolution:Mobile";


//---------------

//test string
NSString *OS_TYPE = @"3";
NSString *OS_CODE = @"ios";
NSString *VTC_REQUEST_SIGN = @"ee8d858aba8412fc916622de58178376";
NSString *deviceSerial = @"3";


//NSString *identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
//NSLog(@"output is : %@", identifier);

- (void)loginWithVTCID:(NSString *)username password:(NSString *)pwd completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    [self login:username password:pwd type:LoginTypeVtcId secure:nil secureType:SecureTypeNone openIdAccessToken:nil signUp:NO autoLogin:NO completion:completionBlock];
}

- (void)quickStartGame:(NSString *)username password:(NSString *)pwd completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    [self login:username password:pwd type: QuickStart secure:nil secureType:SecureTypeNone openIdAccessToken:nil signUp:NO autoLogin:NO completion:completionBlock];
}

- (void)verifyAccount:(NSString *)username password:(NSString *)pwd secureCode:(NSString *)otpString secureType:(SecureType)secureType completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    [self login:username password:pwd type:LoginTypeVtcId secure:otpString secureType:secureType openIdAccessToken:nil signUp:NO autoLogin:NO completion:completionBlock];
}

- (void)loginWithFacebook:(NSString *)facebookToken completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    [self login:nil password:nil type:LoginTypeFacebook secure:nil secureType:SecureTypeNone openIdAccessToken:facebookToken signUp:NO autoLogin:NO completion:completionBlock];
}

- (void)loginWithApple:(NSString *)appleToken completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    [self login:nil password:nil type:LoginTypeApple secure:nil secureType:SecureTypeNone openIdAccessToken:appleToken signUp:NO autoLogin:NO completion:completionBlock];
}

- (void)registerWithFacebook:(NSString *)facebookToken user:(NSString *)username completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    [self login:username password:nil type:LoginTypeFacebook secure:nil secureType:SecureTypeNone openIdAccessToken:facebookToken signUp:YES autoLogin:NO completion:completionBlock];
}

- (void)registerWithApple:(NSString *)appleToken user:(NSString *)username completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    [self login:username password:nil type:LoginTypeApple secure:nil secureType:SecureTypeNone openIdAccessToken:appleToken signUp:YES autoLogin:NO completion:completionBlock];
}

- (void)registerQuickStartGame:(NSString *)username completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    [self login:username password:nil type:QuickStart secure:nil secureType:SecureTypeNone openIdAccessToken:@"" signUp:YES autoLogin:NO completion:completionBlock];
}

- (void)loginWithGoogle:(NSString *)googleToken completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    [self login:nil password:nil type:LoginTypeGoogle secure:nil secureType:SecureTypeNone openIdAccessToken:googleToken signUp:NO autoLogin:NO completion:completionBlock];
}

- (void)registerWithGoogle:(NSString *)googleToken user:(NSString *)username completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    [self login:username password:nil type:LoginTypeGoogle secure:nil secureType:SecureTypeNone openIdAccessToken:googleToken signUp:YES autoLogin:NO completion:completionBlock];
}

- (void)verifySocialAccount:(NSString *)socialToken type:(LoginType)loginType secureCode:(NSString *)otpString secureType:(SecureType)secureType completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    [self login:nil password:nil type:loginType secure:otpString secureType:secureType openIdAccessToken:socialToken signUp:NO autoLogin:NO completion:completionBlock];
}


- (void)login:(NSString *)username password:(NSString *)pwd type:(LoginType)loginType secure:(NSString *)otpString secureType:(SecureType)secureType openIdAccessToken:(NSString *)openIdToken signUp:(BOOL)signUpSocial autoLogin:(BOOL)autoLogging completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    
    NSString *loginTypeString = @"";
    NSString *grandType = @"";
    NSString *authenType = @"";
    switch (loginType) {
        case LoginTypeApple:
        {
            loginTypeString = [VtcEventLogging getGALabelTypeString:GALabelTypeApple];
            grandType = @"password";
            authenType = @"7"; // ~ oauth_system_id = 3
        }
            break;
        case LoginTypeFacebook:
        {
            loginTypeString = [VtcEventLogging getGALabelTypeString:GALabelTypeFacebook];
            grandType = @"password";
            authenType = @"3"; // ~ oauth_system_id = 3
        }
            break;
        case LoginTypeGoogle:
        {
            loginTypeString = [VtcEventLogging getGALabelTypeString:GALabelTypeGoogle];
            grandType = @"password";
            authenType = @"2"; // ~ oauth_system_id = 2
        }
            break;
        case QuickStart:
        {
            loginTypeString = [VtcEventLogging getGALabelTypeString:GALabelTypeVTCID];
            grandType = @"password";
            authenType = @"8"; // ~ oauth_system_id = 8: Quick start
        }
            break;
            
            
        default:
        {
            loginTypeString = [VtcEventLogging getGALabelTypeString:GALabelTypeVTCID];
            grandType = @"password";
            authenType = @"0";
        }
            break;
    }
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    @try {
        
        
        //device_id:
        //        let idForVendor = (UIDevice.current.identifierForVendor?.uuidString)?.replacingOccurrences(of: "-", with: "") ?? ""
        
        
        //-------
        
        [parameters setObject:[[VtcConfig defaultConfig] appSecret] forKey:@"client_secret"];
        [parameters setObject:serviceID forKey:@"service_id"];
        [parameters setObject:grandType forKey:@"grant_type"];
        [parameters setObject:[Global sharedInstance].advertisingIdentifier forKey:@"device_id"];
        [parameters setObject:[Utilities getIPAddress] forKey:@"client_ip"];
        [parameters setObject:@"event" forKey:@"hit_type"];
        [parameters setObject:[[VtcEventLogging manager] getCurrentGAId] forKey:@"gatracking_id"];
        [parameters setObject:[[VtcEventLogging manager] getCurrentGAClientId] forKey:@"gaclient_id"];
        [parameters setObject:[VtcEventLogging getGACategoryTypeString:GACategoryTypeAuthen] forKey:@"event_category"];
        [parameters setObject:[VtcEventLogging getGAActionTypeString:GAActionTypeLogin] forKey:@"event_action"];
        [parameters setObject:loginTypeString forKey:@"event_label"];
        [parameters setObject:[NSNumber numberWithInteger:1] forKey:@"os_type"];
        [parameters setObject:VtcSDKVersionString forKey:@"sdk_version"];
        
        switch (loginType) {
            case LoginTypeApple:
            case LoginTypeFacebook:
            case LoginTypeGoogle:
            {
                [parameters setObject:authenType forKey:@"oauth_system_id"]; // [1: Yahoo, 2: Google, 3: Facebook, 4: Apple]
                [parameters setObject:openIdToken ? openIdToken : @"" forKey:@"access_token"];
                NSString *autoAuthen = @"0";
                if (signUpSocial || autoLogging) autoAuthen = @"1";
                [parameters setObject:autoAuthen forKey:@"auto_authen"]; // [0: signin, 1: signup or auto login]
                [parameters setObject:@"1" forKey:@"authen_social"];
                if (signUpSocial) {
                    [parameters setObject:username ? username : @"" forKey:@"username"];
                }
                if (otpString) {
                    [parameters setObject:otpString forKey:@"secure_code"];
                    [parameters setObject:[NSNumber numberWithInteger:secureType] forKey:@"secure_type"];
                }
            }
                break;
                
            case QuickStart:
            {
//                NSString *uuid = [[UIDevice currentDevice] identifierForVendor].UUIDString;
//                uuid = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
                [parameters setObject:authenType forKey:@"oauth_system_id"]; // [1: Yahoo, 2: Google, 3: Facebook, 4: Apple]
                [parameters setObject:openIdToken ? openIdToken : @"" forKey:@"access_token"];
                NSString *autoAuthen = @"0";
                if (signUpSocial || autoLogging) autoAuthen = @"1";
                [parameters setObject:autoAuthen forKey:@"auto_authen"]; // [0: signin, 1: signup or auto login]
                [parameters setObject:@"1" forKey:@"authen_social"];
                [parameters setObject:@"1" forKey:@"auto_create"];
                
                NSString *uuid = [[UIDevice currentDevice] identifierForVendor].UUIDString;
                uuid = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
                NSString *clientID = [[VtcConfig defaultConfig] appId];
                NSString *gameVersion = [[VtcConfig defaultConfig] gameVersion];
                
                NSString * stringtoencrypt = [NSString stringWithFormat:@"%@-%@-%@", uuid, clientID, gameVersion];
                NSData *encrypted = [NetworkModal tripleDesEncryptString: stringtoencrypt key:@"A1B962EC79F29BB6678F921A" error:nil];
                
                NSUInteger dataLength = [encrypted length];
                NSMutableString *string = [NSMutableString stringWithCapacity:dataLength*2];
                const unsigned char *dataBytes = [encrypted bytes];
                for (NSInteger idx = 0; idx < dataLength; ++idx) {
                    [string appendFormat:@"%02x", dataBytes[idx]];
                }
                
//                NSLog(@"encrypted data: =====\n%@\n=====", string);
                
                [parameters setObject:string forKey:@"device_id"];
                if (signUpSocial) {
                    [parameters setObject:username ? username : @"" forKey:@"username"];
                }
                //                if (otpString) {
                //                    [parameters setObject:otpString forKey:@"secure_code"];
                //                    [parameters setObject:[NSNumber numberWithInteger:secureType] forKey:@"secure_type"];
                //                }
            }
                break;
                
                
            default: // login VTC
            {
                // check the new registered user
                NSString *newUser = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultCachedNewRegisteredUser];
                if (newUser != nil) {
                    if ([username isEqualToString:newUser]) {
                        [parameters setObject:@"1" forKey:@"first_login"];
                        // clear the new registered user
                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kUserDefaultCachedNewRegisteredUser];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                }
                [parameters setObject:username forKey:@"username"];
                [parameters setObject:[pwd MD5] forKey:@"password"];
                [parameters setObject:authenType forKey:@"authen_type"];
                if (otpString) {
                    [parameters setObject:otpString forKey:@"secure_code"];
                    [parameters setObject:[NSNumber numberWithInteger:secureType] forKey:@"secure_type"];
                }
            }
                break;
        }
        
        VTCLog(@"%@", parameters);
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><LoginRequest> some parameter has corrupted. Error: %@", exception.description);
    } @finally {
        [self postRequest:@"login" parameters:parameters completion:^(BOOL status, id responsedObject, NSError *error) {
            completionBlock(status, responsedObject, error);
        }];
    }
}
- (NSString *) encodeURL:(NSString *)unescaped{
    NSString *escapedString = [unescaped stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    return escapedString;
}

//TODO: Luồng đăng ký mới
- (void)registerVTCIDNew:(NSString *)username
                password:(NSString *)pwd
             capchaToken:(NSString *)capchaToken
                     otp:(NSString *)otp
                    step:(NSString *)step
                    sign:(NSString *)sign
              completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    
    NSString *systemVersion = [[UIDevice currentDevice] systemVersion];
    NSString *deviceName = [[UIDevice currentDevice] name];
    deviceName = [self encodeURL:deviceName];
    NSString *identifier = [[[[UIDevice currentDevice] identifierForVendor] UUIDString] stringByReplacingOccurrencesOfString:@"-" withString:@""];
    //    NSString *t = "fingerprint:\(deviceSerial)-\(deviceSerial);devicebrowser:VTCAPP-ios;OS:ios-\(systemVersion);device:\(deviceName);devicetype:Mobile;resolution:Mobile";
    //
    NSString *deviceDetail =[NSString stringWithFormat:@"fingerprint:%@%@%@%@%@%@%@%@",deviceSerial,@"-",deviceSerial,@"devicebrowser:VTCAPP-ios;OS:ios-",systemVersion,@"device:",deviceName,@";devicetype:Mobile;resolution:Mobile"];
    deviceDetail = [deviceDetail stringByTrimmingCharactersInSet:
                    [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    deviceDetail = [deviceDetail stringByReplacingOccurrencesOfString:@" " withString:@""];
    //    NSLog(@"testahihi deviceDetail is : %@%@", deviceDetail,@" meomeo");
    
    NSDictionary *trackingModel;
    NSDictionary *body;
    NSDictionary *headers;
    @try {
        trackingModel = @{
            @"hitType" : @"event",
            @"gaTrackingId" : [[VtcEventLogging manager] getCurrentGAId],
            @"gaClientId" : [[VtcEventLogging manager] getCurrentGAClientId],
            @"category" : [VtcEventLogging getGACategoryTypeString:GACategoryTypeAuthen],
            @"action" : [VtcEventLogging getGAActionTypeString:GAActionTypeRegister],
            @"label" : [VtcEventLogging getGALabelTypeString:GALabelTypeVTC]
        };
        
        body = @{
            @"sign":sign,
            @"otp" : otp,
            @"step" : step,
            @"deviceDetail" : deviceDetail,
            @"capchaToken" : capchaToken,
            @"accountName" : username,
            @"password" : pwd,
            @"mobile" : @"",
            @"serviceId" : serviceID,
            @"registerType" : @"2",
            @"deviceId" : [Global sharedInstance].advertisingIdentifier,
            @"clientIp" : [Utilities getIPAddress],
            @"trackingModel" : trackingModel
        };
        
        headers = @{
            @"client_id" : [[VtcConfig defaultConfig] appId]
        };
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><RegisterRequest> some parameter has corrupted. Error: %@", exception.description);
    } @finally {
        [self postRequest:@"user/registerVtcByMobile" parameters:nil rawData:body headerFields:headers completion:^(BOOL status, id responsedObject, NSError *error) {
            VTCLog(@"%@", responsedObject);
            //            NSLog(@"register: %@", responsedObject);
            NSLog(@"%@",error);
            
            completionBlock(status, responsedObject, error);
        }];
    }
}

- (void)registerVTCID:(NSString *)username password:(NSString *)pwd completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    NSDictionary *trackingModel;
    NSDictionary *body;
    NSDictionary *headers;
    @try {
        trackingModel = @{
            @"hitType" : @"event",
            @"gaTrackingId" : [[VtcEventLogging manager] getCurrentGAId],
            @"gaClientId" : [[VtcEventLogging manager] getCurrentGAClientId],
            @"category" : [VtcEventLogging getGACategoryTypeString:GACategoryTypeAuthen],
            @"action" : [VtcEventLogging getGAActionTypeString:GAActionTypeRegister],
            @"label" : [VtcEventLogging getGALabelTypeString:GALabelTypeVTC]
        };
        
        body = @{
            @"accountName" : username,
            @"password" : pwd,
            @"mobile" : @"",
            @"serviceId" : serviceID,
            @"registerType" : @"2",
            @"deviceId" : [Global sharedInstance].advertisingIdentifier,
            @"clientIp" : [Utilities getIPAddress],
            @"trackingModel" : trackingModel
        };
        
        headers = @{
            @"client_id" : [[VtcConfig defaultConfig] appId]
        };
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><RegisterRequest> some parameter has corrupted. Error: %@", exception.description);
    } @finally {
        [self postRequest:@"user/registerVtc" parameters:nil rawData:body headerFields:headers completion:^(BOOL status, id responsedObject, NSError *error) {
            VTCLog(@"%@", responsedObject);
            NSLog(@"register: %@", responsedObject);
            NSLog(@"%@",error);
            
            completionBlock(status, responsedObject, error);
        }];
    }
}

// MARK: reset password

- (void)resetPassword:(NSString *)pwd forUser:(NSString *)username emailAddress:(NSString *)email sign:(NSString *)signCode completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    
    NSLog(@"testahihi resetPassword by email:%@",email);
    NSDictionary *body;
    NSDictionary *headers;
    @try {
        body = @{
            @"accountName" : username,
            @"clientIP" : [Utilities getIPAddress],
            @"email" : email,
            @"password" : pwd,
            @"sign" : signCode ? signCode : @""
        };
        headers = @{
            @"client_id" : [[VtcConfig defaultConfig] appId]
        };
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><ForgotPasswordRequest> some parameter has corrupted. Error: %@", exception.description);
    } @finally {
        [self postRequest:@"user/resetpassbyemail" parameters:nil rawData:body headerFields:headers completion:^(BOOL status, id responsedObject, NSError *error) {
            completionBlock(status, responsedObject, error);
        }];
    }
}

- (void)resetPassword:(NSString *)pwd forUser:(NSString *)username secureCode:(NSString *)otpString completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    NSDictionary *body;
    NSDictionary *headers;
    @try {
        body = @{
            @"accountName" : username,
            @"clientIP" : [Utilities getIPAddress],
            @"secureCode": otpString,
            @"secureType": @"1",
            @"password" : pwd
        };
        headers = @{
            @"client_id" : [[VtcConfig defaultConfig] appId]
        };
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><ResetPasswordRequest> some parameter has corrupted. Error: %@", exception.description);
    } @finally {
        [self postRequest:@"user/resetpassbyotp" parameters:nil rawData:body headerFields:headers completion:^(BOOL status, id responsedObject, NSError *error) {
            completionBlock(status, responsedObject, error);
        }];
    }
}

- (void)changePassword:(NSString *)username oldPassword:(NSString *)olderPwd newPassword:(NSString *)newPwd secureCode:(NSString *)otpString secureType:(SecureType)otpType completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    NSDictionary *body;
    NSDictionary *headers;
    @try {
        body = @{
            @"clientIP" : [Utilities getIPAddress],
            @"secureCode": otpString,
            @"secureType": [NSNumber numberWithInteger:otpType],
            @"oldPassword" : olderPwd,
            @"newPassword" : newPwd
        };
        headers = @{
            @"client_id" : [[VtcConfig defaultConfig] appId]
        };
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><ChangePasswordRequest> some parameter has corrupted. Error: %@", exception.description);
    } @finally {
        [self postRequest:@"user/changepassword" parameters:nil rawData:body headerFields:headers completion:^(BOOL status, id responsedObject, NSError *error) {
            completionBlock(status, responsedObject, error);
        }];
    }
}

// MARK: auto login
- (void)autoSignInToUser:(NSString *)username userId:(NSString *)uId accessToken:(NSString *)tokenString completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    
    NSDictionary *param;
    NSDictionary *body;
    NSDictionary *headers;
    @try {
        param = @{
            @"username" : username,
            @"access_token" : tokenString,
            @"sdk_verion" : VtcSDKVersionString
        };
        body = @{
            @"accountId" : uId,
            @"osType" : [NSNumber numberWithInteger:1]
        };
        headers = @{
            @"client_id" : [[VtcConfig defaultConfig] appId],
            @"Authorization" : [NSString stringWithFormat:@"Bearer %@", tokenString]
        };
        
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><AutoLogin> some parameter has corrupted. Error: %@", exception.description);
    } @finally {
        [self postRequest:@"account/autologin" parameters:param rawData:body headerFields:headers completion:^(BOOL status, id responsedObject, NSError *error) {
            completionBlock(status, responsedObject, error);
        }];
    }
}

- (void)getOTPText:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    [self getRequest:@"config/getallconfig/DESCRIPTION_OTP" parameters:nil completion:^(BOOL status, id responsedObject, NSError *error) {
        completionBlock(status, responsedObject, error);
    }];
}

- (void)getDKOTPText:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    [self getRequest:@"config/getallconfig/DESCRIPTION_DK" parameters:nil completion:^(BOOL status, id responsedObject, NSError *error) {
        completionBlock(status, responsedObject, error);
    }];
}

- (void)checkAccessToken:(void (^)(BOOL result, NSError *error))completionBlock {
    NSDictionary *params;
    @try {
        params = @{
            @"client_id" : [VtcConfig defaultConfig].appId,
            @"username" : [VIDUser currentUser].userName,
            @"access_token" : [VIDUser currentUser].accessToken
        };
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><AutoLogin> some parameter has corrupted. Error: %@", exception.description);
    } @finally {
        [self getRequest:@"user/checkaccesstoken" parameters:params completion:^(BOOL status, id responsedObject, NSError *error) {
            completionBlock(status, error);
        }];
    }
}

+ (NSData *)tripleDesEncryptString:(NSString *)input
                               key:(NSString *)key
                             error:(NSError **)error
{
    
    NSLog(@"tripleDesEncryptString NSParameterAssert: %@, NSParameterAssert: %@",input,key );
    //NSParameterAssert(input);
    //NSParameterAssert(key);
    
    NSData *inputData = [input dataUsingEncoding:NSUTF8StringEncoding];
    NSData *keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"keyData.length:%lu - %d", (unsigned long)keyData.length, kCCKeySize3DES);
          
    size_t outLength;
    
    //NSAssert(keyData.length == kCCKeySize3DES, @"the keyData is an invalid size");
    
    NSMutableData *outputData = [NSMutableData dataWithLength:(inputData.length  +  kCCBlockSize3DES)];
    
    CCCryptorStatus
    result = CCCrypt(kCCEncrypt, // operation
                     kCCAlgorithm3DES, // Algorithm
                     kCCOptionPKCS7Padding | kCCOptionECBMode, // options
                     keyData.bytes, // key
                     keyData.length, // keylength
                     nil,// iv
                     inputData.bytes, // dataIn
                     inputData.length, // dataInLength,
                     outputData.mutableBytes, // dataOut
                     outputData.length, // dataOutAvailable
                     &outLength); // dataOutMoved
    
    if (result != kCCSuccess) {
        if (error != NULL) {
            *error = [NSError errorWithDomain:@"com.your_domain.your_project_name.your_class_name."
                                         code:result
                                     userInfo:nil];
        }
        return nil;
    }
    [outputData setLength:outLength];
    return outputData;
}

+ (void) testEncryptionAndDecryption {
    NSString *uuid = [[UIDevice currentDevice] identifierForVendor].UUIDString;
    NSString *clientID = [[VtcConfig defaultConfig] appId];
    NSString *gameVersion = [[VtcConfig defaultConfig] gameVersion];
    
    NSString * stringtoencrypt = [NSString stringWithFormat:@"%@-%@-%@", uuid, clientID, gameVersion];
    NSData *encrypted = [self tripleDesEncryptString: stringtoencrypt key:@"A1B962EC79F29BB6678F921A" error:nil];
    
    NSUInteger dataLength = [encrypted length];
    NSMutableString *string = [NSMutableString stringWithCapacity:dataLength*2];
    const unsigned char *dataBytes = [encrypted bytes];
    for (NSInteger idx = 0; idx < dataLength; ++idx) {
        [string appendFormat:@"%02x", dataBytes[idx]];
    }
    
//    NSLog(@"encrypted data: =====\n%@\n=====", string);
//    NSLog(@"encrypted data length: %lu", (unsigned long)encrypted.length);
    NSString *decrypted = [self tripleDesDecryptData:encrypted key:@"A1B962EC79F29BB6678F921A" error:nil];
//    NSLog(@"decrypted text: %@", decrypted);
}

+ (NSString *)tripleDesDecryptData:(NSData *)input
                               key:(NSString *)key
                             error:(NSError **)error
{
//    NSLog(@"tripleDesDecryptData NSParameterAssert: %@, NSParameterAssert: %@",input,key );
//    NSParameterAssert(input);
//    NSParameterAssert(key);
    
    NSData *inputData = input;
    NSData *keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
    
    size_t outLength;
    
//    NSAssert(keyData.length == kCCKeySize3DES, @"the keyData is an invalid size");
    
    NSMutableData *outputData = [NSMutableData dataWithLength:(inputData.length  +  kCCBlockSize3DES)];
    
    CCCryptorStatus result = CCCrypt(kCCDecrypt, // operation
                                     kCCAlgorithm3DES, // Algorithm
                                     kCCOptionPKCS7Padding | kCCOptionECBMode, // options
                                     keyData.bytes, // key
                                     keyData.length, // keylength
                                     nil,// iv
                                     inputData.bytes, // dataIn
                                     inputData.length, // dataInLength,
                                     outputData.mutableBytes, // dataOut
                                     outputData.length, // dataOutAvailable
                                     &outLength); // dataOutMoved
    
    if (result != kCCSuccess) {
        if (error != NULL) {
            *error = [NSError errorWithDomain:@"com.your_domain.your_project_name.your_class_name."
                                         code:result
                                     userInfo:nil];
        }
        return nil;
    }
    [outputData setLength:outLength];
    return [[NSString alloc] initWithData:outputData encoding:NSUTF8StringEncoding];
}

@end

//
//  NetworkModal+Info.m
//  VtcSDK
//
//  Created by Kent Vu on 8/9/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "NetworkModal+Info.h"
#import "UIDevice-Hardware.h"

@implementation NetworkModal (Info)

- (void)getBufferLink:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    [self getRequest:@"app/onelink-url" parameters:nil headerFields:nil completion:^(BOOL status, id responsedObject, NSError *error) {
        completionBlock(status, responsedObject, error);
    }];
}

- (void)getGAInfo:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    NSDictionary *headers;
    @try {
        headers = @{
                    @"client_id" : [[VtcConfig defaultConfig] appId]
                    };
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><Info> Can not generating param to get Google Analytics info. Error: %@", exception.description);
    } @finally {
        [self getRequest:[NSString stringWithFormat:@"manage/trackingid/%@", [[VtcConfig defaultConfig] appId]] parameters:nil headerFields:headers completion:^(BOOL status, id responsedObject, NSError *error) {
            completionBlock(status, responsedObject, error);
        }];
    }
}


- (void)getPaymentViewText:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    NSDictionary *param;
    @try {
        param = @{
                   @"clientId" : [[VtcConfig defaultConfig] appId]
                   };
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><Info> Can not generating param to get view text. Error: %@", exception.description);
    } @finally {
        [self getRequest:[NSString stringWithFormat:@"manage/game-profile/%@", [[VtcConfig defaultConfig] appId]] parameters:param headerFields:nil completion:^(BOOL status, id responsedObject, NSError *error) {
            completionBlock(status, responsedObject, error);
        }];
    }
    
}

- (void)getPackageInfo:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
//    NSLog(@"testahihi test bundle id");
    NSString *direct = [[VtcConfig defaultConfig] isDirectVersion] ? @"1" : @"-1";
    NSDictionary *param;
    @try {
        param = @{
                  @"client_id" : [[VtcConfig defaultConfig] appId],
                  @"game_version" : [[VtcConfig defaultConfig] gameVersion],
                  @"device_type" : generalDeviceType,
                  @"bundle_id" : [[NSBundle mainBundle] bundleIdentifier],
//                  @"bundle_id" : @"audition.au2.dancing.aumobile.vtcgame",
                  @"direct" : direct,
                  @"username" : [VIDUser currentUser].userName
                  };
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><Info> Can not generating param to get all packages. Error: %@", exception.description);
    } @finally {
        [self getRequest:@"manage/game-packages" parameters:param completion:^(BOOL status, id responsedObject, NSError *error) {
            completionBlock(status, responsedObject, error);
        }];
    }
}

@end

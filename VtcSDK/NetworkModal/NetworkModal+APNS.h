//
//  NetworkModal+APNS.h
//  VtcSDK
//
//  Created by Kent Vu on 3/9/17.
//  Copyright © 2017 VTCIntecom. All rights reserved.
//

#import "NetworkModal.h"

@interface NetworkModal (APNS)

- (void)postbackForAPNSType:(NSString *)type time:(NSString *)postTime notificationId:(NSString *)notifId fromUser:(NSString *)userId userName:(NSString *)userName;

@end

//
//  NetworkModal.m
//  VtcSDK
//
//  Created by Kent Vu on 7/28/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "NetworkModal.h"
#import "NSBundle+Additions.h"
#import "SmartParser.h"
#import "UIDevice-Hardware.h"
#import "VIDUser.h"
#import "SDKManager.h"

@implementation NetworkModal

+ (NetworkModal *)sharedModal {
    static NetworkModal *sharedModal_ = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
//        [SDKManager defaultManager].isSandbox = YES;
        if ([SDKManager defaultManager].isSandbox) {
//            NSLog(@"testahihi isSandbox");
            sharedModal_ = [[NetworkModal alloc] initWithBaseURL:[NSURL URLWithString:sanboxBaseURL]];
            baseURL = sanboxBaseURL;
            apnsBaseURL = sabboxApnsBaseURL;
        } else {
//            NSLog(@"testahihi not Sandbox");
            sharedModal_ = [[NetworkModal alloc] initWithBaseURL:[NSURL URLWithString:productBaseURL]];
            baseURL = productBaseURL;
            apnsBaseURL = productApnsBaseURL;
        }
    });
    return sharedModal_;
}

- (NSString *)baseURLString {
    return baseURL;
}

- (BOOL)isDevMode {
    if ([baseURL containsString:@"apisdk1"]) {
        return YES;
    }
    return NO;
}

- (void)handleResponsedObject:(id)responsedObject forRequestPath:(NSString *)requestPath completion:(void (^)(BOOL status, id trueResponsebObject, NSError *error))completion {
    VTCLog(@"true responsed object: %@", responsedObject);
#if DEBUG
    NSLog(@"true response:\n %@", responsedObject);
#endif
    // parse the "error" key
    NSInteger errorCode = [SmartParser intForKey:@"error" from:responsedObject];
    if (errorCode > 0) { // api successes
        if ([requestPath isEqualToString:@"manage/check-game-packages"]) {
            //API check goi tra ve true response
            NSLog(@"testahihi API check goi tra ve true response");
            completion(true, responsedObject, nil);
        }else{
            completion(true, [SmartParser objectForKey:@"info" from:responsedObject], nil);
        }
        
    }
    else { // failed
        NSString *errString = [SmartParser objectForKey:@"message" from:responsedObject];
        if (!errString) {
            errString = [[ErrorDetail sharedInstace] descriptionForErrorCode:errorCode];
            if ([requestPath isEqualToString:@"login"] && !errString) {
                errString = VTCLocalizeString(@"Maintenance break. Please try again later");
            }
        }
        if ([requestPath isEqualToString:@"login"] && [errString isEqualToString:@""]) {
            errString = VTCLocalizeString(@"Maintenance break. Please try again later");
        }
        if (!errString) errString = @""; // if it still nil, set value to @""
        completion(false, [SmartParser objectForKey:@"info" from:responsedObject], [[NSError alloc] initWithDomain:requestPath code:errorCode userInfo:@{@"message" : errString}]);
    }
    // parse the "message" key
    // parse the "info" key
}

- (void)getRequest:(NSString *)requestPath parameters:(NSDictionary *)param completion:(void (^)(BOOL status, id responsedObject, NSError *error))completion {
    NSMutableDictionary *trueParams = [[NSMutableDictionary alloc] initWithDictionary:param];
    @try {
        // add more parameter if you want
        [trueParams setObject:[[VtcConfig defaultConfig] appId] forKey:@"client_id"];
        // ...
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><GetRequest> some parameter has corrupted. Error: %@", exception.description);
    } @finally {
        [self GET:requestPath parameters:trueParams progress:^(NSProgress * _Nonnull downloadProgress) {
            // do something here if you want
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"<VTCSDK-Request>  %@", task.currentRequest.URL);
#if DEBUG
            NSLog(@"%@",responseObject);
#endif
            [self handleResponsedObject:responseObject forRequestPath:requestPath completion:^(BOOL status, id trueResponsebObject, NSError *error) {
                completion(status, trueResponsebObject, error);
            }];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"%@", error);
            NSError *err = [[NSError alloc] initWithDomain:requestPath code:404 userInfo:@{@"message" : @"Không có kết nối"}];
            completion(NO, nil, err);
        }];
    }
}

- (void)getRequest:(NSString *)requestPath parameters:(NSDictionary *)param headerFields:(NSDictionary *)headers completion:(void (^)(BOOL status, id responsedObject, NSError *error))completion {
    NSMutableDictionary *trueParams = [[NSMutableDictionary alloc] initWithDictionary:param];
    @try {
        // add more parameter if you want
//        [trueParams setObject:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] forKey:@"game_version"];
//        [trueParams setObject:VtcSDKVersionString forKey:@"sdk_version"];
//        [trueParams setObject:[[NSBundle mainBundle] bundleIdentifier] forKey:@"bundle_id"];
        
        //
        [trueParams setObject:[[VtcConfig defaultConfig] appId] forKey:@"client_id"];
        [trueParams setObject:generalDeviceType forKey:@"device_type"];
        [trueParams setObject:[[Global sharedInstance] getPushDeviceToken] forKey:@"device_token"];
        [trueParams setObject:[[UIDevice currentDevice] model] forKey:@"manufacture"];
        [trueParams setObject:[[UIDevice currentDevice] modelName] forKey:@"model_name"];
        [trueParams setObject:[NSString stringWithFormat:@"%@ %@", [[UIDevice currentDevice] systemName], [[UIDevice currentDevice] systemVersion]] forKey:@"operating_system"];
        [trueParams setObject:[[UIDevice currentDevice] systemVersion] forKey:@"operating_system_version"];
        [trueParams setObject:[[Global sharedInstance] getUtmString] forKey:@"utm"];
        if ([VIDUser currentUser].userId != nil) {
            if (![trueParams objectForKey:@"account_id"]) {
                [trueParams setObject:[VIDUser currentUser].userId forKey:@"account_id"];
            }
        }
        if ([VIDUser currentUser].userName != nil) {
            if (![trueParams objectForKey:@"account_name"]) {
                [trueParams setObject:[VIDUser currentUser].userName forKey:@"account_name"];
            }
        }
        if ([VIDUser currentUser].email != nil) {
            [trueParams setObject:[VIDUser currentUser].email forKey:@"email"];
        }
        [trueParams setObject:[Global sharedInstance].advertisingIdentifier forKey:@"ads_id"];
        [trueParams setObject:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] forKey:@"game_version"];
        [trueParams setObject:VtcSDKVersionString forKey:@"sdk_version"];
        [trueParams setObject:[[NSBundle mainBundle] bundleIdentifier] forKey:@"bundle_id"];
        [trueParams setObject:[[VtcConfig defaultConfig] isDirectVersion] ? @"1" : @"-1" forKey:@"direct"];
        
        //
        // ...
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><GetRequest-header> some parameter has corrupted. Error: %@", exception.description);
    } @finally {
        #if DEBUG
        NSLog(@"testahihi getRequest trueParams:%@", trueParams);
        #endif
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];
        NSString * url = [NSString stringWithFormat:@"%@%@", baseURL, requestPath];
        NSLog(@"testahihi check buffer link url:%@",url);
        AFJSONRequestSerializer * serializer = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
        serializer.timeoutInterval = 30;
        for (NSString * key in headers)
        {
            [serializer setValue:[headers objectForKey:key] forHTTPHeaderField:key];
        }
        manager.requestSerializer = serializer;
        
        [manager GET:url parameters:trueParams progress:^(NSProgress * _Nonnull downloadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#if DEBUG
            NSLog(@"%@",responseObject);
#endif
            [self handleResponsedObject:responseObject forRequestPath:requestPath completion:^(BOOL status, id trueResponsebObject, NSError *error) {
                completion(status, trueResponsebObject, error);
            }];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"%@", error);
            NSError *err = [[NSError alloc] initWithDomain:requestPath code:404 userInfo:@{@"message" : @"Không có kết nối"}];
            completion(NO, nil, err);
        }];
        
        
    }
    
    
}

- (void)postRequest:(NSString *)requestPath parameters:(NSDictionary *)param completion:(void (^)(BOOL status, id responsedObject, NSError *error))completion {
    NSMutableDictionary *trueParams = [[NSMutableDictionary alloc] initWithDictionary:param];
    
    @try {
        [trueParams setObject:[[VtcConfig defaultConfig] appId] forKey:@"client_id"];
        [trueParams setObject:generalDeviceType forKey:@"device_type"];
        [trueParams setObject:[[Global sharedInstance] getPushDeviceToken] forKey:@"device_token"];
        [trueParams setObject:[[UIDevice currentDevice] model] forKey:@"manufacture"];
        [trueParams setObject:[[UIDevice currentDevice] modelName] forKey:@"model_name"];
        [trueParams setObject:[NSString stringWithFormat:@"%@ %@", [[UIDevice currentDevice] systemName], [[UIDevice currentDevice] systemVersion]] forKey:@"operating_system"];
        [trueParams setObject:[[UIDevice currentDevice] systemVersion] forKey:@"operating_system_version"];
        [trueParams setObject:[[Global sharedInstance] getUtmString] forKey:@"utm"];
        if ([VIDUser currentUser].userId != nil) {
            if (![trueParams objectForKey:@"account_id"]) {
                [trueParams setObject:[VIDUser currentUser].userId forKey:@"account_id"];
            }
        }
        if ([VIDUser currentUser].userName != nil) {
            if (![trueParams objectForKey:@"account_name"]) {
                [trueParams setObject:[VIDUser currentUser].userName forKey:@"account_name"];
            }
        }
        if ([VIDUser currentUser].email != nil) {
            [trueParams setObject:[VIDUser currentUser].email forKey:@"email"];
        }
        [trueParams setObject:[Global sharedInstance].advertisingIdentifier forKey:@"ads_id"];
        [trueParams setObject:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] forKey:@"game_version"];
        [trueParams setObject:VtcSDKVersionString forKey:@"sdk_version"];
        [trueParams setObject:[[NSBundle mainBundle] bundleIdentifier] forKey:@"bundle_id"];
        [trueParams setObject:[[VtcConfig defaultConfig] isDirectVersion] ? @"1" : @"-1" forKey:@"direct"];
        // ...
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><PostRequest> some parameter has corrupted. Error: %@", exception.description);
    } @finally {
        VTCLog(@"%@", trueParams);
        NSError *error;
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:trueParams
                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                             error:&error];
        
        if (! jsonData) {
            NSLog(@"Got an error: %@", error);
        } else {
//            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//            #if DEBUG
//                NSLog(@"testahihi <VTCSDK><PostRequest>  %@", requestPath);
//                NSLog(@"testahihi jsonString:%@", jsonString);
//            #endif
        }
        
        [self POST:requestPath parameters:trueParams progress:^(NSProgress * _Nonnull downloadProgress) {
            // do something here if you want
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            VTCLog(@"<VTCSDK><PostRequest>  %@", task.currentRequest.URL);
            VTCLog(@"%@", responseObject);
            
            
            #if DEBUG
                NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                NSLog(@"testahihi <VTCSDK><PostRequest>  %@", task.currentRequest.URL);
                NSLog(@"testahihi jsonString:%@", jsonString);
            #endif
            
//            NSLog(@"testahihi headers: %@",task.currentRequest.allHTTPHeaderFields);
            [self handleResponsedObject:responseObject forRequestPath:requestPath completion:^(BOOL status, id trueResponsebObject, NSError *error) {
                completion(status, trueResponsebObject, error);
            }];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSError *err = [[NSError alloc] initWithDomain:requestPath code:404 userInfo:@{@"message" : @"Không có kết nối"}];
            completion(NO, nil, err);
        }];
    }
    
}

- (void)postRequest:(NSString *)requestPath parameters:(NSDictionary *)param rawData:(NSDictionary *)rawBody headerFields:(NSDictionary *)headers completion:(void (^)(BOOL status, id responsedObject, NSError *error))completion {
    
    NSLog(@"testahihi postRequest %@%@", baseURL , requestPath);
    
    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc] init];
    
//    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    VTCLog(@"%@", param);
    if (param.allKeys.count > 0) {
        requestPath = [requestPath stringByAppendingString:@"?"];
        for (NSString *key in param) {
            requestPath = [requestPath stringByAppendingFormat:@"&%@=%@", key, [param objectForKey:key]];
        }
    }
    VTCLog(@"%@", requestPath);
    NSString * url = [NSString stringWithFormat:@"%@%@", baseURL, requestPath];
    AFJSONRequestSerializer * serializer = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
    serializer.timeoutInterval = 30;
    for (NSString *key in headers) {
        [serializer setValue:[headers objectForKey:key] forHTTPHeaderField:key];
    }
    manager.requestSerializer = serializer;
    
    NSMutableDictionary *trueParams = [[NSMutableDictionary alloc] initWithDictionary:rawBody];
    @try {
        [trueParams setObject:[[VtcConfig defaultConfig] appId] forKey:@"clientId"];
        [trueParams setObject:generalDeviceType forKey:@"deviceType"];
        [trueParams setObject:[[Global sharedInstance] getPushDeviceToken] forKey:@"deviceToken"];
        [trueParams setObject:[[UIDevice currentDevice] model] forKey:@"manufacture"];
        [trueParams setObject:[[UIDevice currentDevice] modelName] forKey:@"modelName"];
        [trueParams setObject:[NSString stringWithFormat:@"%@ %@", [[UIDevice currentDevice] systemName], [[UIDevice currentDevice] systemVersion]] forKey:@"operatingSystem"];
        [trueParams setObject:[[UIDevice currentDevice] systemVersion] forKey:@"operatingSystemVersion"];
        [trueParams setObject:[[Global sharedInstance] getUtmString] forKey:@"utm"];
        if ([VIDUser currentUser].userId != nil) {
            if (![trueParams objectForKey:@"accountId"]) {
                [trueParams setObject:[VIDUser currentUser].userId forKey:@"accountId"];
            }
        }
        if ([VIDUser currentUser].userName != nil) {
            if (![trueParams objectForKey:@"accountName"]) {
                [trueParams setObject:[VIDUser currentUser].userName forKey:@"accountName"];
            }
        }
        if ([VIDUser currentUser].email != nil && [VIDUser currentUser].email.length > 0) {
            [trueParams setObject:[VIDUser currentUser].email forKey:@"email"];
        }
        [trueParams setObject:[Global sharedInstance].advertisingIdentifier forKey:@"adsId"];
        [trueParams setObject:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] forKey:@"gameVersion"];
        [trueParams setObject:VtcSDKVersionString forKey:@"sdkVersion"];
        [trueParams setObject:[[NSBundle mainBundle] bundleIdentifier] forKey:@"bundleId"];
        [trueParams setObject:[[VtcConfig defaultConfig] isDirectVersion] ? @"1" : @"-1" forKey:@"direct"];
        [trueParams setObject:[Utilities getScreenResolution] forKey:@"screenResolution"];
        VTCLog(@"%@", trueParams);
NSLog(@"testahihi trueParams %@", trueParams);
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><PostRequest-rawdata> some parameter has corrupted. Error: %@", exception.description);
    } @finally {
        [manager POST:url parameters:[trueParams copy] progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#if DEBUG
            NSLog(@"%@",responseObject);
#endif
            [self handleResponsedObject:responseObject forRequestPath:requestPath completion:^(BOOL status, id trueResponsebObject, NSError *error) {
                completion(status, trueResponsebObject, error);
            }];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"%@",error);
            NSError *err = [[NSError alloc] initWithDomain:requestPath code:404 userInfo:@{@"message" : @"Không có kết nối"}];
            completion(NO, nil, err);
        }];
//        NSError *error;
//        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:trueParams options:0 error:&error];
//        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//        [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
//        NSLog(@"params: %@",jsonString);
//        [[manager dataTaskWithRequest:req uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
//            NSLog(@"<VTCSDK-Request> %@", response.URL);
//            NSLog(@"<VTCSDK-Request> response: %@", responseObject);
//            if (!error) {
//                [self handleResponsedObject:responseObject forRequestPath:requestPath completion:^(BOOL status, id trueResponsebObject, NSError *error) {
//                    completion(status, trueResponsebObject, error);
//                }];
//            } else {
//                NSLog(@"%@",error);
//                VTCLog(@"Error: %@, %@, %@", error, response, responseObject);
//                NSError *err = [[NSError alloc] initWithDomain:requestPath code:404 userInfo:@{@"message" : @"Không có kết nối"}];
//                completion(NO, nil, err);
//            }
//        }] resume];
    }
}

@end


/************************* ErrorDetail **************************/

@interface ErrorDetail ()

@property (nonatomic, strong) NSDictionary *errorDictionary;

@end

@implementation ErrorDetail

+ (ErrorDetail *)sharedInstace {
    static ErrorDetail *sharedInstance_ = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance_ = [[ErrorDetail alloc] init];
        sharedInstance_.errorDictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle vtcBundle] pathForResource:@"ErrorCode" ofType:@"plist"]];
    });
    return sharedInstance_;
}

- (NSString *)descriptionForErrorCode:(NSInteger)code {
    return [SmartParser objectForKey:[NSNumber numberWithInteger:code].stringValue from:_errorDictionary];
}

@end

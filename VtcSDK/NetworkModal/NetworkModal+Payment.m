//
//  NetworkModal+Payment.m
//  VtcSDK
//
//  Created by Kent Vu on 8/8/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "NetworkModal+Payment.h"
#import "VIDUser.h"
#import "UIDevice-Hardware.h"
#import "AppleIAPHelper.h"
#import "VPurchasedItem.h"

@implementation NetworkModal (Payment)

// MARK: get all payment packages

- (void)retrieveAllPaymentPackages:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    NSMutableDictionary *param;
    NSDictionary *headers;
    @try {
        param = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                 [[VtcConfig defaultConfig] appId], @"client_id",
                 [VtcConfig defaultConfig].gameVersion, @"game_version",
                 generalDeviceType, @"device_type",
                 nil];
        headers = @{
                    @"client_id" : [[VtcConfig defaultConfig] appId]
                    };
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><Payment> Can not generating param to get all packages. Error: %@", exception.description);
    } @finally {
        [self getRequest:@"manage/packages" parameters:param headerFields:headers completion:^(BOOL status, id responsedObject, NSError *error) {
            VTCLog(@"%@", responsedObject);
            completionBlock(status, responsedObject, error);
        }];
    }
}


// MARK: new package apis
- (void)retrieveAllInAppPackages:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    NSMutableDictionary *param;
    NSDictionary *headers;
    @try {
        param = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                 [[VtcConfig defaultConfig] appId], @"client_id",
                 [VtcConfig defaultConfig].gameVersion, @"game_version",
                 generalDeviceType, @"device_type",
                 nil];
        headers = @{
                    @"client_id" : [[VtcConfig defaultConfig] appId]
                    };
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><Payment> Can not generating param to get IAP packages. Error: %@", exception.description);
    } @finally {
        [self getRequest:@"manage/packageiap" parameters:param headerFields:headers completion:^(BOOL status, id responsedObject, NSError *error) {
            VTCLog(@"%@", responsedObject);
            completionBlock(status, responsedObject, error);
        }];
    }
}

- (void)retrieveAllNormalPackages:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    NSMutableDictionary *param;
    NSDictionary *headers;
    @try {
        param = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                 [[VtcConfig defaultConfig] appId], @"client_id",
                 [VtcConfig defaultConfig].gameVersion, @"game_version",
                 generalDeviceType, @"device_type",
                 nil];
        
        headers = @{
                    @"client_id" : [[VtcConfig defaultConfig] appId]
                    };
    } @catch (NSException *exception) {
    } @finally {
        [self getRequest:@"manage/packagenormal" parameters:param headerFields:headers completion:^(BOOL status, id responsedObject, NSError *error) {
            VTCLog(@"%@", responsedObject);
            completionBlock(status, responsedObject, error);
        }];
    }
}



// MARK: create and verify in-app purchase

- (void)createOrderIdForPackage:(NSString *)packId packageType:(NSInteger)type completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    NSDictionary *param;
    NSDictionary *body;
    NSDictionary *headers;
    
    @try {
        param = @{
                  @"username" : [VIDUser currentUser].userName,
                  @"access_token" : [VIDUser currentUser].accessToken
                  };
        body = @{
                 @"clientIP" : [Utilities getIPAddress],
                 @"packageId" : packId,
                 @"authenType" : [NSNumber numberWithInteger:0],
                 @"packageType" : [NSNumber numberWithInteger:type]
                 };
        headers = @{
                    @"client_id" : [[VtcConfig defaultConfig] appId],
                    @"Authorization" : [NSString stringWithFormat:@"Bearer %@", [VIDUser currentUser].accessToken]
                    };
        NSLog(@"%@",param);
        NSLog(@"%@",body);
        NSLog(@"%@",headers);
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><VerifyReceipt> Can not generating param to verify receipt. Error: %@", exception.description);
    } @finally {
        [self postRequest:@"transaction/createorderpayment" parameters:param rawData:body headerFields:headers completion:^(BOOL status, id responsedObject, NSError *error) {
            completionBlock(status, responsedObject, error);
        }];
    }
}

- (void)checkPackage:(NSString *)packId serverID:(NSString *)serverID completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    NSDictionary *param;
    NSDictionary *body;
    NSDictionary *headers;
    
    @try {
        param = @{
            @"username" : [VIDUser currentUser].userName,
            @"access_token" : [VIDUser currentUser].accessToken,
            @"clientIP" : [Utilities getIPAddress],
            @"package_id" : packId,
//            @"server_id" : [NSNumber numberWithInteger:serverID],
            @"server_id" : serverID,
            @"client_id" : [[VtcConfig defaultConfig] appId],
                  };
        body = @{
                
                 };
        headers = @{
//                    @"client_id" : [[VtcConfig defaultConfig] appId],
                    @"Authorization" : [NSString stringWithFormat:@"Bearer %@", [VIDUser currentUser].accessToken]
                    };
        NSLog(@"%@",param);
        NSLog(@"%@",body);
        NSLog(@"%@",headers);
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><VerifyReceipt> Can not generating param to verify receipt. Error: %@", exception.description);
    } @finally {
//        [self postRequest:@"manage/check-game-packages" parameters:param rawData:body headerFields:headers completion:^(BOOL status, id responsedObject, NSError *error) {
//            completionBlock(status, responsedObject, error);
//        }];
        [self getRequest:@"manage/check-game-packages" parameters:param headerFields:headers completion:^(BOOL status, id responsedObject, NSError *error) {
            VTCLog(@"%@", responsedObject);
            completionBlock(status, responsedObject, error);
        }];
    }
}

- (void)verifyTransaction:(NSDictionary *)transactionInfo completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    NSDictionary *param;
    NSDictionary *body;
    NSDictionary *headers;
    @try {
        param = @{
                  @"username" : [transactionInfo objectForKey:@"username"],
                  @"access_token" : [transactionInfo objectForKey:@"access_token"]
                  };
        body = @{
                 @"fromIp" : [transactionInfo objectForKey:@"fromIp"],
                 @"authenType" : [transactionInfo objectForKey:@"authenType"],
                 @"toAccountName" : [transactionInfo objectForKey:@"username"],
                 @"typeIAP" : [transactionInfo objectForKey:@"typeIAP"],
                 @"receiptData" : [transactionInfo objectForKey:@"receiptData"],
                 @"relatedTransactionId" : [transactionInfo objectForKey:@"transactionIdentifier"],
                 @"purchaseDate" : [transactionInfo objectForKey:@"purchaseDate"],
                 @"productId" : [transactionInfo objectForKey:@"productIdentifier"],
                 @"serverGame" : [transactionInfo objectForKey:@"serverGame"],
                 @"uuid" : [transactionInfo objectForKey:@"uuid"],
                 @"packageType" : [transactionInfo objectForKey:@"packageType"],
                 @"extendData" : [transactionInfo objectForKey:@"extendData"],
                 };
        headers = @{
                    @"client_id" : [transactionInfo objectForKey:@"client_id"],
                    @"Authorization" : [NSString stringWithFormat:@"Bearer %@", [transactionInfo objectForKey:@"access_token"]]
                    };
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><VerifyReceipt> Can not generating param to verify receipt. Error: %@", exception.description);
    } @finally {
        [self postRequest:@"transaction/gamepaymentiap" parameters:param rawData:body headerFields:headers completion:^(BOOL status, id responsedObject, NSError *error) {
            completionBlock(status, responsedObject, error);
        }];
    }
}

- (void)listSuccessfulTransactions:(NSInteger)page size:(NSInteger)pageSize completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock {
    NSDictionary *body;
    @try {
        body = @{
                  @"clientId" : [VtcConfig defaultConfig].appId,
                  @"accountName" : [VIDUser currentUser].userName,
                  @"deviceType" : generalDeviceType,
                  @"page" : @(page),
                  @"size" : @(pageSize)
                  };
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><VerifyReceipt> Can not generating param. Error: %@", exception.description);
    } @finally {
        [self postRequest:@"report/transaction-history-success" parameters:nil rawData:body headerFields:nil completion:^(BOOL status, id responsedObject, NSError *error) {
            completionBlock(status, responsedObject, error);
        }];
    }
}

- (void)listRefundTransactions:(NSInteger)page size:(NSInteger)pageSize completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock; {
    NSDictionary *body;
    @try {
        body = @{
                 @"clientId" : [VtcConfig defaultConfig].appId,
                 @"accountName" : [VIDUser currentUser].userName,
                 @"deviceType" : generalDeviceType,
                 @"page" : @(page),
                 @"size" : @(pageSize)
                 };
    } @catch (NSException *exception) {
        NSLog(@"<VtcSDK><VerifyReceipt> Can not generating param. Error: %@", exception.description);
    } @finally {
        [self postRequest:@"report/transaction-history-refund" parameters:nil rawData:body headerFields:nil completion:^(BOOL status, id responsedObject, NSError *error) {
            completionBlock(status, responsedObject, error);
        }];
    }
}

@end

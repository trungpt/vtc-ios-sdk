//
//  NetworkModal+Authen.h
//  VtcSDK
//
//  Created by Kent Vu on 7/28/16.
//  Copyright © 2016 VTCIntecom. All rights reserved.
//

#import "NetworkModal.h"
#import "VIDUser.h"

typedef NS_ENUM(NSInteger, LoginType) {
    LoginTypeVtcId,
    LoginTypeFacebook,
    LoginTypeGoogle,
    LoginTypeApple,
    QuickStart
    
};

typedef NS_ENUM(NSInteger, SecureType) {
    SecureTypeNone = 0,
    SecureTypeSMS = 1,
    SecureTypeApp = 2
};

@interface NetworkModal (Authen)

+ (void) testEncryptionAndDecryption;
- (void)loginWithVTCID:(NSString *)username password:(NSString *)pwd completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;

- (void)quickStartGame:(NSString *)username password:(NSString *)pwd completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)registerQuickStartGame:(NSString *)username completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock ;

- (void)verifyAccount:(NSString *)username password:(NSString *)pwd secureCode:(NSString *)otpString secureType:(SecureType)secureType completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)registerVTCID:(NSString *)username password:(NSString *)pwd completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)registerVTCIDNew:(NSString *)username
                password:(NSString *)pwd
             capchaToken:(NSString *)capchaToken
                     otp:(NSString *)otp
                    step:(NSString *)step
                    sign:(NSString *)sign
              completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)loginWithFacebook:(NSString *)facebookToken completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)loginWithApple:(NSString *)appleToken completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)registerWithFacebook:(NSString *)facebookToken user:(NSString *)username completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)registerWithApple:(NSString *)appleToken user:(NSString *)username completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)loginWithGoogle:(NSString *)googleToken completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)registerWithGoogle:(NSString *)googleToken user:(NSString *)username completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)verifySocialAccount:(NSString *)socialToken type:(LoginType)loginType secureCode:(NSString *)otpString secureType:(SecureType)secureType completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;

// MARK: reset password

- (void)resetPassword:(NSString *)pwd forUser:(NSString *)username secureCode:(NSString *)otpString completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)resetPassword:(NSString *)pwd forUser:(NSString *)username emailAddress:(NSString *)email sign:(NSString *)signCode completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)changePassword:(NSString *)username oldPassword:(NSString *)olderPwd newPassword:(NSString *)newPwd secureCode:(NSString *)otpString secureType:(SecureType)otpType completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;

// MARK: auto login
- (void)autoSignInToUser:(NSString *)username userId:(NSString *)uId accessToken:(NSString *)tokenString completion:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;

- (void)getOTPText:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)getDKOTPText:(void (^)(BOOL status, id responsedObject, NSError *error))completionBlock;
- (void)checkAccessToken:(void (^)(BOOL result, NSError *error))completionBlock;

@end
